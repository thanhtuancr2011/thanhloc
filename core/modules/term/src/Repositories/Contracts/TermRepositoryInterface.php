<?php

namespace Core\Modules\Term\Repositories\Contracts;

interface TermRepositoryInterface
{
	/**
     * Get all categories with tree format
     * @param  Request $request Request
     * @return Response         
     */
    public function getCategoriesTree($id, $taxonomy);

    /**
     * Get all categories with tree format
     * @param  Request $request Request
     * @return Response         
     */
    public function getAllCategoriesTree();

    /**
     * search categories
     * @param  Request $request Request
     * @return Response
     */
    public function searchCategoriesBy($options);

    /**
     * Get tags by tag name
     * @param  String $tagName The tag name
     * @return Void       
     */
    public function getTagsByTagName($tagName, $type);

    /**
     * Get url by slug
     * @param  String $slug The data input
     * @return Void
     */
    public function getUrlTag($slug);

    /**
     * Get all categories
     * @param  String $slug The data input
     * @return Void
     */
    public function getAllCategories();

    /**
     * Get all tags
     * @param  String $slug The data input
     * @return Void
     */
    public function getAllTags();

    /**
     * Get url by slug
     * @author  AnhVN <anhvn@diziweb.com>
     * @param  String $slug The data input
     * @param  boole $is_fullUrl
     * @return Void
     */
    public function getUrlCategory($slug, $is_fullUrl = true);
}


