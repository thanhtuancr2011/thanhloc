<?php

namespace Core\Modules\Term\Repositories\Eloquents;

use Core\Modules\Term\Models\TermModel;

use Core\Base\Repositories\Eloquents\EloquentRepository;
use Core\Modules\Term\Repositories\Contracts\TermRepositoryInterface;

class EloquentTermRepository extends EloquentRepository implements TermRepositoryInterface
{
    /**
     * Get model
     * @return string
     */
    public function getModel()
    {
        return TermModel::class;
    }

    /**
     * Get categories with tree format
     * @return \Illuminate\Http\Response
     */
    public function getCategoriesTree($id, $taxonomy)
    {
        // Call function get categories
        return $this->model->getCategoriesTree($id, $taxonomy);
    }

    /**
     * Get categories with tree format
     * @return \Illuminate\Http\Response
     */
    public function getAllCategoriesTree()
    {
        // Call function get categories
        return $this->model->getAllCategoriesTree();
    }

    /**
     * search categories
     * @param  Request $request Request
     * @return Response
     */
    public function searchCategoriesBy($options)
    {
        return $this->model->searchCategoriesBy($options);
    }

    /**
     * Get tags by tag name
     * @param  String $tagName The tag name
     * @return Void
     */
    public function getTagsByTagName($tagName, $type)
    {
        $tags = $this->model->where('name', 'LIKE', ('%' . $tagName . '%'))->where('taxonomy', $type)->get();
        return $tags;
    }

    /**
     * @param String $slug
     * @return String $url
     */
    public function getUrlTag($slug)
    {
        return url('/tag/' . $slug);
    }

    /**
     * Get all categories
     * @param  String $slug The data input
     * @return Void
     */
    public function getAllCategories()
    {
        return $this->model->where('taxonomy', 'LIKE', 'category%')->get();
    }

    /**
     * Get all tags
     * @param  String $slug The data input
     * @return Void
     */
    public function getAllTags()
    {
        return $this->model->where('taxonomy', 'post_tag')->get();
    }

    public function getBySlug($slug)
    {
        return $this->model->where('slug', $slug)->first();
    }

    /**
     * @param String $slug
     * @param boolean $is_fullUrl
     * @return String $url
     */
    public function getUrlCategory($slug, $is_fullUrl = true)
    {
        $category = $this->model->where('slug', $slug)->first();
        
        // check if categories not exist
        if (!$category) {
            if (!$is_fullUrl) {
                return $slug;
            }
            return url($slug);
        }

        $ancestor_ids = [];
        if (!empty($category->ancestor_ids)) {
            $ancestor_ids = array_reverse(json_decode($category->ancestor_ids, true));
        }

        // get list parent category
        $parentCategories = $this->model->whereIn('id', $ancestor_ids)->pluck('slug', 'id');
        if ($parentCategories->count() <= 0) {
            if (!$is_fullUrl) {
                return $slug;
            }
            return url($slug);
        }

        // get url string for category
        $str_url = '';
        foreach ($ancestor_ids as $key => $value) {
            if ($value != 0) {
                if (isset($parentCategories[$value]))
                    $str_url .= '/' . $parentCategories[$value];
            }
        }
        $str_url .= '/' . $slug;
        if (!$is_fullUrl) {
            return $str_url;
        }
        return url($str_url);
    }
}