<?php

namespace Core\Modules\Term\Repositories\Eloquents\En;

use Core\Modules\Term\Models\En\TermModel;

use Core\Base\Repositories\Eloquents\EloquentRepository;
use Core\Modules\Term\Repositories\Contracts\TermRepositoryInterface;

class EloquentTermRepository extends EloquentRepository implements TermRepositoryInterface
{
    /**
     * Get model
     * @return string
     */
    public function getModel()
    {
        return TermModel::class;
    }

    /**
     * Get categories with tree format
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @return \Illuminate\Http\Response
     */
    public function getCategoriesTree($type = 'category', $id = null)
    {
        // Call function get categories
        return $this->model->getCategoriesTree($type, $id);
    }

    /**
     * Get categories with tree format
     * @return \Illuminate\Http\Response
     */
    public function getAllCategoriesTree()
    {
        // Call function get categories
        return $this->model->getAllCategoriesTree();
    }

    /**
     * search categories
     * @author AnhVN <anhvn@diziweb.com>
     * @param  Request $request Request
     * @return Response
     */
    public function searchCategoriesBy($options)
    {
        return $this->model->searchCategoriesBy($options);
    }

    /**
     * Get tags by tag name
     * @param  String $tagName The tag name
     * @return Void
     */
    public function getTagsByTagName($tagName, $type)
    {
        $tags = $this->model->where('name', 'LIKE', ('%' . $tagName . '%'))->where('taxonomy', $type)->get();
        return $tags;
    }

    /**
     * @param String $slug
     * @param boolean $is_fullUrl
     * @return String $url
     */
    public function getUrlCategory($slug, $is_fullUrl = true)
    {

        $category = $this->model->where('slug', $slug)->first();
        
        // check if categories not exist
        if (!$category) {
            if (!$is_fullUrl) {
                return '/en/'.$slug;
            }
            return url('/en/'.$slug);
        }

        $ancestor_ids = [];
        if (!empty($category->ancestor_ids)) {
            $ancestor_ids = array_reverse(json_decode($category->ancestor_ids, true));
        }

        // get list parent category
        $parentCategories = $this->model->whereIn('id', $ancestor_ids)->pluck('slug', 'id');
        if ($parentCategories->count() <= 0) {
            if (!$is_fullUrl) {
                return '/en/'.$slug;
            }
            return url('/en/'.$slug);
        }

        // get url string for category
        $str_url = '';
        foreach ($ancestor_ids as $key => $value) {
            if ($value != 0) {
                if (isset($parentCategories[$value]))
                    $str_url .= '/' . $parentCategories[$value];
            }
        }
        $str_url .= '/' . $slug;
        if (!$is_fullUrl) {
            return '/en/'.$str_url;
        }
        return url('/en/'.$str_url);
    }

    /**
     * @param String $slug
     * @return String $url
     */
    public function getUrlTag($slug)
    {
        return url('/en/tag/' . $slug);
    }

    /**
     * Get all categories
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  String $slug The data input
     * @return Void
     */
    public function getAllCategories()
    {
        return $this->model->where('taxonomy', 'category')->get();
    }

    /**
     * Get term by type
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  String $type The type of term
     * @return Void
     */
    public function getTermsByType($type)
    {
        return $this->model->where('taxonomy', $type)->get();
    }

    /**
     * Get all tags
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  String $slug The data input
     * @return Void
     */
    public function getAllTags()
    {
        return $this->model->where('taxonomy', 'post_tag')->get();
    }

    public function getBySlug($slug)
    {
        return $this->model->where('slug', $slug)->first();
    }
}