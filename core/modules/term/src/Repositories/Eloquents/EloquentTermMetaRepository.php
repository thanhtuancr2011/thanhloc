<?php

namespace Core\Modules\Term\Repositories\Eloquents;

use Core\Modules\Term\Models\TermMetaModel;

use Core\Base\Repositories\Eloquents\EloquentRepository;
use Core\Modules\Term\Repositories\Contracts\TermMetaRepositoryInterface;

class EloquentTermMetaRepository extends EloquentRepository implements TermMetaRepositoryInterface
{
    /**
     * Get model
     * @return string
     */
    public function getModel()
    {
        return TermMetaModel::class;
    }
}