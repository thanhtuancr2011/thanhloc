<?php

namespace Core\Modules\Term\Models\En;

use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Config;
use Illuminate\Database\Eloquent\Model;

class TermMetaModel extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'term_meta_en';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['term_id', 'meta_key', 'meta_value'];


}
