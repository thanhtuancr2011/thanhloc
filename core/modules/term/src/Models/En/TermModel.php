<?php

namespace Core\Modules\Term\Models\En;

use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Config;
use Illuminate\Database\Eloquent\Model;

use Core\Modules\Post\Models\En\PostModel;
use Core\Modules\Term\Models\En\TermMetaModel;

class TermModel extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'terms_en';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'slug', 'description', 'post_number', 'parent_id', 'ancestor_ids', 'taxonomy'];

    /**
     * Relationship category
     *
     *
     * @return Voids
     */
    public function childs()
    {
        return $this->hasMany(TermModel::class, 'parent_id');
    }

    /**
     * Relationship Post
     *
     *
     * @return Voids
     */
    public function posts()
    {
        return $this->belongsToMany(PostModel::class, 'post_term_en', 'category_id', 'post_id');
    }

    /**
     * Relationship
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @return Voids
     */
    public function termMetas()
    {
        return $this->hasMany(TermMetaModel::class, 'term_id');
    }

    /**
     * Get list terms
     * @param  Array $data The data input
     * @return Array       The result
     */
    public function items($data)
    {
        if (isset($data['pageOptions'])) {
            $currentPage = $data['pageOptions']['currentPage'];
            $itemsPerPage = $data['pageOptions']['itemsPerPage'];
        }

        if (isset($data['searchOptions'])) {

            // Seach datas 
            $searchOptions = [];

            // If search with name
            if (isset($data['searchOptions']['name'])) {
                if ($data['searchOptions']['name'][0] == '"' && substr($data['searchOptions']['name'], -1) == '"') {
                    $searchOptions[] = ['name', 'like', substr($data['searchOptions']['name'], 1, -1)];
                } else {
                    $searchOptions[] = ['name', 'like', ('%' . $data['searchOptions']['name'] . '%')];
                }
            }

            // Search with taxonomy
            if (isset($data['searchOptions']['taxonomy'])) {
                $searchOptions[] = ['taxonomy', 'like', $data['searchOptions']['taxonomy']];
            }
        }

        // Set current page
        Paginator::currentPageResolver(function () use ($currentPage) {
            return $currentPage;
        });
        $paginate = self::where($searchOptions)->where('slug', '!=', 'uncategory')->orderBy('created_at', 'desc')->paginate($itemsPerPage)->toArray();

        $totalItems = $paginate['total'];
        $totalPages = ceil($totalItems / $itemsPerPage);

        return [
            'status' => 1,
            'terms' => $paginate['data'],
            'totalPages' => $totalPages,
            'totalItems' => $totalItems,
        ];
    }

    /**
     * Get categories with tree format
     * @return Array   Hierachy categories
     */
    public function getCategoriesTree($id, $taxonomy = '')
    {
        // Contain data output
        $result = [];

        // Get all categories
        if (empty($taxonomy)) {
            $categories = self::where('taxonomy', 'category')->orderBy('created_at', 'desc')->get()->toArray();
        } else {
            $categories = self::where('taxonomy', $taxonomy)->orderBy('created_at', 'desc')->get()->toArray();
        }

        // Null category
        $nullCategory = ['id' => 0, 'name' => 'Trống', 'parent_id' => '0', 'ancestor_ids' => [0], 'subFolder' => []];

        if (!empty($categories)) {
            foreach ($categories as &$category) {
                // Update value category
                $category['subFolder'] = [];
                $category['ancestor_ids'] = json_decode($category['ancestor_ids']);

                $referenceCategories[$category['id']] = $category;
            }

            // Put a folder to property subFolder of a parent folder that it should belong to
            foreach ($categories as &$category) {
                if (!empty($category['parent_id']) && $id != $category['parent_id']) {
                    $referenceCategories[$category['parent_id']]['subFolder'][] = &$referenceCategories[$category['id']];
                }
            }

            // Get root folders
            $hierachyCategories = $referenceCategories;
            foreach ($hierachyCategories as $key => $hierachyCategory) {

                // Get tree
                if (!empty($hierachyCategory['parent_id']) && ($hierachyCategory['parent_id'] != '0')) {
                    unset($hierachyCategories[$key]);
                }
            }
            
            // Add null category
            $treeCategories = array_values($hierachyCategories);
            array_push($treeCategories, $nullCategory);

            return $treeCategories;
        } 

        return [$nullCategory];
    }

    /**
     * Get categories with tree format
     * @return Array   Hierachy categories
     */
    public function getAllCategoriesTree()
    {
        // Contain data output
        $result = [];

        // Get all categories
        if (empty($taxonomy)) {
            $categories = self::where('taxonomy', 'LIKE', 'category%')->orderBy('created_at', 'desc')->get()->toArray();
        }

        // Null category
        $nullCategory = ['id' => 0, 'name' => 'Trống', 'parent_id' => '0', 'ancestor_ids' => [0], 'subFolder' => []];

        if (!empty($categories)) {
            foreach ($categories as &$category) {
                // Update value category
                $category['subFolder'] = [];
                $category['ancestor_ids'] = json_decode($category['ancestor_ids']);

                $referenceCategories[$category['id']] = $category;
            }

            // Put a folder to property subFolder of a parent folder that it should belong to
            foreach ($categories as &$category) {
                if (!empty($category['parent_id'])) {
                    $referenceCategories[$category['parent_id']]['subFolder'][] = &$referenceCategories[$category['id']];
                }
            }

            // Get root folders
            $hierachyCategories = $referenceCategories;
            foreach ($hierachyCategories as $key => $hierachyCategory) {

                // Get tree
                if (!empty($hierachyCategory['parent_id']) && ($hierachyCategory['parent_id'] != '0')) {
                    unset($hierachyCategories[$key]);
                }
            }
            
            // Add null category
            $treeCategories = array_values($hierachyCategories);
            array_push($treeCategories, $nullCategory);

            return $treeCategories;
        } 

        return [$nullCategory];
    }

    /**
     * Get all ancestor ids of category
     * @param  Object $parentId Category parent id
     * @param  Array &$ancestor_ids Array ancestor ids
     * @return Void
     */
    public function getAncestorCategoryIds($parentId, &$ancestor_ids)
    {
        $ancestor_ids[] = $parentId;

        // If category has parent
        if ($parentId != 0) {
            $category = self::find($parentId);
            self::getAncestorCategoryIds($category->parent_id, $ancestor_ids);
        }
    }

    /**
     * Create category
     * @param  Array $data Data input
     * @return Object       Category
     */
    public function storeItem($data)
    {
        // Create new category
        $category = self::create($data);

        if ($data['taxonomy'] == 'category' || strpos($data['taxonomy'], 'category_') !== false) {

            // Ancestor_ids of category
            $ancestor_ids = [];
            $this->getAncestorCategoryIds(
                empty($category->parent_id) ? 0 : $category->parent_id,
                $ancestor_ids
            );

            // Update category
            $category->ancestor_ids = json_encode($ancestor_ids);
            $category->update();
        }

        return $category;
    }

    /**
     * Update category
     * @param  Array $data Data input
     * @return Object       Category
     */
    public function updateItem($data)
    {
        // Update category
        $this->update($data);

        if ($data['taxonomy'] == 'category' || strpos($data['taxonomy'], 'category_') !== false) {

            // Ancestor_ids of category
            $ancestor_ids = [];
            $this->getAncestorCategoryIds(
                empty($this->parent_id) ? 0 : $this->parent_id,
                $ancestor_ids
            );

            // Update category
            $this->ancestor_ids = json_encode($ancestor_ids);
            $this->update();
        }

        return $this;
    }

    /**
     * Delete categories
     * @param  Array $ids List ids
     * @return Void
     */
    public function deleteItems($ids)
    {
        $categories = self::whereIn('id', $ids)->get();

        // Each category
        foreach ($categories as $key => $category) {

            // Get all categories has parent is category
            $childCategories = $category->childs;

            // If has childs
            if (!empty($childCategories)) {

                // Update parent_id for all childs
                foreach ($childCategories as $key => $value) {
                    $value->parent_id = $category->parent_id;
                    $value->update();
                }
            }

            // Delete category
            $status = $category->delete();
        }

        return $status;
    }

    /**
     * search categories
     * @author AnhVN <anhvn@diziweb.com>
     * @param  Request $request Request
     * @return Response
     */
    public function searchCategoriesBy($options)
    {
        $categoryModel = new self;

        if (isset($options['child_of'])) {
            $categoryModel = $categoryModel->where('parent_id', $options['child_of']);
        }

        if (!isset($options['taxonomy']) || !$options['taxonomy']) {
            $options['taxonomy'] = 'category';
        }

        $categoryModel = $categoryModel->where('taxonomy', 'LIKE', $options['taxonomy'] . '%');
        
        if (isset($options['hide_empty']) && $options['hide_empty']) {
            $categoryModel = $categoryModel->whereHas('posts', function ($q) use ($options) {
                $statusCode = StatusModel::where('name', 'publish')->first();
                $q->where('status', $statusCode->code);
            });
        }

        if (!isset($options['is_uncategory']) || !$options['is_uncategory']) {
            $categoryModel = $categoryModel->where('slug', '!=', 'uncategory');
        }

        if (isset($options['orderby']) && $options['orderby']) {
            if (!$options['order']) $options['order'] = 'ASC';
            $categoryModel = $categoryModel->orderBy($options['orderby'], $options['order']);
        }

        if (isset($options['slug']) && $options['slug']) {
            $categoryModel = $categoryModel->where('slug', $options['slug']);
        }

        return $categoryModel->get();
    }
}
