<?php

namespace Core\Modules\Term\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Foundation\AliasLoader;

use Core\Modules\Term\Repositories\Contracts\TermRepositoryInterface;
use Core\Modules\Term\Repositories\Eloquents\EloquentTermRepository;

use Core\Modules\Term\Repositories\Contracts\TermMetaRepositoryInterface;
use Core\Modules\Term\Repositories\Eloquents\EloquentTermMetaRepository;

class TermServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        // Publishes file
        $this->publishes([
            __DIR__.'/../../public/backend' => base_path('public/backend/app/components'),
        ], 'all');

        // Load route
        $this->loadRoutesFrom(__DIR__.'/../../routes/web.php');

        // Load view
        $this->loadViewsFrom(__DIR__ .'/../../views/', 'term');

        // Load migrate
        $this->loadMigrationsFrom(__DIR__.'/../../database/migrations');

        $this->app->bind(
            TermRepositoryInterface::class,
            EloquentTermRepository::class
        );

        $this->app->bind(
            TermMetaRepositoryInterface::class,
            EloquentTermMetaRepository::class
        );
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        // Register helper
        foreach (glob(__DIR__ . '/../Helpers/*.php') as $filename) {
            require_once $filename;
        }
    }

}
