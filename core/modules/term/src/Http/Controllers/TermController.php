<?php

namespace Core\Modules\Term\Http\Controllers;

use Auth;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Core\Base\Services\FileService;
use Core\Modules\Term\Repositories\Contracts\TermRepositoryInterface;

class TermController extends Controller
{
    protected $termRepository;

    public function __construct(TermRepositoryInterface $termRepository)
    {
        $this->middleware('auth');
        $this->termRepository = $termRepository;
    }

    public function index()
    {
        // Type term
        $taxonomy = $_GET['taxonomy'];
        
        return view('term::index', compact('taxonomy'));
    }

    public function create()
    {
        // Type create term
        $taxonomy = $_GET['taxonomy'];
        
        // Get model
        $item = $this->termRepository->create();
        
        // Set categories tree
        $categoriesTree = $this->termRepository->getCategoriesTree('', $taxonomy);
        
        return view('term::create', compact('item', 'categoriesTree', 'taxonomy'));
    }

    public function edit($id)
    {
        // Type create term
        $taxonomy = $_GET['taxonomy'];

        // Get term
        $item = $this->termRepository->edit($id);

        // Call function get tree category
        $categoriesTree = $this->termRepository->getCategoriesTree($id, $taxonomy);

        return view('term::create', compact('item', 'categoriesTree', 'taxonomy'));
    }
}
