<?php

namespace Core\Modules\Term\Http\Controllers\Api;


use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;

use Core\Modules\Term\Repositories\Contracts\TermRepositoryInterface;

class TermController extends Controller
{
    protected $termRepository;

    public function __construct(TermRepositoryInterface $termRepository)
    {
        $this->middleware('auth');
        $this->termRepository = $termRepository;
    }

    /**
     * Get terms
     * @param  Request $request Data input
     * @return Void           
     */
    public function terms(Request $request)
    {
    	// Get all data input
    	$data = $request->all();

    	// Call function get all terms
        $category = $this->termRepository->items($data);

        return new JsonResponse($category);
    }

    /**
     * Set data validate
     * @param  Array $data The data input
     * @return Void       
     */
    public function dataValidate($data)
    {
        if (!isset($data['id'])) {
            $validateSlug = Rule::unique('terms')->where(function ($query) use ($data) {
                return $query->where('slug', $data['slug'])->where('taxonomy', $data['taxonomy']);
            });
        } else {
            $validateSlug = Rule::unique('terms')->ignore($data['id'], 'id')->where(function ($query) use ($data) {
                return $query->where('slug', $data['slug'])->where('taxonomy', $data['taxonomy']);
            });
        }

        $validate = [
            'rules' => [
                'name' => 'required',
                'slug' => $validateSlug
            ],
            'messages' => [
                'name.required' => 'Mời nhập tên',
                'slug.unique'   => 'Tên đã tồn tại trong hệ thống'
            ]
        ];

        return $validate;
    }

    /**
     * Save term
     * @param  Request $request Data input
     * @return Void           
     */
    public function store(Request $request)
    {
        // All data input
        $data = $request->all();

        // Call function set data validate and validate the data input
        $vali = $this->dataValidate($data);
        $data = $this->termRepository->validate($data, $vali['rules'], $vali['messages']);

        // Validate fail
        if (isset($data['errors'])) {
            return new JsonResponse($data);
        }

        // Call function save term
        $term = $this->termRepository->store($data);

        if ($term) {
            $result = ['status' => 1, 'term' => $term, 'msg' => 'Thêm thành công'];
        } else {
            $result = ['status' => 0, 'msg' => 'Đã xảy ra lỗi. Mời thử lại.'];
        }

        return new JsonResponse($result);
    }

    /**
     * Update term
     * @param  Request $request Data input
     * @return Void           
     */
    public function update($id, Request $request)
    {
        // All data input
        $data = $request->all();

        // Call function set data validate and validate the data input
        $vali = $this->dataValidate($data);
        $data = $this->termRepository->validate($data, $vali['rules'], $vali['messages']);

        // Validate fail
        if (isset($data['errors'])) {
            return new JsonResponse($data);
        }

        // Call function save term
        $term = $this->termRepository->update($id, $data);

        if ($term) {
            $result = ['status' => 1, 'term' => $term, 'msg' => 'Cập nhật thành công'];
        } else {
            $result = ['status' => 0, 'msg' => 'Đã xảy ra lỗi. Mời thử lại.'];
        }

        return new JsonResponse($result);
    }

    /**
     * Delete multiple categories
     * @param  Request $request The request data
     * @return Void           
     */
    public function delete(Request $request)
    {
        // All ids
        $ids = $request->all();

        // Call function save term
        $status = $this->termRepository->delete($ids);

        if ($status) {
            $result = ['status' => $status, 'msg' => 'Xóa thành công'];
        } else {
            $result = ['status' => $status, 'msg' => 'Đã xảy ra lỗi. Mời thử lại.'];
        }

        // Return blogger 
        return new JsonResponse($result);
    }

    /**
     * Get tags by tag name
     * @param  Request $request The request data
     * @return Array            List tags
     */
    public function getTagsByTagName(Request $request)
    {   
        $tags = [];

        $type = isset($_GET['type']) ? $_GET['type'] : 'post_tag';

        // Data input
        $data = json_decode($request->get('json'), TRUE);

        // Call function save term
        if (isset($data['term']))
            $tags = $this->termRepository->getTagsByTagName($data['term'], $type);

        if (!empty($tags)) {
            $result = ['status' => 1, 'msg' => 'Success.', 'tags' => $tags];
        } else {
            $result = ['status' => 0, 'msg' => 'Fail.'];
        }

        return $result;
    }
}
