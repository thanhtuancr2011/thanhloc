<?php

Route::group(['middleware' => ['web', 'status']], function () {
    
    /* Admin route */
    Route::group(['prefix' => 'admin'], function () {
        // Term route
        Route::resource('term', 'Core\Modules\Term\Http\Controllers\TermController');
    });

    /* Api route */
    Route::group(['prefix' => 'api'], function () {

        // Term route
        Route::post('term/terms', 'Core\Modules\Term\Http\Controllers\Api\TermController@terms');
        Route::post('term/delete', 'Core\Modules\Term\Http\Controllers\Api\TermController@delete');
        Route::post('term/tags', 'Core\Modules\Term\Http\Controllers\Api\TermController@getTagsByTagName');
        Route::resource('term', 'Core\Modules\Term\Http\Controllers\Api\TermController');
    });

});

