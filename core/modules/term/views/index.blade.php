@extends('base::layouts.master')

@section('title')
    {{$taxonomy == 'category' ? 'Danh mục' : 'Từ khóa'}}
@endsection

@section('breadcrumb')
    <div class="row">
        <div class="container col-md-12 admin-breadcrumb">
            <div class="breadcrumb">
                <div class="col-md-6 no-padding">
                    <h3>{{$taxonomy == 'category' ? 'Danh mục' : 'Từ khóa'}}</h3>
                </div>
                <div class="col-md-6 no-padding content-end">
                    <span class="breadcrumb-item">Trang chủ</span>
                    <span class="breadcrumb-item active">{{$taxonomy == 'category' ? 'Danh mục' : 'Từ khóa'}}</span>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div ng-controller="TermController" ng-init="taxonomy='{{$taxonomy}}'">
        <div class="card-body">
            <!-- Header title and button -->
            <div class="group-search">
                <form class="frm-search" id="formSearch">
                    <input type="text" class="form-control" ng-model="searchOptions.searchText"
                           placeholder="Tìm kiếm" ng-keypress="searchTerms($event)"/>
                </form>
                <div class="group-btn">
                    <a class="btn btn-primary" href="javascript:void(0)" ng-click="getModalTerm()">
                        <i class="fa fa-plus"></i>
                    </a>
                    <a href="javascript:void(0)" class="btn btn-danger" ng-show="showBtn" ng-click="removeTerm()">
                        <i class="fa fa-trash-o"></i>
                    </a>
                </div>
            </div>

            <div class="clearfix"></div>

            <table class="table table-bordered table-hover table-striped">
                <thead>
                    <th style="text-align: center; width: 5%;">#</th>
                    <th style="text-align: center; width: 5%;">
                        <input type="checkbox" class="css-checkbox ckb-all" id="checkbox-all" ng-click="toggleSelection('all')">
                        <label for="checkbox-all" class="css-label lite-blue-check"></label>
                    </th>
                    <th>Tên</th>
                    {{-- <th>Đường dẫn</th> --}}
                    <th>Mô tả</th>
                    <th>Ngày tạo</th>
                    <th></th>
                </thead>
                <tbody>
                    <tr ng-repeat="term in terms track by $index" ng-class="{'bg-tr': $index%2 == 0}">

                        <td class="text-center">
                            @{{ ($index + 1) + (pageOptions.currentPage - 1) * pageOptions.itemsPerPage }}
                        </td>

                        <td class="text-center cls-chk">
                            <input type="checkbox" class="css-checkbox ckb" id="checkbox@{{$index}}" value="@{{term.id}}"
                                   ng-click="toggleSelection()">
                            <label for="checkbox@{{$index}}" class="css-label lite-blue-check"></label>
                        </td>

                        <td>@{{term.name}}</td>

                        {{-- <td>@{{term.url}}</td> --}}

                        <td>@{{term.description}}</td>

                        <td>
                            @{{formatDate (term.created_at) | date : "yyyy/MM/dd"}}
                        </td>

                        <td class="text-center">
                            <a href="javascript:void(0)" ng-click="getModalTerm(term.id)" class="btn btn-primary" title="Chỉnh sửa">
                                <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                            </a>

                            <a href="javascript:void(0)" class="btn btn-danger" ng-click="removeTerm(term.id)" title="Xóa">
                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </a>
                        </td>
                    </tr>
                </tbody>
            </table>
            <pagination-directive></pagination-directive>
        </div>
    </div>

@endsection

@section('script')
    {!! Html::script('backend/app/components/term/TermService.js?v='.getVersionScript()) !!}
    {!! Html::script('backend/app/components/term/TermController.js?v='.getVersionScript()) !!}
    {!! Html::script('backend/app/shared/pagination/PaginationDirective.js?v='.getVersionScript()) !!}
@endsection

