termApp.controller('TermController', ['$scope', '$uibModal', '$filter', 'TermService', '$timeout', '$rootScope', function ($scope, $uibModal, $filter, TermService, $timeout, $rootScope) {

	// When js didn't  loaded then hide table term
	$('.container-fluid').removeClass('hidden');
	HoldOn.open(optionsHoldOn);
	
	/**
	 * Get list items
	 * @author Thanh Tuan <thanhtuancr2011@gmail.com>
	 * @return {Void} 
	 */
	$scope.getItems = function() {

		// Reset all checkbox
		$scope.toggleSelection('none');

		TermService.getItems({
			pageOptions: $scope.pageOptions,
			searchOptions: $scope.searchOptions,
		}).then(function (data) {
			if (data.status == 1) {

				// Set terms value
				$scope.terms = angular.copy(data.terms);
				$scope.pageOptions.totalPages = data.totalPages;
				$scope.pageOptions.totalItems = data.totalItems;
				$rootScope.$broadcast('pageOptions', $scope.pageOptions);

				// Show loading
				HoldOn.close();
			}
		});
	}

	$timeout(function() {
		// Set type search
		$scope.searchOptions.taxonomy = $scope.taxonomy;
		$scope.getItems();
	});

	/**
     * Search terms
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @return {Void}                 */
    $scope.searchTerms = function(keyEvent) {

    	if (keyEvent == 'submit' || keyEvent.which === 13) {
    		HoldOn.open(optionsHoldOn);
	    	$scope.getItems();
    	}
    } 
	
	$scope.getModalTerm = function(id) {
		
		// When create
		var template = '/admin/term/create?taxonomy=' + $scope.taxonomy + '&v='+ new Date().getTime();

		// When update
		if(typeof id != 'undefined'){
			template = '/admin/term/'+ id + '/edit?taxonomy=' + $scope.taxonomy + '&v=' + new Date().getTime();
		}

		var modalInstance = $uibModal.open({
		    animation: true,
		    templateUrl: window.baseUrl + template,
		    controller: 'ModalCreateTagCtrl',
		    size: null,
		    resolve: {
		    }
		});

		/* After create or edit term then reset term and reload ng-table */
		modalInstance.result.then(function (data) {
			$scope.getItems();
		}, function () {

		   });
	};

	/**
	 * Delete the term
	 * @author Thanh Tuan <thanhtuancr2011@gmail.com>
	 * @param  {Integer} id   The term id
	 * @param  {String}  size Type of modal
	 * @return {Void}     
	 */
	$scope.removeTerm = function(id){

		if (angular.isDefined(id)) {
			var termIds = [id];
		} else {
			var termIds = $scope.listIdsSelected;
		}
		
		var template = '/backend/app/components/term/views/delete.html?v=' + new Date().getTime();
		var modalInstance = $uibModal.open({
		    animation: true,
		    templateUrl: window.baseUrl + template,
		    controller: 'ModalDeleteTermCtrl',
		    size: 'sm',
		    resolve: {
		    	termIds: function(){
		            return termIds;
		        }
		    }
		});

		modalInstance.result.then(function (data) {
			$scope.getItems();
		}, function () {});
	};

	/**
     * Onload when controller call to set page options
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  {Object} event The event 
     * @param  {Object} data) The page options
     * @return {Void}       
     */
    $scope.$on('setPageOptions',function(event, data) {
        HoldOn.open(optionsHoldOn);
        $scope.pageOptions = data;
        $scope.getItems();
    });

}])
.controller('ModalCreateCategoryCtrl', ['$scope', 'TermService', '$timeout', function ($scope, TermService, $timeout) {

	// When js didn't  loaded then hide table term
	$('.container-fluid').removeClass('hidden');
	HoldOn.open(optionsHoldOn);

	$scope.$watch('termItem.name', function(newVal, oldVal) {
		if (angular.isDefined(newVal)) {
			$scope.termItem.slug = slugValue(newVal, '-');
		}
	}, true);

	$timeout(function() {

		if (angular.isDefined($scope.termItem.id)) {
			$("#ckb-status-" + $scope.termItem.parent_id).prop("checked", true);
		}

		// If term has not exists table 
		if (angular.isUndefined($scope.termItem.table) || $scope.termItem.table == null) {
			$scope.termItem.table = [
				{
					'name': '',
					'slug': '',
					'display': true,
				}
			];
		}

		$scope.addField = function() {
			$scope.termItem.table.push({
				'name': '',
				'slug': '',
				'display': true,
			});
		}

		$scope.removeField = function(index) {
			$scope.termItem.table.splice(index, 1);
		}

		// Init tinymce
        tinyMCE.init({
            selector: 'textarea#tinymceEditor',
            height: 300,
            entity_encoding : "raw",
            relative_urls: false,
            theme: 'modern',
            relative_urls : false,
            remove_script_host : false,

            plugins : "link image code table textcolor colorpicker",

            theme_advanced_buttons3_add : "tablecontrols",
            table_styles : "Header 1=header1;Header 2=header2;Header 3=header3",
            table_cell_styles : "Header 1=header1;Header 2=header2;Header 3=header3;Table Cell=tableCel1",
            table_row_styles : "Header 1=header1;Header 2=header2;Header 3=header3;Table Row=tableRow1",
            table_cell_limit : 100,
            table_row_limit : 5,
            table_col_limit : 5,

            toolbar: 'styleselect | table | undo redo | bold italic | image imagetools | alignleft aligncenter alignright | link unlink | fontselect fontsizeselect | code | forecolor backcolor',
            init_instance_callback: function (editor) {
                editor.on('click', function (e) {
                    $scope.termItem.description = editor.getContent();
                });
                editor.on('keyup', function(e) {
                    $scope.termItem.description = editor.getContent();
                });
                editor.on('SetContent', function (e) {
                    $scope.termItem.description = editor.getContent();
                });
            }
        });

        HoldOn.close();
	});

	// When term click add or edit term
	$scope.submit = function (validate) {

		// Validate
		$scope.submitted = true;
		if (!validate) return;

		// Set term slug
		if (!angular.isDefined($scope.termItem.slug) || $scope.termItem.slug == '') {	
			$scope.termItem.slug = slugValue($scope.termItem.name, '-');
		} else {
			$scope.termItem.slug = slugValue($scope.termItem.slug, '-');
		}

		// Reset error
		$scope.errors = [];
		
		// Loading
		HoldOn.open(optionsHoldOn);

		// Format field name get field slug
		if ($scope.termItem.view == 'list') {
			angular.forEach($scope.termItem.table, function(value, key) {
				if (value.slug.length == 0) {
					value.slug = slugValue(value.name, '_');
				} else {
					value.slug = slugValue(value.slug, '_')
				}
			});
		} else {
			delete $scope.termItem.table;
		}

		TermService.createTermProvider($scope.termItem).then(function (data) {

			// Close loading
			HoldOn.close();

			if (data.status == -1) {
				$scope.errors = data.errors;
			} else {
				Notify(data.msg);
				window.location.href = window.baseUrl + '/admin/term?taxonomy=category';
			}
		})
	};

	/* When term click cancel then close modal popup */
	$scope.cancel = function () {
	    $uibModalInstance.dismiss('cancel');
	};
}])
.controller('ModalCreateTagCtrl', ['$scope', '$uibModalInstance', 'TermService', '$timeout', function ($scope, $uibModalInstance, TermService, $timeout) {

	// When js didn't  loaded then hide table term
	$('.container-fluid').removeClass('hidden');
	HoldOn.open(optionsHoldOn);

	$scope.$watch('termItem.name', function(newVal, oldVal) {
		if (angular.isDefined(newVal)) {
			$scope.termItem.slug = slugValue(newVal, '-');
		}
	}, true);

	$timeout(function() {

        HoldOn.close();
	});

	// When term click add or edit term
	$scope.submit = function (validate) {

		// Validate
		$scope.submitted = true;
		if (!validate) return;

		// Set term slug
		if (!angular.isDefined($scope.termItem.slug) || $scope.termItem.slug == '') {	
			$scope.termItem.slug = slugValue($scope.termItem.name, '-');
		} else {
			$scope.termItem.slug = slugValue($scope.termItem.slug, '-');
		}

		// Reset error
		$scope.errors = [];
		
		// Loading
		HoldOn.open(optionsHoldOn);

		TermService.createTermProvider($scope.termItem).then(function (data) {

			// Close loading
			HoldOn.close();

			if (data.status == -1) {
				$scope.errors = data.errors;
			} else {
				$uibModalInstance.close();
				Notify(data.msg);
			}
		})
	};

	/* When term click cancel then close modal popup */
	$scope.cancel = function () {
	    $uibModalInstance.dismiss('cancel');
	};
}])
.controller('ModalDeleteTermCtrl', ['$scope', '$uibModalInstance', 'termIds', 'TermService', function ($scope, $uibModalInstance, termIds, TermService) {
	
	// When term click update the post for term
	$scope.submit = function () {

		// Loading
		HoldOn.open(optionsHoldOn);

		TermService.deleteTerms(termIds).then(function (data) {
			
			// Close loading
			HoldOn.close();

			$uibModalInstance.close();
			Notify(data.msg);
		});
	};

	// When term click cancel then close modal popup
	$scope.cancel = function () {
	    $uibModalInstance.dismiss('cancel');
	};
}]);