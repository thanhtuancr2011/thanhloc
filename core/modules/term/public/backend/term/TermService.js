var termApp = angular.module('appTerm', []);
termApp.factory('TermResource',['$resource', function ($resource){
    return $resource('/api/term/:method/:id', {'method':'@method','id':'@id'}, {
        add: {method: 'post'},
        save:{method: 'post'},
        update:{method: 'put'}
    });
}])
.service('TermService', ['TermResource', '$q', function (TermResource, $q) {
    var that = this;

    /**
     * Get items
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @return {Void} 
     */
    this.getItems = function(data) {
        var defer = $q.defer(); 
        var temp  = new TermResource(data);
        temp.$save({method: 'terms'}, function success(data) {
            defer.resolve(data);
        }, function error(reponse) {
            defer.resolve(reponse.data);
        });
        return defer.promise;  
    }

    /**
     * Function create new term
     * @author Thanh Tuan <tuannt@acro.vn>
     * @param  {Array} data The data input
     * @return {Void}      
     */
	this.createTermProvider = function(data){

        // If isset id of term then call function edit term
        if(typeof data['id'] != 'undefined') {
            return that.editTermProvider(data);
        }

		var defer = $q.defer(); 
        var temp  = new TermResource(data);

        temp.$save({}, function success(data) {
            // Resolve result 
            defer.resolve(data);
        }, function error(reponse) {
            // Resolve result 
        	defer.resolve(reponse.data);
        });
        return defer.promise;  
	};

    /**
     * The function edit term
     * @author Thanh Tuan <tuannt@acro.vn>
     * @param  {Array} data The data input 
     * @return {Void}      
     */
    this.editTermProvider = function(data){

        var defer = $q.defer(); 
        var temp  = new TermResource(data);

        // Update term successfull 
        temp.$update({id: data['id']}, function success(data) {
            defer.resolve(data);
        }, function error(reponse) {
            // If create term is error 
            defer.resolve(reponse.data);
        });
        
        return defer.promise;  
    };

    /**
     * Delete the terms
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  {Integer} id The term ids
     * @return {Void}    
     */
    this.deleteTerms = function (ids) {

        var defer = $q.defer(); 
        var temp  = new TermResource(ids);

        // Delete terms is successfull
        temp.$save({method: 'delete'}, function success(data) {
            defer.resolve(data);
        },
        
        // If delete term is error
        function error(reponse) {
            defer.resolve(reponse.data);
        });
        return defer.promise;  
    }

}]);
