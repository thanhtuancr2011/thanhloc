<?php

namespace Core\Modules\Base\Setting\Providers;

use Core\Modules\Base\Setting\Facades\SettingFacade;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\ServiceProvider;


class SettingServiceProvider extends ServiceProvider
{
    /**
     * @author Sang Nguyen
     */
    public function register()
    {

        AliasLoader::getInstance()->alias('Setting', SettingFacade::class);

        $helpers = glob(__DIR__ . '/../../helpers' . '/*.php');
        foreach ($helpers as $helper) {
            require_once($helper);
        }

    }

    /**
     * @author Sang Nguyen
     */
    public function boot()
    {

    }
}
