<?php
if (!function_exists('setting')) {
    /**
     * Get the setting instance.
     */
    function setting($key = null, $default = null)
    {
        if (!empty($key)) {
            return Setting::get($key, $default);
        }
        return app(\Dizi\Core\Setting\Setting::class);
    }
}