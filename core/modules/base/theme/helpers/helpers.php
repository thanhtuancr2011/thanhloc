<?php

use Core\Modules\Base\Site\Http\Controllers\SiteController;

if (!function_exists('theme')) {
    /**
     * Get the theme instance.
     *
     * @param  string $themeName
     * @param  string $layoutName
     * @return Theme
     * @author Teepluss <admin@laravel.in.th>
     */
    function theme($themeName = null, $layoutName = null)
    {
        $theme = app('theme');

        if ($themeName) {
            $theme->theme($themeName);
        }

        if ($layoutName) {
            $theme->layout($layoutName);
        }
        return $theme;
    }
}
if (!function_exists('scan_folder')) {
    /**
     * @param $path
     * @param array $ignore_files
     * @return array
     * @author Sang Nguyen
     */
    function scan_folder($path, $ignore_files = [])
    {
        try {
            if (is_dir($path)) {
                $data = array_diff(scandir($path), array_merge(['.', '..'], $ignore_files));
                natsort($data);
                return $data;
            }
            return [];
        } catch (Exception $ex) {
            return [];
        }
    }
}

if (!function_exists('get_file_data')) {
    /**
     * @param $file
     * @param $convert_to_array
     * @return bool|mixed
     * @author Sang Nguyen
     */
    function get_file_data($file, $convert_to_array = true)
    {
        if (!file_exists($file)) {
            return false;
        }
        $file = File::get($file);
        if (!empty($file)) {
            if ($convert_to_array) {
                return json_decode($file, true);
            } else {
                return $file;
            }
        }
        return false;
    }
}