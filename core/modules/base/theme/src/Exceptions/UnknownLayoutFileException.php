<?php
namespace  Core\Modules\Base\Theme;

use UnexpectedValueException;

class UnknownLayoutFileException extends UnexpectedValueException
{
}
