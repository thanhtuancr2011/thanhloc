<?php
namespace  Core\Modules\Base\Theme;

use UnexpectedValueException;

class UnknownPartialFileException extends UnexpectedValueException
{
}
