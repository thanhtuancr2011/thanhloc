<?php
namespace  Core\Modules\Base\Theme;

use UnexpectedValueException;

class UnknownThemeException extends UnexpectedValueException
{

}
