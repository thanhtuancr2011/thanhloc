<?php

namespace Core\Modules\Base\Theme\Http\Controllers;

use App\Http\Controllers\Controller;
use Assets;
use MediaLibrary;
use ThemeOption;
use Setting;
use Dizi;
use Illuminate\Http\Request;

class ThemeController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @author Sang Nguyen
     */
    public function getList()
    {
        // Check permission
        Base::canOrRedirect('index.forms');

        // page_title()->setTitle(trans('theme::theme.theme'));
        return view('theme::list');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @author Sang Nguyen
     */
    public function getOptions()
    {
        // page_title()->setTitle(trans('theme::theme.theme_options'));

        // MediaLibrary::registerMediaLibrary();
        // Assets::addJavascript(['bootstrap-tagsinput', 'typeahead', 'are-you-sure']);
        // Assets::addStylesheets(['bootstrap-tagsinput']);
        return view('theme::options');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @author Sang Nguyen
     */
    public function postUpdate(Request $request)
    {
        $sections = ThemeOption::constructSections();
        foreach ($sections as $section) {
            foreach ($section['fields'] as $field) {
                $key = $field['attributes']['name'];
                ThemeOption::setOption($key, $request->input($key, 0));
            }
        }
        
        Setting::save();
        return redirect()->back()->with('success_msg', trans('bases::notices.update_success_message'));
    }
}