<?php

namespace Core\Modules\Base\Theme\Providers;

use Core\Modules\Base\Theme\Commands\ThemeCreateCommand;
use Core\Modules\Base\Theme\Commands\ThemeRemoveCommand;

use Core\Modules\Base\Theme\Facades\ThemeFacade;
use Core\Modules\Base\Theme\Facades\ManagerFacade;
use Core\Modules\Base\Theme\Facades\ThemeOptionFacade;

use Core\Modules\Base\Theme\Contracts\Theme as ThemeContract;

use Core\Modules\Base\Theme\Theme;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\ServiceProvider;

class ThemeServiceProvider extends ServiceProvider
{
    /**
     * @author Sang Nguyen
     */
    public function register()
    {
        AliasLoader::getInstance()->alias('Theme', ThemeFacade::class);
        AliasLoader::getInstance()->alias('ThemeOption', ThemeOptionFacade::class);
        AliasLoader::getInstance()->alias('ThemeManager', ManagerFacade::class);

        $this->app->bind(ThemeContract::class, Theme::class);

        if (app()->runningInConsole()) {
            $this->commands([
                ThemeCreateCommand::class,
                ThemeRemoveCommand::class,
            ]);
        }

        $helpers = glob(__DIR__ . '/../../helpers' . '/*.php');
        foreach ($helpers as $helper) {
            require_once($helper);
        }

    }

    /**
     * @author Sang Nguyen
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__ . '/../../routes/web.php');
        $this->loadViewsFrom(__DIR__ . '/../../resources/views', 'theme');
        $this->mergeConfigFrom(__DIR__ . '/../../config/theme.php', 'theme');

        // Load translate theme core
        $this->loadTranslationsFrom(__DIR__ . '/../../resources/lang', 'theme');

        // Load translate theme
        $themeName = setting('theme', 'default');
        $this->loadTranslationsFrom(public_path() . '/themes/' . $themeName . '/lang', $themeName);

        if (app()->runningInConsole()) {
            $this->publishes([__DIR__ . '/../../config/theme.php' => config_path('theme.php')], 'config');
        }
    }
}
