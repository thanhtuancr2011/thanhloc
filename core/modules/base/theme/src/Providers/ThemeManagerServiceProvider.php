<?php

namespace Core\Modules\Base\Theme\Providers;

use Illuminate\Support\ServiceProvider;

class ThemeManagerServiceProvider extends ServiceProvider
{
    /**
     * @author Sang Nguyen
     */
    public function boot()
    {
        if (check_database_connection()) {
            foreach (glob(public_path() . DIRECTORY_SEPARATOR . config('theme.themeDir') . DIRECTORY_SEPARATOR . (setting('theme') ? setting('theme') : 'default') . '/functions/*.php') as $helper) {
                require_once($helper);
            }
        }
    }
}
