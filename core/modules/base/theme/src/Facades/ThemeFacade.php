<?php

namespace  Core\Modules\Base\Theme\Facades;

use Core\Modules\Base\Theme\Theme;
use Illuminate\Support\Facades\Facade;

class ThemeFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     * @author Sang Nguyen
     */
    protected static function getFacadeAccessor()
    {
        return Theme::class;
    }
}
