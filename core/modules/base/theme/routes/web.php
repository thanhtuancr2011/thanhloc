<?php

Route::group(['namespace' => 'Core\Modules\Base\Theme\Http\Controllers', 'middleware' => ['web', 'language']], function () {
    Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {
        Route::group(['prefix' => 'theme'], function () {
            Route::get('/', [
                'as' => 'theme.list',
                'uses' => 'ThemeController@getList',
            ]);

            Route::get('/options', [
                'as' => 'theme.options',
                'uses' => 'ThemeController@getOptions',
            ]);

            Route::post('/options', [
                'as' => 'theme.options',
                'uses' => 'ThemeController@postUpdate',
            ]);

        });
    });
});