<?php

return [
	'home' => 'Home',
    'theme' => 'Theme',
    '404_theme' => '404 theme',
    'author' => 'Author',
    'version' => 'Version',
    'description' => 'Description',
    'active_success' => 'Active theme successfully!',
    'active' => 'Active',
    'activated' => 'Activated',
    'theme_options' => 'Theme options',
];