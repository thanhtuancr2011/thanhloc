<?php

return [
	'home' => 'Trang chủ',
    'theme' => 'Giao diện',
    '404_theme' => 'Trang 404',
    'author' => 'Tác giả',
    'version' => 'Phiên bản',
    'description' => 'Mô tả ',
    'active_success' => 'Đổi giao diện thành công',
    'active' => 'Kích hoạt',
    'activated' => 'Đã kích hoạt',
    'theme_options' => 'Giao diện tùy chọn',
];