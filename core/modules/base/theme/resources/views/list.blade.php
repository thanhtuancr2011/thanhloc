@extends('dizi-core::layouts.master')

@section('title')
    {{trans('theme::theme.theme')}}
@endsection

@section('breadcrumb')
    <div class="row">
        <div class="container col-md-12 admin-breadcrumb">
            <div class="breadcrumb">
                <div class="col-md-10">
                    <h3>{{trans('theme::theme.theme')}}</h3>
                    <ul>
                        <li class="breadcrumb-item">{{trans('theme::theme.home')}}</li>
                        <li class="breadcrumb-item active">{{trans('theme::theme.theme')}}</li>
                    </ul>
                </div>
                <div class="col-md-2">
                    <i class="fa fa-bar-chart fa-2 pull-right" aria-hidden="true"></i>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="card-body" style="padding: 0;" ng-controller="SettingController">
        <div class="col-md-12" style="padding: 15px 0;" ng-init="listThemes = {{ json_encode(ThemeManager::getThemes()) }}; item.theme='{{setting('theme')}}'">
            <div class="col-sm-6 col-md-4 col-lg-4" ng-repeat="(key, theme) in listThemes">
                <div class="thumbnail-theme">
                    <div class="img-thumbnail-wrap" style="background-image: url('{{ url('dizi/' . config('theme.themeDir')) }}/@{{key}}/screenshot.png');"></div>
                    <div class="caption">
                        <div class="row">
                            <div class="col-md-12" style="word-break: break-all; text-align: left; margin-top: 15px;">
                                <h4>@{{theme.name}}</h4>
                                <p>{{trans('theme::theme.author')}}: @{{theme.author}}</p>
                                <p>{{trans('theme::theme.version')}}: @{{theme.version}}</p>
                                <p>{{trans('theme::theme.description')}}: @{{theme.description}}</p>
                            </div>
                            <div class="clearfix"></div>
                            <div class="btn-active">
                                <a href="javascript:void(0)" ng-if="item.theme == key" class="btn btn-primary none-radius" disabled="disabled">
                                    <i class="fa fa-check"></i> {{ trans('theme::theme.activated') }}
                                </a>
                                <a ng-click="submit(item.theme = key)" ng-if="item.theme != key" class="btn btn-danger none-radius">
                                    {{ trans('theme::theme.active') }}
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
@endsection

@section('script')
    {!! Html::script('/backend/app/components/setting/SettingService.js?v='.getVersionScript()) !!}
    {!! Html::script('/backend/app/components/setting/SettingController.js?v='.getVersionScript()) !!}
@endsection