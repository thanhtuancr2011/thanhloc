<?php

namespace Core\Modules\Base\Menu;


use Collective\Html\HtmlBuilder;
use Core\Base\Repositories\Contracts\MenuRepositoryInterface;

use Exception;
use Theme;

class Menu
{
    /**
     * @var mixed
     */
    protected $menuRepository;

    /**
     * @var HtmlBuilder
     */
    protected $html;

    /**
     * @var MenuContentInterface
     */
    protected $menuContentRepository;

    /**
     * @var MenuNodeInterface
     */
    protected $menuNodeRepository;

    public function __construct(HtmlBuilder $html, MenuRepositoryInterface $menuRepository)
    {
        $this->html = $html;
        $this->menuRepository = $menuRepository;
    }

    /**
     * @return mixed
     */
    public function model()
    {
        return $this->menuRepository->getModel();
    }

    /**
     * @param $args
     * @return mixed|null|string
     * @author Sang Nguyen, Tedozi Manson
     */
    public function generateMenu($args = [])
    {
        $slug = array_get($args, 'slug');
        if (!$slug) {
            return null;
        }
        $parent_id = array_get($args, 'parent_id', 0);
        $view = array_get($args, 'view');
        $active = array_get($args, 'active', true);
        $theme = array_get($args, 'theme', true);
        $options = $this->html->attributes(array_get($args, 'options', []));

        $menu_nodes = $this->menuRepository->display($slug);
        if (!$menu_nodes)
            return null;
        if ($theme && $view) {
            return Theme::partial($view, compact('menu_nodes', 'options','view'));
        } elseif ($view) {
            return view($view, compact('menu_nodes', 'options','view'))->render();
        }
        return view('menu::partials.default', compact('menu_nodes', 'menu', 'options','view'))->render();
    }

    /**
     * @param array $args
     * @return mixed|null|string
     * @author Sang Nguyen, Tedozi Manson
     */
    public function generateSelect($args = [])
    {
        $model = array_get($args, 'model');
        if (!$model) {
            return null;
        }
        $parent_id = array_get($args, 'parent_id', 0);
        $view = array_get($args, 'view');
        $active = array_get($args, 'active', true);
        $theme = array_get($args, 'theme', true);
        $options = $this->html->attributes(array_get($args, 'options', []));

        $object = $model->whereParentId($parent_id);
        if ($active) {
            $object = $object->where('status', $active);
        }
        $object = $object->orderBy('name', 'asc')->get();

        if (empty($object)) {
            return null;
        }

        if ($theme && $view) {
            return Theme::partial($view, compact('object', 'model', 'options'));
        } elseif ($view) {
            return view($view, compact('object', 'model', 'options'))->render();
        }
        return view('menu::partials.select', compact('object', 'model', 'options'))->render();
    }

    /**
     * @param $slug
     * @param $active
     * @return bool
     * @author Sang Nguyen
     */
    public function hasMenu($slug, $active)
    {
        $menu = $this->menuRepository->findBySlug($slug, $active);
        if (!$menu) {
            return false;
        }
        return true;
    }

    /**
     * @param $menu_nodes
     * @param $menu_content_id
     * @param $parent_id
     * @author Sang Nguyen, Tedozi Manson
     */
    public function recursiveSaveMenu($menu_nodes, $menu_content_id, $parent_id)
    {
        try {
            foreach ($menu_nodes as $row) {
                $parent = $this->saveMenuNode($row, $menu_content_id, $parent_id);
                if (!empty($parent)) {
                    $this->recursiveSaveMenu(array_get($row, 'children'), $menu_content_id, $parent);
                }
            }
        } catch (Exception $ex) {
            info($ex->getMessage());
        }
    }

    /**
     * @param $menu_item
     * @param $menu_content_id
     * @param $parent_id
     * @return mixed
     * @author Sang Nguyen, Tedozi Manson
     */
    private function saveMenuNode($menu_item, $menu_content_id, $parent_id)
    {
        $item = MenuNode::find(array_get($menu_item, 'id'));
        if (!$item) {
            $item = new MenuNode();
        }

        $item->title = array_get($menu_item, 'title');
        $item->url = array_get($menu_item, 'customUrl');
        $item->css_class = array_get($menu_item, 'class');
        $item->position = array_get($menu_item, 'position');
        $item->icon_font = array_get($menu_item, 'iconFont');
        $item->target = array_get($menu_item, 'target');
        $item->type = array_get($menu_item, 'type');
        $item->menu_content_id = $menu_content_id;
        $item->parent_id = $parent_id;

        switch ($item->type) {
            case 'custom-link':
                $item->related_id = 0;
                break;
            default:
                $item->related_id = (int)array_get($menu_item, 'relatedId');
                break;
        }
        $this->menuNodeRepository->createOrUpdate($item);

        return $item->id;
    }
}
