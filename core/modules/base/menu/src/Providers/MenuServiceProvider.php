<?php

namespace Core\Modules\Base\Menu\Providers;

use Core\Modules\Base\Menu\Facades\MenuFacade;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\ServiceProvider;

class MenuServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     * @author Sang Nguyen
     */
    public function register()
    {

        AliasLoader::getInstance()->alias('Menu', MenuFacade::class);

        // Register helper
        foreach (glob(__DIR__ . '/../../helpers/*.php') as $filename) {
            require_once($filename);
        }
    }

    /**
     * Boot the service provider.
     * @author Sang Nguyen
     */
    public function boot()
    {
        $this->mergeConfigFrom(__DIR__ . '/../../config/menu.php', 'menu');
        $this->loadViewsFrom(__DIR__ . '/../../resources/views', 'menu');

    }
}
