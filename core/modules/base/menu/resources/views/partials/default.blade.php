<ul {!! $options !!}>
    @foreach ($menu_nodes as $key => $row)
        <li class=" @if (getUrl($row['route'],$row['url']) == Request::url()) current @endif">
            <a href="{{ getUrl($row['route'],$row['url']) }}">
                 <span>{{ $row['title'] }}</span>
            </a>
            @include('menu::partials.default',['menu_nodes' => $row['nodes'], 'options' => $options])
        </li>
    @endforeach
</ul>
