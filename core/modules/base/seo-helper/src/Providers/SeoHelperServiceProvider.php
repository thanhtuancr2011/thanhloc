<?php

namespace Core\Modules\Base\SeoHelper\Providers;

use Core\Modules\Base\Supports\Helper;
use Core\Modules\Base\SeoHelper\Contracts\SeoHelperContract;
use Core\Modules\Base\SeoHelper\Facades\SeoHelperFacade;
use Core\Modules\Base\SeoHelper\SeoHelper;
use Core\Modules\Base\SeoHelper\SeoMeta;
use Core\Modules\Base\SeoHelper\SeoOpenGraph;
use Core\Modules\Base\SeoHelper\SeoTwitter;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\ServiceProvider;
use Core\Modules\Base\SeoHelper\Contracts\SeoMetaContract;
use Core\Modules\Base\SeoHelper\Contracts\SeoOpenGraphContract;
use Core\Modules\Base\SeoHelper\Contracts\SeoTwitterContract;

/**
 * Class SEOServiceProvider
 * @package Botble\SEO
 * @author Sang Nguyen
 * @since 02/12/2015 14:09 PM
 */
class SeoHelperServiceProvider extends ServiceProvider
{
    /**
     * @author Sang Nguyen
     */
    public function register()
    {

        $this->app->bind(SeoMetaContract::class, SeoMeta::class);
        $this->app->bind(SeoHelperContract::class, SeoHelper::class);
        $this->app->bind(SeoOpenGraphContract::class, SeoOpenGraph::class);
        $this->app->bind(SeoTwitterContract::class, SeoTwitter::class);

        AliasLoader::getInstance()->alias('SeoHelper', SeoHelperFacade::class);

        Helper::autoload(__DIR__ . '/../../helpers');
    }

    /**
     * Boot the service provider.
     * @author Sang Nguyen
     */
    public function boot()
    {
        $this->mergeConfigFrom(__DIR__ . '/../../config/seo-helper.php', 'seo-helper');

        if (app()->runningInConsole()) {
            $this->publishes([__DIR__ . '/../../config/seo-helper.php' => config_path('seo-helper.php')], 'config');
        }
    }
}
