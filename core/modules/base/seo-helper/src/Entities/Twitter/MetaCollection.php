<?php
namespace Core\Modules\Base\SeoHelper\Entities\Twitter;

use Core\Modules\Base\SeoHelper\Bases\MetaCollection as BaseMetaCollection;

class MetaCollection extends BaseMetaCollection
{

    /**
     * Meta tag prefix.
     *
     * @var string
     */
    protected $prefix = 'twitter:';
}
