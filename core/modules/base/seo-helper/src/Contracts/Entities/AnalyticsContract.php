<?php
namespace Core\Modules\Base\SeoHelper\Contracts\Entities;

use Core\Modules\Base\SeoHelper\Contracts\RenderableContract;

interface AnalyticsContract extends RenderableContract
{
    /**
     * Set Google Analytics code.
     *
     * @param  string $code
     *
     * @return self
     * @author ARCANEDEV <arcanedev.maroc@gmail.com>
     */
    public function setGoogle($code);
}
