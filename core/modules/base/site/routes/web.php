<?php

Route::group(['middleware' => ['web']], function () {
	Route::get('/', 'Core\Modules\Base\Site\Http\Controllers\SiteController@home');
	Route::get('/sitemap.xml', 'Core\Modules\Base\Site\Http\Controllers\SiteController@sitemap')->name('sitemaps');
});

Route::group(['middleware' => ['web', 'customer']], function () {

    Route::get('/home', 'Core\Modules\Base\Site\Http\Controllers\SiteController@home1');

    Route::get('/search/{search}/{page?}', 'Core\Modules\Base\Site\Http\Controllers\SiteController@search');

    Route::get('/{slug}', 'Core\Modules\Base\Site\Http\Controllers\SiteController@index')->where('slug', '([A-Za-z0-9\-\/\.]+)');
});


