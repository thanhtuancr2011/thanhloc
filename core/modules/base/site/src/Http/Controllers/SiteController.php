<?php

namespace Core\Modules\Base\Site\Http\Controllers;

use URL;
use Auth;
use Theme;
use Sitemap;
use SeoHelper;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Request;

use Core\Base\Facades\Base;
use Core\Base\Repositories\Contracts\SettingRepositoryInterface;
use Core\Modules\Post\Repositories\Contracts\PostRepositoryInterface;
use Core\Modules\Term\Repositories\Contracts\TermRepositoryInterface;
use Core\Modules\Product\Repositories\Contracts\ProductRepositoryInterface;
use Core\Modules\Product\Repositories\Contracts\CategoryRepositoryInterface;

class SiteController extends Controller
{
	protected $postRepository;
    protected $termRepository;
    protected $settingRepository;
    protected $productRepository;
    protected $categoryRepository;

    public function __construct(PostRepositoryInterface $postRepository, TermRepositoryInterface $termRepository, SettingRepositoryInterface $settingRepository, ProductRepositoryInterface $productRepository, CategoryRepositoryInterface $categoryRepository)
    {
        $this->postRepository = $postRepository;
        $this->termRepository = $termRepository;
        $this->settingRepository = $settingRepository;
        $this->productRepository = $productRepository;
        $this->categoryRepository = $categoryRepository;
    }

    public function home() {
        SeoHelper::setTitle('Trang chủ')->setDescription(setting('description'));
        return Theme::scope('index')->render();
    }

    public function home1() {
        SeoHelper::setTitle('Trang chủ')->setDescription(setting('description'));
        return Theme::scope('index1')->render();
    }

    /**
     * View data with options
     * @param  String $prefix The type search (page, post, category, tag)
     * @param  String $slug The slug title
     * @param  Integer $page The current page
     * @return Void
     */
    public function index()
    {
        // Get request params
        $request = Base::getRequest();

        $items_per_page = setting('items_per_page', 5);
        $current_page = isset($_GET['page']) ? $_GET['page'] : 1;

        $option = [
            'current_page' => $current_page,
            'posts_per_page' => $items_per_page
        ];

        // check request is category with pageinator
        $lastParameter = $request[count($request) - 1];
        $firstParameter = $request[0];

        // Get products of category
        if (!empty($lastParameter)) {

            if (strpos($lastParameter, '.html') !== false) {
                $opt = ['slug' => str_replace('.html', '', $lastParameter)];
                $opt = array_merge($option, $opt);
                $data = $this->getSingleProduct($opt);
            } else {
                $opt = ['category' => $lastParameter];
                $opt = array_merge($option, $opt);
                $data = $this->getProductsOfCategory($opt);
            }

            if (!empty($data))
                return $this->renderView($data['view'], $data['data'])->render();
        }

        if (Request::has('page')) {
            $page = Request::get('page');
        }

        if (strlen(intval($lastParameter)) == strlen($lastParameter) && count($request)>1) {

            $option['current_page'] = $lastParameter;
            if ($firstParameter == 'tu-khoa') {
                // Get post of tag
                $opt = ['tag' => $request[count($request) - 2]];
                $opt = array_merge($option, $opt);
                $data = $this->getPostsOfTag($opt);
            } else {
                // Get post of category
                $opt = ['category' => $request[count($request) - 2]];
                $opt = array_merge($option, $opt);
                $data = $this->getPostsOfCategory($opt);
            }

        } elseif (isset($page)) {
            $option['current_page'] = $page;
            if ($firstParameter == 'tu-khoa') {
                // Get post of tag
                $opt = ['tag' => $request[count($request) - 1]];
                $opt = array_merge($option, $opt);
                $data = $this->getPostsOfTag($opt);
            } else {
                // Get post of category
                $opt = [
                    'category' => $request[count($request) - 1]
                ];
                $opt = array_merge($option, $opt);
                $data = $this->getPostsOfCategory($opt);
            }

        } elseif (count($request) == 1) {

            // Get Single page
            $opt = [
                'post' => $request[0],
                'post_type' => 'page'
            ];
            $opt = array_merge($option, $opt);

            // Get page
            $data = $this->getSinglePage($opt);

            // If is not page then get page thanks
            if (count($data['data']['item']) <= 0) {

                // Get page thank
                $opt = [
                    'post' => $request[0],
                    'post_type' => 'thank'
                ];
                $opt = array_merge($option, $opt);

                // Get page thanks
                $data = $this->getSinglePage($opt);

                // If is not page thanks then get category
                if (count($data['data']['item']) <= 0) {

                    // Get post of category
                    $opt = ['category' => $request[0]];
                    $opt = array_merge($option, $opt);
                    
                    $data = $this->getPostsOfCategory($opt);
                }
            }
        } else {
            if (substr($lastParameter, strlen($lastParameter) - 5, strlen($lastParameter)) == '.html') {
                // Get Single post
                $opt = [
                    'post' => str_replace('.html', '', $lastParameter),
                    'post_type' => 'post'
                ];
                $opt = array_merge($option, $opt);

                // Get post
                $data = $this->getSinglePost($opt);

            } elseif (substr($lastParameter, strlen($lastParameter) - 4, strlen($lastParameter)) == '.amp') {
                $opt = [
                    'post' => str_replace('.amp', '', $lastParameter),
                    'post_type' => 'post'
                ];
                $opt = array_merge($option, $opt);

                // Get post amp
                $data = $this->getSinglePost($opt, true);
            } else {
                if ($firstParameter == 'tu-khoa') {
                    $opt = [
                        'tag' => $request[1]
                    ];
                    $opt = array_merge($option, $opt);

                    // Get post by tag
                    $data = $this->getPostsOfTag($opt);
                } else {
                    // Get post of category
                    $opt = [
                        'category' => $lastParameter
                    ];
                    $opt = array_merge($option, $opt);
                    $data = $this->getPostsOfCategory($opt);
                }
            }
        }

        if (!$data) abort(404);
        return $this->renderView($data['view'], $data['data'])->render();
    }

    public function getSinglePage($opt)
    {
        if ($opt['post_type'] == 'page')
            $opt['post_type'] = 'page_';
        $item = $this->postRepository->searchPostsBy($opt);

        $temp = PAGE_THEME;
        $view = PAGE;
        $view .= '.' . $temp;
        $data['option'] = $opt;
        $data['option']['type_of_page'] = PAGE;
        $data['item'] = $item;

        return [
            'view' => $view,
            'data' => $data
        ];
    }

    /**
     * Get posts
     * @param  Array $options List search options
     * @return Void
     */
    public function getPosts($options)
    {
        return $this->postRepository->searchPostsBy($options);
    }

    /**
     * Get setting with key
     * @param  String $key The key name
     * @return Void
     */
    public function getConfig($key)
    {
        return $this->settingRepository->getConfig($key);
    }

    /**
     * Get categories
     * @param  Array $options List search options
     * @return Void
     */
    public function getCategories($options)
    {
        return $this->termRepository->searchCategoriesBy($options);
    }

    /**
     * Search
     * @param $search text search of post or page
     * @param $page
     * @return Void
     */
    public function search($search, $categorySlug = '')
    {
        // Current page
        $page = Request::get('page') ? Request::get('page') : 1;

        // Item per page
        $itemsPerPage = Request::get('items_per_page') ? Request::get('items_per_page') : 15;

        // Search options
        $options = [
            'current_page' => $page,
            'posts_per_page' => $itemsPerPage,
            'search' => $search,
            'type_of_page' => SEARCH,
            'with_categories' => true
        ];

        if (!empty($categorySlug))
            $options['category'] = $categorySlug;
        
        // Get post
        $item = $this->postRepository->searchPostsBy($options);

        $data['item'] = $item;
        $data['option'] = $options;
        $data['option']['category'] = $categorySlug;
        $view = 'search.' . SEARCH_THEME;

        return $this->renderView($view, $data)->render();
    }

    protected function getSinglePost($opt, $isAmp = false)
    {
        $opt['post_type'] = 'post_';
        $item = $this->postRepository->searchPostsBy($opt);
        if (count($item) <= 0) abort(404);

        $temp = POST_THEME;
        if ($isAmp) {
            $temp = POST_THEME_AMP;
            Theme::layout(POST_THEME_AMP);
        }

        // config temp for vinhomes
        if (isset($item->first()->post_type)) {
            if ($item->first()->post_type == 'post_real_estate') {
                $temp = 'real-eatate';


                $postMetas = $item->first()->postMetas->where('meta_key', 'vinhomes_detail')->first();
                $detail = json_decode($postMetas->meta_value);
                $d = [];
                foreach ($detail as $de) {
                    if ($de->type == 'content') {
                        $d = $de;
                        break;
                    }
                }
                if (isset($item->first()->excerpt)) {
                    SeoHelper::setDescription($item->first()->excerpt);
                } elseif ($d && $d->type == 'content') {
                    SeoHelper::setDescription($d->content);
                } else {
                    SeoHelper::setDescription('');
                }
            }
        }

        $view = POST;
        $view .= '.' . $temp;
        $data['option'] = $opt;
        $data['option']['type_of_page'] = POST;
        $data['option']['isAmp'] = $isAmp;
        $data['item'] = $item;

        // Count view
        $countView = !empty($item->first()->view) ? $item->first()->view : 0;
        $item->first()->view = $countView + 1;
        $item->first()->update();

        $this->checkUrl('post.' . $opt['post'], $isAmp);
        return [
            'view' => $view,
            'data' => $data
        ];
    }

    protected function getSingleProduct($opt)
    {
        $item = $this->productRepository->searchProductsBy($opt);
        if (count($item) <= 0) return [];

        $temp = PRODUCT_THEME;

        $view = PRODUCT . '.' . PRODUCT_THEME;
        $data['option'] = $opt;
        $data['option']['type_of_page'] = PRODUCT;
        $data['item'] = $item;

        return [
            'view' => $view,
            'data' => $data
        ];
    }

    protected function getPostsOfTag($opt)
    {
        $term = $this->termRepository->searchCategoriesBy([
            'slug' => $opt['tag'],
            'taxonomy' => TAG
        ]);

        if ($term->count() <= 0) abort(404);

        $item = $this->postRepository->searchPostsBy($opt);

        $view = TAG . '.' . TAG_THEME;
        $data['option'] = $opt;
        $data['item'] = $item;
        $data['option']['term'] = $term;
        $data['option']['type_of_page'] = TAG;

        return [
            'view' => $view,
            'data' => $data
        ];

    }

    protected function getPostsOfCategory($opt)
    {
        // Find category
        $term = $this->termRepository->searchCategoriesBy([
            'slug' => $opt['category'],
            'is_uncategory' => true,
        ]);

        // If not creategory then return 404 page
        if ($term->count() <= 0) abort(404);

        // Get item
        $opt['posts_per_page'] = 9;
        
        // Get posts
        $item = $this->postRepository->searchPostsBy($opt);

        // Template
        $temp = CATEGORY_THEME;

        $view = CATEGORY;
        $view .= '.' . $temp;
        $data['option'] = $opt;
        $data['item'] = $item;
        $data['option']['term'] = $term;
        $data['option']['type_of_page'] = CATEGORY;
        $this->checkUrl('category.' . $opt['category']);

        return [
            'view' => $view,
            'data' => $data
        ];
    }

    protected function getProductsOfCategory($opt)
    {
        // Find category
        $category = $this->categoryRepository->searchCategoriesBy([
            'slug' => $opt['category'],
            'is_uncategory' => true,
        ]);

        // If not creategory then return 404 page
        if ($category->count() <= 0) return [];

        // Set category opt
        $opt['category'] = $category->first()->slug;

        // Product items per page
        $opt['items_per_page'] = 15;
        
        // Get posts
        $item = $this->productRepository->searchProductsBy($opt);

        // Template
        $view = CATEGORY_PRODUCT. '.' . CATEGORY_PRODUCT_THEME;
        $data['option'] = $opt;
        $data['item'] = $item;
        $data['option']['category'] = $category;
        $data['option']['type_of_page'] = CATEGORY_PRODUCT;
        $this->checkUrl('category.' . $opt['category']);

        return [
            'view' => $view,
            'data' => $data
        ];
    }

    private function checkUrl($slug, $isAmp = false)
    {
        $url = $this->getUrl($slug, '', $isAmp);
        $currentUrl = Request::url();

        if (strpos($currentUrl, $url) === false) {
            return redirect($url)->send();
        }
    }

    /**
	 * Get url
	 * @param  String $slug Slug url
	 * @param  String $url url
	 * @return Void
	 */
	public function getUrl($slug, $url = '',$isAmp = false)
	{
	    if ($url) return $url;
	    $slug = explode('.', $slug);

	    switch ($slug[0]) {
	        case POST:
	            $url = app(PostRepositoryInterface::class)->getUrlPost($slug[1],$isAmp);
	            return $url;
	            break;
	        case PAGE:
	            $url = app(PostRepositoryInterface::class)->getUrlPage($slug['1']);
	            return $url;
	            break;
	        case CATEGORY:
	            $url = app(TermRepositoryInterface::class)->getUrlCategory($slug['1']);
	            return $url;
	            break;
	        case TAG:
	            $url = app(TermRepositoryInterface::class)->getUrlTag($slug['1']);
	            return $url;
	            break;
	        default:
	            return '';
	            break;
	    }
	}

	/**
	 * Render view
	 * @param  String $view View name
	 * @param  Object $item Data view
	 * @return Void
	 */
	public function renderView($view, $data = [])
	{
	    if ($data) {
	        // Load default theme
	        $item = $data['item'];
	        $option = $data['option'];
	    } else {
	        $item = [];
	        $option = ['type_of_page' => 'home'];
	    }

	    switch ($option['type_of_page']) {
	        case HOME:
	            SeoHelper::setTitle(setting('title'))->setDescription(setting('description'));
	            SeoHelper::openGraph()->setTitle(setting('title'))
	                ->setDescription(setting('description'))
	                ->addProperties([
	                    'locale' => 'vi',
	                    'type' => 'website',
	                    'url' => url(),
	                    'image:width' => '',
	                    'image:height' => '',
	                    'updated_time' => ''
	                ]);
	            $index = setting('dizi_0homeindex')?'index':'noindex';
	            $follow = setting('dizi_1homefollow')?'follow':'nofollow';
	            SeoHelper::meta()->addMeta('robots', "$index, $follow");

	            break;

	        case SEARCH:

	            Theme::breadcrumb()->add('Trang chủ', url('/'));
	            Theme::breadcrumb()->add('Tìm kiếm', url('/'));

	            SeoHelper::setTitle("Search:" . $option['search'])->setDescription($option['search']);
	            SeoHelper::meta()->addMeta('robots', 'index, follow');
	            break;

	        case TAG:
	            SeoHelper::setTitle($option['term']->first()->name)->setDescription($option['term']->first()->description);
	            $index = setting('dizi_0tagindex')?'index':'noindex';
	            $follow = setting('dizi_1tagfollow')?'follow':'nofollow';
	            SeoHelper::meta()->addMeta('robots', "$index, $follow");

	            Theme::breadcrumb()->add('Trang chủ', url('/'));

	            break;

	        case POST:
	        case PAGE:
            case PRODUCT;

	            $title = $item->first()->seo_title ? $item->first()->seo_title : $item->first()->title;
	            $excerpt = $item->first()->seo_description ? $item->first()->seo_description : $item->first()->excerpt;
	            SeoHelper::meta()->setTitle($title)->setDescription($excerpt);

	            // add breadcrumb
	            Theme::breadcrumb()->add('Trang chủ', url('/'));

	            $categories = getParamsUrl();

	            if ($option['type_of_page'] == PAGE) {
	                Theme::breadcrumb()->add($title, app(PostRepositoryInterface::class)->getUrlPage($item->first()->slug));
	            } else {
	                Theme::breadcrumb()->add($title, app(PostRepositoryInterface::class)->getUrlPost($item->first()->slug),(isset($data['option']['isAmp'])?($data['option']['isAmp']):false));
	            }

	            // Facebook and Tweeter
	            $title = $item->first()->seo_facebook_title ? $item->first()->seo_facebook_title : $item->first()->title;
	            $excerpt = $item->first()->seo_facebook_description ? $item->first()->seo_facebook_description : $item->first()->excerpt;
	            $img = $item->first()->seo_facebook_image ? url($item->first()->seo_facebook_image) : $item->first()->image_feature;

	            // $size = @getimagesize($img);

	            SeoHelper::openGraph()->setTitle($title)
	                ->setDescription($excerpt)
	                ->setImage($img)->addProperties([
	                    'locale' => 'vi',
	                    'type' => 'content',
	                    'url' => getUrl($item->first()->post_type . '.' . $item->first()->slug),
	                    'image:width' => isset($size[0]) ? '' . $size[0] : '600',
	                    'image:height' => isset($size[1]) ? '' . $size[1] : '315',
	                    'updated_time' => formatDate($item->first()->updated_at, 'd-m-Y H:i:s')
	                ]);
	            SeoHelper::twitter()->setTitle($title)
	                ->setDescription($excerpt)
	                ->addMetas([
	                    'card' => setting('title'),
	                    'image' => $img
	                ]);
	            $noindex = $item->first()->no_index ? 'noindex' : 'index';
	            $nofollow = $item->first()->no_follow ? 'nofollow' : 'follow';
	            SeoHelper::meta()->addMeta('robots', $noindex . ', ' . $nofollow);

	            break;

	        case CATEGORY:
	            Theme::breadcrumb()->add('Trang chủ', url('/'));

                SeoHelper::setTitle($option['term']->first()->name)->setDescription($option['term']->first()->description);

	            $categories = getParamsUrl();

	            $title = $option['term']->first()->seo_title ? $option['term']->first()->seo_title : $option['term']->first()->name;
	            $excerpt = $option['term']->first()->seo_description ? $option['term']->first()->seo_description : $option['term']->first()->description;
	            break;

            case CATEGORY_PRODUCT:
                Theme::breadcrumb()->add('Trang chủ', url('/'));
                Theme::breadcrumb()->add($option['category']->first()->name);
                SeoHelper::setTitle($option['category']->first()->name)->setDescription($option['category']->first()->description);

                $title = $option['category']->first()->seo_title ? $option['category']->first()->seo_title : $option['category']->first()->name;
                $excerpt = $option['category']->first()->seo_description ? $option['category']->first()->seo_description : $option['category']->first()->description;
                break;

	        default:
	    }

        // View category product
        if ($option['type_of_page'] == CATEGORY_PRODUCT) {
            return Theme::scope($view, compact('item', 'option'));
        }

        if (isset($option) && $option['type_of_page'] == 'search') {
            return Theme::scope($view, compact('item', 'option'));
        }

	    // Render thank page
	    if (isset($option['post_type']) && $option['post_type'] == 'thank') {
	        return getThankTheme($item->toArray());
	    } else {
	        return Theme::scope($view, compact('item', 'option'));
	    }
	}

    /**
     * View site map
     * author  NgocAnh <avn@diziweb.com>
     * @return Void
     */
    public function sitemap()
    {
        // Get all categories
        $terms = $this->termRepository->create()->get();

        // Get all posts
        $posts = $this->postRepository->create()->where('post_type', 'post')
                      ->with('categories')->get();

        // Get all page
        $pages = $this->postRepository->create()->where('post_type', 'page')
                      ->get();

        // Get all categories product
        $categories = $this->categoryRepository->create()->get();

        // Get all product
        $products = $this->productRepository->create()
                      ->with('categories')->get();

        // Each term
        foreach ($terms as $term) {

            if ($term->taxonomy == 'category') {
                $url = URL::to($term->url);
            } else {
                $url = URL::to('tu-khoa/' . $term->url); 
            }

            if (!empty($url))
                Sitemap::addTag($url, $term->updated_at, 'daily', '0.8');
        }

        // Each category
        foreach ($categories as $category) {

            $url = URL::to($category->url);

            if (!empty($url))
                Sitemap::addTag($url, $category->updated_at, 'daily', '0.8');
        }

        // Each post
        foreach ($posts as $post) {
            $url = URL::to($post->categories->first()->url . '/' . $post->slug . '.html');

            if (empty($url)) continue;
            $tag = Sitemap::addTag($url, $post->updated_at, 'daily', '0.8');
            $img = json_decode($post->image_feature, true);
            $src = isset($img['src']) ? $img['src'] : '';
            $alt = isset($img['alt']) ? $img['alt'] : '';

            if ($src) {
                $src = str_replace(' ', '', $src);
                $src = str_replace(';', '', $src);
                if (!strpos($src, url(''))) continue;
                $tag->addImage(url($src), '');
            }
        }

        // Each product
        foreach ($products as $product) {
            $url = URL::to($product->categories->first()->url . '/' . $product->slug . '.html');

            if (empty($url)) continue;
            $tag = Sitemap::addTag($url, $product->updated_at, 'daily', '0.8');
            $img = json_decode($product->image_feature, true);
            $src = isset($img['src']) ? $img['src'] : '';
            $alt = isset($img['alt']) ? $img['alt'] : '';

            if ($src) {
                $src = str_replace(' ', '', $src);
                $src = str_replace(';', '', $src);
                if (!strpos($src, url(''))) continue;
                $tag->addImage(url($src), '');
            }
        }

        return Sitemap::render();
    }
}
