<?php

namespace Core\Modules\Base\Site\Providers;

use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\ServiceProvider;

use Core\Modules\Base\Setting\Providers\SettingServiceProvider;
use Core\Modules\Base\SeoHelper\Providers\SeoHelperServiceProvider;

use Watson\Sitemap\Facades\Sitemap;
use Watson\Sitemap\SitemapServiceProvider;

class SiteServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->register(SettingServiceProvider::class);
        $this->app->register(SeoHelperServiceProvider::class);
        $this->app->register(SitemapServiceProvider::class);

        // List aliases
        $loader = AliasLoader::getInstance();
        $loader->alias('Sitemap', Sitemap::class);

        // Load route
        $this->loadRoutesFrom(__DIR__.'/../../routes/web.php');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        // Register helper
        foreach (glob(__DIR__ . '/../../helpers/*.php') as $filename) {
            require_once $filename;
        }
    }

}
