<?php

// use \Dizi\Core\Models\MenuModel;
use Core\Modules\Base\Site\Http\Controllers\SiteController;
use Core\Modules\Term\Repositories\Contracts\TermRepositoryInterface;
use Core\Modules\Post\Repositories\Contracts\PostRepositoryInterface;

/**
 * Get posts
 * @param  Array $options List options
 * @return Void
 */
function get_post($options = [])
{
    $config = [
        'offset' => 0,
        'author' => '',
        'order' => 'DESC',
        'category' => '',
        'category_id' => '',
        'author_name' => '',
        'posts_per_page' => 5,
        'category_name' => '',
        'orderby' => 'created_at',
        'post_status' => 'publish',
        'post_type' => 'post_',
        'post_id' => '',
    ];

    $options = array_merge($config, $options);
    return app(SiteController::class)->getPosts($options);
}

/**
 * Get pages
 * @param  Array $option List options
 * @return Void
 */
function get_page($options = [])
{
    $config = [
        'title' => '',
        'slug' => '',
        'author' => '',
        'author_name' => '',
        'sort_column' => '',
        'sort_order' => 'ASC',
        'posts_per_page' => 1,
        'meta_key' => '',
        'meta_value' => '',
        'post_status' => 'publish'
    ];

    $options = array_merge($config, $options);
    return app(SiteController::class)->getPages($options);
}

// get data of categories
function get_category($option = [])
{
    $config = [
        'hide_empty' => 0,
        'order' => 'ASC',
        'orderby' => 'id',
        'slug' => '',
        'taxonomy' => 'category'
    ];

    $option = array_merge($config, $option);
    return app(SiteController::class)->getCategories($option);
}

// get data of tags
function tags($option)
{
    //
}

if (!function_exists('getParamsUrl')) {
    /**
     * Get param url
     * @return Void
     */
    function getParamsUrl()
    {
        $url = \Illuminate\Support\Facades\Request::url();
        $url = str_replace(url('/'), '', $url);
        $urlList = explode('/', $url);
        if (strpos(end($urlList), '.html') !== false) {
            unset($urlList[count($urlList) - 1]);
        }
        if (strpos(end($urlList), '.amp') !== false) {
            unset($urlList[count($urlList) - 1]);
        }
        if (strpos(end($urlList), 'category') !== false) {
            unset($urlList[count($urlList) - 1]);
        }
        return $urlList;
    }
}

/**
 * Render view
 * @param  String $view View name
 * @param  Object $item Data view
 * @return Void
 */
function renderView($view, $data = [])
{
    if ($data) {
        // If theme not exists
        // Load default theme
        $item = $data['item'];
        $option = $data['option'];
    } else {
        $item = [];
        $option = ['type_of_page' => 'home'];
    }

    SeoHelper::setCanonical(\Illuminate\Support\Facades\Request::url());

    switch ($option['type_of_page']) {
        case HOME:
            SeoHelper::setTitle(setting('title'))->setDescription(setting('description'));
            SeoHelper::openGraph()->setTitle(setting('title'))
                ->setDescription(setting('description'))
                ->addProperties([
                    'locale' => 'vi',
                    'type' => 'website',
                    'url' => url(),
                    'image:width' => '',
                    'image:height' => '',
                    'updated_time' => ''
                ]);
            $index = setting('dizi_0homeindex')?'index':'noindex';
            $follow = setting('dizi_1homefollow')?'follow':'nofollow';
            SeoHelper::meta()->addMeta('robots', "$index, $follow");
            break;

        case SEARCH:
            Theme::breadcrumb()->add('Trang chủ', url('/'));
            Theme::breadcrumb()->add('Tìm kiếm', url('/'));
            SeoHelper::meta()->addMeta('robots', 'index, follow');
            SeoHelper::setTitle("Search:" . $option['search'])->setDescription($option['search']);
            break;

        case TAG:
            $title = $option['term']->first()->name;
            $index = setting('dizi_0tagindex')?'index':'noindex';
            $follow = setting('dizi_1tagfollow')?'follow':'nofollow';
            $description = $option['term']->first()->description;
            SeoHelper::setTitle($title)->setDescription($description);
            SeoHelper::meta()->addMeta('robots', "$index, $follow");
            Theme::breadcrumb()->add('Trang chủ', url('/'));
            $tags = getParamsUrl();
            foreach ($tags as $tag) {
                if ($tag && $tag != 'tag') {
                    Theme::breadcrumb()->add(app(TermRepositoryInterface::class)->getBySlug($tag)->name, app(TermRepositoryInterface::class)->getUrlTag($tag, true));
                }
            }
            break;

        case POST:
        case PAGE:
        
            $title = $item->first()->seo_title ? $item->first()->seo_title : $item->first()->title;
            $excerpt = $item->first()->seo_description ? $item->first()->seo_description : $item->first()->excerpt;
            SeoHelper::meta()->setTitle($title)->setDescription($excerpt);

            // add breadcrumb
            Theme::breadcrumb()->add('Trang chủ', url('/'));

            $categories = getParamsUrl();

            if ($option['type_of_page'] == PAGE) {
                Theme::breadcrumb()->add($title, app(PostRepositoryInterface::class)->getUrlPage($item->first()->slug));
            } else {
                foreach ($categories as $cat) {
                    if ($cat) {
                        Theme::breadcrumb()->add(app(TermRepositoryInterface::class)->getBySlug($cat)->name, app(TermRepositoryInterface::class)->getUrlCategory($cat, true));
                    }
                }
                Theme::breadcrumb()->add($title, app(PostRepositoryInterface::class)->getUrlPost($item->first()->slug),(isset($data['option']['isAmp'])?($data['option']['isAmp']):false));
            }

            // Facebook and Tweeter
            $title = $item->first()->seo_facebook_title ? $item->first()->seo_facebook_title : $item->first()->title;
            $excerpt = $item->first()->seo_facebook_description ? $item->first()->seo_facebook_description : $item->first()->excerpt;
            $img = $item->first()->seo_facebook_image ? url($item->first()->seo_facebook_image) : $item->first()->image_feature;

            // $size = @getimagesize($img);

            SeoHelper::openGraph()->setTitle($title)
                ->setDescription($excerpt)
                ->setImage($img)->addProperties([
                    'locale' => 'vi',
                    'type' => 'content',
                    'url' => getUrl($item->first()->post_type . '.' . $item->first()->slug),
                    'image:width' => isset($size[0]) ? '' . $size[0] : '600',
                    'image:height' => isset($size[1]) ? '' . $size[1] : '315',
                    'updated_time' => formatDate($item->first()->updated_at, 'd-m-Y H:i:s')
                ]);
            SeoHelper::twitter()->setTitle($title)
                ->setDescription($excerpt)
                ->addMetas([
                    'card' => setting('title'),
                    'image' => $img
                ]);
            $noindex = $item->first()->no_index ? 'noindex' : 'index';
            $nofollow = $item->first()->no_follow ? 'nofollow' : 'follow';
            SeoHelper::meta()->addMeta('robots', $noindex . ', ' . $nofollow);

            break;

        case CATEGORY:

            Theme::breadcrumb()->add('Trang chủ', url('/'));

            $categories = getParamsUrl();
            foreach ($categories as $cat) {
                if ($cat) {
                    Theme::breadcrumb()->add(app(TermRepositoryInterface::class)->getBySlug($cat)->name, app(TermRepositoryInterface::class)->getUrlCategory($cat, true));
                }
            }

            $title = $option['term']->first()->seo_title ? $option['term']->first()->seo_title : $option['term']->first()->name;
            $excerpt = $option['term']->first()->seo_description ? $option['term']->first()->seo_description : $option['term']->first()->description;
            SeoHelper::meta()->setTitle($title)->setDescription($excerpt);

            // Facebook and Tweeter
            SeoHelper::openGraph()->setTitle($title)
                ->setDescription($excerpt)
                ->addProperties([
                    'locale' => 'vi',
                    'type' => 'content',
                    'url' => getUrl($option['term']->first()->taxonomy . '.' . $option['term']->first()->slug),
                    'image:width' => '',
                    'image:height' => '',
                    'updated_time' => formatDate($option['term']->first()->updated_at, 'd-m-Y H:i:s')
                ]);
            SeoHelper::twitter()->setTitle($title)
                ->setDescription($excerpt)
                ->addMetas([
                    'card' => setting('title'),
                ]);
            $index = setting('dizi_0categoryindex')?'index':'noindex';
            $follow = setting('dizi_1categoryfollow')?'follow':'nofollow';
            SeoHelper::meta()->addMeta('robots', "$index, $follow");
            break;

        default:
    }
    
    // Render thank page
    if (isset($option['post_type']) && $option['post_type'] == 'thank') {
        return getThankTheme($item->toArray());
    } else {
        return Theme::scope($view, compact('item', 'option'));
    }
}

/**
 * Get thank theme html
 * @param  Object $data List page
 * @return Void       
 */
function getThankTheme($data)
{
    $templateName = $data['data'][0]['template'];
    $templateFolder = public_path() . '/dizi/partials/thanks/' . $templateName;

    // Contain script
    $script = $data['data'][0]['script'];

    // Contain html
    $html = false;

    // Check file theme.json is exists
    if (file_exists($templateFolder . '/theme.json')) {

        $themeJson = json_decode(\File::get($templateFolder . '/theme.json'), TRUE);

        if (isset($themeJson['view'])) {

            // Add temporary path with a hint type.
            view()->addNamespace('thank', $templateFolder);

            // Get file theme is exists
            if (view()->exists('thank::' . $themeJson['view'])) {
                $html = view('thank::' . $themeJson['view'], compact('script'))->render();
            }
        }
    }

    return view('dizi-site::layouts.thank', compact('html'));
}

/**
 * Get menu
 * @param  String $menuName The menu slug name
 * @param  String $type The view name
 * @param  Array $options The options
 * @return Void
 */
function menu($menuName, $type = null, array $options = [])
{
    return MenuModel::display($menuName, $type, $options);
}

/**
 * Get url
 * @param  String $slug Slug url
 * @param  String $url url
 * @return Void
 */
function getUrl($slug, $url = '',$isAmp = false)
{
    if ($url) return $url;
    $slug = explode('.', $slug);

    switch ($slug[0]) {
        case POST:
            $url = app(PostRepositoryInterface::class)->getUrlPost($slug[1],$isAmp);
            return $url;
            break;
        case PAGE:
            $url = app(PostRepositoryInterface::class)->getUrlPage($slug['1']);
            return $url;
            break;
        case CATEGORY:
            $url = app(TermRepositoryInterface::class)->getUrlCategory($slug['1']);
            return $url;
            break;
        case TAG:
            $url = app(TermRepositoryInterface::class)->getUrlTag($slug['1']);
            return $url;
            break;
        default:
            return '';
            break;
    }
}

/**
 * Convert image feature
 * @param  Array $data List image feature data
 * @return Void
 */
function getImgFeature($data)
{
    $tmp = json_decode($data, TRUE);
    return $tmp;
}


if (!function_exists('formatDate')) {
    /**
     * @param $time
     * @param string $format
     * @return mixed
     * @author AnhVN
     */
    function formatDate($time, $format = 'd-m-Y')
    {
        return format_time(\Carbon\Carbon::parse($time), $format);
    }
}

if (!function_exists('format_time')) {
    /**
     * @param DateTime $timestamp
     * @param $format
     * @return mixed
     * @author AnhVN
     */
    function format_time(DateTime $timestamp, $format = 'j M Y H:i')
    {
        $first = \Carbon\Carbon::create(0000, 0, 0, 00, 00, 00);
        if ($timestamp->lte($first)) {
            return '';
        }

        return $timestamp->format($format);
    }
}
if (!function_exists('formatDateStr')) {
    /**
     * @param $time
     * @param string $format
     * @return mixed
     * @author AnhVN
     */
    function formatDateStr($time, $format = 'd-m-Y')
    {
        return rebuild_date($format,strtotime($time));
    }
}

function rebuild_date( $format, $time = 0 )
{
    if ( ! $time ) $time = time();

    $lang = array();
    $lang['sun'] = 'CN';
    $lang['mon'] = 'T2';
    $lang['tue'] = 'T3';
    $lang['wed'] = 'T4';
    $lang['thu'] = 'T5';
    $lang['fri'] = 'T6';
    $lang['sat'] = 'T7';
    $lang['sunday'] = 'Chủ nhật';
    $lang['monday'] = 'Thứ hai';
    $lang['tuesday'] = 'Thứ ba';
    $lang['wednesday'] = 'Thứ tư';
    $lang['thursday'] = 'Thứ năm';
    $lang['friday'] = 'Thứ sáu';
    $lang['saturday'] = 'Thứ bảy';
    $lang['january'] = 'Tháng 1';
    $lang['february'] = 'Tháng 2';
    $lang['march'] = 'Tháng 3';
    $lang['april'] = 'Tháng 4';
    $lang['may'] = 'Tháng 5';
    $lang['june'] = 'Tháng 6';
    $lang['july'] = 'Tháng 7';
    $lang['august'] = 'Tháng 8';
    $lang['september'] = 'Tháng 9';
    $lang['october'] = 'Tháng 10';
    $lang['november'] = 'Tháng 11';
    $lang['december'] = 'Tháng 12';
    $lang['jan'] = 'T01';
    $lang['feb'] = 'T02';
    $lang['mar'] = 'T03';
    $lang['apr'] = 'T04';
    $lang['may2'] = 'T05';
    $lang['jun'] = 'T06';
    $lang['jul'] = 'T07';
    $lang['aug'] = 'T08';
    $lang['sep'] = 'T09';
    $lang['oct'] = 'T10';
    $lang['nov'] = 'T11';
    $lang['dec'] = 'T12';

    $format = str_replace( "r", "D, d M Y H:i:s O", $format );
    $format = str_replace( array( "D", "M" ), array( "[D]", "[M]" ), $format );
    $return = date( $format, $time );

    $replaces = array(
        '/\[Sun\](\W|$)/' => $lang['sun'] . "$1",
        '/\[Mon\](\W|$)/' => $lang['mon'] . "$1",
        '/\[Tue\](\W|$)/' => $lang['tue'] . "$1",
        '/\[Wed\](\W|$)/' => $lang['wed'] . "$1",
        '/\[Thu\](\W|$)/' => $lang['thu'] . "$1",
        '/\[Fri\](\W|$)/' => $lang['fri'] . "$1",
        '/\[Sat\](\W|$)/' => $lang['sat'] . "$1",
        '/\[Jan\](\W|$)/' => $lang['jan'] . "$1",
        '/\[Feb\](\W|$)/' => $lang['feb'] . "$1",
        '/\[Mar\](\W|$)/' => $lang['mar'] . "$1",
        '/\[Apr\](\W|$)/' => $lang['apr'] . "$1",
        '/\[May\](\W|$)/' => $lang['may2'] . "$1",
        '/\[Jun\](\W|$)/' => $lang['jun'] . "$1",
        '/\[Jul\](\W|$)/' => $lang['jul'] . "$1",
        '/\[Aug\](\W|$)/' => $lang['aug'] . "$1",
        '/\[Sep\](\W|$)/' => $lang['sep'] . "$1",
        '/\[Oct\](\W|$)/' => $lang['oct'] . "$1",
        '/\[Nov\](\W|$)/' => $lang['nov'] . "$1",
        '/\[Dec\](\W|$)/' => $lang['dec'] . "$1",
        '/Sunday(\W|$)/' => $lang['sunday'] . "$1",
        '/Monday(\W|$)/' => $lang['monday'] . "$1",
        '/Tuesday(\W|$)/' => $lang['tuesday'] . "$1",
        '/Wednesday(\W|$)/' => $lang['wednesday'] . "$1",
        '/Thursday(\W|$)/' => $lang['thursday'] . "$1",
        '/Friday(\W|$)/' => $lang['friday'] . "$1",
        '/Saturday(\W|$)/' => $lang['saturday'] . "$1",
        '/January(\W|$)/' => $lang['january'] . "$1",
        '/February(\W|$)/' => $lang['february'] . "$1",
        '/March(\W|$)/' => $lang['march'] . "$1",
        '/April(\W|$)/' => $lang['april'] . "$1",
        '/May(\W|$)/' => $lang['may'] . "$1",
        '/June(\W|$)/' => $lang['june'] . "$1",
        '/July(\W|$)/' => $lang['july'] . "$1",
        '/August(\W|$)/' => $lang['august'] . "$1",
        '/September(\W|$)/' => $lang['september'] . "$1",
        '/October(\W|$)/' => $lang['october'] . "$1",
        '/November(\W|$)/' => $lang['november'] . "$1",
        '/December(\W|$)/' => $lang['december'] . "$1" );

    return preg_replace( array_keys( $replaces ), array_values( $replaces ), $return );
}
if (!function_exists('get_related_posts')) {
    /**
     * @param $current_slug
     * @param $limit
     * @return mixed
     * @author AnhVN
     */
    function get_related_posts($current_slug, $limit)
    {
        return app(PostRepositoryInterface::class)->getRelated($current_slug, $limit);
    }
}

if (!function_exists('get_popular_posts')) {
    /**
     * @param integer $limit
     * @param array $args
     * @return mixed
     * @author Sang Nguyen
     */
    function get_popular_posts($limit = 10, array $args = [])
    {
        return app(PostRepositoryInterface::class)->getPopularPosts($limit, $args);
    }
}