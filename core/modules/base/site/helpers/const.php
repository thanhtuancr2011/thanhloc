<?php

define('POST', 'post');
define('PAGE', 'page');
define('HOME', 'home');
define('TAG', 'post_tag');
define('SEARCH', 'search');
define('PRODUCT', 'product');
define('CATEGORY', 'category');
define('CATEGORY_PRODUCT', 'category-product');

define('THEME', 'default');
define('TAG_THEME', 'tag');
define('PAGE_THEME', 'page');
define('POST_THEME', 'post');
define('PRODUCT_THEME', 'product');
define('POST_THEME_AMP', 'amp');
define('SEARCH_THEME', 'search');
define('CATEGORY_THEME', 'category');
define('CATEGORY_PRODUCT_THEME', 'category');