<?php

namespace Core\Modules\User\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

use Core\Modules\User\Models\UserModel;

class InstallCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'user:create-admin';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create data';

    
    public function createData()
    {
        // Find user
        $user = UserModel::where('email', 'admin@gmail.com')->first();

        if (empty($user)) {

            // Create user and assign role is admin
            $superAdmin = UserModel::create([
                'full_name' => 'Supper Admin',
                'email' => 'admin@gmail.com',
                'avatar' => '50x50_avatar_default.png',
                'password' => bcrypt('admin'),
                'status' => 'active',
                'type' => 'admin',
                'remember_token' => str_random(40),
            ]);

        } else {
            $this->info('--- Email admin@gmail.com đã tồn tại trong hệ thống. ---');
        }
    }


    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        $this->info('--- Tạo tài khoản quản trị ---');

        // Create data user
        $this->createData();

        $this->info('--- Thành công ---');
    }
}