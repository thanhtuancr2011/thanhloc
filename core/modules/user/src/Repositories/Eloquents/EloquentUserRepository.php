<?php

namespace Core\Modules\User\Repositories\Eloquents;

use Core\Modules\User\Models\UserModel;

use Core\Base\Repositories\Eloquents\EloquentRepository;
use Core\Modules\User\Repositories\Contracts\UserRepositoryInterface;

class EloquentUserRepository extends EloquentRepository implements UserRepositoryInterface
{

    /**
     * get model
     * @return string
     */
    public function getModel()
    {
        return UserModel::class;
    }

    /**
     * Update profile of user    
     * @param  Request $request Request
     * @param  Int     $userId  Id of user 
     * @return \Illuminate\Http\Response
     */
    public function updateProfile($id, $data)
    {
        // Get user
        $user = $this->model->findOrFail($id);

        // Call function update profile
        return $user->updateProfile($data);
    }

    /**
     * Change avatar for user
     * @param  Request $request Request
     * @param  String  $id      Id of user 
     * @return Array            
     */
    public function changeAvatar($id, $data)
    {
        // Get user
        $user = $this->model->findOrFail($id);

        // Call function change avatar
        return $user->changeAvatar($data);
    }

    /**
     * Change status for user
     * @param  String  $id      Id of user 
     * @return Array            
     */
    public function changeStatus($id)
    {
        // Get user
        $user = $this->model->findOrFail($id);

        // Call function change avatar
        return $user->changeStatus();
    }

    /**
     * Change password for user
     * @param  Request $request Request
     * @param  String  $id      Id of user 
     * @return Response         
     */
    public function changePassword($id, $data) 
    {
        // Get user
        $user = $this->model->findOrFail($id);

        // Call function change password
        return $user->changePassword($data);
    }

    /**
     * Create new customer
     * @param  Request $request The request data
     * @return Void           
     */
    public function createCustomer ($data)
    {
        // Call function create customer
        return $this->model->createCustomer($data);
    }   
}