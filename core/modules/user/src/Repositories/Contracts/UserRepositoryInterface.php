<?php

namespace Core\Modules\User\Repositories\Contracts;

interface UserRepositoryInterface
{
    /**
     * Update profile of user   
     * @param  Request $request Request
     * @param  Int     $userId  Id of user 
     * @return \Illuminate\Http\Response
     */
    public function updateProfile($id, $data);

    /**
     * Change avatar for user
     * @param  Request $request Request
     * @param  String  $id      Id of user 
     * @return Array            
     */
    public function changeAvatar($id, $data);

    /**
     * Change password for user
     * @param  Request $request Request
     * @param  String  $id      Id of user 
     * @return Response         
     */
    public function changePassword($id, $data);

    /**
     * Create new customer
     * @param  Request $request Request
     * @return Response         
     */
    public function createCustomer($data);
}


