<?php

namespace Core\Modules\User\Models;

use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Config;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;


use Core\Base\Services\FileService;
use Core\Base\Traits\Users;

class UserModel extends Authenticatable
{
    use Notifiable;
    use Users;
    
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['full_name', 'name_of_drugstore', 'email', 'phone', 'address', 'password', 'avatar', 'type', 'provider', 'provider_id', 'status', 'role_id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token', 'password'
    ];

    /**
     * Relation Role
     * @author AnhVN
     */
    public function role()
    {
        return $this->belongsTo(Config::get('roles.models.role'), 'role_id');
    }

    /**
     * Get list users
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Array $data The data input
     * @return Array       The result
     */
    public function items($data)
    {
        if (isset($data['pageOptions'])) {
            $currentPage = $data['pageOptions']['currentPage'];
            $itemsPerPage = $data['pageOptions']['itemsPerPage'];
        }

        // Set current page
        Paginator::currentPageResolver(function () use ($currentPage) {
            return $currentPage;
        });

        // If isset search data
        if (isset($data['searchOptions']['searchText'])) {
            $paginate = self::where('type', 'LIKE', $data['searchOptions']['type'])
                ->where(function ($query) use ($data) {
                    $query->where('name', 'LIKE', '%' . $data['searchOptions']['searchText'] . '%')
                        ->orWhere('email', 'LIKE', '%' . $data['searchOptions']['searchText'] . '%');
                })->orderBy('created_at', 'desc')->where('type', '!=', 'super_admin')->paginate($itemsPerPage)->toArray();
        } else {
            $paginate = self::where('type', 'LIKE', $data['searchOptions']['type'])
                ->orderBy('created_at', 'desc')->where('type', '!=', 'super_admin')->paginate($itemsPerPage)->toArray();
        }

        $totalItems = $paginate['total'];
        $totalPages = ceil($totalItems / $itemsPerPage);

        return [
            'status' => 1,
            'users' => $paginate['data'],
            'totalPages' => $totalPages,
            'totalItems' => $totalItems,
        ];
    }

    /**
     * Get avatar url for user
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  string $avatar Avatar
     * @param  String $size Size of image
     * @return String         Url
     */
    public function getAvatarUrl($avatar = '', $size = '50x50')
    {
        // If user is not avatar then set default avatar for user
        if (empty($avatar)) {
            $avatar = '50x50_avatar_default.png?t=1';
        }

        return '/dizi/admin/avatars/' . $avatar;
    }

    /**
     * Create new user
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Array $data The data input
     * @return Void
     */
    public function storeItem($data)
    {
        // Encrypt password
        $data['password'] = bcrypt($data['password']);

        // Set avatar
        $data['avatar'] = '50x50_avatar_default.png?t=1';

        // Get remember token
        $data['remember_token'] = str_random(40);

        // Create new user
        $user = self::create($data);

        return $user;
    }

    /**
     * Update user
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Array $data Data input
     * @return Object       User
     */
    public function updateItem($data)
    {
        // Encrypt password if isset password
        if (!empty($data['password']) || isset($data['password'])) {
            $data['password'] = bcrypt($data['password']);
        }

        // Update user
        $this->update($data);

        return $this;
    }

    /**
     * Delete users
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Array $ids List ids
     * @return Void
     */
    public function deleteItems($ids)
    {
        $status = self::whereIn('id', $ids)->delete();
        return $status;
    }

    /**
     * Update user profile
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Array $data Data input
     * @return Object       User
     */
    public function updateProfile($data)
    {
        $status = 0;

        // Encrypt password if isset password
        if (isset($data['password'])) {
            unset($data['password']);
        }

        // Update user
        if ($this->update($data)) {
            $status = 1;
        }

        // Format created date
        $this->created_at = date('Y-m-d', strtotime($this->created_at));

        // Get full name for user
        $this->name = trim($this->last_name) . ' ' . trim($this->first_name);

        // Get avatar default if user hasn't avatar
        if (empty($this->avatar)) {
            $this->avatar = '160x160_avatar_default.png?t=1';
        }

        return $this;
    }

    /**
     * Change avatar for user
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Array $data Data input
     * @return Void
     */
    public function changeAvatar($data)
    {
        // Endcode and save image
        $data['file'] = str_replace('data:image/png;base64,', '', $data['file']);
        $data['file'] = str_replace(' ', '+', $data['file']);
        $result = FileService::saveAvatar(base64_decode($data['file']), $this->id);

        // Change image success
        if (!isset($result['error'])) {
            $this->avatar = $result;
            $this->save();
            return $this;
        } else {
            return $result['error'];
        }
    }

    /**
     * Change status for user
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Array $data Data input
     * @return Void
     */
    public function changeStatus()
    {
        $this->status = $this->status == 1 ? 0 : 1;

        // Update user
        return $this->update();
    }

    /**
     * Change password for user
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Array $data Data input
     * @return Int Status
     */
    public function changePassword($data)
    {
        // Crypt password
        $this->password = bcrypt($data['password']);

        // Save password
        $result = $this->save();

        return $result;
    }
}
