<?php

namespace Core\Modules\User\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Foundation\AliasLoader;

use Core\Modules\User\Repositories\Contracts\UserRepositoryInterface;
use Core\Modules\User\Repositories\Eloquents\EloquentUserRepository;

class UserServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        // Publishes file
        $this->publishes([
            __DIR__.'/../../public/backend' => base_path('public/backend/app/components'),
        ], 'all');

        // Load route
        $this->loadRoutesFrom(__DIR__.'/../../routes/web.php');

        // Load view
        $this->loadViewsFrom(__DIR__ .'/../../views/', 'user');

        // Load migrate
        $this->loadMigrationsFrom(__DIR__.'/../../database/migrations');

        // Load middle ware
        $this->app['router']->middlewareGroup('customer', [\Core\Modules\User\Middleware\Customer::class]);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        // Register helper
        foreach (glob(__DIR__ . '/../Helpers/*.php') as $filename) {
            require_once $filename;
        }

        $this->app->bind(
            UserRepositoryInterface::class,
            EloquentUserRepository::class
        );

    }

}
