<?php 

namespace Core\Modules\User\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Application;

class Status {

    public function __construct(Application $app, Request $request) {
        $this->app = $app;
        $this->request = $request;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Get user
        $user = $request->user();

        // If user is not active
        if (!empty($user) && $user->status == 0) {
            Auth::logout();
            return redirect('auth/login')->send();
        }
        
        // If user is customer
        if (!empty($user) && $user->type == 'customer') {
            return redirect('/')->send();
        }

        return $next($request);
    }

}