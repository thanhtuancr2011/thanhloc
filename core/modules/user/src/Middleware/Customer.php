<?php

namespace Core\Modules\User\Middleware;

use Cart;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Application;

class Customer
{
    public function __construct(Application $app, Request $request) {
        $this->app = $app;
        $this->request = $request;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Get user
        $user = $request->user();

        // If isset Cart
        if (empty(Cart::content()->toArray()) && strpos($request->url(), 'thanh-toan') !== false) {
            return redirect('/')->send();
        }   

        // If user is not active
        if (empty($user) || $user->status == "0") {
            Auth::logout();
            return redirect('/dang-nhap')->send();
        }

        return $next($request);
    }
}
