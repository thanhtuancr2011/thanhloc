<?php

namespace Core\Modules\User\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\ValidationException;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Login form
     *
     * @var string
     */
    public function show()
    {
        if(!session()->has('url.intended')) {
            session(['url.intended' => url()->previous()]);
        }

        return view('user::auth.login');
    }

    public function register()
    {
        return view('user::auth.register');
    }

    protected function authenticated(Request $request, $user)
    {
        if(!session()->has('url.intended')) {
            session(['url.intended' => url()->previous()]);
        }
        
        return redirect('/admin');
    }

    protected function validateLogin(Request $request)
    {
        $this->validate($request, [
            $this->username() => 'required|exists:users,' . $this->username() . ',status,1',
            'password' => 'required',
        ], [
            $this->username() . '.exists' => 'Email không tồn tại hoặc đã bị xóa.',
            $this->username() . '.required' => 'Mời nhập email',
            'password.required' => 'Mời nhập mật khẩu'
        ]);
    }

    protected function sendFailedLoginResponse(Request $request)
    {
        return redirect()->back()
            ->withInput($request->only($this->username(), 'remember'))
            ->withErrors([
                $this->username() => 'Mật khẩu không chính xác',
            ]);
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
