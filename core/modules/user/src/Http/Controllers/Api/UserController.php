<?php

namespace Core\Modules\User\Http\Controllers\Api;

use Auth;
use Input;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;

use Core\Base\Facades\Base;
use Core\Modules\User\Repositories\Contracts\UserRepositoryInterface;

class UserController extends Controller
{
    protected $userRepository;

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->middleware('auth')->except('createCustomer');
        $this->userRepository = $userRepository;
    }

    /**
     * Get users
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Request $request Data input
     * @return Void           
     */
    public function users(Request $request)
    {
        // Check permission
        Base::canOrAbort('index.user', 404);

    	// Get all data input
    	$data = $request->all();

    	// Call function get all users
        $user = $this->userRepository->items($data);

        return new JsonResponse($user);
    }

    /**
     * Set data validate
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Array $data The data input
     * @return Void       
     */
    public function dataValidate($data)
    {
        $validate = [
            'rules' => [
                'full_name' => 'required',
                'email'      => 'required|unique:users,email',
                'password'   => 'required'
            ],
            'messages' => [
                'full_name.required'  => 'Mời nhập tên',
                'email.required'      => 'Mời nhập email',
                'email.unique'        => 'Email đã tồn tại trong hệ thống',
                'password.required'   => 'Mời nhập mật khẩu'
            ]
        ];

        // If update
        if (!empty($data['id'])) {

            // Not required password
            unset($validate['rules']['password']);

            // Find user
            $user = $this->userRepository->edit($data['id']);

            // If email input = email of category edit
            if($user->email == $data['email']){
                $validate['rules']['email'] = 'unique:users,email,NULL,id,email,$user->email';
            }
        }

        return $validate;
    }

    /**
     * Save user
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Request $request Data input
     * @return Void           
     */
    public function store(Request $request)
    {
        // Check permission
        Base::canOrAbort('add.user', 404);

        // All data input
        $data = $request->all();

        // Call function set data validate and validate the data input
        $vali = $this->dataValidate($data);
        $data = $this->userRepository->validate($data, $vali['rules'], $vali['messages']);

        // Validate fail
        if (isset($data['errors'])) {
            return new JsonResponse($data);
        }

        // Call function save user
        $user = $this->userRepository->store($data);

        if ($user) {
            $result = ['status' => 1, 'msg' => 'Thêm thành công'];
        } else {
            $result = ['status' => 0, 'msg' => 'Đã xảy ra lỗi. Mời thử lại'];
        }
        
        return new JsonResponse($result);
    }

    /**
     * Update user
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Request $request Data input
     * @return Void           
     */
    public function update($id, Request $request)
    {
        // Check permission
        Base::canOrAbort('edit.user', 404);

        // All data input
        $data = $request->all();

        // Call function set data validate and validate the data input
        $vali = $this->dataValidate($data);
        $data = $this->userRepository->validate($data, $vali['rules'], $vali['messages']);

        // Validate fail
        if (isset($data['errors'])) {
            return new JsonResponse($data);
        }

        // Call function save user
        $user = $this->userRepository->update($id, $data);

        if ($user) {
            $result = ['status' => 1, 'msg' => 'Cập nhật thành công'];
        } else {
            $result = ['status' => 0, 'msg' => 'Đã xảy ra lỗi. Mời thử lại'];
        }

        return new JsonResponse($result);
    }

    /**
     * Delete multiple users
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Request $request The request data
     * @return Void           
     */
    public function delete(Request $request)
    {
        // Check permission
        Base::canOrAbort('delete.user', 404);

        // All ids
        $ids = $request->all();

        // Call function save user
        $status = $this->userRepository->delete($ids);

        if ($status) {
            $result = ['status' => $status, 'msg' => 'Xóa thành công'];
        } else {
            $result = ['status' => $status, 'msg' => 'Đã xảy ra lỗi. Mời thử lại'];
        }

        // Return blogger 
        return new JsonResponse($result);
    }

    /**
     * Change status for user
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Request $request Request
     * @param  String  $id      Id of user 
     * @return Array            
     */
    public function changeStatus($id) 
    {
        // Check permission
        Base::canOrAbort('edit.user', 404);

        // Call function save user
        $status = $this->userRepository->changeStatus($id);

        if ($status) {
            $result = ['status' => 1, 'msg' => 'Cập nhật thành công'];
        } else {
            $result = ['status' => 0, 'msg' => 'Đã xảy ra lỗi. Mời thử lại'];
        }

        // Return blogger 
        return new JsonResponse($result);
    }

    /**
     * Create new customer
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Request $request The request data
     * @return Void           
     */
    public function createCustomer (Request $request)
    {
        // Get not power role
        $role = $this->roleRepository->getRoleNotPower();

        // Data input
        $data = $request->all();
        $data['type']    = 'customer';
        $data['status']  = 'active';
        $data['role_id'] = $role->id;

        // Call function set data validate and validate the data input
        $vali = $this->dataValidate($data);
        $data = $this->userRepository->validate($data, $vali['rules'], $vali['messages']);

        // Validate fail
        if (isset($data['errors'])) {
            return new JsonResponse($data);
        }

        // Call function save user
        $user = $this->userRepository->store($data);

        if ($user) {
            $result = ['status' => 1, 'msg' => 'Thêm thành công'];
        } else {
            $result = ['status' => 0, 'msg' => 'Đã xảy ra lỗi. Mời thử lại'];
        }

        return new JsonResponse($result);
    }   
    
}
