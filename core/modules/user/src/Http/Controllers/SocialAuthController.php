<?php

namespace Core\Modules\User\Http\Controllers;

use Auth;
use Socialite;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Core\Modules\User\Models\UserModel;

class SocialAuthController extends Controller
{
    use AuthenticatesUsers;

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        return redirect('/');
    }

    /**
     * Redirect the user to the OAuth Provider.
     *
     * @return Response
     */
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    /**
     * Obtain the user information from provider.  Check if the user already exists in our
     * database by looking up their provider_id in the database.
     * If the user exists, log them in. Otherwise, create a new user then log them in. After that 
     * redirect them to the authenticated users homepage.
     *
     * @return Response
     */
    public function handleProviderCallback($provider)
    {
        $user = Socialite::driver($provider)->user();

        $authUser = $this->findOrCreateUser($user, $provider);

        if ($authUser->status != 0) {
            Auth::login($authUser, true);
        }

        return redirect('/');
    }

    /**
     * If a user has registered before using social auth, return the user
     * else, create a new user object.
     * @param  $user Socialite user object
     * @param $provider Social auth provider
     * @return  User
     */
    public function findOrCreateUser($user, $provider)
    {
        $authUser = UserModel::where('provider_id', $user->id)->first();

        if ($authUser) {
            return $authUser;
        }

        return UserModel::create([
            'full_name' => $user->name,
            'email'    => $user->email,
            'password' => bcrypt($user->email),
            'avarar' => '50x50_avatar_default.png?t=1',
            'type' => 'customer',
            'provider' => $provider,
            'provider_id' => $user->id,
            'status' => 0,
        ]);
    }
}