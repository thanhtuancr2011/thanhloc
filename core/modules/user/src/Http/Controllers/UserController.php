<?php

namespace Core\Modules\User\Http\Controllers;

use Auth;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Core\Base\Facades\Base;
use Core\Base\Repositories\Contracts\RoleRepositoryInterface;
use Core\Modules\User\Repositories\Contracts\UserRepositoryInterface;

class UserController extends Controller
{
    protected $userRepository;
    protected $roleRepository;

    public function __construct(UserRepositoryInterface $userRepository, RoleRepositoryInterface $roleRepository)
    {
        $this->middleware('auth')->except('register');
        $this->userRepository = $userRepository;
        $this->roleRepository = $roleRepository;
    }

    public function index()
    {
        // Check permission
        Base::canOrRedirect('index.user');

        // Type user
        $type = (!isset($_GET['type']) || !in_array($_GET['type'], ['admin', 'customer'])) ? 'admin' : $_GET['type'];

        return view('user::index', compact('type'));
    }

    public function create()
    {
        // Check permission
        Base::canOrRedirect('add.user');

        // Type user
        $type = (!isset($_GET['type']) || !in_array($_GET['type'], ['admin', 'customer'])) ? 'admin' : $_GET['type'];

        $item = $this->userRepository->create();

        $roles = $this->roleRepository->all();

        return view('user::create', compact('item', 'type', 'roles'));
    }

    public function edit($id)
    {
        // Check permission
        Base::canOrRedirect('edit.user');

        // Type user
        $type = (!isset($_GET['type']) || !in_array($_GET['type'], ['admin', 'customer'])) ? 'admin' : $_GET['type'];

        $item = $this->userRepository->edit($id);

        $roles = $this->roleRepository->all();

        return view('user::create', compact('item', 'type', 'roles'));
    }

    public function show($id)
    {
        $item = $this->userRepository->show($id);
        return view('user::profile', compact('item'));
    }

    public function changePassword()
    {
        return view('user::password');
    }
}
