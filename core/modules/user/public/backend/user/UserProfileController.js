userApp.controller('UserProfileControler', ['$scope', '$uibModal', 'UserService', '$q', '$http', '$timeout', function($scope, $uibModal, UserService, $q, $http, $timeout) {

    // When js didn't  loaded then hide table term
    $('.container-fluid').removeClass('hidden');
    HoldOn.open(optionsHoldOn);

    /**
     * dnd-dragging determines what data gets serialized and send to the receiver
     * of the drop. While we usually just send a single object, we send the array
     * of all selected items here.
     */
    $scope.getSelectedItemsIncluding = function(list, item) {
        item.selected = true;
        return list.items.filter(function(item) { return item.selected; });
    };

    /**
     * We set the list into dragging state, meaning the items that are being
     * dragged are hidden. We also use the HTML5 API directly to set a custom
     * image, since otherwise only the one item that the user actually dragged
     * would be shown as drag image.
     */
    $scope.onDragstart = function(list, event) {
        list.dragging = true;
        if (event.dataTransfer.setDragImage) {
            var img = new Image();
            img.src = window.baseUrl + '/dizi/admin/images/icon-copy.png';
            event.dataTransfer.setDragImage(img, 0, 0);
        }
    };

    /**
     * In the dnd-drop callback, we now have to handle the data array that we
     * sent above. We handle the insertion into the list ourselves. By returning
     * true, the dnd-list directive won't do the insertion itself.
     */
    $scope.onDrop = function(list, items, index) {
        angular.forEach(items, function(item) { item.selected = false; });
        list.items = list.items.slice(0, index)
                               .concat(items)
                               .concat(list.items.slice(index));
        return true;
    }

    /**
     * Last but not least, we have to remove the previously dragged items in the
     * dnd-moved callback.
     */
    $scope.onMoved = function(type, list) {

        // Contain list items selected
        $scope.itemSelected = [];
        list.items = list.items.filter(function(item) { 
            if (item.selected) {
                $scope.itemSelected.push(item);
            }
            return !item.selected; 
        });

        // Call function change roles for user
        $timeout(function() {

            if (type == 'role') {
                // If user not change roles
                if ($scope.listRolesTmp[0].items.length == $scope.listRoles[0].items.length &&
                    $scope.listRolesTmp[1].items.length == $scope.listRoles[1].items.length) {
                    return;
                }
                // Contain list roles
                $scope.listRolesTmp = angular.copy($scope.listRoles);
            } else {
                // If user not change roles
                if ($scope.listPermissionsTmp[0].items.length == $scope.listPermissions[0].items.length &&
                    $scope.listPermissionsTmp[1].items.length == $scope.listPermissions[1].items.length) {
                    return;
                }
                // Contain list permissions
                $scope.listPermissionsTmp = angular.copy($scope.listPermissions);
            }


            // Loading
            HoldOn.open(optionsHoldOn);

            // Data input
            $scope.dataInput = {};
            $scope.dataInput.id    = $scope.userProfile.id;
            $scope.dataInput.type  = list.type;
            $scope.dataInput.items = $scope.itemSelected;
            $scope.dataInput.opt   = type;

            UserService.changeRoles($scope.dataInput).then(function(data) {
                // Close loading
                HoldOn.close();
            });
        })
    };

    $timeout(function() {

        // Close loading
        HoldOn.close();

        // Contain list roles
        $scope.listRolesTmp = angular.copy($scope.listRoles);

        // Contain list permissions
        $scope.listPermissionsTmp = angular.copy($scope.listPermissions);
    });

    /**
     * Get modal change password
     * @author Thanh Tuan <tuannt@acro.vn>
     * @param  {Integet} userId The user id
     * @return {Void}        
     */
    $scope.getModalChangePassword = function(userId) {
        var modalInstance = $uibModal.open({
            templateUrl: window.baseUrl + '/admin/user/change-password/',
            controller: 'ModalChangePassword',
            size: null,
            resolve: {
                userId: function() {
                    return userId;
                }
            },
        });
        modalInstance.result.then(function() {
            window.location.href = '/auth/logout';
        }, function() {
        });
    };

    /**
     * When user click in button camera
     * @author Thanh Tuan <tuannt@acro.vn>
     * @param  {Object} files File upload
     * @return {Void}  
     */
    $scope.upload = function(files, type) {
        if (files.length != 0) {
            $scope.files = files;
            $scope.getModalCropAvatar();
        }
    }

    /**
     * Function show modal crop avatar
     * @author Thanh Tuan <tuannt@acro.vn>
     * @param  {String} size The modal size
     * @return {Void}      
     */
    $scope.getModalCropAvatar = function(size) {
        var modalInstance = $uibModal.open({
            templateUrl: window.baseUrl + '/backend/app/components/user/views/avatar.html?v='+ new Date().getTime(),
            controller: 'ModalChangeAvatar',
            size: size,
            resolve: {
                files: function() {
                    return $scope.files;
                },
                userId: function() {
                    return $scope.userProfile.id;
                }
            },
        });
        modalInstance.result.then(function(avatarUrl) {
            $scope.userProfile.avatar = avatarUrl;
        }, function() {});
    };

    /**
     * Function check the first name
     * @author Thanh Tuan <tuannt@acro.vn>
     * @param  {Object} firstName The first name
     * @return {Void}      
     */
    $scope.checkFirstName = function(firstName) {
        if (firstName == '') {
            return "Mời bạn nhập tên";
        } else {
            $scope.userProfile.first_name = firstName;
            $scope.updateUser();
        }
    };

    /**
     * Function check the last name
     * @author Thanh Tuan <tuannt@acro.vn>
     * @param  {Object} lastName The last name
     * @return {Void}      
     */
    $scope.checkLastName = function(lastName) {
        if (lastName == '') {
          return "Mời bạn nhập họ";
        } else {
            $scope.userProfile.last_name = lastName;
            $scope.updateUser();
        }
    };

    /**
     * Function check the address
     * @author Thanh Tuan <tuannt@acro.vn>
     * @param  {Object} address The address
     * @return {Void}      
     */
    $scope.checkAddress = function(address) {
        if (address == '') {
          return "Mời bạn nhập địa chỉ";
        } else {
            $scope.userProfile.address = address;
            $scope.updateUser();
        }
    };

    /**
     * Function check the phone
     * @author Thanh Tuan <tuannt@acro.vn>
     * @param  {Object} phone The phone number
     * @return {Void}      
     */
    $scope.checkPhoneNumber = function(phone) {

        var rgxPhone = /^([0-9]{10,11})$/i;

        if (phone == '') {
          return "Mời bạn nhập số điện thoại";
        } else if(rgxPhone.test(phone) == false) {
            return "Số điện thoại không đúng định dạng";
        } else {
            $scope.userProfile.phone = phone;
            $scope.updateUser();
        }
    };

    /**
     * Check email description
     * @author Thanh Tuan <tuannt@acro.vn>
     * @param  {String}  email The email address
     * @return {Void}      
     */
    $scope.checkEmail = function(email) {

        var rgxEmail = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/i;

        /* Check email is empty*/ 
        if(email == '') {
            return "Mời bạn nhập email!";
        } else if(rgxEmail.test(email) == false) {
            return "Email không đúng định dạng";
        } else {
            $scope.userProfile.email = email;
            $scope.updateUser();
        }
    }

    /**
     * Update user
     * @author Thanh Tuan <tuannt@acro.vn>
     * @return {Void} 
     */
    $scope.updateUser = function() {

        // Loading
        HoldOn.open(optionsHoldOn);

        UserService.update($scope.userProfile).then(function(data){
            if (data.status) {

                // Close loading
                HoldOn.close();

                if (data.status == -1) {
                    $scope.errors = data.errors;
                } else {
                    Notify(data.msg);
                    $scope.userProfile = data.user;
                }
            }
        });
    }

}])
.controller('ModalChangePassword', ['$scope', '$uibModalInstance', 'UserService', 'userId', '$timeout', function($scope, $uibModalInstance, UserService, userId, $timeout) {

    /**
     * Change password
     * @author Thanh Tuan <tuannt@acro.vn>
     * @return {Void} 
     */
    $scope.submit = function(validate) {

        // Validate
        $scope.submitted = true;
        if (!validate) return;
        
        // Loading
        HoldOn.open(optionsHoldOn);

        $scope.user.userId = userId;
        UserService.changePassword($scope.user).then(function(data) {

            // Close loading
            HoldOn.close();

            Notify(data.msg);

            // If success
            if (data.status == 1) {
                $uibModalInstance.close();
            }

        });
    };

    // When user click cancel then close modal popup
    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };
}])
.controller('ModalChangeAvatar', ['$scope', '$timeout', '$uibModalInstance', 'Upload', 'UserService', 'files', 'userId', function($scope, $timeout, $uibModalInstance, Upload, UserService, files, userId) {

        // Set image crop
        $scope.myImage = '';
        $scope.myCroppedImage = '';
        var reader = new FileReader();

        // If isset file
        if (files.length != 0) {
            reader.readAsDataURL(files[0]);
            reader.onload = function(evt) {
                $scope.$apply(function() {
                    $scope.myImage = evt.target.result;
                    $scope.myCroppedImage = evt.target.result;
                });
            };
        }

        /**
         * Function change avatar
         * @author Thanh Tuan <tuannt@acro.vn>
         * @return {Void} 
         */
        $scope.changeAvatar = function() {
            // Loading
            HoldOn.open(optionsHoldOn);

            // Change avatar
            UserService.changeAvatar(userId, $scope.myCroppedImage).then(function(response) {

                // Close loading
                HoldOn.close();

                Notify(response.msg);

                // If success
                if (response.status == 1) {
                    $uibModalInstance.close(response.item.avatar);
                }
            });
        }

        // When user click cancel then close modal popup
        $scope.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        };
    }
]);