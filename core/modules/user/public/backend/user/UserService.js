var userApp = angular.module('appUser', []);
userApp.factory('UserResource',['$resource', function ($resource){
    return $resource('/api/user/:method/:id', {'method':'@method','id':'@id'}, {
        add: {method: 'post'},
        save:{method: 'post'},
        update:{method: 'put'}
    });
}])
.service('UserService', ['UserResource', '$q', function (UserResource, $q) {
    var that = this;

    /**
     * Get items
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @return {Void} 
     */
    this.getItems = function(data) {
        var defer = $q.defer(); 
        var temp  = new UserResource(data);
        temp.$save({method: 'users'}, function success(data) {
            defer.resolve(data);
        }, function error(reponse) {
            defer.resolve(reponse.data);
        });
        return defer.promise;  
    }

    /**
     * Function create new user
     * @author Thanh Tuan <tuannt@acro.vn>
     * @param  {Array} data The data input
     * @return {Void}      
     */
	this.createUserProvider = function(data){

        // If isset id of user then call function edit user
        if(typeof data['id'] != 'undefined') {
            return that.editUserProvider(data);
        }

		var defer = $q.defer(); 
        var temp  = new UserResource(data);

        temp.$save({}, function success(data) {
            // Resolve result 
            defer.resolve(data);
        }, function error(reponse) {
            // Resolve result 
        	defer.resolve(reponse.data);
        });
        return defer.promise;  
	};

    /**
     * The function edit user
     * @author Thanh Tuan <tuannt@acro.vn>
     * @param  {Array} data The data input 
     * @return {Void}      
     */
    this.editUserProvider = function(data){

        var defer = $q.defer(); 
        var temp  = new UserResource(data);

        // Update user successfull 
        temp.$update({id: data['id']}, function success(data) {
            defer.resolve(data);
        }, function error(reponse) {
            // If create user is error 
            defer.resolve(reponse.data);
        });
        
        return defer.promise;  
    };

    /**
     * Delete the users
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  {Integer} id The user ids
     * @return {Void}    
     */
    this.deleteUsers = function (userIds) {

        var defer = $q.defer(); 
        var temp  = new UserResource(userIds);

        // Delete categories is successfull
        temp.$save({method: 'delete'}, function success(data) {
            defer.resolve(data);
        },
        
        // If delete category is error
        function error(reponse) {
            defer.resolve(reponse.data);
        });
        return defer.promise;  
    }

    /**
     * Change user status
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  {Integer} id The user ids
     * @return {Void}    
     */
    this.changeStatus = function (userId) {

        var defer = $q.defer(); 
        var temp  = new UserResource();

        // Delete categories is successfull
        temp.$save({method: 'change-status', id: userId}, function success(data) {
            defer.resolve(data);
        },
        
        // If delete category is error
        function error(reponse) {
            defer.resolve(reponse.data);
        });
        return defer.promise;  
    }

    /**
     * Change avarta for user
     * @author Tuan <thanhtuancr2011@gmail.com>
     * @param  {Int}    id        User id
     * @param  {Object} avatar    Image
     * @return {Void}       
     */
    this.changeAvatar = function(id, avatar)
    {
        var defer = $q.defer();
        UserResource.save({method:'change-avatar', file: avatar, id: id},
            function success(data) {
                defer.resolve(data);
            },
            function error(reponse) {
               defer.resolve(reponse.data);
            });

        return defer.promise;
    };

    /**
     * Call server to update user
     * @param  {[object]} data user info
     * @return {[promise]}      promise
     */
    this.update = function(data)
    {
        var defer = $q.defer();
        var temp = new UserResource(data);

        temp.$update({'method': 'update-profile', 'id': data['id']},
            function success(data) {
                defer.resolve(data);
            },
            function error(reponse) {
               defer.resolve(reponse.data);
            });

        return defer.promise;
    };

    /**
     * Change password for user
     * @author Tuan <thanhtuancr2011@gmail.com>
     * @param  {Array}  data  The data input
     * @return {Void}       
     */
    this.changePassword = function(data)
    {
        var defer = $q.defer(); 
        var temp  = new UserResource(data);

        // Update password successfull
        temp.$save({method: 'change-password', 'id': data['userId']}, function success(data) {
            defer.resolve(data);
        },
        
        // If create password is error
        function error(reponse) {
            defer.resolve(reponse.data);
        });
        
        return defer.promise; 
    };

    /**
     * Change roles 
     * @author Tuan <thanhtuancr2011@gmail.com>
     * @param  {Array}  data  The data input
     * @return {Void}       
     */
    this.changeRoles = function(data)
    {
        var defer = $q.defer(); 
        var temp  = new UserResource(data);

        // Update password successfull
        temp.$save({method: 'change-role', id: data.id}, function success(data) {
            defer.resolve(data);
        },
        
        // If create password is error
        function error(reponse) {
            defer.resolve(reponse.data);
        });
        
        return defer.promise; 
    };

}]);
