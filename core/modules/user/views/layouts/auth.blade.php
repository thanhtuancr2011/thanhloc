<!DOCTYPE html>
<html lang="{{ config('app.locale') }}" ng-app="myApp" ng-controller="BaseController">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>@yield('title')</title>

        <!-- Styles -->
        @include('base::shared.css')
        {!! Html::script('bower_components/jquery/dist/jquery.min.js')!!}
        {!! Html::script('bower_components/jquery-ui/jquery-ui.min.js')!!}

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Roboto|Roboto+Condensed" rel="stylesheet">
    </head>
    <body class="app flex-row align-items-center">

        @yield('content')

        <!-- Scripts -->
        @include('base::shared.script')
    </body>
</html>