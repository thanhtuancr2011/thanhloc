{{-- @extends('base::layouts.master')

@section('title')
    Trang cá nhân
@endsection

@section('breadcrumb')
    <h1>Trang cá nhân</h1>
    <ol class="breadcrumb">       
        <li><a href="#"><i class="fa fa-dashboard"></i> Bảng điều khiển</a></li>
        <li class="active">Trang cá nhân</li>
    </ol>
@endsection

@section('content')

    <div class="row container-fluid hidden" ng-controller="UserProfileControler">
        <div class="content content-profile" id="module_profile">
            <div class="user-profile-info" ng-init="userProfile={{json_encode($item)}}; listRoles={{json_encode($listRoles)}}; listPermissions={{json_encode($listPermissions)}}">
                <!-- User profile -->
                <div class="col-md-3" style="padding-left: 5px;">
                    <!-- Profile Image -->
                    <div class="box box-primary">
                        <div class="box-body box-profile">
                            <img class="profile-user-img img-responsive img-circle" ng-if="userProfile.avatar" ng-src="/dizi/admin/avatars/@{{userProfile.avatar}}">
                            <img class="profile-user-img img-responsive img-circle" ng-if="!userProfile.avatar" ng-src="/dizi/admin/avatars/160x160_avatar_default.png">
                            <a class="camera" type="submit" 
                               ng-model="file" 
                               accept="image/*" 
                               ngf-select
                               ngf-change="upload($files)">
                                <i class="fa fa-camera"></i>
                            </a>
                            <h3 class="profile-username text-center">@{{userProfile.last_name}} @{{userProfile.first_name}}</h3>
                            <ul class="list-group list-group-unbordered">
                                <li class="list-group-item">
                                    <b>Họ:</b> 
                                    <a editable-text="userProfile.last_name" 
                                        e-ng-model="userProfile.last_name" 
                                        onbeforesave="checkLastName($data)">
                                        @{{userProfile.last_name || 'empty'}}
                                    </a>
                                </li>
                                <li class="list-group-item">
                                    <b>Tên:</b>
                                    <a editable-text="userProfile.first_name"  
                                        e-ng-model="userProfile.first_name"
                                        onbeforesave="checkFirstName($data)">
                                        @{{userProfile.first_name || 'empty'}} 
                                    </a>
                                </li>
                                <li class="list-group-item">
                                    <b>Địa chỉ:</b>
                                    <a editable-text="userProfile.address"  
                                        e-ng-model="userProfile.address"
                                        onbeforesave="checkAddress($data)">
                                        @{{userProfile.address || 'empty'}} 
                                    </a>
                                </li>
                                <li class="list-group-item">
                                    <b>Số điện thoại:</b>
                                    <a editable-text="userProfile.phone"  
                                        e-ng-model="userProfile.phone"
                                        onbeforesave="checkPhoneNumber($data)">
                                        @{{userProfile.phone || 'empty'}} 
                                    </a>
                                </li>
                                <li class="list-group-item">
                                    <b>Email:</b>
                                    <a editable-text="userProfile.email" 
                                       e-name="userProfile.email" 
                                       onbeforesave="checkEmail($data,userProfile.id)" >
                                       @{{userProfile.email || 'empty'}} 
                                    </a>
                                </li>
                            </ul>
                            <button class="btn btn-primary full-for-image" ng-click="getModalChangePassword(userProfile.id)" style="">
                                <span class="glyphicon glyphicon-retweet"></span> 
                                Đổi mật khẩu
                            </button>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- End user profile -->
                <!-- User permission -->
                <div class="col-md-9">
                    <!-- Profile Image -->
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#activity" data-toggle="tab" aria-expanded="true">Lịch sử hoạt động</a></li>
                            {{-- <li class=""><a href="#timeline" data-toggle="tab" aria-expanded="false">Chức năng</a></li> --}}
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="activity">
                                <ul class="timeline timeline-inverse">
                                    <!-- timeline time label -->
                                    <li class="time-label">
                                        <span class="bg-green">
                                        10 Feb. 2014
                                        </span>
                                    </li>
                                    <!-- /.timeline-label -->
                                    <!-- timeline item -->
                                    <li>
                                        <i class="fa fa-plus bg-blue"></i>
                                        <div class="timeline-item">
                                            <span class="time"><i class="fa fa-clock-o"></i> 12:05</span>
                                            <h3 class="timeline-header"><a href="#">Admin</a> thêm tập tin</h3>
                                            <div class="timeline-body">
                                                Thêm tập tin: hinh-anh-moi.png
                                            </div>
                                            <div class="timeline-footer">
                                                <a class="btn btn-primary btn-xs">View</a>
                                            </div>
                                        </div>
                                    </li>
                                    <!-- END timeline item -->
                                    <!-- timeline item -->
                                    <li>
                                        <i class="fa fa-trash-o bg-yellow"></i>
                                        <div class="timeline-item">
                                            <span class="time"><i class="fa fa-clock-o"></i> 5 mins ago</span>
                                            <h3 class="timeline-header"><a href="#">Admin</a> xóa chuyên mục</h3>
                                            <div class="timeline-body">
                                                Xóa chuyên mục: thể thao
                                            </div>
                                        </div>
                                    </li>
                                    <!-- END timeline item -->
                                    <!-- timeline item -->
                                    <li>
                                        <i class="fa fa-pencil-square-o bg-aqua"></i>
                                        <div class="timeline-item">
                                            <span class="time"><i class="fa fa-clock-o"></i> 27 mins ago</span>
                                            <h3 class="timeline-header"><a href="#">Admin</a> chỉnh sửa bài viết</h3>
                                            <div class="timeline-body">
                                                Chỉnh sửa bài viết: giải bóng đá ngoại hạng Anh
                                            </div>
                                        </div>
                                    </li>

                                    <!-- timeline time label -->
                                    <li class="time-label">
                                        <span class="bg-red">
                                        10 Feb. 2014
                                        </span>
                                    </li>
                                    <!-- /.timeline-label -->
                                    <!-- timeline item -->
                                    <li>
                                        <i class="fa fa-plus bg-blue"></i>
                                        <div class="timeline-item">
                                            <span class="time"><i class="fa fa-clock-o"></i> 12:05</span>
                                            <h3 class="timeline-header"><a href="#">Admin</a> thêm tập tin</h3>
                                            <div class="timeline-body">
                                                Thêm tập tin: hinh-anh-moi.png
                                            </div>
                                            <div class="timeline-footer">
                                                <a class="btn btn-primary btn-xs">View</a>
                                            </div>
                                        </div>
                                    </li>
                                    <!-- END timeline item -->
                                    <!-- timeline item -->
                                    <li>
                                        <i class="fa fa-trash-o bg-yellow"></i>
                                        <div class="timeline-item">
                                            <span class="time"><i class="fa fa-clock-o"></i> 5 mins ago</span>
                                            <h3 class="timeline-header"><a href="#">Admin</a> xóa chuyên mục</h3>
                                            <div class="timeline-body">
                                                Xóa chuyên mục: thể thao
                                            </div>
                                        </div>
                                    </li>
                                    <!-- END timeline item -->
                                    <!-- timeline item -->
                                    <li>
                                        <i class="fa fa-pencil-square-o bg-aqua"></i>
                                        <div class="timeline-item">
                                            <span class="time"><i class="fa fa-clock-o"></i> 27 mins ago</span>
                                            <h3 class="timeline-header"><a href="#">Admin</a> chỉnh sửa bài viết</h3>
                                            <div class="timeline-body">
                                                Chỉnh sửa bài viết: giải bóng đá ngoại hạng Anh
                                            </div>
                                        </div>
                                    </li>

                                </ul>
                                {{-- <div ng-repeat="role in listRoles" class="col-md-6"  style="margin-top: 10px;">
                                    <div class="panel panel-info multiSelect">
                                        <div class="panel-heading">
                                            <h3 class="panel-title ng-binding">@{{role.listName}}</h3>
                                        </div>
                                        <ul dnd-list dnd-drop="onDrop(role, item, index)">
                                            <li ng-repeat="item in role.items"
                                                dnd-draggable="getSelectedItemsIncluding(role, item)"
                                                dnd-dragstart="onDragstart(role, event)"
                                                dnd-moved="onMoved('role', role)"
                                                dnd-dragend="role.dragging = false"
                                                dnd-effect-allowed="move"
                                                dnd-selected="item.selected = !item.selected"
                                                ng-class="{'selected': item.selected}"
                                                ng-hide="role.dragging && item.selected">
                                                @{{item.name}}
                                            </li>
                                        </ul>
                                    </div>
                                </div> --}}
                            </div>
                            <!-- /.tab-pane -->
                            <div class="tab-pane" id="timeline">
                                <div ng-repeat="permission in listPermissions" class="col-md-6"  style="margin-top: 10px;">
                                    <div class="panel panel-info multiSelect">
                                        <div class="panel-heading">
                                            <h3 class="panel-title ng-binding">@{{permission.listName}}</h3>
                                        </div>
                                        <ul dnd-list dnd-drop="onDrop(permission, item, index)">
                                            <li ng-repeat="item in permission.items"
                                                dnd-draggable="getSelectedItemsIncluding(permission, item)"
                                                dnd-dragstart="onDragstart(permission, event)"
                                                dnd-moved="onMoved('permission', permission)"
                                                dnd-dragend="permission.dragging = false"
                                                dnd-effect-allowed="move"
                                                dnd-selected="item.selected = !item.selected"
                                                ng-class="{'selected': item.selected}"
                                                ng-hide="permission.dragging && item.selected">
                                                @{{item.name}}
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- /.tab-pane -->
                        </div>
                        <div class="clearfix"></div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- End user permission -->
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
    
@endsection
@section('script')
    {!! Html::script('/backend/app/components/user/UserService.js?v='.getVersionScript())!!}
    {!! Html::script('/backend/app/components/user/UserProfileController.js?v='.getVersionScript())!!}
@endsection






 --}}