<!-- Modal change password for user -->
    <div class="modal-header"> 
        <button type="button" class="close" ng-click="cancel()" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Thay đổi mật khẩu</h4>
    </div>
    <div class="modal-body sizer">
        <form name="formChangePassword" method="PUT" accept-charset="UTF-8">
            <input type="hidden" name="_token" id="_token" value="{!! csrf_token() !!}}" />
            
            <div class="form-group" ng-class="{true: 'has-error'}[submitted && formChangePassword.password.$invalid]">
                <label for="new_password">Mật khẩu mới</label>
                <div>
                    <input type="password" name="password" ng-model="user.password" class="form-control" required="true">
                    <label class="control-label" ng-show="submitted && formChangePassword.password.$error.required" >
                        Bạn chưa nhập mật khẩu mới
                    </label>
                </div>
            </div>

            <div ng-class="{true: 'has-error'}[submitted && (formChangePassword.password_confirmation.$invalid || user.password_confirmation != user.password)]"  class="form-group">
                <label for="confirm_new_password">Xác nhận mật khẩu</label>
                <div>
                    <input type="password" class="form-control" name="password_confirmation" ng-model="user.password_confirmation" required="true">
                    <label class="control-label" ng-show="submitted && formChangePassword.password_confirmation.$error.required" >
                        Mời nhập lại mật khẩu
                    </label>
                    <label class="control-label" ng-show="submitted && (user.password_confirmation != user.password) && !formChangePassword.password_confirmation.$error.required" >
                        Nhập lại mật khẩu chưa đúng
                    </label>
                </div>
            </div>
            <div class="modal-footer" style="padding-bottom: 0px;">
                <div class="form-group center-block"> 
                    <button type="button" class="btn btn-default" ng-click="cancel()"><span class="glyphicon glyphicon-remove"></span> Hủy</button>
                    <button class="btn btn-primary" ng-click="submit(formChangePassword.$valid && (user.password_confirmation == user.password))"><span class="glyphicon glyphicon-retweet"></span> Thay đổi</button>
                </div>
            </div>
        </form>
    </div>
<!-- End modal change password for user -->