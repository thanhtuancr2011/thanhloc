{{-- @extends('base::layouts.auth')

@section('title')
    Đăng ký
@endsection

@section('content')

<style type="text/css">
    .control-label {
        width: 100%;
        display: block;
        float: left;
        color: red;
    }
    .input-group-addon {
        width: 40px;
        display: inline-table;
        float: left;
    }
    .form-control {
        width: calc(100% - 40px)!important;
        float: left;
    }
</style>

<div class="container" ng-controller="CustomerController">
    <div class="row justify-content-center hidden">
        <div class="col-md-6">
            <div class="card mx-4">
                <div class="card-body p-4">
                    <h1>Đăng ký</h1>
                    <form method="POST" action="{{{ URL::to('users') }}}" accept-charset="UTF-8" name="formAddUser">
                        <div class="form-group" ng-class="{true: 'has-error'}[submitted && formAddUser.last_name.$invalid]">
                            <div class="tuan">
                                <span class="input-group-addon"><i class="icon-user"></i></span>
                                <input class="form-control" placeholder="Nhập họ" type="text" name="last_name" ng-model="userItem.last_name" required="true">
                                <label class="control-label" ng-show="submitted && formAddUser.last_name.$error.required">
                                    Bạn chưa nhập họ
                                </label>
                                <div class="clearfix"></div>
                            </div>
                        </div>

                        <div class="form-group" ng-class="{true: 'has-error'}[submitted && formAddUser.first_name.$invalid]">
                            <div class="">
                                <span class="input-group-addon"><i class="icon-user"></i></span>
                                <input class="form-control" placeholder="Nhập tên" type="text" name="first_name" ng-model="userItem.first_name" required="true">
                                <label class="control-label" ng-show="submitted && formAddUser.first_name.$error.required">
                                    Bạn chưa nhập tên
                                </label>
                                <div class="clearfix"></div>
                            </div>
                        </div>

                        <div class="form-group" ng-class="{true: 'has-error'}[submitted && formAddUser.email.$invalid]">
                            <div class="">
                                <span class="input-group-addon"><i class="icon-envelope"></i></span>
                                <input class="form-control" placeholder="Nhập email" type="text"
                                       ng-pattern="/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/" 
                                       name="email" ng-model="userItem.email" required="true">
                                <label class="control-label" ng-show="submitted && formAddUser.email.$error.pattern">
                                    Nhập sai định dạng email
                                </label>
                                <label class="control-label" ng-show="submitted && formAddUser.email.$error.required">
                                    Bạn chưa nhập email
                                </label>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="form-group" ng-class="{true: 'has-error'}[submitted && formAddUser.address.$invalid]">
                            <div class="">
                                <span class="input-group-addon"><i class="icon-direction"></i></span>
                                <input class="form-control" placeholder="Nhập địa chỉ" type="text" 
                                       name="address" ng-model="userItem.address">
                                <div class="clearfix"></div>
                            </div>
                        </div>

                        <div class="form-group" ng-class="{true: 'has-error'}[submitted && formAddUser.phone.$invalid]">
                            <div class="">
                                <span class="input-group-addon"><i class="icon-phone"></i></span>
                                <input class="form-control" placeholder="Nhập số điện thoại" type="text" 
                                       ng-pattern="/^([0-9]{10,11})$/"
                                       name="phone" ng-model="userItem.phone">
                                <label class="control-label" ng-show="submitted && formAddUser.phone.$error.pattern">
                                    Số điện thoại bao gồm 10 hoặc 11 số
                                </label>
                                <div class="clearfix"></div>
                            </div>
                        </div>

                        <div class="form-group" ng-class="{true: 'has-error'}[submitted && formAddUser.password.$invalid]">
                            <div class="">
                                <span class="input-group-addon"><i class="icon-lock"></i></span>
                                <input class="form-control" placeholder="Mật khẩu" type="password" name="password" ng-model="userItem.password" required="true">
                                <label class="control-label" ng-show="submitted && formAddUser.password.$error.required" >
                                    Bạn chưa nhập mật khẩu
                                </label>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        
                        <div class="form-group" ng-class="{true: 'has-error'}[submitted && (formAddUser.rePassword.$invalid || userItem.rePassword != userItem.password)]">
                            <div class="">
                                <span class="input-group-addon"><i class="icon-lock"></i></span>
                                <input class="form-control" placeholder="Nhập lại mật khẩu" type="password" name="rePassword" ng-model="userItem.rePassword" required="true">
                                <label class="control-label" ng-show="submitted && formAddUser.rePassword.$error.required" >
                                    Mời nhập lại mật khẩu
                                </label>
                                <label class="control-label" ng-show="submitted && (userItem.rePassword != userItem.password) && !formAddUser.rePassword.$error.required" >
                                    Nhập lại mật khẩu chưa đúng
                                </label>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="alert alert-error alert-danger" ng-show="errors" ng-repeat="error in errors">
                            @{{error}}
                        </div>
                        <button type="button" class="btn btn-block btn-success" ng-click="createUser(formAddUser.$valid && (userItem.rePassword == userItem.password))">Đăng Ký</button>
                    </form>
                </div>
                <div class="card-footer p-4">
                    <div class="row">
                        <div class="col-6">
                            <a class="btn btn-block btn-facebook" href="{{ URL::to('redirect/facebook') }}">
                            <span>Facebook</span>
                            </a>
                        </div>
                        <div class="col-6">
                            <a class="btn btn-block btn-google-plus" href="{{ URL::to('redirect/google') }}">
                            <span>Google</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
    {!! Html::script('/backend/app/components/customer/CustomerService.js?v='.getVersionScript()) !!}
    {!! Html::script('/backend/app/components/customer/CustomerController.js?v='.getVersionScript()) !!}
@endsection

 --}}