@extends('user::layouts.auth')

@section('title')
    Đăng nhập
@endsection

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-4">
            <div class="card-group mb-0">
                <div class="card p-4">
                    <div style="height: 60px;text-align: center"><img style="width: auto;height: 100%" src="{{url('admin/images/logo.png')}}" alt=""></div>
                    <div class="card-body">
                        <h1 style="font-size: 24px">Đăng nhập</h1>
                        <p class="text-muted">Đăng nhập vào tài khoản</p>
                        <form action="{{ asset('auth/login') }}" method="post" id="loginForm">
                            {{ csrf_field() }}
                            <div class="input-group mb-3">
                                <span class="input-group-addon"><i class="icon-user"></i></span>
                                <input style="padding-left: 10px" type="text" name="email" placeholder="Email" value="{{ old('email') }}">
                            </div>
                            <div class="input-group mb-4">
                                <span class="input-group-addon"><i class="icon-lock"></i></span>
                                <input style="padding-left: 10px" type="password" name="password" placeholder="Mật khẩu" >
                            </div>
                            @if (count($errors))
                                <ul class="alert alert-warning">
                                    @foreach($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            @endif
                            <div class="input-group mb-4">
                                <input type="checkbox" class="css-checkbox ckb-all" id="checkbox-remember" name="remember">
                                <label for="checkbox-remember" class="css-label lite-blue-check">
                                    <small class="txt-14 no-padding">Ghi nhớ đăng nhập</small>
                                </label>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="header-social wthree">
                                        <input type="submit" class="btn btn-primary" value="Đăng nhập">
                                    </div>
                                </div>
                                <div class="col-6 text-right">
                                    <button type="button" class="btn btn-link px-0">Quên mật khẩu</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

