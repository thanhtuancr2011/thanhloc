<div class="modal-header">
    @if(!empty($item->id))
        <h4 class="modal-title">Chỉnh sửa</h4>
    @else
        <h4 class="modal-title">Thêm</h4>
    @endif
    <button aria-label="Close" data-dismiss="modal" class="close" type="button" ng-click="cancel()"><span aria-hidden="true">×</span></button>
</div>

<div class="modal-body">
    <div class="innerAll">
        <div class="innerLR">
            <form method="POST" action="{{{ URL::to('users') }}}" accept-charset="UTF-8" name="formAddUser" ng-init='userItem={{$item}}; roles={{$roles}}'>     
                <div class="form-group">
                    <input type="text" ng-show="false" name="type" ng-model="userItem.type" ng-init="userItem.type='{{$type}}'">
                    <div class="form-group" ng-class="{true: 'has-error'}[submitted && formAddUser.full_name.$invalid]">
                        <label for="full_name">Họ tên (*)</label>
                        <div class="">
                            <input class="form-control" type="text" placeholder="Họ" name="full_name" ng-model="userItem.full_name" required="true">
                            <label class="control-label" ng-show="submitted && formAddUser.full_name.$error.required">
                                Mời nhập họ tên
                            </label>
                        </div>
                    </div>

                    <div class="form-group" ng-class="{true: 'has-error'}[submitted && formAddUser.email.$invalid]">
                        <label for="email">Email (*)</label>
                        <div class="">
                            <input class="form-control" type="text" placeholder="Email" 
                                   ng-pattern="/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/" 
                                   name="email" ng-model="userItem.email" required="true">
                            <label class="control-label" ng-show="submitted && formAddUser.email.$error.pattern">
                                Không đúng định dạng email
                            </label>
                            <label class="control-label" ng-show="submitted && formAddUser.email.$error.required">
                                Mời nhập email
                            </label>
                        </div>
                    </div>

                    <div class="form-group" ng-class="{true: 'has-error'}[submitted && formAddUser.status.$invalid]">
                        <label for="email">Trạng thái (*)</label>
                        <div class="">
                            <input class="with-font" id="ckb-status-1" name="status" ng-model="userItem.status" type="radio" value="1" required />
                            <label for="ckb-status-1" class="css-label" style="display: inline-block;">Kích hoạt</label>
                            <input class="with-font" id="ckb-status-0" name="status" ng-model="userItem.status" type="radio" value="0" required />
                            <label for="ckb-status-0" class="css-label" style="display: inline-block; margin-left: 20px;">Không kích hoạt</label>
                            <div class="clearfix"></div>
                            <label class="control-label" ng-show="submitted && formAddUser.status.$error.required" >
                                Mời chọn trạng thái
                            </label>
                        </div>
                    </div>

                    @if ($type == 'admin')
                    <div class="form-group" ng-class="{true: 'has-error'}[submitted && formAddUser.role_id.$invalid]">
                        <label for="role_id">Chọn quyền (*)</label>
                        <div class="">
                            <select class="form-control" id="list-role" name="role_id" ng-model="userItem.role_id" required="true">
                                <option value="">Chọn quyền</option>
                                <option ng-repeat="role in roles" ng-value="@{{role.id}}">@{{role.name}}</option>
                            </select>
                            <div class="clearfix"></div>
                            <label class="control-label" ng-show="submitted && formAddUser.role_id.$error.required" >
                                Bạn chưa chọn quyền cho tài khoản
                            </label>
                        </div>
                    </div>
                    @endif

                    <!-- If create user -->
                    @if (!isset($item->id))
                    <div class="form-group" ng-class="{true: 'has-error'}[submitted && formAddUser.password.$invalid]">
                        <label for="inputPassword">Mật khẩu (*)</label>
                        <div class="">
                            <input class="form-control" type="password" placeholder="Mật khẩu" name="password" ng-model="userItem.password" required="true">
                            <label class="control-label" ng-show="submitted && formAddUser.password.$error.required" >
                                Mời nhập mật khẩu
                            </label>
                        </div>
                    </div>
                    
                    <div class="form-group" ng-class="{true: 'has-error'}[submitted && (formAddUser.rePassword.$invalid || userItem.rePassword != userItem.password)]">
                        <label for="inputRePassword">Nhập lại mật khẩu (*)</label>
                        <div class="">
                            <input class="form-control" type="password" placeholder="Nhập lại mật khẩu" name="rePassword" ng-model="userItem.rePassword" required="true">
                            <label class="control-label" ng-show="submitted && formAddUser.rePassword.$error.required" >
                                Xác nhận mật khẩu
                            </label>
                            <label class="control-label" ng-show="submitted && (userItem.rePassword != userItem.password) && !formAddUser.rePassword.$error.required" >
                                Mật khẩu xác nhận chưa chính xác
                            </label>
                        </div>
                    </div>
                    @else

                    <div class="form-group" ng-class="{true: 'has-error'}[submitted && formAddUser.password.$invalid]">
                        <label for="inputPassword">Mật khẩu (*)</label>
                        <div class="">
                            <input class="form-control" type="password" placeholder="Mật khẩu" name="password" ng-model="userItem.password">
                        </div>
                    </div>

                    <div class="form-group" ng-class="{true: 'has-error'}[submitted && (formAddUser.rePassword.$invalid || userItem.rePassword != userItem.password)]">
                        <label for="inputRePassword">Nhập lại mật khẩu (*)</label>
                        <div class="">
                            <input class="form-control" type="password" placeholder="Nhập lại mật khẩu" name="rePassword" ng-model="userItem.rePassword">
                            <label class="control-label" ng-show="submitted && userItem.rePassword != userItem.password" >
                                Mật khẩu xác nhận chưa chính xác
                            </label>
                        </div>
                    </div>
                    @endif
                    <div class="alert alert-error alert-danger" ng-show="errors" ng-repeat="error in errors">
                        @{{error}}
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="modal-footer">
    <button class="btn btn-default" ng-click="cancel()"><i class="fa fa-times"></i> Hủy</button>
    <button class="btn btn-primary" ng-click="submit(formAddUser.$valid && (userItem.rePassword == userItem.password))">
        <span>
            @if(!empty($item->id) || Base::can('edit.user'))
                <i class="fa fa-pencil-square-o"></i> Cập nhật 
            @else
                @if (Base::can('add.user'))
                    <i class="fa fa-plus"></i> Thêm 
                @endif
            @endif
        </span>
    </button>
</div>
