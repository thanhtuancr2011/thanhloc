@extends('base::layouts.master')

@section('title')
    Tài khoản
@endsection

@section('breadcrumb')
    <div class="row">
        <div class="container col-md-12 admin-breadcrumb">
            <div class="breadcrumb">
                <div class="col-md-6 no-padding">
                    <h3>Tài khoản</h3>
                </div>
                <div class="col-md-6 no-padding content-end">
                    <span class="breadcrumb-item">Trang chủ</span>
                    <span class="breadcrumb-item active">Tài khoản</span>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div ng-controller="UserController" ng-init="type='{{$type}}'">
        <div class="card-body">
            <!-- Header title and button -->
            <div class="group-search">
                <form class="frm-search" id="formSearch">
                    <input type="text" class="form-control" ng-model="searchOptions.searchText" placeholder="Tìm kiếm" ng-keypress="searchUsers($event)"/>
                </form>
                <div class="group-btn">
                    @if (Base::can('add.user'))
                    <a href="javascript:void(0)" class="btn btn-primary" ng-click="getModalUser()">
                        <i class="fa fa-plus"></i>
                    </a>
                    @endif

                    @if (Base::can('delete.user'))
                    <a href="javascript:void(0)" class="btn btn-danger" ng-show="showBtn" ng-click="removeUser()">
                        <i class="fa fa-trash-o"></i>
                    </a>
                    @endif
                </div>
            </div>
            <div class="clearfix"></div>

            <!-- User data -->
            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th class="percent-5">#</th>
                    <th class="percent-5">
                        <input type="checkbox" class="css-checkbox ckb-all" id="checkbox-all"
                               ng-click="toggleSelection('all')">
                        <label for="checkbox-all" class="css-label lite-blue-check"></label>
                    </th>
                    <th>Họ tên</th>
                    <th>Email</th>
                    <th>Trạng thái</th>
                    <th>Ngày tạo</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <tr ng-repeat="user in users track by $index" ng-class="{'bg-tr': $index%2 == 0}">

                    <td class="text-center">
                        @{{ ($index + 1) + (pageOptions.currentPage - 1) * pageOptions.itemsPerPage }}
                    </td>

                    <td class="text-center cls-chk">
                        <input type="checkbox" class="css-checkbox ckb" id="checkbox@{{$index}}" value="@{{user.id}}"
                               ng-click="toggleSelection()">
                        <label for="checkbox@{{$index}}" class="css-label lite-blue-check"></label>
                    </td>

                    <td>@{{user.full_name}}</td>

                    <td>@{{user.email}}</td>

                    <td>
                        <button @if (Base::can('edit.user')) ng-click="changeStatus(user.id)" @endif ng-if="user.status == 1" type="button" class="btn btn-success none-radius">
                            Đã kích hoạt
                        </button>
                        <button @if (Base::can('edit.user')) ng-click="changeStatus(user.id)" @endif ng-if="user.status != 1" type="button" class="btn btn-secondary none-radius">
                            Chưa kích hoạt
                        </button>
                    </td>

                    <td>
                        @{{formatDate (user.created_at) | date : "yyyy/MM/dd"}}
                    </td>

                    <td class="text-center">
                        @if (Base::can('edit.user'))
                        <a href="javascript:void(0)" class="btn btn-primary" ng-click="getModalUser(user.id)" title="Chỉnh sửa">
                           <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                        </a>
                        @endif

                        @if (Base::can('delete.user'))
                        <a href="javascript:void(0)" class="btn btn-danger" ng-click="removeUser(user.id)" title="Xóa">
                           <i class="fa fa-trash-o" aria-hidden="true"></i>
                        </a>
                        @endif
                    </td>
                </tr>
                </tbody>
            </table>
            <pagination-directive></pagination-directive>
        </div>
    </div>

@endsection

@section('script')
    {!! Html::script('backend/app/components/user/UserService.js?v='.getVersionScript()) !!}
    {!! Html::script('backend/app/components/user/UserController.js?v='.getVersionScript()) !!}
    {!! Html::script('backend/app/shared/pagination/PaginationDirective.js?v='.getVersionScript()) !!}
@endsection
