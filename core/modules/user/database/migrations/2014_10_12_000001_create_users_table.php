<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('full_name', 255);
            $table->string('name_of_drugstore', 255)->nullable();
            $table->string('email', 60); 
            $table->string('phone', 15)->nullable(); 
            $table->string('address', 255)->nullable(); 
            $table->string('password');
            $table->string('avatar')->nullable();
            $table->string('type')->nullable();
            $table->string('provider', 60)->nullable();
            $table->string('provider_id', 60)->nullable();
            $table->string('status', 30)->default(0);
            $table->integer('role_id')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
