<?php

Route::group(['middleware' => ['web']], function () {

    // Social login
    Route::get('/redirect/{social}', 'Core\Modules\User\Http\Controllers\SocialAuthController@redirectToProvider');
    Route::get('/callback/{social}', 'Core\Modules\User\Http\Controllers\SocialAuthController@handleProviderCallback');

    // Logout route
    Route::get('auth/logout', 'Core\Modules\User\Http\Controllers\Auth\AuthController@logout');
});

Route::group(['middleware' => ['web', 'status']], function () {

    // Authentication routes...
    Route::group(['prefix' => 'auth'], function () {

        // Login routes
        Route::get('login', ['as' => 'login', 'uses' => 'Core\Modules\User\Http\Controllers\Auth\AuthController@show']);
        Route::post('login', 'Core\Modules\User\Http\Controllers\Auth\AuthController@login');

        // Register account
        Route::get('register', 'Core\Modules\User\Http\Controllers\Auth\AuthController@register');
    });

    /* Admin route */
    Route::group(['prefix' => 'admin'], function () {

        // User route
        Route::get('user/change-password', 'Core\Modules\User\Http\Controllers\UserController@changePassword');
        Route::resource('user', 'Core\Modules\User\Http\Controllers\UserController');
    });

    /* Api route */
    Route::group(['prefix' => 'api'], function () {

        // User route
        Route::post('user/users', 'Core\Modules\User\Http\Controllers\Api\UserController@users');
        Route::post('user/delete', 'Core\Modules\User\Http\Controllers\Api\UserController@delete');
        Route::post('user/create-customer', 'Core\Modules\User\Http\Controllers\Api\UserController@createCustomer');
        Route::post('user/change-status/{id}', 'Core\Modules\User\Http\Controllers\Api\UserController@changeStatus');
        Route::post('user/change-avatar/{id}', 'Core\Modules\User\Http\Controllers\Api\UserController@changeAvatar');
        Route::post('user/change-role/{id}', 'Core\Modules\User\Http\Controllers\Api\UserController@changeRoles');
        Route::post('user/change-password/{id}', 'Core\Modules\User\Http\Controllers\Api\UserController@changePassword');
        Route::put('user/update-profile/{id}', 'Core\Modules\User\Http\Controllers\Api\UserController@updateProfile');
        Route::resource('user', 'Core\Modules\User\Http\Controllers\Api\UserController');
    });

});

