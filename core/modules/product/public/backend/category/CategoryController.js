categoryApp.controller('CategoryController', ['$scope', '$uibModal', '$filter', 'CategoryService', '$timeout', '$rootScope', function ($scope, $uibModal, $filter, CategoryService, $timeout, $rootScope) {

	// When js didn't  loaded then hide table category
	$('.container-fluid').removeClass('hidden');
	HoldOn.open(optionsHoldOn);
	
	/**
	 * Get list items
	 * @author Thanh Tuan <thanhtuancr2011@gmail.com>
	 * @return {Void} 
	 */
	$scope.getItems = function() {

		// Reset all checkbox
		$scope.toggleSelection('none');

		CategoryService.getItems({
			pageOptions: $scope.pageOptions,
			searchOptions: $scope.searchOptions,
		}).then(function (data) {
			if (data.status == 1) {

				// Set categorys value
				$scope.categorys = angular.copy(data.categorys);
				$scope.pageOptions.totalPages = data.totalPages;
				$scope.pageOptions.totalItems = data.totalItems;
				$rootScope.$broadcast('pageOptions', $scope.pageOptions);

				// Show loading
				HoldOn.close();
			}
		});
	}

	$timeout(function() {
		// Set type search
		$scope.searchOptions.taxonomy = $scope.taxonomy;
		// Call function get items
		$scope.getItems();
	});

	/**
     * Search categorys
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @return {Void}                 */
    $scope.searchCategorys = function(keyEvent) {

    	if (keyEvent == 'submit' || keyEvent.which === 13) {
    		HoldOn.open(optionsHoldOn);
	    	$scope.getItems();
    	}
    } 
	
	$scope.getModalCategory = function(id) {
		
		// When create
		var template = '/admin/category/create?taxonomy=' + $scope.taxonomy + '&v='+ new Date().getTime();

		// When update
		if(typeof id != 'undefined'){
			template = '/admin/category/'+ id + '/edit?taxonomy=' + $scope.taxonomy + '&v=' + new Date().getTime();
		}

		var modalInstance = $uibModal.open({
		    animation: true,
		    templateUrl: window.baseUrl + template,
		    controller: 'ModalCreateCategoryCtrl',
		    size: null,
		    resolve: {
		    }
		});

		/* After create or edit category then reset category and reload ng-table */
		modalInstance.result.then(function (data) {
			$scope.getItems();
		}, function () {

		   });
	};

	/**
	 * Delete the category
	 * @author Thanh Tuan <thanhtuancr2011@gmail.com>
	 * @param  {Integer} id   The category id
	 * @param  {String}  size Type of modal
	 * @return {Void}     
	 */
	$scope.removeCategory = function(id){
		
		if (angular.isDefined(id)) {
			var categoryIds = [id];
		} else {
			var categoryIds = $scope.listIdsSelected;
		}
		
		var template = '/backend/app/components/category/views/delete.html?v=' + new Date().getTime();
		var modalInstance = $uibModal.open({
		    animation: $scope.animationsEnabled,
		    templateUrl: window.baseUrl + template,
		    controller: 'ModalDeleteCategoryCtrl',
		    size: 'sm',
		    resolve: {
		    	categoryIds: function(){
		            return categoryIds;
		        }
		    }
		});

		modalInstance.result.then(function (data) {
			$scope.getItems();
		}, function () {});
	};

	/**
     * Onload when controller call to set page options
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  {Object} event The event 
     * @param  {Object} data) The page options
     * @return {Void}       
     */
    $scope.$on('setPageOptions',function(event, data) {
        HoldOn.open(optionsHoldOn);
        $scope.pageOptions = data;
        $scope.getItems();
    });

}])
.controller('ModalCreateCategoryCtrl', ['$scope', '$uibModalInstance', 'CategoryService', '$timeout', function ($scope, $uibModalInstance, CategoryService, $timeout) {

	$scope.$watch('categoryItem.name', function(newVal, oldVal) {
		if (angular.isDefined(newVal) && !angular.isDefined($scope.categoryItem.id)) {
			$scope.categoryItem.slug = slugValue(newVal, '-');
		}
	}, true);

	$timeout(function() {
		if (angular.isDefined($scope.categoryItem.id)) {
			$("#ckb-status-" + $scope.categoryItem.parent_id).prop("checked", true);
		}
	});

	// When category click add or edit category
	$scope.submit = function (validate) {

		// Validate
		$scope.submitted = true;
		if (!validate) return;

		// Set category slug
		if (!angular.isDefined($scope.categoryItem.slug) || $scope.categoryItem.slug == '') {	
			$scope.categoryItem.slug = slugValue($scope.categoryItem.name, '-');
		} else {
			$scope.categoryItem.slug = slugValue($scope.categoryItem.slug, '-');
		}
		
		// Loading
		HoldOn.open(optionsHoldOn);
		
		CategoryService.createCategoryProvider($scope.categoryItem).then(function (data) {

			// Close loading
			HoldOn.close();

			if (data.status == -1) {
				$scope.errors = data.errors;
			} else {
				$uibModalInstance.close();
				Notify(data.msg);
			}
		})
	};

	/* When category click cancel then close modal popup */
	$scope.cancel = function () {
	    $uibModalInstance.dismiss('cancel');
	};
}])
.controller('ModalDeleteCategoryCtrl', ['$scope', '$uibModalInstance', 'categoryIds', 'CategoryService', function ($scope, $uibModalInstance, categoryIds, CategoryService) {
	
	// When category click update the post for category
	$scope.submit = function () {

		// Loading
		HoldOn.open(optionsHoldOn);

		CategoryService.deleteCategorys(categoryIds).then(function (data) {
			
			// Close loading
			HoldOn.close();

			$uibModalInstance.close();
			Notify(data.msg);
		});
	};

	// When category click cancel then close modal popup
	$scope.cancel = function () {
	    $uibModalInstance.dismiss('cancel');
	};
}]);