postApp.controller('PostController', ['$scope', '$uibModal', '$filter', 'PostService', '$timeout', '$rootScope', function ($scope, $uibModal, $filter, PostService, $timeout, $rootScope) {

	// When js didn't  loaded then hide table post
	$('.container-fluid').removeClass('hidden');
	HoldOn.open(optionsHoldOn);

	/**
	 * Get list items
	 * @author Thanh Tuan <thanhtuancr2011@gmail.com>
	 * @return {Void} 
	 */
	$scope.getItems = function() {

		// Reset all checkbox
		$scope.toggleSelection('none');
		
		PostService.getItems({
			pageOptions: $scope.pageOptions,
			searchOptions: $scope.searchOptions,
		}).then(function (data) {
			if (data.status == 1) {

				// Set posts value
				$scope.posts = angular.copy(data.posts);
				$scope.authors = angular.copy(data.authors);
				$scope.lstStatus = angular.copy(data.lstStatus);
				$scope.pageOptions.totalPages = data.totalPages;
				$scope.pageOptions.totalItems = data.totalItems;
				$rootScope.$broadcast('pageOptions', $scope.pageOptions);

				// Show loading
				HoldOn.close();
			}
		});
	}

	$timeout(function() {
		$scope.searchOptions.type = $scope.type;
		$scope.getItems();
	});

	/**
     * Onload when controller call to set page options
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  {Object} event The event 
     * @param  {Object} data) The page options
     * @return {Void}       
     */
    $scope.$on('setPageOptions',function(event, data) {
        HoldOn.open(optionsHoldOn);
        $scope.pageOptions = data;
        $scope.getItems();
    });

	/**
     * Search posts
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @return {Void}                 
	 */
    $scope.searchPosts = function(keyEvent) {
    	if (keyEvent == 'submit' || keyEvent == 'ng-change' || keyEvent.which === 13) {
    		HoldOn.open(optionsHoldOn);
	    	$scope.getItems();
    	}
    } 

	/**
	 * Delete the post
	 * @author Thanh Tuan <thanhtuancr2011@gmail.com>
	 * @param  {Integer} id   The post id
	 * @return {Void}     
	 */
	$scope.removePost = function(id){

		if (angular.isDefined(id)) {
			var postIds = [id];
		} else {
			var postIds = $scope.listIdsSelected;
		}
		
		var template = '/backend/app/components/post/views/delete.html?v=' + new Date().getTime();
		var modalInstance = $uibModal.open({
		    animation: true,
		    templateUrl: window.baseUrl + template,
		    controller: 'ModalDeletePostCtrl',
		    size: 'sm',
		    resolve: {
		    	postIds: function(){
		            return postIds;
		        }
		    }
		});

		modalInstance.result.then(function (data) {
			$scope.getItems();
		}, function () {});
	};

}]).controller('ModalDeletePostCtrl', ['$scope', '$uibModalInstance', 'postIds', 'PostService', function ($scope, $uibModalInstance, postIds, PostService) {
	
	// When post click update the post for post
	$scope.submit = function () {

		// Loading
		HoldOn.open(optionsHoldOn);

		PostService.deletePosts(postIds).then(function (data) {
			
			// Close loading
			HoldOn.close();

			$uibModalInstance.close();
			Notify(data.msg);
		});
	};

	// When post click cancel then close modal popup
	$scope.cancel = function () {
	    $uibModalInstance.dismiss('cancel');
	};
}]).controller('CreatePostController', ['$scope', 'PostService', '$uibModal', '$http', '$timeout', 'SettingService', '$filter', function ($scope, PostService, $uibModal, $http, $timeout, SettingService, $filter) {
	
	// When js didn't  loaded then hide table post
	$('.container-fluid').removeClass('hidden');

	// Get domain name
	$scope.domainName = $('<a id="domain-name">').prop('href', window.baseUrl).prop('hostname');

	// List keywords
	$('.key-words').on("select2:select", function(e) { 
		var value = $(e.currentTarget).val();
		$timeout(function() {
			if ($scope.listTags.indexOf(value[value.length - 1]) == -1) {
				$scope.listTags.push(value[value.length -1]);
			}
			$('.key-words').val(null).trigger('change');
		})
	});

	/**
     * Select 2 with ajax
     * @return {Void} 
     */
    $scope.selectAjax = function() {
		$('.key-words').select2({
	   		placeholder: $scope.tagSearch,
	   		width: '100%',
	   		// allowClear: true,
	   		tags: true,
	   		tokenSeparators: [','],
	   		ajax: {
	   			type: 'POST',
	   			url: window.baseUrl + '/api/term/tags',
	   			dataType: 'json',
	   			data: function(json) {
	   				return {
	   					json: JSON.stringify(json),
	   					delay: 0.5
	   				};
	   			},
	   			processResults: function(data) {
	   				return {
	   					results: $.map(data.tags, function(value, key) {
	   						return {
	   							id: value.name,
	   							text: value.name
	   						};
	   					})
	   				};
	   			}
	   		},
	   		language: {
	   			errorLoading: function() {
	   				return $scope.searching + " <img style='width: 50px; height: 50px;' src='/backend/images/loading.gif' />";
	   			},
	   			noResults: function() {
	   				return $scope.noResult;
	   			},
	   			inputTooShort: function(args) {
	   				var remainingChars = args.minimum - args.input.length;
	   				var message = $scope.tagSearchCharacter;
	   				return message;
	   			}
	   		},
	   		escapeMarkup: function(markup) {
	   			return markup;
	   		},
	   		minimumInputLength: 3
	   	});
	}

	// setup variable for date picker
	$scope.format = 'dd-MM-yyyy';

	// Date picker popup
	$scope.dateOptions = {
	    formatYear: 'yy',
	    // minDate: new Date(),
	    startingDay: 1
  	};

	/* Open calendar when create page*/
	$scope.open = function($event, name) {
    	$event.preventDefault();
    	$event.stopPropagation();
    	$scope.opened = {};
    	$scope.opened[name] = true;
  	};
	
	/**
	 * Calculating all score for SEO
	 * @param  {Object} allScores All score
	 * @return {Void}           
	 */
	$scope.calcScore = function(allScores) {
		$scope.currentScore = 26;
		angular.forEach(allScores, function(value, key) {
			$scope.currentScore += value;
		});
	}

	// Set category checkbox when edit
	$timeout(function() {

		// Select 2 ajax
		$scope.selectAjax();

		// Contain list tags
		$scope.listTags = [];

		// When edit
		if (angular.isDefined($scope.postItem.id)) {
			$.map($scope.postItem.categories, function(category) {
				$('#checkbox' + category.id).prop('checked', true);
			});

			$('#no-index').prop('checked', $scope.postItem.no_index);
			$('#no-follow').prop('checked', $scope.postItem.no_follow);

			$.map($scope.postItem.tags, function(tag) {
				$scope.listTags.push(tag.name);
			});
		}

		// Published date
		if (!angular.isDefined($scope.postItem.published_at) || $scope.postItem.published_at == null) {
            if (angular.isDefined($scope.postItem.created_at)) {
                $scope.postItem.published_at = angular.copy($scope.postItem.created_at);
            } else {
                $scope.postItem.published_at = new Date();
            }
        }
        $scope.postItem.published_at = new Date($scope.postItem.published_at);

		// TinyMCE options
		$scope.tinymceOptions = {
			height: 500,
			selector: 'textarea#tinymceEditor',
			entity_encoding : "raw",
			relative_urls: false,
			theme: 'modern',
			relative_urls : false,
			remove_script_host : false,
			plugins : "link image code table",
			theme_advanced_buttons3_add : "tablecontrols",
			table_styles : "Header 1=header1;Header 2=header2;Header 3=header3",
			table_cell_styles : "Header 1=header1;Header 2=header2;Header 3=header3;Table Cell=tableCel1",
			table_row_styles : "Header 1=header1;Header 2=header2;Header 3=header3;Table Row=tableRow1",
			table_cell_limit : 100,
			table_row_limit : 5,
			table_col_limit : 5,
			toolbar: 'styleselect | table | undo redo | bold italic | image imagetools | alignleft aligncenter alignright | link unlink | fontselect fontsizeselect | code',
			init_instance_callback: function (editor) {
		    	editor.on('click', function (e) {
		    		$scope.postItem.content = tinyMCE.activeEditor.getContent();
					$scope.$apply();
		    	});
		    	editor.on('keyup', function(e) {
					$scope.postItem.content = tinyMCE.activeEditor.getContent();
					$scope.$apply();
		        });
			    editor.on('SetContent', function (e) {
			      	$scope.postItem.content = tinyMCE.activeEditor.getContent();
					$scope.$apply();
			    });
	  		}
		}

		var firstLoadJS = true;
		var editor = tinyMCE.init($scope.tinymceOptions);

		// Set variable for check seo score
		$scope.currentScore = 26;
		$scope.totalScore = 100;

		$scope.allScores = {};

		// When content change
		$scope.$watch('postItem.content', function(newVal, oldVal) {
			// console.log('postItem.content');
			$scope.percentAppear = 0;
			$scope.keyWordInAltImg = false;
			$scope.keyWordInContent = false;
			$scope.hasBUITagInContent = false;
			$scope.appearKeyWordInContent = false;
			$scope.keyWordInLast150Chracters = false;
			$scope.keyWordInFirst150Chracters = false;
			$scope.existsInternalLinkInContent = false;
			$scope.notExistsInternalLinkInContent = false;

			$scope.allScores.scoreKeyWordInAltImg = 0;
			$scope.allScores.scoreKeyWordInContent = 0;
			$scope.allScores.scoreHasBUITagInContent = 0;
			$scope.allScores.scoreAppearKeyWordInContent = 0;
			$scope.allScores.scoreKeyWordInLast150Chracters = 0;
			$scope.allScores.scoreKeyWordInFirst150Chracters = 0;
			$scope.allScores.scoreNotExistsInternalLinkInContent = 0;
			if (angular.isDefined(newVal) && newVal != '' && newVal != null) {

				// If the key word in alt tag
				var divTmp = document.createElement('div');
				divTmp.innerHTML = newVal;

				$scope.postItem.in_link = 0;
				$scope.postItem.out_link = 0;

				// Has exists internal link and external link
				angular.forEach(divTmp.getElementsByTagName('a'), function(value1, key1) {
					if (value1.href.indexOf($scope.domainName) != -1) {
						$scope.existsInternalLinkInContent = true;
						$scope.allScores.scoreExistsInternalLinkInContent = 5;
						$scope.postItem.in_link += 1; 
					} else {
						$scope.notExistsInternalLinkInContent = true;
						$scope.allScores.scoreNotExistsInternalLinkInContent = 5;
						$scope.postItem.out_link += 1; 
					}
				});

				// Check primary keyword in content
				if (angular.isDefined($scope.postItem.primary_keyword) && $scope.postItem.primary_keyword != null) {

					// Isset in the content
					if (newVal.toLowerCase().replace(/(<([^>]+)>)/ig,"").indexOf($scope.postItem.primary_keyword.toLowerCase()) != -1) {
						$scope.keyWordInContent = true;
						$scope.allScores.scoreKeyWordInContent = 5;
					}
					// Isset in the first 150 characters of content
					if (newVal.toLowerCase().replace(/(<([^>]+)>)/ig,"").substring(0, 150).indexOf($scope.postItem.primary_keyword.toLowerCase()) != -1) {
						$scope.keyWordInFirst150Chracters = true;
						$scope.allScores.scoreKeyWordInFirst150Chracters = 2;
					}

					// Isset in the last 150 characters of content
					if (newVal.toLowerCase().replace(/(<([^>]+)>)/ig,"").length > 150 && 
						newVal.toLowerCase().replace(/(<([^>]+)>)/ig,"").substring(newVal.toLowerCase().replace(/(<([^>]+)>)/ig,"").length - 150).indexOf($scope.postItem.primary_keyword.toLowerCase()) != -1) {
						$scope.keyWordInLast150Chracters = true;
						$scope.allScores.scoreKeyWordInLast150Chracters = 2;
					}

					// The primary keywords exists in content with percent appear is 2 - 5%
					if ((newVal.replace(/(<([^>]+)>)/ig,"").toLowerCase().match(new RegExp($scope.postItem.primary_keyword.toLowerCase(), 'g')) || []).length > 0) {
						var numberAppear  = ((newVal.replace(/(<([^>]+)>)/ig,"").toLowerCase().match(new RegExp($scope.postItem.primary_keyword.toLowerCase(), 'g')) || []).length);
						var numberTotal   = newVal.replace(/(<([^>]+)>)/ig,"").toLowerCase().length;
						var keyWordLength = angular.copy($scope.postItem.primary_keyword).length;
						$scope.percentAppear = parseFloat(numberAppear*keyWordLength/numberTotal*100).toFixed(2);
						if ($scope.percentAppear >= 2 && $scope.percentAppear <= 5) {
							$scope.appearKeyWordInContent = true;
							$scope.allScores.scoreAppearKeyWordInContent = 5;
						}
					}

					// Has exists key word in img alt
					angular.forEach(divTmp.getElementsByTagName('img'), function(value, key) {
						if (value.alt.toLowerCase().indexOf($scope.postItem.primary_keyword.toLowerCase()) != -1) {
							$scope.keyWordInAltImg = true;
							$scope.allScores.scoreKeyWordInAltImg = 5;
						}
					});

					// Has exists keywords is bold
					angular.forEach(divTmp.getElementsByTagName('strong'), function(value2, key2) {
						if (value2.innerHTML.toLowerCase().indexOf($scope.postItem.primary_keyword.toLowerCase()) != -1) {
							$scope.hasBUITagInContent = true;
							$scope.allScores.scoreHasBUITagInContent = 5;
						}
					});

					// Has exists keywords is bold
					angular.forEach(divTmp.getElementsByTagName('b'), function(value3, key3) {
						if (value3.innerHTML.toLowerCase().indexOf($scope.postItem.primary_keyword.toLowerCase()) != -1) {
							$scope.hasBUITagInContent = true;
							$scope.allScores.scoreHasBUITagInContent = 5;
						}
					});

					// Has exists keywords is italic
					angular.forEach(divTmp.getElementsByTagName('em'), function(value4, key4) {
						if (value4.innerHTML.toLowerCase().indexOf($scope.postItem.primary_keyword.toLowerCase()) != -1) {
							$scope.hasBUITagInContent = true;
							$scope.allScores.scoreHasBUITagInContent = 5;
						}
					});

					// Has exists keywords is italic
					angular.forEach(divTmp.getElementsByTagName('i'), function(value5, key5) {
						if (value5.innerHTML.toLowerCase().indexOf($scope.postItem.primary_keyword.toLowerCase()) != -1) {
							$scope.hasBUITagInContent = true;
							$scope.allScores.scoreHasBUITagInContent = 5;
						}
					});

					// Has exists keywords is underline
					angular.forEach(divTmp.getElementsByTagName('u'), function(value6, key6) {
						if (value6.innerHTML.toLowerCase().indexOf($scope.postItem.primary_keyword.toLowerCase()) != -1) {
							$scope.hasBUITagInContent = true;
							$scope.allScores.scoreHasBUITagInContent = 5;
						}
					});
				}
			}

			$timeout(function() {
				$scope.calcScore($scope.allScores);
			})
		}, true);

		// When primary keyword change
		$scope.$watch('postItem.primary_keyword', function(newVal, oldVal) {
			// console.log('postItem.primary_keyword');
			
			$scope.percentAppear = 0;
			$scope.keyWordInUrl = false;
			$scope.keyWordInTitle = false;
			$scope.keyWordInAltImg = false;
			$scope.keyWordInContent = false;
			$scope.hasBUITagInContent = false;
			$scope.keyWordInDescription = false;
			$scope.appearKeyWordInContent = false;
			$scope.keyWordInLast150Chracters = false;
			$scope.keyWordInFirst150Chracters = false;

			$scope.allScores.scoreKeyWordInUrl = 0;
			$scope.allScores.scoreKeyWordInTitle = 0;
			$scope.allScores.scoreKeyWordInAltImg = 0;
			$scope.allScores.scoreKeyWordInContent = 0;
			$scope.allScores.scoreHasBUITagInContent = 0;
			$scope.allScores.scoreKeyWordInDescription = 0;
			$scope.allScores.scoreAppearKeyWordInContent = 0;
			$scope.allScores.scoreKeyWordInLast150Chracters = 0;
			$scope.allScores.scoreKeyWordInFirst150Chracters = 0;

			if (angular.isDefined(newVal) && newVal != '' && newVal != null) {

				// Check primary keyword isset in title
				if (angular.isDefined($scope.postItem.seo_title) && $scope.postItem.seo_title != '' && $scope.postItem.seo_title != null) {
					if ($scope.postItem.seo_title.toLowerCase().indexOf(newVal.toLowerCase()) != -1) {
						$scope.keyWordInTitle = true;
						$scope.allScores.scoreKeyWordInTitle = 5;
					}
				}

				// Check primary keyword isset in url
				if (angular.isDefined($scope.postItem.slug) && $scope.postItem.slug != '' && $scope.postItem.slug != null) {
					if (slugValue($scope.postItem.slug, '-').indexOf(slugValue(newVal, '-')) != -1) {
						$scope.keyWordInUrl = true;
						$scope.allScores.scoreKeyWordInUrl = 10;
					}
				}

				// Check primary keyword isset in description
				if (angular.isDefined($scope.postItem.seo_description) && $scope.postItem.seo_description != '' && $scope.postItem.seo_description != null) {
					if ($scope.postItem.seo_description.toLowerCase().indexOf(newVal.toLowerCase()) != -1) {
						$scope.keyWordInDescription = true;
						$scope.allScores.scoreKeyWordInDescription = 5;
					}
				}

				if (!firstLoadJS) {
					// Set post content
					$scope.postItem.content = tinyMCE.activeEditor.getContent();
				}

				// Check primary keyword in content
				if (angular.isDefined($scope.postItem.content) && $scope.postItem.content != '' && $scope.postItem.content != null) {

					// If the key word in alt tag
					var divTmp = document.createElement('div');
					divTmp.innerHTML = $scope.postItem.content;

					// Has exists key word in img alt
					angular.forEach(divTmp.getElementsByTagName('img'), function(value, key) {
						if (value.alt.toLowerCase().indexOf(newVal.toLowerCase()) != -1) {
							$scope.keyWordInAltImg = true;
							$scope.allScores.scoreKeyWordInAltImg = 5;
						}
					});

					// Has exists keywords is bold
					angular.forEach(divTmp.getElementsByTagName('strong'), function(value2, key2) {
						if (value2.innerHTML.toLowerCase().indexOf(newVal.toLowerCase()) != -1) {
							$scope.hasBUITagInContent = true;
							$scope.allScores.scoreHasBUITagInContent = 5;
						}
					});

					// Has exists keywords is bold
					angular.forEach(divTmp.getElementsByTagName('b'), function(value3, key3) {
						if (value3.innerHTML.toLowerCase().indexOf(newVal.toLowerCase()) != -1) {
							$scope.hasBUITagInContent = true;
							$scope.allScores.scoreHasBUITagInContent = 5;
						}
					});

					// Has exists keywords is italic
					angular.forEach(divTmp.getElementsByTagName('em'), function(value4, key4) {
						if (value4.innerHTML.toLowerCase().indexOf(newVal.toLowerCase()) != -1) {
							$scope.hasBUITagInContent = true;
							$scope.allScores.scoreHasBUITagInContent = 5;
						}
					});

					// Has exists keywords is italic
					angular.forEach(divTmp.getElementsByTagName('i'), function(value5, key5) {
						if (value5.innerHTML.toLowerCase().indexOf(newVal.toLowerCase()) != -1) {
							$scope.hasBUITagInContent = true;
							$scope.allScores.scoreHasBUITagInContent = 5;
						}
					});

					// Has exists keywords is underline
					angular.forEach(divTmp.getElementsByTagName('u'), function(value6, key6) {
						if (value6.innerHTML.toLowerCase().indexOf(newVal.toLowerCase()) != -1) {
							$scope.hasBUITagInContent = true;
							$scope.allScores.scoreHasBUITagInContent = 5;
						}
					});

					// Isset in the content
					if ($scope.postItem.content.replace(/(<([^>]+)>)/ig,"").toLowerCase().indexOf(newVal.toLowerCase()) != -1) {
						$scope.keyWordInContent = true;
						$scope.allScores.scoreKeyWordInContent = 5;
					}
					// Isset in the first 150 characters of content
					if ($scope.postItem.content.replace(/(<([^>]+)>)/ig,"").toLowerCase().substring(0, 150).indexOf(newVal.toLowerCase()) != -1) {
						$scope.keyWordInFirst150Chracters = true;
						$scope.allScores.scoreKeyWordInFirst150Chracters = 2;
					}
					// Isset in the last 150 characters of content
					if ($scope.postItem.content.replace(/(<([^>]+)>)/ig,"").length >= 150 && $scope.postItem.content.replace(/(<([^>]+)>)/ig,"").toLowerCase().substring($scope.postItem.content.replace(/(<([^>]+)>)/ig,"").toLowerCase().length - 150).indexOf(newVal.toLowerCase()) != -1) {
						$scope.keyWordInLast150Chracters = true;
						$scope.allScores.scoreKeyWordInLast150Chracters = 2;
					}

					// The primary keywords exists in content with percent appear is 2 - 5%
					if (($scope.postItem.content.replace(/(<([^>]+)>)/ig,"").toLowerCase().match(new RegExp(newVal.toLowerCase(), 'g')) || []).length > 0) {
						var numberAppear = ((angular.copy($scope.postItem.content).replace(/(<([^>]+)>)/ig,"").toLowerCase().match(new RegExp(newVal.toLowerCase(), 'g')) || []).length);
						var numberTotal  = angular.copy($scope.postItem.content).replace(/(<([^>]+)>)/ig,"").toLowerCase().length;
						var keyWordLength = angular.copy(newVal).length;
						$scope.percentAppear = parseFloat(numberAppear*keyWordLength/numberTotal*100).toFixed(2);
						if ($scope.percentAppear >= 2 && $scope.percentAppear <= 5) {
							$scope.appearKeyWordInContent = true;
							$scope.allScores.scoreAppearKeyWordInContent = 5;
						}
					}
				}

				$timeout(function() {
					firstLoadJS = false;
				})
			}
			$timeout(function() {
				$scope.calcScore($scope.allScores);
			});
		}, true);

		// When sub keyword change
		$scope.$watch('postItem.sub_keyword', function(newVal, oldVal) {
			// console.log('postItem.sub_keyword');
			
			$scope.subKeyWordInTitle = false;
			$scope.subKeyWordInDescription = false;
			$scope.allScores.scoreSubKeyWordInTitle = 0;
			$scope.allScores.scoreSubKeyWordInDescription = 0;

			if (angular.isDefined(newVal) && newVal != '' && newVal !== null) {
				// Check primary keyword isset in description
				if (angular.isDefined($scope.postItem.seo_title) && $scope.postItem.seo_title != '' && $scope.postItem.seo_title != null) {
					if ($scope.postItem.seo_title.toLowerCase().indexOf(newVal.toLowerCase()) != -1) {
						$scope.subKeyWordInTitle = true;
						$scope.allScores.scoreSubKeyWordInTitle = 5;
					}
				}
				// Check sub keyword isset in seo description
				if (angular.isDefined($scope.postItem.seo_description) && $scope.postItem.seo_description != '' && $scope.postItem.seo_description != null) {
					if ($scope.postItem.seo_description.toLowerCase().indexOf(newVal.toLowerCase()) != -1) {
						$scope.subKeyWordInDescription = true;
						$scope.allScores.scoreSubKeyWordInDescription = 5;
					}
				}
			}
			$scope.calcScore($scope.allScores);
		}, true);

		// When seo description change
		$scope.$watch('postItem.seo_description', function(newVal, oldVal) {
			// console.log('postItem.seo_description');
			
			$scope.keyWordInDescription = false;
			$scope.subKeyWordInDescription = false;
			$scope.allScores.scoreKeyWordInDescription = 0;
			$scope.allScores.scoreSubKeyWordInDescription = 0;

			if (angular.isDefined(newVal) && newVal != '' && newVal !== null) {
				// Check primary keyword isset in description
				if (angular.isDefined($scope.postItem.primary_keyword) && $scope.postItem.primary_keyword != '' && $scope.postItem.primary_keyword != null) {
					if (newVal.toLowerCase().indexOf($scope.postItem.primary_keyword.toLowerCase()) != -1) {
						$scope.keyWordInDescription = true;
						$scope.allScores.scoreKeyWordInDescription = 5;
					}
				}
				// Check sub keyword isset in seo description
				if (angular.isDefined($scope.postItem.sub_keyword) && $scope.postItem.sub_keyword != '' && $scope.postItem.sub_keyword != null) {
					if (newVal.toLowerCase().indexOf($scope.postItem.sub_keyword.toLowerCase()) != -1) {
						$scope.subKeyWordInDescription = true;
						$scope.allScores.scoreSubKeyWordInDescription = 5;
					}
				}
			}
			$scope.calcScore($scope.allScores);
		}, true);

		// Get uncategory
		angular.forEach($scope.categoriesTree, function(value, key) {
			if (value.slug == 'uncategory')
				$scope.uncategory = value;
		});

		// When post slug change
		$scope.$watch('postItem.slug', function(newVal, oldVal) {
			// console.log('postItem.slug');
			
			$scope.keyWordInUrl = false;
			$scope.allScores.scoreKeyWordInUrl = 0;

			if (angular.isDefined(newVal) && newVal != '' && newVal !== null) {

				// Set slug for link
				$scope.slugView = slugValue(newVal, '-');

				// Get category slug for link
				if (angular.isDefined($scope.postItem.categories[0])) {
					$scope.categorySlug = $scope.postItem.categories[0].slug;
				} else {
					$scope.categorySlug = $scope.uncategory.slug;
				}

				// Check primary keyword isset in url
				if (angular.isDefined($scope.postItem.primary_keyword) && $scope.postItem.primary_keyword != '' && $scope.postItem.primary_keyword != null) {
					if ($scope.slugView.indexOf(slugValue($scope.postItem.primary_keyword, '-')) != -1) {
						$scope.keyWordInUrl = true;
						$scope.allScores.scoreKeyWordInUrl = 10;
					}
				}
			}
			$scope.calcScore($scope.allScores);
		}, true);

		// When seo title change
		$scope.$watch('postItem.seo_title', function(newVal, oldVal) {
			// console.log('postItem.seo_title');
			
			$scope.keyWordInTitle = false;
			$scope.subKeyWordInTitle = false;
			$scope.allScores.scoreKeyWordInTitle = 0;
			$scope.allScores.scoreSubKeyWordInTitle = 0;

			if (angular.isDefined(newVal) && newVal != '' && newVal !== null) {
				// Check primary keyword isset in description
				if (angular.isDefined($scope.postItem.primary_keyword) && $scope.postItem.primary_keyword != '' && $scope.postItem.primary_keyword != null) {
					if (newVal.toLowerCase().indexOf($scope.postItem.primary_keyword.toLowerCase()) != -1) {
						$scope.keyWordInTitle = true;
						$scope.allScores.scoreKeyWordInTitle = 5;
					}
				}
				// Check sub keyword isset in seo description
				if (angular.isDefined($scope.postItem.sub_keyword) && $scope.postItem.sub_keyword != '' && $scope.postItem.sub_keyword != null) {
					if (newVal.toLowerCase().indexOf($scope.postItem.sub_keyword.toLowerCase()) != -1) {
						$scope.subKeyWordInTitle = true;
						$scope.allScores.scoreSubKeyWordInTitle = 5;
					}
				}
			}
			$scope.calcScore($scope.allScores);
		}, true);
	});

	/**
	 * Show libraries model
	 * @param  {String} type      Type view file
	 * @param  {String} _function Function name use
	 * @return {Void}           
	 */
	$scope.getModalLibraries = function(type, _function) {

		// When update
		template = '/admin/post/library?v=' + new Date().getTime();

		var modalInstance = $uibModal.open({
		    animation: true,
		    templateUrl: window.baseUrl + template,
		    controller: 'ModalLibrariesCtrl',
		    size: 'lg',
		    resolve: {
		    	type: function() {
		    		return type;
		    	},
		    	_function: function() {
		    		return _function;
		    	},
		    	maxSizeUpload: function() {
		    		return $scope.maxSizeUpload;
		    	}
		    }
		});

		modalInstance.result.then(function (data) {

			// Set image feature
			if (type == 'image') {
				if (_function == 'feature') {
					$scope.postItem.image_feature = {
						'src': data[0].url,
						'alt': data[0].title
					};
				} else {
					$scope.postItem.seo_facebook_image = window.baseUrl + data[0].url;
				}
			} else {
				// Each file
				angular.forEach(data, function(value, key) {

					// Get cursor position
					var ed = tinyMCE.activeEditor;
					var range = ed.selection.getRng();

					// Media file type
					var typeMedia = $scope.mimeContentTypes[value.file_name.split('.').pop()]['group'];

					if (typeMedia == 'Image') {
						var newNode = ed.getDoc().createElement("img"); 
						newNode.src = value.url;  						
						newNode.alt = value.title; 
						newNode.width  = 560;			
					} else if (typeMedia == 'Video' || typeMedia == 'Audio') {
						var newNode = ed.getDoc().createElement("iframe"); 
						newNode.src = value.url;  						
						newNode.alt = value.excerpt; 
						newNode.width  = 560; 
						newNode.height = 315; 
						newNode.frameborder = 0; 
						newNode.allowfullscreen = 'true'; 
					} else {
						// Set link
						var newNode = ed.getDoc().createElement("a");
						newNode.innerText = value.title;
						newNode.href = value.url;  						
						newNode.alt = value.title;  						
						newNode.target  = '_self';  
					}
					// Insert content
					range.insertNode(newNode);  
				});
			}
		}, function () {});
	};

	/**
	 * Remove tag 
	 * @param  {String} tagName The tag name
	 * @return {Void}          
	 */
	$scope.removeTag = function(tagName) {
		var index = $scope.listTags.indexOf(tagName);
		if (index > -1) {
		    $scope.listTags.splice(index, 1);
		}
	}

	// Category invalid
	$scope.invalidCategory = true;

	$scope.chooseCategory = function(id) {
		if ($scope.postItem.categories.indexOf(id) == -1) {
			$scope.postItem.categories.push(id);
		} else {
			$scope.postItem.categories.splice($scope.postItem.categories.indexOf(id)	, 1);
		}

		$timeout(function() {
			$scope.invalidCategory = true;
			if ($scope.postItem.categories.length > 0) {
				$scope.invalidCategory = false;
			}
		})
	}

	/**
	 * Save or update the post
	 * @param  {Boolean} validate The validate value
	 * @return {Void}          
	 */
	$scope.submit = function (validate) {

		// Category ids
		$scope.postItem.categories = $('.ckb-cate:checked').map(function() {
		    return parseInt($(this).val());
		}).get();

		// Validate category
		if ($scope.postItem.categories.length > 0) {
			$scope.invalidCategory = false;
		} else {
			$scope.invalidCategory = true;
		}

		// If type is page
		if ($scope.postItem.post_type == 'page') {
			$scope.invalidCategory = false;
		}
		
		// Validate
		$scope.submitted = true;
		if (!validate || $scope.invalidCategory) return;
		
		$scope.postItem.no_index = 0;
		$scope.postItem.no_follow = 0;

		// No index and no follow
		if ($('#no-index').is(':checked')) {
			$scope.postItem.no_index = 1;
		}
		if ($('#no-follow').is(':checked')) {
			$scope.postItem.no_follow = 1;
		}

		// Set tags
		$scope.postItem.tags = $scope.listTags;

		// Set post content
		$scope.postItem.content = tinyMCE.activeEditor.getContent();

		// Set score
		$scope.postItem.score = $scope.currentScore;

		// Set post slug
		if (angular.isDefined($scope.postItem.slug) && $scope.postItem.slug != '' && $scope.postItem.slug != null) {
			$scope.postItem.slug = slugValue($scope.postItem.slug, '-');
		} else {
			$scope.postItem.slug = slugValue($scope.postItem.title, '-');
		}

		// Publish date
        $scope.postItem.published_at = $filter('date')(new Date($scope.postItem.published_at), 'yyyy-MM-dd HH:mm:ss');

        // Save post
		PostService.createPostProvider($scope.postItem).then(function (data) {

			// Close loading
			HoldOn.close();

			if (data.status == -1) {
				$scope.errors = data.errors;
			} else {
				Notify(data.msg);

				$timeout(function() {
					if (!angular.isDefined($scope.postItem.id)) {
						window.location.href = '/admin/post/' + data.post.id + '/edit?type=' + $scope.type; 
					} else {
						window.location.reload();
					}
				});
			}
		})
	};

	/* When post click cancel then close modal popup */
	$scope.cancel = function () {
	    $uibModalInstance.dismiss('cancel');
	};
}]).controller('ModalLibrariesCtrl', ['$scope', '$uibModalInstance', '$timeout', 'LibraryService', 'type', '_function', 'maxSizeUpload', '$rootScope', function ($scope, $uibModalInstance, $timeout, LibraryService, type, 
	_function, maxSizeUpload, $rootScope) {

	// Loading
	HoldOn.open(optionsHoldOn);
	
	$timeout(function() {

		$scope.maxSizeUpload = maxSizeUpload;

		$('.modal-content').css('height', ($(window).height() - 40) + 'px');
		$('.left-detail').css('height', ($(window).height() - 115) + 'px');
		$('.right-detail').css('height', ($(window).height() - 115) + 'px');
		$('.modal.fade .modal-dialog').css({'width': '100%', 'height': '100%', 'top': 0, 'padding': '20px'});
		$('.modal-body .tab-pane').css({'padding': '0'});

		/**
		 * Load file when user scroll
		 * @return {Void}   
		 */
		document.getElementById("myDIV").onscroll = function(e) {scrollDiv(e)};
		function scrollDiv(e) {
			var element = event.target;
			if (element.scrollHeight - element.scrollTop === element.clientHeight
				&& $scope.pageOptions.currentPage < $scope.pageOptions.totalPages) {
				$scope.pageOptions.currentPage++;
		        $scope.getItems();
		    }
		}

		$("#tree").fancytree({
	        source: $scope.folders,
	        activate: function(event, data) {

	            // Set scope node active
	            $scope.activeNode = data.node;

	            // Get directory path
	            $scope.directoryPath = data.node.getParentList(includeRoot = false, includeSelf = true);

	            // Search data
	            $scope.searchOptions.asset_id = data.node.key;

	            // Active and expand node
	            data.node.setActive();
	            data.node.setExpanded();

	            // Reset list files
	            $scope.librariesGridView = [];

	            if (!$scope.$$phase) {
	                $scope.$apply();
	            }

	            $scope.getItems();

	            // Set folder upload file
                $rootScope.$broadcast('userChooseFolder', data.node.key);
	        },
	        extensions: ["glyph"],
	        glyph: {
	            // preset: "awesome4",
	            map: {
	                _addClass: "glyphicon",
	                checkbox: "fa-square-o",
	                checkboxSelected: "fa-check-square-o",
	                checkboxUnknown: "fa-square",
	                dragHelper: "glyphicon-chevron-right",
	                dropMarker: "fa-long-arrow-right",
	                error: "fa-warning",
	                expanderClosed: "glyphicon-chevron-right",
	                expanderLazy: "glyphicon-chevron-right",
	                expanderOpen: "glyphicon-chevron-down",
	                loading: "fa-spinner fa-pulse",
	                nodata: "fa-meh-o",
	                noExpander: "",
	                radio: "fa-circle-thin",
	                radioSelected: "fa-circle",
	                doc: "fa-file",
	                docOpen: "fa-file",
	                folder: "glyphicon-folder-close",
	                folderOpen: "glyphicon-folder-open"
	            }
	        },
	    });

	    var tree = $('#tree').fancytree("getTree");
	    $timeout(function() {
	        var node = tree.getNodeByKey(tree.getFirstChild().key.toString());
	        node.setActive();
	        node.setExpanded();
	    });
	});

	// List colected
	$scope.listIdsSelected = [];

	/**
	 * When user click on file
	 * @param  {Void} e Element
	 * @return {Void}   
	 */
	$scope.chooseFile = function(file, e) {

		$scope.fileShow = file;

		// Set image feature
		if (_function == 'feature' || _function == 'facebook') {
			var active = false;

			// Active class check
			if ($('.want-active-' + file.id).hasClass('active')) {
				active = true;
			}

			// Remove all active
			$('.check-file-active').removeClass('active');
			
			if (active == true) return;

			$scope.currentSelected = [file];
			$('.want-active-' + file.id).addClass('active');

		} else {

			if ($scope.listIdsSelected.indexOf(file.id) == -1) {
				$scope.listIdsSelected.push(file.id);
				$('.want-active-' + file.id).addClass('active');
			} else {
				$scope.listIdsSelected.splice($scope.listIdsSelected.indexOf(file.id), 1);
				$('.want-active-' + file.id).removeClass('active');
			}

			$scope.currentSelected = angular.copy($scope.librariesGridView).filter(function(obj) {
			  	return ($scope.listIdsSelected.indexOf(obj.id) != -1);
			});
		}
	}

	$scope.pageOptions = {};
	$scope.pageOptions.totalPages   = 0;
	$scope.pageOptions.totalItems   = 0;
	$scope.pageOptions.currentPage  = 1;
	$scope.pageOptions.itemsPerPage = 30;

	$scope.searchOptions = {};
	$scope.searchOptions.type = type;

	/**
	 * Get list items
	 * @author Thanh Tuan <thanhtuancr2011@gmail.com>
	 * @return {Void} 
	 */
	$scope.getItems = function(event = null) {

		LibraryService.getItems({
			pageOptions: $scope.pageOptions,
			searchOptions: $scope.searchOptions,
		}).then(function (data) {
			
			if (data.status == 1) {
				
				// Folder path
				$scope.folderPath = data.folderPath;

				if (angular.isUndefined($scope.librariesGridView)) {
					$scope.librariesGridView = [];
				}

				$scope.folderPath = data.folderPath;

				// Set page options
				$scope.pageOptions.totalPages = data.totalPages;
				$scope.pageOptions.totalItems = data.totalItems;

				$scope.librariesGridView = $scope.librariesGridView.concat(angular.copy(data.libraries));

				// Set height
				$timeout(function() {
					$scope.gridMediaHeight = $('.grid-view-media').width();
				});

				// Show loading
				HoldOn.close();
			}
		});
	}

	/**
     * Onload when file upload finished
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  {Object} event The event 
     * @param  {Object} data) The page options
     * @return {Void}       
     */
    $scope.$on('uploadFinish',function(event, data) {
    	console.log(data, 'datauploaded');
		$scope.librariesGridView = [data.file].concat($scope.librariesGridView);
    });

	// When post click add or edit post
	$scope.submit = function (validate) {
		if (angular.isDefined($scope.currentSelected) && $scope.currentSelected.length > 0) {
			angular.forEach($scope.currentSelected, function(value, key) {
				value.url = $scope.folderPath + value.file_name;
			});
			$timeout(function() {
				$uibModalInstance.close($scope.currentSelected);
			});
		}
	};

	/* When post click cancel then close modal popup */
	$scope.cancel = function () {
	    $uibModalInstance.dismiss('cancel');
	};
}]).controller('CreateThankController', ['$scope', 'PostService', '$http', '$timeout', function ($scope, PostService, $http, $timeout) {
	
	// When js didn't  loaded then hide table post
	$('.container-fluid').removeClass('hidden');

	$timeout(function() {

		// Code mirror options
		$scope.codeMirrorOptions = {
			lineNumbers: true,
		    mode: "javascript",
		    styleActiveLine: true,
		    matchBrackets: true,
		    theme: 'xq-dark'
		}

		if (document.getElementById("script") != null) {
			$scope.editorScript = CodeMirror.fromTextArea(document.getElementById("script"), $scope.codeMirrorOptions);
		}
	});

	/**
	 * When user click choose template
	 * @param  {String} themeName The theme name
	 * @return {Void}           
	 */
	$scope.chooseTheme = function(themeName) {
		$scope.postItem.template = themeName;
	}

	/**
	 * Save or update the post
	 * @param  {Boolean} validate The validate value
	 * @return {Void}          
	 */
	$scope.submit = function (validate) {

		// Validate
		$scope.submitted = true;
		if (!validate) return;

		// Set post slug
		if (angular.isDefined($scope.postItem.slug) && $scope.postItem.slug != '' && $scope.postItem.slug != null) {
			$scope.postItem.slug = slugValue($scope.postItem.slug, '-');
		} else {
			$scope.postItem.slug = slugValue($scope.postItem.title, '-');
		}

		if (angular.isDefined($scope.editorScript)) {
			$scope.postItem.script = $scope.editorScript.getValue();
		}

		PostService.createPostProvider($scope.postItem).then(function (data) {

			// Close loading
			// HoldOn.close();

			if (data.status == -1) {
				$scope.errors = data.errors;
			} else {
				Notify(data.msg);

				$timeout(function() {
					if (!angular.isDefined($scope.postItem.id)) {
						window.location.href = '/admin/post/' + data.post.id + '/edit?type=' + $scope.type; 
					} else {
						window.location.reload();
					}
				})
			}
		})
	};

	/* When post click cancel then close modal popup */
	$scope.cancel = function () {
	    $uibModalInstance.dismiss('cancel');
	};

}]).filter('spaceless',function() {
    return function(input) {
        if (input) {
            return input.replace(/\s+/g, '%');    
        }
    }
}).filter('listNameCategories',function() {
    return function(categories) {
    	var strCats = '';
    	angular.forEach(categories, function(value, key) {
    		if (strCats == '') 
    			strCats += value.name;
    		else
    			strCats += ', ' + value.name;
    	});

    	return strCats;
    }
});