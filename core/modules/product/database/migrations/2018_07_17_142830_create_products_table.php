<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 255);
            $table->string('slug', 255);
            $table->integer('status');
            $table->longText('excerpt')->nullable();
            $table->longText('content')->nullable();
            $table->longText('image_feature')->nullable();
            $table->double('price');
            $table->double('sale_price')->nullable(); 
            $table->string('origin', 255)->nullable();
            $table->string('seo_title', 255)->nullable();
            $table->string('seo_description', 255)->nullable();
            $table->string('primary_keyword', 255)->nullable();
            $table->string('sub_keyword', 255)->nullable();
            $table->integer('no_index')->default(0);
            $table->integer('no_follow')->default(0);
            $table->string('seo_facebook_title', 255)->nullable();
            $table->string('seo_facebook_description', 255)->nullable();
            $table->string('seo_facebook_image', 255)->nullable();
            $table->integer('score')->nullable();
            $table->integer('view')->nullable();
            $table->integer('out_link')->nullable();
            $table->integer('in_link')->nullable();
            $table->dateTime('published_at', 255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
