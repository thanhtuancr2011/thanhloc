@extends('base::layouts.master')

@section('title')
    Đơn hàng
@endsection

@section('breadcrumb')
    <div class="row">
        <div class="container col-md-12 admin-breadcrumb">
            <div class="breadcrumb">
                <div class="col-md-6 no-padding">
                    <h3>Đơn hàng</h3>
                </div>
                <div class="col-md-6 no-padding content-end">
                    <span class="breadcrumb-item">Trang chủ</span>
                    <span class="breadcrumb-item active">Đơn hàng</span>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content')

    <div class="card-body" ng-controller="OrderController">

        <!-- Header title and button -->
        <div class="group-search">
            <form class="frm-search" id="formSearch">
                <select class="form-control" ng-model="searchOptions.status" ng-change="searchOrders('ng-change')">
                    <option value="">Tất cả Trạng thái</option>
                    <option value="1">Đã hoàn thành</option>
                    <option value="0">Đang xử lý</option>
                </select>
                <label for="">Từ ngày</label>
                <div class="">
                    <input type="text" class="form-control"
                           name="created_at_from" ng-change="searchOrders('ng-change')"
                           uib-datepicker-popup="@{{format}}"
                           ng-click="open($event, 'created_at_from')" ng-model="searchOptions.created_at_from"
                           is-open="opened['created_at_from']"
                           datepicker-options="dateOptions"
                           placeholder="@{{format}}" close-text="Close"/>
                </div>

                <label for="">Đến ngày</label>
                <div class="">
                    <input type="text" class="form-control"
                           name="created_at_to" ng-change="searchOrders('ng-change')"
                           uib-datepicker-popup="@{{format}}"
                           ng-click="open($event, 'created_at_to')" ng-model="searchOptions.created_at_to"
                           is-open="opened['created_at_to']"
                           datepicker-options="dateOptions"
                           placeholder="@{{format}}" close-text="Close"/>
                </div>

                <div class="">
                    <button class="btn btn-success" ng-click="exportExcel()"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Xuất Excel</button>
                </div>
            </form>
            <div class="group-btn">
                <a href="javascript:void(0)" class="btn btn-danger" ng-show="showBtn" ng-click="removeOrder()">
                    <i class="fa fa-trash-o"></i>
                </a>
            </div>
        </div>

        <div class="clearfix"></div>

        <table class="table table-bordered table-hover table-striped">
            <thead>
                <th style="text-align: center; width: 5%;">#</th>
                <th style="text-align: center; width: 5%;">
                    <input type="checkbox" class="css-checkbox ckb-all" id="checkbox-all" ng-click="toggleSelection('all')">
                    <label for="checkbox-all" class="css-label lite-blue-check"></label>
                </th>
                <th style="width: 20%">Đơn hàng</th>
                <th>Ngày đặt hàng</th>
                <th>Trạng thái</th>
                <th>Tổng cộng</th>
                <th></th>
            </thead>
            <tbody>
            <tr ng-repeat="order in orders track by $index" ng-class="{'bg-tr': $index%2 == 0}">

                <td class="text-center">
                    @{{ ($index + 1) + (pageOptions.currentPage - 1) * pageOptions.itemsPerPage }}
                </td>

                <td class="text-center cls-chk">
                    <input type="checkbox" class="css-checkbox ckb" id="checkbox@{{$index}}" value="@{{order.id}}"
                           ng-click="toggleSelection()">
                    <label for="checkbox@{{$index}}" class="css-label lite-blue-check"></label>
                </td>

                <td>#@{{order.id}} @{{users[order.user_id]}}</td>

                <td>
                    @{{formatDate (order.created_at) | date : "yyyy/MM/dd"}}
                </td>

                <td>
                    <button ng-click="changeStatus(order.id)" ng-if="order.status == 1" type="button" class="btn btn-success none-radius">
                        Đã hoàn thành
                    </button>
                    <button ng-click="changeStatus(order.id)" ng-if="order.status != 1" type="button" class="btn btn-secondary none-radius">
                        Đang xử lý
                    </button>
                </td>

                <td>@{{order.subtotal|currency:"":0}} ₫</td>

                <td class="text-center">
                    <a href="/admin/order/@{{order.id}}/edit" class="btn btn-primary" title="Chi tiết">
                        <i class="fa fa-list" aria-hidden="true"></i>
                    </a>
                    <a href="javascript:void(0)" class="btn btn-danger" ng-click="removeOrder(order.id)" title="Xóa">
                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                    </a>
                </td>
            </tr>
            </tbody>
        </table>
        <pagination-directive></pagination-directive>
    </div>

@endsection

@section('script')
    {!! Html::script('backend/app/components/order/OrderService.js?v='.getVersionScript()) !!}
    {!! Html::script('backend/app/components/order/OrderController.js?v='.getVersionScript()) !!}
    {!! Html::script('backend/app/shared/pagination/PaginationDirective.js?v='.getVersionScript()) !!}
@endsection
