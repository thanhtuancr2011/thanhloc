
@extends('base::layouts.master')

@section('title')
    Chi tiết đơn hàng
@endsection

@section('breadcrumb')
    <div class="row">
        <div class="container col-md-12 admin-breadcrumb">
            <div class="breadcrumb">
                <div class="col-md-6 no-padding">
                    <h3>Chi tiết đơn hàng</h3>
                </div>
                <div class="col-md-6 no-padding content-end">
                    <span class="breadcrumb-item">Trang chủ</span>
                    <span class="breadcrumb-item"><a href="/admin/order">Đơn hàng</a></span>
                    <span class="breadcrumb-item active">Chi tiết đơn hàng</span>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content')

    <div class="card-body" ng-controller="OrderDetailController" style="padding-bottom: 0px;">
        <form class="checkout-page" method="POST" accept-charset="UTF-8" name="orderForm" ng-init="orders = {{json_encode($orders)}}" novalidate="">
            <h3 class="checkout-sep">1. Thông tin hóa đơn</h3>
            <div class="box-border">
                <ul>
                    <li class="row">
                        <div class="col-md-6">
                            <label for="billing_full_name" class="required">Họ và tên</label>
                            <input type="text" class="input form-control" name="billing_full_name" ng-model="orders[0].billing_full_name" id="billing_full_name">
                        </div>
                        <div class="col-md-6">
                            <label for="billing_email" class="required">Email</label>
                            <input type="text" class="input form-control" name="billing_email" ng-model="orders[0].billing_email" id="billing_email">
                        </div>
                    </li>
                    <li class="row">
                        <div class="col-md-6">
                            <label for="billing_company" class="required">Công ty</label>
                            <input type="text" class="input form-control" name="billing_company" ng-model="orders[0].billing_company" id="billing_company">
                        </div>
                        <div class="col-md-6">
                            <label for="billing_address" class="required">Địa chỉ</label>
                            <input type="text" class="input form-control" name="billing_address" ng-model="orders[0].billing_address" id="billing_address">
                        </div>
                    </li>
                    <li class="row">
                        <div class="col-md-6">
                            <label for="billing_phone" class="required">Số điện thoại</label>
                            <input type="text" class="input form-control" name="billing_phone" ng-model="orders[0].billing_phone" id="billing_phone">
                        </div>
                        <div class="col-md-6">
                            <label for="billing_fax" class="required">Số fax</label>
                            <input type="text" class="input form-control" name="billing_fax" ng-model="orders[0].billing_fax" id="billing_fax">
                        </div>
                    </li>
                </ul>
            </div>
            <h3 class="checkout-sep">2. Thông tin giao hàng</h3>
            <div class="box-border">
                <ul>       
                    <li class="row">
                        <div class="col-md-6" ng-class="{true: 'has-error'}[submitted && orderForm.shipping_full_name.$invalid]">
                            <label for="shipping_full_name" class="required">Họ và tên (*)</label>
                            <input type="text" class="input form-control" name="shipping_full_name" ng-model="orders[0].shipping_full_name" id="shipping_full_name" required="">
                            <label class="control-label" ng-show="submitted && orderForm.shipping_full_name.$error.required">
                                Bạn chưa nhập tên
                            </label>
                        </div>
                        <div class="col-md-6" ng-class="{true: 'has-error'}[submitted && orderForm.shipping_phone.$invalid]">
                            <label for="shipping_phone" class="required">Số điện thoại (*)</label>
                            <input type="text" class="input form-control" name="shipping_phone" ng-model="orders[0].shipping_phone" id="shipping_phone" required="">
                            <label class="control-label" ng-show="submitted && orderForm.shipping_phone.$error.required">
                                Bạn chưa nhập số điện thoại
                            </label>
                        </div>
                    </li>
                    <li class="row">
                        <div class="col-md-6" ng-class="{true: 'has-error'}[submitted && orderForm.shipping_address.$invalid]">
                            <label for="shipping_address" class="required">Địa chỉ (*)</label>
                            <input type="text" class="input form-control" name="shipping_address" ng-model="orders[0].shipping_address" id="shipping_address" required="">
                            <label class="control-label" ng-show="submitted && orderForm.shipping_address.$error.required">
                                Bạn chưa nhập địa chỉ
                            </label>
                        </div>
                        <div class="col-md-6">
                            <label for="note" class="required">Ghi chú</label>
                            <textarea type="text" class="input form-control" name="note" ng-model="orders[0].note" id="note"></textarea>
                        </div>
                    </li>
                </ul>
            </div>
            <h3 class="checkout-sep">3. Phương thức thanh toán</h3>
            <div class="box-border">
                <ul class="type_payment">
                    <li>
                        <label for="radio_button_5">
                            <input type="radio" name="payment_method" id="radio_button_5" ng-model="orders[0].payment_method" ng-value="0"> 
                            Thanh toán trực tiếp - COD
                        </label>
                        <div class="cnt cnt_tab" id="show_tab3" style="display: block;">
                            <p>Quý khách có thể chuyển tiền sau khi nhận được sản phẩm</p>
                        </div>
                    </li>
                    <li>
                        <label for="radio_button_6" style="margin-top: 10px;">
                            <input type="radio" name="payment_method" id="radio_button_6" ng-model="orders[0].payment_method" ng-value="1">
                            Chuyển khoản qua tài khoản ngân hàng
                        </label>
                        <div class="cnt cnt_tab" id="show_tab4" style="display: block;">
                            <p>Quý khách có thể chuyển tiền thanh toán tới một trong các tài khoản sau:</p>
                            <p class="txt_ctk">
                                Chủ tài khoản: <strong>CÔNG TY CỔ PHẦN DƯỢC PHẨM THANH HÀ</strong>
                            </p>
                            <p>
                                - Ngân hàng <strong>Vietinbank</strong> 
                                - Chi nhánh Thanh Xuân - Số tài khoản: <strong class="red">119000086840</strong>
                            </p>
                            <p>
                                - Ngân hàng <strong>Vietcombank</strong> 
                                - Sở giao dịch - Số tài khoản: <strong class="red">0011004049424</strong>
                            </p>
                        </div>
                    </li>
                </ul>
            </div>
            
            <h3 class="checkout-sep">4. Thông tin giỏ hàng</h3>
            <div class="box-border">
                <table class="table table-bordered table-hover table-striped cart_summary">
                    <thead>
                        <th class="cart_product">Hình ảnh</th>
                        <th>Tên sản phẩm</th>
                        <th>Giá từng sản phẩm</th>
                        <th>Số lượng</th>
                        <th class="text-center">Tổng</th>
                    </thead>
                    <tbody>
                        <tr ng-repeat="order in orders">
                            <td class="cart_product">
                                <a href="@{{order.product_url}}">
                                    <img ng-src="@{{order.image_feature.src}}" alt="@{{order.alt}}">
                                </a>
                            </td>
                            <td class="cart_description">
                                <p class="product-name"><a href="@{{order.product_url}}" target="_blank">@{{order.title}} </a></p>
                            </td>
                            <td class="price">
                                <span>@{{order.price|currency:"":0}} ₫</span>
                            </td>
                            <td class="qty">@{{order.quantity}}</td>

                            <td class="price">
                                <span>@{{order.price * order.quantity|currency:"":0}} ₫</span>
                            </td>
                        </tr>
                        <tr style="text-align: right;">
                            <td colspan="4"><strong>Tổng cộng</strong></td>
                            <td colspan="2"><strong class="ng-binding">@{{orders[0].subtotal|currency:"":0}} ₫</strong></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer" style="border-top: 0; padding-top: 0">
                <button class="btn btn-default" ng-click="cancel()"><i class="fa fa-times">
                    </i> Hủy
                </button>
                <button class="btn btn-primary" ng-click="submit(orderForm.$valid)">
                    <span><i class="fa fa-pencil-square-o"></i> Cập nhật</span>
                </button>
            </div>
        </form>
    </div>

@endsection

@section('script')
    {!! Html::script('backend/app/components/order/OrderService.js?v='.getVersionScript()) !!}
    {!! Html::script('backend/app/components/order/OrderController.js?v='.getVersionScript()) !!}
    {!! Html::script('backend/app/shared/pagination/PaginationDirective.js?v='.getVersionScript()) !!}
@endsection
