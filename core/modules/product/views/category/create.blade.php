<div class="modal-header">
    @if(!empty($item->id))
        <h4 class="modal-title">Chỉnh sửa</h4>
    @else
        <h4 class="modal-title">Thêm</h4>
    @endif
    <button aria-label="Close" data-dismiss="modal" class="close" type="button" ng-click="cancel()"><span aria-hidden="true">×</span></button>
</div>

<div class="modal-body">
    <div class="innerAll">
        <form method="POST" accept-charset="UTF-8" name="formAddCategory" ng-init='categoryItem={{$item}}; categoriesTree={{json_encode($categoriesTree)}}'>
            <input type="hidden" name="_token" value="csrf_token()" >
            <div class="form-group">
                <div class="form-group" ng-class="{true: 'has-error'}[submitted && formAddCategory.name.$invalid]">
                    <label for="name">Tên (*)</label>
                    <div class="">
                        <input class="form-control" placeholder="Tên" type="text" name="name" ng-model="categoryItem.name" required="true">
                        <label class="control-label" ng-show="submitted && formAddCategory.name.$error.required">
                            Bạn chưa nhập tên
                        </label>
                    </div>
                </div>

                <div class="form-group" ng-class="{true: 'has-error'}[submitted && formAddCategory.slug.$invalid]">
                    <label for="slug">Đường dẫn</label>
                    <div class="">
                        <input class="form-control" placeholder="Đường dẫn" type="text" name="slug" ng-model="categoryItem.slug">
                    </div>
                </div>

                <div class="form-group" ng-class="{true: 'has-error'}[submitted && requiredCategoryParent]">
                    <label for="name">Danh mục cha</label>
                    <div class="">
                        <script type="text/ng-template" id="subSelect">
                            <input class="with-font" ng-disabled="@{{categoryItem.id == value.id ? 'true' : ''}}" id="ckb-status-@{{value.id}}" name="parent_id" ng-model="categoryItem.parent_id" type="radio" value="@{{value.id}}" />
                            <label for="ckb-status-@{{value.id}}" class="css-label" style="display: inline-block;">@{{value.name}}</label>
                            <ul ng-if="value.subFolder" class="list-tree-child">
                                <li class="" ng-repeat="(key, value) in value.subFolder | orderBy:'id'" ng-include="'subSelect'"></li>
                            </ul>
                        </script>
                        <ul class="list-tree">
                            <li class="" ng-repeat="(index, value) in categoriesTree | orderBy:'id'" ng-include="'subSelect'"></li>
                        </ul>
                    </div>
                </div>

                <div class="form-group">
                    <label for="name">Mô tả</label>
                    <div class="">
                        <textarea class="form-control" placeholder="Mô tả" type="text" name="description" ng-model="categoryItem.description"></textarea>
                    </div>
                </div>

                <div class="alert alert-error alert-danger" ng-show="errors" ng-repeat="error in errors">
                    @{{error}}
                </div>
            </div>
        </form>
    </div>
</div>
<div class="clearfix"></div>
<div class="modal-footer">
    <button class="btn btn-default" ng-click="cancel()"><i class="fa fa-times">
        </i> Hủy
    </button>
    <button class="btn btn-primary" ng-click="submit(formAddCategory.$valid)">
        <span>
            @if(!empty($item->id))
                <i class="fa fa-pencil-square-o"></i> Cập nhật
            @else
                <i class="fa fa-plus"></i> Thêm
            @endif
        </span>
    </button>
</div>

