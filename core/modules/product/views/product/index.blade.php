@extends('base::layouts.master')

@section('title')
    Sản phẩm
@endsection

@section('breadcrumb')
    <div class="row">
        <div class="container col-md-12 admin-breadcrumb">
            <div class="breadcrumb">
                <div class="col-md-6 no-padding">
                    <h3>Sản phẩm</h3>
                </div>
                <div class="col-md-6 no-padding content-end">
                    <span class="breadcrumb-item">Trang chủ</span>
                    <span class="breadcrumb-item active">Sản phẩm</span>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content')

    <div class="card-body" ng-controller="ProductController" ng-init="categories={{json_encode($categories)}};">

        <!-- Header title and button -->
        <div class="group-search">
            <form class="frm-search" id="formSearch">
                <input type="text" class="form-control" ng-model="searchOptions.searchText" placeholder="Tìm kiếm..." ng-keypress="searchProducts($event)"/>
                <select class="form-control" ng-model="searchOptions.category" ng-change="searchProducts('ng-change')">
                    <option value="">Tất cả danh mục</option>
                    @foreach($categories as $category)
                        <option value="{{$category['id']}}">{{$category['name']}}</option>
                        @include('product::product.select-level', ['category' => $category, 'txt' => '&nbsp&nbsp'])
                    @endforeach
                </select>
            </form>
            <div class="group-btn">
                <a href="{{ URL::to('admin/product/create') }}" class="btn btn-primary"><i class="fa fa-plus"></i></a>
                <a href="javascript:void(0)" class="btn btn-danger" ng-show="showBtn" ng-click="removeProduct()">
                    <i class="fa fa-trash-o"></i>
                </a>
            </div>
        </div>

        <div class="clearfix"></div>

        <table class="table table-bordered table-hover table-striped">
            <thead>
                <th style="text-align: center; width: 5%;">#</th>
                <th style="text-align: center; width: 5%;">
                    <input type="checkbox" class="css-checkbox ckb-all" id="checkbox-all" ng-click="toggleSelection('all')">
                    <label for="checkbox-all" class="css-label lite-blue-check"></label>
                </th>
                <th style="width: 20%">Tiêu đề</th>
                <th style="width: 30%">Danh mục</th>
                <th>Ngày tạo</th>
                <th></th>
                </thead>
            <tbody>
            <tr ng-repeat="product in products track by $index" ng-class="{'bg-tr': $index%2 == 0}">

                <td class="text-center">
                    @{{ ($index + 1) + (pageOptions.currentPage - 1) * pageOptions.itemsPerPage }}
                </td>

                <td class="text-center cls-chk">
                    <input type="checkbox" class="css-checkbox ckb" id="checkbox@{{$index}}" value="@{{product.id}}"
                           ng-click="toggleSelection()">
                    <label for="checkbox@{{$index}}" class="css-label lite-blue-check"></label>
                </td>

                <td>@{{product.title}}</td>

                <td>@{{product.categories | listNameCategories}}</td>

                <td>
                    @{{formatDate (product.created_at) | date : "yyyy/MM/dd"}}
                </td>

                <td class="text-center">
                    <a href="/admin/product/@{{product.id}}/edit" class="btn btn-primary"
                       title="Chỉnh sửa">
                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                    </a>
                    <a href="javascript:void(0)" class="btn btn-danger" ng-click="removeProduct(product.id)"
                       title="Xóa">
                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                    </a>
                </td>
            </tr>
            </tbody>
        </table>
        <pagination-directive></pagination-directive>
    </div>

@endsection

@section('script')
    {!! Html::script('backend/app/components/product/ProductService.js?v='.getVersionScript()) !!}
    {!! Html::script('backend/app/components/product/ProductController.js?v='.getVersionScript()) !!}
    {!! Html::script('backend/app/shared/pagination/PaginationDirective.js?v='.getVersionScript()) !!}
@endsection
