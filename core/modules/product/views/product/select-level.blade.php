@if (isset($category['subFolder']) && count($category['subFolder']) > 0)
    @foreach ($category['subFolder'] as $k => $cate)
        <option value="{{$cate['id']}}">{!!$txt!!} {{$cate['name']}}</option>
        @include('post::select-level', ['category' => $cate, 'txt' => '&nbsp&nbsp&nbsp&nbsp' . $txt])
    @endforeach
@endif