<?php

Route::group(['middleware' => ['web', 'customer']], function () {
    // Search product
    Route::get('tim-kiem/{search}', 'Core\Modules\Product\Http\Controllers\SearchController@search');
});

Route::group(['middleware' => ['web', 'status']], function () {
    
    // Admin route
    Route::group(['prefix' => 'admin'], function () {
        // Category route
        Route::resource('category', 'Core\Modules\Product\Http\Controllers\CategoryController');
        
        // Product route
        Route::get('product/library', 'Core\Modules\Post\Http\Controllers\PostController@library');
        Route::resource('product', 'Core\Modules\Product\Http\Controllers\ProductController');

        // Order route
        Route::get('order/download', 'Core\Modules\Product\Http\Controllers\OrderController@download');
        Route::resource('order', 'Core\Modules\Product\Http\Controllers\OrderController');
    });

    // Api route
    Route::group(['prefix' => 'api'], function () {

        // Category route
        Route::post('category/items', 'Core\Modules\Product\Http\Controllers\Api\CategoryController@items');
        Route::post('category/delete', 'Core\Modules\Product\Http\Controllers\Api\CategoryController@delete');
        Route::resource('category', 'Core\Modules\Product\Http\Controllers\Api\CategoryController');

        // Product route
        Route::post('product/items', 'Core\Modules\Product\Http\Controllers\Api\ProductController@items');
        Route::post('product/delete', 'Core\Modules\Product\Http\Controllers\Api\ProductController@delete');
        Route::resource('product', 'Core\Modules\Product\Http\Controllers\Api\ProductController');

        // Order route
        Route::post('order/items', 'Core\Modules\Product\Http\Controllers\Api\OrderController@items');
        Route::post('order/export', 'Core\Modules\Product\Http\Controllers\Api\OrderController@export');
        Route::post('order/change-status/{id}', 'Core\Modules\Product\Http\Controllers\Api\OrderController@changeStatus');
        Route::resource('order', 'Core\Modules\Product\Http\Controllers\Api\OrderController');
    });

});

Route::group(['middleware' => ['web']], function () {

    // Api route
    Route::group(['prefix' => 'api'], function () {
        // Cart route
        Route::post('cart/items', 'Core\Modules\Product\Http\Controllers\Api\CartController@items');
        Route::post('cart/checkout', 'Core\Modules\Product\Http\Controllers\Api\OrderController@store');
        Route::resource('cart', 'Core\Modules\Product\Http\Controllers\Api\CartController');
    });

});

