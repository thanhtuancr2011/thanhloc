<?php

namespace Core\Modules\Product\Models;

use Cart;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\Paginator;
use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;

use Core\Modules\User\Models\UserModel;
use Core\Modules\Product\Models\OrderDetailModel;

class OrderModel extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'orders';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'billing_full_name', 'billing_company', 'billing_email', 'billing_address', 'billing_phone', 'billing_fax', 'shipping_full_name', 'shipping_address', 'shipping_phone', 'note', 'payment_method', 'status', 'subtotal'];

    /**
     * Get the order details
     */
    public function orderDetails()
    {
        return $this->hasMany(OrderDetailModel::class, 'order_id');
    }

    /**
     * Get list terms
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Array $data The data input
     * @return Array       The result
     */
    public function items($data)
    {
        if (isset($data['pageOptions'])) {
            $currentPage = $data['pageOptions']['currentPage'];
            $itemsPerPage = $data['pageOptions']['itemsPerPage'];
        }

        // Set current page
        Paginator::currentPageResolver(function () use ($currentPage) {
            return $currentPage;
        });

        $query = new $this;

        // If search 
        if (isset($data['searchOptions'])) {
            // Search by date created at
            if (isset($data['searchOptions']['created_at_from']) && isset($data['searchOptions']['created_at_to'])) {
                $query = $query->whereDate('orders.created_at', '>=', date('Y-m-d', strtotime($data['searchOptions']['created_at_from'])))
                               ->whereDate('orders.created_at', '<=', date('Y-m-d', strtotime($data['searchOptions']['created_at_to'])));
            }

            // Search by status
            if (isset($data['searchOptions']['status'])) {
                $query = $query->where('status', $data['searchOptions']['status']);
            }
        }

        $paginate = $query->orderBy('created_at', 'desc')->paginate($itemsPerPage);

        // Total pages
        $totalItems = $paginate->total();
        $totalPages = ceil($totalItems / $itemsPerPage);

        return [
            'status' => 1,
            'totalPages' => $totalPages,
            'totalItems' => $totalItems,
            'orders' => $paginate->items(),
            'users' => UserModel::pluck('full_name', 'id')
        ];
    }

    /**
     * Get list orders
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Array $data The data input
     * @return Array       The result
     */
    public function export($data)
    {
        if (isset($data['pageOptions'])) {
            $currentPage = $data['pageOptions']['currentPage'];
            $itemsPerPage = $data['pageOptions']['itemsPerPage'];
        }

        $query = new $this;

        // If search 
        if (isset($data['searchOptions'])) {
            // Search by date created at
            if (isset($data['searchOptions']['created_at_from']) && isset($data['searchOptions']['created_at_to'])) {
                $query = $query->whereDate('orders.created_at', '>=', date('Y-m-d', strtotime($data['searchOptions']['created_at_from'])))
                               ->whereDate('orders.created_at', '<=', date('Y-m-d', strtotime($data['searchOptions']['created_at_to'])));
            }

            // Search by status
            if (isset($data['searchOptions']['status'])) {
                $query = $query->where('orders.status', $data['searchOptions']['status']);
            }
        }

        $orders = $query->join('order_detail', 'orders.id', '=', 'order_detail.order_id')
                        ->join('users', 'orders.user_id', '=', 'users.id')
                        ->join('products', 'order_detail.product_id', '=', 'products.id')
                        ->select('orders.*', 'orders.id as order_id', 'orders.created_at as order_created_at', 
                                 'products.title', 'order_detail.*', 'users.full_name')
                        ->orderBy('orders.id', 'asc')
                        ->get();

        return $orders;
    }

    /**
     * Create product
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Array $data Data input
     * @return Object       Product
     */
    public function storeItem($data)
    {
        // Owner
        $data['user_id'] = Auth::user()->id;
        $data['subtotal'] = Cart::subtotal();

        // Save order
        $order = self::create($data);

        // Save order success
        if (!empty($order)) {

            // Contain lst cart details
            $dataOrders = [];

            foreach (Cart::content() as $key => $cart) {
                $dataOrders[] = [
                    'order_id' => $order->id,
                    'product_id' => $cart->id,
                    'price' => $cart->price,
                    'quantity' => $cart->qty
                ];
            }

            // Create order details
            $orderDetails = OrderDetailModel::insert($dataOrders);

            // Delete all cart items
            Cart::destroy();
        }

        return $order;
    }

    /**
     * Update product
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Array $data Data input
     * @return Object       Product
     */
    public function updateItem($data)
    {
        // Data update
        $dataOrder = [
            'billing_full_name' => $data[0]['billing_full_name'],
            'billing_company' => $data[0]['billing_company'],
            'billing_email' => $data[0]['billing_email'],
            'billing_address' => $data[0]['billing_address'],
            'billing_phone' => $data[0]['billing_phone'],
            'billing_fax' => $data[0]['billing_fax'],
            'shipping_full_name' => $data[0]['shipping_full_name'],
            'shipping_address' => $data[0]['shipping_address'],
            'shipping_phone' => $data[0]['shipping_phone'],
            'note' => $data[0]['note'],
            'payment_method' => $data[0]['payment_method']
        ];

        // Update product
        $this->update($dataOrder);

        return $this;
    }
}
