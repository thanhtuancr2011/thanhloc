<?php

namespace Core\Modules\Product\Models;

use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Config;
use Illuminate\Database\Eloquent\Model;

class CategoryMetaModel extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'category_meta';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['category_id', 'meta_key', 'meta_value'];


}
