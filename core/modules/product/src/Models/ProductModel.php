<?php

namespace Core\Modules\Product\Models;

use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\Paginator;
use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;

use Core\Modules\Product\Models\CategoryModel;

class ProductModel extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'products';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'slug', 'status', 'excerpt', 'content', 'image_feature', 'price', 'sale_price', 'price_fluctuations', 'origin', 'seo_title', 'seo_description', 'primary_keyword', 'sub_keyword', 'no_index', 'no_follow', 'seo_facebook_title', 'seo_facebook_description', 'seo_facebook_image', 'score', 'view', 'out_link', 'in_link', 'published_at'];

    /**
     * Many-to-Many relations with the category model.
     * Named "perms" for backwards compatibility. Also because "perms" is short and sweet.
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany(CategoryModel::class, 'product_category', 'product_id', 'category_id');
    }

    /**
     * Relationship
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @return Voids
     */
    public function productMetas()
    {
        return $this->hasMany(ProductMetaModel::class, 'product_id');
    }

    /**
     * Get list products
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Array $data The data input
     * @return Array       The result
     */
    public function items($data)
    {
        if (isset($data['pageOptions'])) {
            $currentPage = $data['pageOptions']['currentPage'];
            $itemsPerPage = $data['pageOptions']['itemsPerPage'];
        }

        // Set current page
        Paginator::currentPageResolver(function () use ($currentPage) {
            return $currentPage;
        });

        // If isset search with category
        if (!isset($data['searchOptions']['category'])) {
            $query = $this;
        } else {
            $query = CategoryModel::where('id', $data['searchOptions']['category'])->first()->products();
        }

        // Query data
        if (isset($data['searchOptions']['searchText']) && !empty($data['searchOptions']['searchText'])) {
            $query = $query->where('title', 'LIKE', ('%' . $data['searchOptions']['searchText'] . '%'));
        }

        $paginate = $query->orderBy('created_at', 'desc')->paginate($itemsPerPage);

        // Total pages
        $totalItems = $paginate->total();
        $totalPages = ceil($totalItems / $itemsPerPage);

        // Get product category and product meta
        foreach ($paginate->items() as $key => $product) {
            $product->categories;
            $product->product_metas = $product->productMetas()->pluck('meta_value', 'meta_key');
        }

        return [
            'status' => 1,
            'totalPages' => $totalPages,
            'totalItems' => $totalItems,
            'products' => $paginate->items(),
        ];
    }

    /**
     * Create product
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Array $data Data input
     * @return Object       Product
     */
    public function storeItem($data)
    {
        // Isset image feature
        if (isset($data['image_feature']))
            $data['image_feature'] = json_encode($data['image_feature']);

        // Save product
        $product = self::create($data);

        if (isset($data['product_metas']) && !empty($data['product_metas'])) {
            foreach ($data['product_metas'] as $key => &$value) {

                // Format meta key
                $value['meta_key'] = str_slug($value['meta_key'], '_');

                // If meta value is array
                if (is_array($value['meta_value'])) {
                    $value['meta_value'] = json_encode($value['meta_value']);
                }

                // Create product metas
                $product->productMetas()->create($value);
            }
        }

        // Not isset categories
        if (!isset($data['categories']) || empty($data['categories'])) {
            // Get uncategory
            $uncategory = CategoryModel::where('slug', 'uncategory')->first();
            $data['categories'][] = $uncategory->id;
        }

        // Save categories for product
        $product->categories()->sync($data['categories']);

        return $product;
    }

    /**
     * Update product
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Array $data Data input
     * @return Object       Product
     */
    public function updateItem($data)
    {
        // Isset image feature
        if (isset($data['image_feature']))
            $data['image_feature'] = json_encode($data['image_feature']);

        // Update product
        $this->update($data);

        if (isset($data['product_metas']) && !empty($data['product_metas'])) {
            foreach ($data['product_metas'] as $key => &$value) {

                // Format meta key
                $value['meta_key'] = str_slug($value['meta_key'], '_');

                // If meta value is array
                if (is_array($value['meta_value'])) {
                    $value['meta_value'] = json_encode($value['meta_value']);
                }

                // Find product metas
                $productMeta = ProductMetaModel::where('product_id', $this->id)->where('meta_key', $value['meta_key'])->first();

                // Isset product meta then update
                // Else Create product meta
                if (!empty($productMeta)) {
                    $productMeta->meta_value = $value['meta_value'];
                    $productMeta->update();
                } else {
                    $this->productMetas()->create($value);
                }
            }
        }

        // Not isset categories
        if (!isset($data['categories']) || empty($data['categories'])) {
            // Get uncategory
            $uncategory = TermModel::where('slug', 'uncategory')->first();
            $data['categories'][] = $uncategory->id;
        }

        // Set categories for product
        $this->categories()->sync($data['categories']);

        return $this;
    }

    /**
     * Search products with search options
     * @param  Array $option List options search
     * @return Void
     */
    public function searchProductsBy($option)
    {
        $productModel = new self;

        // Get products with post slug
        if (isset($option['post']) && $option['post']) {
            $productModel = $productModel->where('slug', $option['post']);
        }

        // Get products with post id
        if (isset($option['product_id']) && $option['product_id']) {
            $productModel = $productModel->where('id', $option['product_id']);
        }

        // Check not paginator
        if (!(isset($option['not_paginate']) && $option['not_paginate'])) {
            $option['not_paginate'] = false;
        }

        // Get products with slug
        if (isset($option['slug']) && $option['slug']) {
            $productModel = $productModel->where('slug', $option['slug']);
        }

        // Find by category
        if (isset($option['category']) && $option['category']) {
            $productModel = $productModel->whereHas('categories', function ($q) use ($option) {
                $q->where('categories.slug', $option['category']);
            });
        }

        // Get products with post status
        if (isset($option['product_status']) && $option['product_status']) {
            $productModel = $productModel->where('status', $option['product_status']);
        }

        // Get products with order by column
        if (isset($option['orderby']) && $option['orderby']) {
            $productModel = $productModel->orderBy($option['orderby'], $option['order']);
        } else {
            $productModel = $productModel->orderBy('published_at', 'DESC');
        }

        // Set current page and page size
        if (!isset($option['items_per_page']) || empty($option['items_per_page'])) {
            $option['items_per_page'] = (setting('items_per_page') != "") ? setting('items_per_page') : 5;
        }

        if (!(isset($option['current_page']) && $option['current_page'])) {
            $option['current_page'] = 1;
        }

        // Check pushlish_at
        $productModel = $productModel->where(function ($q){
            $q->whereDate('published_at', '<=', date('Y-m-d H:i:s'))->orWhere('published_at', null);
        });

        //
        if ($option['current_page']) {
            $currentPage = $option['current_page'];
            Paginator::currentPageResolver(function () use ($currentPage) {
                return $currentPage;
            });
        }

        //get post with post meta
        if (!isset($option['is_collection'])) {
            $option['is_collection'] = false;
        }
        if ($option['is_collection']) {
            return $productModel;
        }

        if ($option['not_paginate']) {
            return $productModel->get();
        }

        if (isset($option['with_categories'])) {
            return $productModel->with('categories')->paginate($option['items_per_page']);
        }
        
        return $productModel->paginate($option['items_per_page']);
    }

    /**
     * Delete products
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Array $ids List ids
     * @return Void
     */
    public function deleteItems($ids)
    {
        $status = self::whereIn('id', $ids)->delete();

        return $status;
    }
}
