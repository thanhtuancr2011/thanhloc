<?php

namespace Core\Modules\Product\Models;

use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Config;
use Illuminate\Database\Eloquent\Model;

class ProductMetaModel extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'product_meta';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['product_id', 'meta_key', 'meta_value'];

}
