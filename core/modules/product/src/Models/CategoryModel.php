<?php

namespace Core\Modules\Product\Models;

use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Config;
use Illuminate\Database\Eloquent\Model;

use Core\Modules\Product\Models\ProductModel;
use Core\Modules\Product\Models\CategoryMetaModel;

class CategoryModel extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'categories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'slug', 'url', 'description', 'parent_id', 'ancestor_ids'];

    /**
     * Relationship category
     *
     *
     * @return Voids
     */
    public function childs()
    {
        return $this->hasMany(CategoryModel::class, 'parent_id');
    }

    /**
     * Relationship Post
     *
     *
     * @return Voids
     */
    public function products()
    {
        return $this->belongsToMany(ProductModel::class, 'product_category', 'category_id', 'product_id');
    }

    /**
     * Relationship
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @return Voids
     */
    public function categoryMetas()
    {
        return $this->hasMany(CategoryMetaModel::class, 'category_id');
    }

    /**
     * Get list items
     * @param  Array $data The data input
     * @return Array       The result
     */
    public function items($data)
    {
        if (isset($data['pageOptions'])) {
            $currentPage = $data['pageOptions']['currentPage'];
            $itemsPerPage = $data['pageOptions']['itemsPerPage'];
        }

        // Init
        $query = $this;

        // If search by searchText
        if (isset($data['searchOptions']['searchText'])) {
            if ($data['searchOptions']['searchText'][0] == '"' && substr($data['searchOptions']['searchText'], -1) == '"') {
                $query = self:: where('name', 'like', substr($data['searchOptions']['searchText'], 1, -1));
            } else {
                $query = self:: where('name', 'like', ('%' . $data['searchOptions']['searchText'] . '%'));
            }
        }

        // Set current page
        Paginator::currentPageResolver(function () use ($currentPage) {
            return $currentPage;
        });
        $paginate = $query->orderBy('created_at', 'desc')->paginate($itemsPerPage)->toArray();

        $totalItems = $paginate['total'];
        $totalPages = ceil($totalItems / $itemsPerPage);

        return [
            'status' => 1,
            'categories' => $paginate['data'],
            'totalPages' => $totalPages,
            'totalItems' => $totalItems,
        ];
    }

    /**
     * Get categories with tree format
     * @return Array   Hierachy categories
     */
    public function getCategoriesTree($id)
    {
        // Contain data output
        $result = [];

        // Get all categories
        $categories = self::orderBy('created_at', 'desc')->get()->toArray();

        // Null category
        $nullCategory = ['id' => 0, 'name' => 'Trống', 'parent_id' => '0', 'ancestor_ids' => [0], 'subFolder' => []];

        if (!empty($categories)) {
            foreach ($categories as &$category) {
                // Update value category
                $category['subFolder'] = [];
                $category['ancestor_ids'] = json_decode($category['ancestor_ids']);

                $referenceCategories[$category['id']] = $category;
            }

            // Put a folder to property subFolder of a parent folder that it should belong to
            foreach ($categories as &$category) {
                if (!empty($category['parent_id']) && $id != $category['parent_id']) {
                    $referenceCategories[$category['parent_id']]['subFolder'][] = &$referenceCategories[$category['id']];
                }
            }

            // Get root folders
            $hierachyCategories = $referenceCategories;
            foreach ($hierachyCategories as $key => $hierachyCategory) {

                // Get tree
                if (!empty($hierachyCategory['parent_id']) && ($hierachyCategory['parent_id'] != '0')) {
                    unset($hierachyCategories[$key]);
                }
            }
            
            // Add null category
            $treeCategories = array_values($hierachyCategories);
            array_push($treeCategories, $nullCategory);

            return $treeCategories;
        } 

        return [$nullCategory];
    }

    /**
     * Get categories with tree format
     * @return Array   Hierachy categories
     */
    public function getAllCategoriesTree()
    {
        // Contain data output
        $result = [];

        // Get all categories
        $categories = self::orderBy('created_at', 'desc')->get()->toArray();

        // Null category
        $nullCategory = ['id' => 0, 'name' => 'Trống', 'parent_id' => '0', 'ancestor_ids' => [0], 'subFolder' => []];

        if (!empty($categories)) {
            foreach ($categories as &$category) {
                // Update value category
                $category['subFolder'] = [];
                $category['ancestor_ids'] = json_decode($category['ancestor_ids']);

                $referenceCategories[$category['id']] = $category;
            }

            // Put a folder to property subFolder of a parent folder that it should belong to
            foreach ($categories as &$category) {
                if (!empty($category['parent_id'])) {
                    $referenceCategories[$category['parent_id']]['subFolder'][] = &$referenceCategories[$category['id']];
                }
            }

            // Get root folders
            $hierachyCategories = $referenceCategories;
            foreach ($hierachyCategories as $key => $hierachyCategory) {

                // Get tree
                if (!empty($hierachyCategory['parent_id']) && ($hierachyCategory['parent_id'] != '0')) {
                    unset($hierachyCategories[$key]);
                }
            }
            
            // Add null category
            $treeCategories = array_values($hierachyCategories);
            array_push($treeCategories, $nullCategory);

            return $treeCategories;
        } 

        return [$nullCategory];
    }

    /**
     * Get all ancestor ids of category
     * @param  Object $parentId Category parent id
     * @param  Array &$ancestorIds Array ancestor ids
     * @return Void
     */
    public function getAncestorCategoryIds($parentId, &$ancestorIds)
    {
        $ancestorIds[] = $parentId;

        // If category has parent
        if ($parentId != 0) {
            $category = self::find($parentId);
            self::getAncestorCategoryIds($category->parent_id, $ancestorIds);
        }
    }

    /**
     * Create category
     * @param  Array $data Data input
     * @return Object       Category
     */
    public function storeItem($data)
    {
        // Set url
        $data['url'] = $data['slug'];

        // AncestorIds of category
        $ancestorIds = [];
        $this->getAncestorCategoryIds(isset($data['parent_id']) ? $data['parent_id'] : 0, $ancestorIds);
        $data['ancestor_ids'] = json_encode($ancestorIds);

        // Find category and set url
        if (isset($data['parent_id']) && $data['parent_id'] != 0) {
            $categoryParent = self::find($data['parent_id']);
            if (!empty($categoryParent)) {
                $data['url'] = $categoryParent->url . '/' . $data['slug'];
            }
        }

        // Create new category
        $category = self::create($data);

        return $category;
    }

    /**
     * Update category
     * @param  Array $data Data input
     * @return Object       Category
     */
    public function updateItem($data)
    {
        // Set url and parent_id
        $data['url'] = $data['slug'];
        $data['parent_id'] = isset($data['parent_id']) ? $data['parent_id'] : 0;

        // Content old parent id value of category
        $parentIdTmp = $this->parent_id;

        // AncestorIds of category
        $ancestorIds = [];
        $this->getAncestorCategoryIds($data['parent_id'], $ancestorIds);
        $data['ancestor_ids'] = json_encode($ancestorIds);

        // Find category and set url
        if ($data['parent_id'] != 0) {
            $categoryParent = self::find($data['parent_id']);
            if (!empty($categoryParent)) {
                $data['url'] = $categoryParent->url . '/' . $data['slug'];
            }
        }

        // Update category
        $this->update($data);

        // If parent_id's category not equal parent_id input
        if ($parentIdTmp != $data['parent_id']) {
            // Call function update all url's category children
            $this->recursiveUpdateUrl($this->id);
        }

        return $this;
    }

    /**
     * Recursive update category url
     * @param  Integer $categoryId The category id
     * @return Void             
     */
    public function recursiveUpdateUrl($categoryId)
    {
        // Find all child of category
        $categories = self::where('parent_id', $categoryId)->get();

        foreach ($categories as $key => &$category) {
            // Find category parent
            $categoryParent = self::find($category->parent_id);

            if (!empty($categoryParent)) {

                // Set url and update category
                $category->url = $categoryParent->url . '/' . $category['slug'];
                $category->update();

                // Call function update all url's category children
                $this->recursiveUpdateUrl($category->id);
            }
        }
    }

    /**
     * Delete categories
     * @param  Array $ids List ids
     * @return Void
     */
    public function deleteItems($ids)
    {
        $categories = self::whereIn('id', $ids)->get();

        // Each category
        foreach ($categories as $key => $category) {

            // Get all categories has parent is category
            $childCategories = $category->childs;

            // If has childs
            if (!empty($childCategories)) {

                // Update parent_id for all childs
                foreach ($childCategories as $key => $value) {
                    $value->parent_id = $category->parent_id;
                    $value->update();
                }
            }

            // Find category parent
            $categoryParent = self::find($category->parent_id);

            if (!empty($categoryParent)) {
                // Call function update all url's category children
                $this->recursiveUpdateUrl($categoryParent->id);
            }

            // Delete category
            $status = $category->delete();
        }

        return $status;
    }

    /**
     * search categories
     * @author AnhVN <anhvn@diziweb.com>
     * @param  Request $request Request
     * @return Response
     */
    public function searchCategoriesBy($options)
    {
        $categoryModel = new self;

        if (isset($options['child_of'])) {
            $categoryModel = $categoryModel->where('parent_id', $options['child_of']);
        }
        
        if (isset($options['hide_empty']) && $options['hide_empty']) {
            $categoryModel = $categoryModel->whereHas('posts', function ($q) use ($options) {
                $statusCode = StatusModel::where('name', 'publish')->first();
                $q->where('status', $statusCode->code);
            });
        }

        if (!isset($options['is_uncategory']) || !$options['is_uncategory']) {
            $categoryModel = $categoryModel->where('slug', '!=', 'uncategory');
        }

        if (isset($options['orderby']) && $options['orderby']) {
            if (!$options['order']) $options['order'] = 'ASC';
            $categoryModel = $categoryModel->orderBy($options['orderby'], $options['order']);
        }

        if (isset($options['slug']) && $options['slug']) {
            $categoryModel = $categoryModel->where('slug', $options['slug']);
        }

        return $categoryModel->get();
    }
}
