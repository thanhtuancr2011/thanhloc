<?php

namespace Core\Modules\Product\Http\Controllers;

use Auth;
use Theme;
use SeoHelper;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Core\Base\Facades\Base;
use Core\Base\Services\FileService;
use Core\Base\Repositories\Contracts\AssetRepositoryInterface;
use Core\Base\Repositories\Contracts\StatusRepositoryInterface;

use Core\Modules\User\Repositories\Contracts\UserRepositoryInterface;
use Core\Modules\Product\Repositories\Contracts\OrderRepositoryInterface;
use Core\Modules\Product\Repositories\Contracts\ProductRepositoryInterface;

class SearchController extends Controller
{
    protected $userRepository;
    protected $orderRepository;
    protected $productRepository;

    public function __construct(ProductRepositoryInterface $productRepository, OrderRepositoryInterface $orderRepository, UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
        $this->orderRepository = $orderRepository;
        $this->productRepository  = $productRepository;
    }

    public function search($search)
    {
        // If not data search
        if (empty($search)) {
            return redirect('/')->send();
        }   

        // Search str
        $search = str_replace('+', ' ', $search);

        // Get product
        $item = $this->productRepository->create()->where('title', 'LIKE', '%' . $search . '%')->paginate('15');

        SeoHelper::meta()->setTitle('Tìm kiếm')->setDescription('Tìm kiếm');
        return Theme::scope('search.index', compact('item', 'search'))->render();   
    }
}
