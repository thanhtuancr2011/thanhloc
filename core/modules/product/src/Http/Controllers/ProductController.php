<?php

namespace Core\Modules\Product\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Core\Base\Facades\Base;
use Core\Base\Services\FileService;
use Core\Base\Repositories\Contracts\AssetRepositoryInterface;
use Core\Base\Repositories\Contracts\StatusRepositoryInterface;

use Core\Modules\Product\Repositories\Contracts\CategoryRepositoryInterface;
use Core\Modules\Product\Repositories\Contracts\ProductRepositoryInterface;

class ProductController extends Controller
{
    protected $productRepository;
    protected $categoryRepository;
    protected $assetRepository;
    protected $statusRepository;

    public function __construct(ProductRepositoryInterface $productRepository, CategoryRepositoryInterface $categoryRepository, StatusRepositoryInterface $statusRepository, AssetRepositoryInterface $assetRepository)
    {
        $this->middleware('auth');
        $this->productRepository  = $productRepository;
        $this->categoryRepository = $categoryRepository;
        $this->assetRepository    = $assetRepository;
        $this->statusRepository   = $statusRepository;
    }

    public function index()
    {
        // Get all categories
        $categories = $this->categoryRepository->getCategoriesTree('');
        array_pop($categories);
        
        return view('product::product.index', compact('categories'));
    }

    public function create()
    {
        // Get model
        $item = $this->productRepository->create();
        $item->categories;

        // Fields product
        $productFields = function_exists('productFields') ? productFields() : [];
        
        // Call function get tree category
        $categoriesTree = $this->categoryRepository->getCategoriesTree('');
        array_pop($categoriesTree);
        
        $maxSizeUpload    = intval(ini_get("upload_max_filesize")); // Max file size
        $mimeContentTypes = FileService::listMimeTypes(); // List mime type

        return view('product::product.create', compact('item', 'categoriesTree', 'mimeContentTypes', 'maxSizeUpload', 'productFields'));
    }

    public function edit($id)
    {
        // Get model
        $item = $this->productRepository->edit($id);

        $item->categories;
        $item->image_feature = json_decode($item->image_feature, TRUE); 

        // Fields product
        $productFields = function_exists('productFields') ? productFields() : [];

        // Get product metas
        $productMetas = $item->productMetas()->pluck('meta_value', 'meta_key')->all();
        foreach ($productFields as $fieldName => &$field) {
            if (!isset($productMetas[$fieldName])) continue;
            $field['value'] = $productMetas[$fieldName];
        }

        // Call function get tree category
        $categoriesTree = $this->categoryRepository->getCategoriesTree('');
        array_pop($categoriesTree);

        $maxSizeUpload    = intval(ini_get("upload_max_filesize")); // Max file size
        $mimeContentTypes = FileService::listMimeTypes(); // List mime type
        
        return view('product::product.create', compact('item', 'categoriesTree', 'mimeContentTypes', 'maxSizeUpload', 'productFields'));
    }

    public function library()
    {
        // List mime type
        $mimeContentTypes = FileService::listMimeTypes();

        // Get list folder
        $folders = $this->assetRepository->getFoldersTree();

        return view('product::product.library', compact('mimeContentTypes', 'folders'));
    }
}
