<?php

namespace Core\Modules\Product\Http\Controllers\Api;

use URL;
use Cart;
use Request;

use App\Http\Requests;
use Illuminate\Validation\Rule;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request as HttpRequest;

use Core\Modules\User\Repositories\Contracts\UserRepositoryInterface;
use Core\Modules\Product\Repositories\Contracts\OrderRepositoryInterface;
use Core\Modules\Product\Repositories\Contracts\ProductRepositoryInterface;

class CartController extends Controller
{
    protected $userRepository;
    protected $orderRepository;
    protected $productRepository;

    public function __construct(ProductRepositoryInterface $productRepository, UserRepositoryInterface $userRepository, OrderRepositoryInterface $orderRepository)
    {
        // $this->middleware('auth');
        $this->userRepository = $userRepository;
        $this->orderRepository = $orderRepository;
        $this->productRepository = $productRepository;
    }

    /**
     * Get items
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Request $request Data input
     * @return Void           
     */
    public function items(HttpRequest $request)
    {
        // Get all data input
        $data = $request->all();

        // Call function get all items
        $carts = Cart::content();

        // Get items number
        $numberItems = Cart::content()->count();

        // Get subtotal
        $subtotal = number_format(Cart::subtotal() , 0, ',', ',');

        return new JsonResponse(['carts' => $carts, 'numberItems' => $numberItems, 'subtotal' => $subtotal]);
    }

    /**
     * Save product
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Request $request Data input
     * @return Void           
     */
    public function store(HttpRequest $request)
    {
        // All data input
        $data = $request->all();
        
        // Find product
        $product = $this->productRepository->create()->find($data['productId']);

        // Sale prices of product
        $salePrices = $product->productMetas()
                        ->whereIn('meta_key', ['sale_price_10', 'sale_price_50', 'sale_price_100'])
                        ->pluck('meta_value', 'meta_key');
        
        if ($product) {

            // Get cart item
            $item = Cart::search(function ($cartItem, $rowId) {
                return $cartItem->id === Request::get('productId');
            })->first();

            // When quantity less than 1
            // When quantity more than 100
            if ($data['quantity'] < 1) {
                $result = ['status' => 0, 'msg' => 'Đã xảy ra lỗi, mời thử lại'];

            // Save cart
            } else {

                // Product price
                if (isset($product->sale_price)) {
                    $product->price = $product->sale_price;
                }
                
                // Update cart
                if ($item) {

                    if (!empty($salePrices)) {
                        if ($item->qty + $data['quantity'] >= 100 && !empty($salePrices['sale_price_100'])) {
                            $product->price = $salePrices['sale_price_100'];
                        } elseif ($item->qty + $data['quantity'] >= 50 && !empty($salePrices['sale_price_50'])) {
                            $product->price = $salePrices['sale_price_50'];
                        } elseif ($item->qty + $data['quantity'] >= 10 && !empty($salePrices['sale_price_10'])) {
                            $product->price = $salePrices['sale_price_10'];
                        }
                    }

                    Cart::update($item->rowId, [
                        'price' => $product->price,
                        'qty' => isset($data['isCheckOut']) ? $data['quantity'] : ($item->qty + $data['quantity'])
                    ]);

                } else {

                    if (!empty($salePrices)) {
                        if ($data['quantity'] >= 100 && !empty($salePrices['sale_price_100'])) {
                            $product->price = $salePrices['sale_price_100'];
                        } elseif ($data['quantity'] >= 50 && !empty($salePrices['sale_price_50'])) {
                            $product->price = $salePrices['sale_price_50'];
                        } elseif ($data['quantity'] >= 10 && !empty($salePrices['sale_price_10'])) {
                            $product->price = $salePrices['sale_price_10'];
                        }
                    }

                    Cart::add([
                        'id' => $product->id, 
                        'name' => $product->title, 
                        'qty' => $data['quantity'], 
                        'price' => $product->price, 
                        'options' => [
                            'image_feature' => json_decode($product->image_feature)->src,
                            'url' => URL::to($product->categories()->first()->slug . '/' . $product->slug . '.html')
                        ]
                    ]);
                }

                $result = ['status' => 1, 'msg' => (isset($data['isCheckOut']) ? 'Cập nhật' : 'Thêm') . ' giỏ hàng thành công'];
            }

        } else {
            $result = ['status' => 0, 'msg' => 'Đã xảy ra lỗi, mời thử lại'];
        }

        return new JsonResponse($result);
    }

    /**
     * Update product
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Request $request Data input
     * @return Void           
     */
    public function update($id, HttpRequest $request)
    {
    }

    /**
     * Delete Cart
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Request $request The request data
     * @return Void           
     */
    public function destroy($rowId)
    {
        // Remove cart
        if (Cart::remove($rowId) == null) {
            $result = ['status' => 1, 'msg' => 'Xóa sản phẩm thành công'];
        } else {
            $result = ['status' => 0, 'msg' => 'Đã xảy ra lỗi, mời thử lại'];
        }

        // Return blogger 
        return new JsonResponse($result);
    }
}
