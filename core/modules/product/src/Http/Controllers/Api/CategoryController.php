<?php

namespace Core\Modules\Product\Http\Controllers\Api;


use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;

use Core\Modules\Product\Repositories\Contracts\CategoryRepositoryInterface;

class CategoryController extends Controller
{
    protected $categoryRepository;

    public function __construct(CategoryRepositoryInterface $categoryRepository)
    {
        $this->middleware('auth');
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * Get items
     * @param  Request $request Data input
     * @return Void           
     */
    public function items(Request $request)
    {
    	// Get all data input
    	$data = $request->all();

    	// Call function get all categories
        $categories = $this->categoryRepository->items($data);

        return new JsonResponse($categories);
    }

    /**
     * Set data validate
     * @param  Array $data The data input
     * @return Void       
     */
    public function dataValidate($data)
    {
        if (!isset($data['id'])) {
            $validateSlug = Rule::unique('categories')->where(function ($query) use ($data) {
                return $query->where('slug', $data['slug']);
            });
        } else {
            $validateSlug = Rule::unique('categories')->ignore($data['id'], 'id')->where(function ($query) use ($data) {
                return $query->where('slug', $data['slug']);
            });
        }

        $validate = [
            'rules' => [
                'name' => 'required',
                'slug' => $validateSlug
            ],
            'messages' => [
                'name.required' => 'Mời nhập tên',
                'slug.unique'   => 'Tên đã tồn tại trong hệ thống'
            ]
        ];

        return $validate;
    }

    /**
     * Save category
     * @param  Request $request Data input
     * @return Void           
     */
    public function store(Request $request)
    {
        // All data input
        $data = $request->all();

        // Call function set data validate and validate the data input
        $vali = $this->dataValidate($data);
        $data = $this->categoryRepository->validate($data, $vali['rules'], $vali['messages']);

        // Validate fail
        if (isset($data['errors'])) {
            return new JsonResponse($data);
        }

        // Call function save category
        $category = $this->categoryRepository->store($data);

        if ($category) {
            $result = ['status' => 1, 'category' => $category, 'msg' => 'Thêm thành công'];
        } else {
            $result = ['status' => 0, 'msg' => 'Đã xảy ra lỗi. Mời thử lại.'];
        }

        return new JsonResponse($result);
    }

    /**
     * Update category
     * @param  Request $request Data input
     * @return Void           
     */
    public function update($id, Request $request)
    {
        // All data input
        $data = $request->all();

        // Call function set data validate and validate the data input
        $vali = $this->dataValidate($data);
        $data = $this->categoryRepository->validate($data, $vali['rules'], $vali['messages']);

        // Validate fail
        if (isset($data['errors'])) {
            return new JsonResponse($data);
        }

        // Call function save category
        $category = $this->categoryRepository->update($id, $data);

        if ($category) {
            $result = ['status' => 1, 'category' => $category, 'msg' => 'Cập nhật thành công'];
        } else {
            $result = ['status' => 0, 'msg' => 'Đã xảy ra lỗi. Mời thử lại.'];
        }

        return new JsonResponse($result);
    }

    /**
     * Delete multiple categories
     * @param  Request $request The request data
     * @return Void           
     */
    public function delete(Request $request)
    {
        // All ids
        $ids = $request->all();

        // Call function save category
        $status = $this->categoryRepository->delete($ids);

        if ($status) {
            $result = ['status' => $status, 'msg' => 'Xóa thành công'];
        } else {
            $result = ['status' => $status, 'msg' => 'Đã xảy ra lỗi. Mời thử lại.'];
        }

        // Return blogger 
        return new JsonResponse($result);
    }

    /**
     * Get tags by tag name
     * @param  Request $request The request data
     * @return Array            List tags
     */
    public function getTagsByTagName(Request $request)
    {   
        $tags = [];

        $type = isset($_GET['type']) ? $_GET['type'] : 'post_tag';

        // Data input
        $data = json_decode($request->get('json'), TRUE);

        // Call function save category
        if (isset($data['category']))
            $tags = $this->categoryRepository->getTagsByTagName($data['category'], $type);

        if (!empty($tags)) {
            $result = ['status' => 1, 'msg' => 'Success.', 'tags' => $tags];
        } else {
            $result = ['status' => 0, 'msg' => 'Fail.'];
        }

        return $result;
    }
}
