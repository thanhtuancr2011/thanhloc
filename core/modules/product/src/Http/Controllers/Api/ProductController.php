<?php

namespace Core\Modules\Product\Http\Controllers\Api;


use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;

use Core\Modules\Product\Repositories\Contracts\ProductRepositoryInterface;
use Core\Modules\User\Repositories\Contracts\UserRepositoryInterface;

class ProductController extends Controller
{
    protected $productRepository;

    public function __construct(ProductRepositoryInterface $productRepository, UserRepositoryInterface $userRepository)
    {
        $this->middleware('auth');
        $this->productRepository = $productRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * Get items
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Request $request Data input
     * @return Void           
     */
    public function items(Request $request)
    {
    	// Get all data input
    	$data = $request->all();

    	// Call function get all items
        $products = $this->productRepository->items($data);

        return new JsonResponse($products);
    }

    /**
     * Set data validate
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Array $data The data input
     * @return Void       
     */
    public function dataValidate($data)
    {
        $validate = [
            'rules' => [
                'title' => 'required',
                'slug'  => 'unique:products,slug'
            ],
            'messages' => [
                'title.required' => 'Bạn chưa nhập tiêu đề',
                'slug.unique'    => 'Tiêu đề đã tồn tại trong hệ thống'
            ]
        ];

        // If update
        if (!empty($data['id'])){

            // Find product
            $product = $this->productRepository->edit($data['id']);

            // If slug input = slug of product edit
            if($product->slug == $data['slug']){
                $validate['rules']['slug'] = 'unique:products,slug,NULL,id,slug,$product->slug';
            }
        }

        return $validate;
    }

    /**
     * Save product
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Request $request Data input
     * @return Void           
     */
    public function store(Request $request)
    {
        // All data input
        $data = $request->all();

        // Call function set data validate and validate the data input
        $vali = $this->dataValidate($data);
        $data = $this->productRepository->validate($data, $vali['rules'], $vali['messages']);

        // Validate fail
        if (isset($data['errors'])) {
            return new JsonResponse($data);
        }

        // Call function save product
        $product = $this->productRepository->store($data);

        if ($product) {
            $result = ['status' => 1, 'msg' => 'Thêm thành công', 'product' => $product];
        } else {
            $result = ['status' => 0, 'msg' => 'Đã xảy ra lỗi, mời thử lại'];
        }

        return new JsonResponse($result);
    }

    /**
     * Update product
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Request $request Data input
     * @return Void           
     */
    public function update($id, Request $request)
    {
        // All data input
        $data = $request->all();

        // Call function set data validate and validate the data input
        $vali = $this->dataValidate($data);
        $data = $this->productRepository->validate($data, $vali['rules'], $vali['messages']);

        // Validate fail
        if (isset($data['errors'])) {
            return new JsonResponse($data);
        }

        // Call function save product
        $product = $this->productRepository->update($id, $data);

        if ($product) {
            $result = ['status' => 1, 'msg' => 'Cập nhật thành công'];
        } else {
            $result = ['status' => 0, 'msg' => 'Đã xảy ra lỗi, mời thử lại'];
        }

        return new JsonResponse($result);
    }

    /**
     * Delete multiple products
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Request $request The request data
     * @return Void           
     */
    public function delete(Request $request)
    {

        // All ids
        $ids = $request->all();

        // Call function save product
        $status = $this->productRepository->delete($ids);

        if ($status) {
            $result = ['status' => $status, 'msg' => 'Xóa thành công'];
        } else {
            $result = ['status' => $status, 'msg' => 'Đã xảy ra lỗi, mời thử lại'];
        }

        // Return blogger 
        return new JsonResponse($result);
    }
}
