<?php

namespace Core\Modules\Product\Http\Controllers\Api;

use URL;
use Cart;
use Excel;
use Request;

use App\Http\Requests;
use Illuminate\Validation\Rule;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request as HttpRequest;

use Core\Modules\User\Repositories\Contracts\UserRepositoryInterface;
use Core\Modules\Product\Repositories\Contracts\OrderRepositoryInterface;
use Core\Modules\Product\Repositories\Contracts\ProductRepositoryInterface;

class OrderController extends Controller
{
    protected $userRepository;
    protected $orderRepository;
    protected $productRepository;

    public function __construct(ProductRepositoryInterface $productRepository, UserRepositoryInterface $userRepository, OrderRepositoryInterface $orderRepository)
    {
        // $this->middleware('auth');
        $this->userRepository = $userRepository;
        $this->orderRepository = $orderRepository;
        $this->productRepository = $productRepository;
    }

    /**
     * Get items
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Request $request Data input
     * @return Void           
     */
    public function items(HttpRequest $request)
    {
        // Get all data input
        $data = $request->all();

        // Call function get all items
        $orders = $this->orderRepository->items($data);

        return new JsonResponse($orders);
    }

    /**
     * Save product
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Request $request Data input
     * @return Void           
     */
    public function store(HttpRequest $request)
    {
        // Get all data input
        $data = $request->all();

        // Call function save category
        $order = $this->orderRepository->store($data);

        if ($order) {
            $result = ['status' => 1, 'msg' => 'Thêm thành công'];
        } else {
            $result = ['status' => 0, 'msg' => 'Đã xảy ra lỗi. Mời thử lại.'];
        }

        return new JsonResponse($result);
    }

    public function export(HttpRequest $request)
    { 
        $data = $request->all();

        $orders = $this->orderRepository->export($data);

        if (!empty($orders)) {
            Excel::create('Đơn hàng', function($excel) use ($orders) {
                $excel->sheet('Sheet 1', function($sheet) use ($orders) {

                    $sheet->setStyle(array(
                        'font' => array(
                            'name'      =>  'Times New Roman',
                            'size'      =>  13
                        ),
                    ));

                    $sheet->cells('A1:L1', function($cells) {
                        $cells->setBackground('#0070C0');
                        $cells->setFontColor('#FFFFFF');
                        $cells->setFontSize(14);
                        $cells->setFontWeight('bold');
                        $cells->setValignment('center');
                        $cells->setBorder('solid', 'none', 'none', 'solid');
                    });

                    $sheet->cell('A1', function($cell) {$cell->setValue('Đơn hàng');});
                    $sheet->cell('B1', function($cell) {$cell->setValue('Tên sản phẩm');});
                    $sheet->cell('C1', function($cell) {$cell->setValue('Số lượng');});
                    $sheet->cell('D1', function($cell) {$cell->setValue('Đơn giá (VNĐ)');});
                    $sheet->cell('E1', function($cell) {$cell->setValue('Thành tiền (VNĐ)');});
                    $sheet->cell('F1', function($cell) {$cell->setValue('Ngày đặt hàng');});
                    $sheet->cell('G1', function($cell) {$cell->setValue('Trạng thái');});
                    $sheet->cell('H1', function($cell) {$cell->setValue('Người nhận hàng');});
                    $sheet->cell('I1', function($cell) {$cell->setValue('Số điện thoại');});
                    $sheet->cell('J1', function($cell) {$cell->setValue('Địa chỉ');});
                    $sheet->cell('K1', function($cell) {$cell->setValue('Ghi chú');});
                    $sheet->cell('L1', function($cell) {$cell->setValue('Kiểu thanh toán');});
                    foreach ($orders as $key => $order) {
                        $i = $key + 2;
                        $sheet->cell('A' . $i, '#' . $order['order_id'] . ' ' . $order['full_name']); 
                        $sheet->cell('B' . $i, $order['title']); 
                        $sheet->cell('C' . $i, $order['quantity']); 
                        $sheet->cell('D' . $i, number_format($order['price'] , 0, ',', ',')); 
                        $sheet->cell('E' . $i, number_format($order['quantity'] * $order['price'] , 0, ',', ',')); 
                        $sheet->cell('F' . $i, $order['order_created_at']); 
                        $sheet->cell('G' . $i, $order['status'] ? 'Đã hoàn thành' : 'Đang xử lý'); 
                        $sheet->cell('H' . $i, $order['shipping_full_name']); 
                        $sheet->cell('I' . $i, $order['shipping_phone']); 
                        $sheet->cell('J' . $i, $order['shipping_address']); 
                        $sheet->cell('K' . $i, $order['note']); 
                        $sheet->cell('L' . $i, $order['payment_method'] ? 'Ngân hàng' : 'COD'); 
                    }
                });

            })->store('xlsx');

            $result = ['status' => 1, 'msg' => 'Thành công'];
        } else {
            $result = ['status' => 0, 'msg' => 'Đã xảy ra lỗi. Mời thử lại.'];
        }

        return new JsonResponse($result);
    }

    /**
     * Update product
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Request $request Data input
     * @return Void           
     */
    public function update($id, HttpRequest $request)
    {
        // Get all data input
        $data = $request->all();

        // Call function save category
        $order = $this->orderRepository->update($id, $data);

        if ($order) {
            $result = ['status' => 1, 'msg' => 'Cập nhật thành công'];
        } else {
            $result = ['status' => 0, 'msg' => 'Đã xảy ra lỗi. Mời thử lại.'];
        }

        return new JsonResponse($result);
    }

    /**
     * Change status for order
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Request $request Request
     * @param  String  $id      Id of order 
     * @return Array            
     */
    public function changeStatus($id) 
    {
        // Call function save order
        $order = $this->orderRepository->create()->find($id);

        // Isset order
        if (!empty($order)) {
            // Update order status
            $order->status = !$order->status;
            $status = $order->update();
        }

        if (isset($status) && $status) {
            $result = ['status' => 1, 'msg' => 'Cập nhật thành công'];
        } else {
            $result = ['status' => 0, 'msg' => 'Đã xảy ra lỗi. Mời thử lại'];
        }

        // Return blogger 
        return new JsonResponse($result);
    }

    /**
     * Delete Cart
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Request $request The request data
     * @return Void           
     */
    public function destroy($rowId)
    {
        // Remove cart
        if (Cart::remove($rowId) == null) {
            $result = ['status' => 1, 'msg' => 'Xóa thành công'];
        } else {
            $result = ['status' => 0, 'msg' => 'Đã xảy ra lỗi, mời thử lại'];
        }

        // Return blogger 
        return new JsonResponse($result);
    }
}
