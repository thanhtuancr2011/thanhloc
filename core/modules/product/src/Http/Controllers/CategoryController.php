<?php

namespace Core\Modules\Product\Http\Controllers;

use Auth;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Core\Base\Services\FileService;
use Core\Modules\Product\Repositories\Contracts\CategoryRepositoryInterface;

class CategoryController extends Controller
{
    protected $categoryRepository;

    public function __construct(CategoryRepositoryInterface $categoryRepository)
    {
        $this->middleware('auth');
        $this->categoryRepository = $categoryRepository;
    }

    public function index()
    {
        return view('product::category.index');
    }

    public function create()
    {
        // Get model
        $item = $this->categoryRepository->create();
        
        // Set categories tree
        $categoriesTree = $this->categoryRepository->getCategoriesTree('');
        
        return view('product::category.create', compact('item', 'categoriesTree'));
    }

    public function edit($id)
    {
        // Get category
        $item = $this->categoryRepository->edit($id);

        // Call function get tree category
        $categoriesTree = $this->categoryRepository->getCategoriesTree($id);    

        return view('product::category.create', compact('item', 'categoriesTree'));
    }
}
