<?php

namespace Core\Modules\Product\Http\Controllers;

use Auth;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Core\Base\Facades\Base;
use Core\Base\Services\FileService;
use Core\Base\Repositories\Contracts\AssetRepositoryInterface;
use Core\Base\Repositories\Contracts\StatusRepositoryInterface;

use Core\Modules\User\Repositories\Contracts\UserRepositoryInterface;
use Core\Modules\Product\Repositories\Contracts\OrderRepositoryInterface;
use Core\Modules\Product\Repositories\Contracts\ProductRepositoryInterface;

class OrderController extends Controller
{
    protected $userRepository;
    protected $orderRepository;
    protected $productRepository;

    public function __construct(ProductRepositoryInterface $productRepository, OrderRepositoryInterface $orderRepository, UserRepositoryInterface $userRepository)
    {
        $this->middleware('auth');
        $this->userRepository = $userRepository;
        $this->orderRepository = $orderRepository;
        $this->productRepository  = $productRepository;
    }

    public function index()
    {
        return view('product::order.index');
    }

    public function create()
    {
    }

    /**
     * Show order detail
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Integer $orderId The order id
     * @return Void          
     */
    public function show($orderId)
    {
    }

    public function download()
    {
        $path = storage_path('exports/Đơn hàng.xlsx');
        return response()->download($path);
    }

    public function edit($id)
    {
        $orders = \DB::table('orders')->where('orders.id', $id)
                    ->join('order_detail', 'orders.id', '=', 'order_detail.order_id')
                    ->join('products', 'order_detail.product_id', '=', 'products.id')
                    ->select('orders.*', 'products.title', 'products.image_feature', 
                             'products.id as product_id', 'order_detail.*')
                    ->get();

        foreach ($orders as $key => &$order) {

            // Product imge
            $order->image_feature = json_decode($order->image_feature);

            // Find product and get product url
            $product = $this->productRepository->create()->find($order->product_id);
            $order->product_url = $product->categories()->first()->slug . '/' . $product->slug . '.html';
        }

        return view('product::order.create', compact('orders'));
    }

    public function library()
    {
        // List mime type
        $mimeContentTypes = FileService::listMimeTypes();

        // Get list folder
        $folders = $this->assetRepository->getFoldersTree();

        return view('product::product.library', compact('mimeContentTypes', 'folders'));
    }
}
