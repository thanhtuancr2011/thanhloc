<?php

namespace Core\Modules\Product\Repositories\Eloquents;

use Core\Modules\Category\Models\CategoryMetaModel;
use Core\Base\Repositories\Eloquents\EloquentRepository;
use Core\Modules\Category\Repositories\Contracts\CategoryMetaRepositoryInterface;

class EloquentCategoryMetaRepository extends EloquentRepository implements CategoryMetaRepositoryInterface
{
    /**
     * Get model
     * @return string
     */
    public function getModel()
    {
        return CategoryMetaModel::class;
    }
}