<?php

namespace Core\Modules\Product\Repositories\Eloquents;

use Core\Modules\Product\Models\CategoryModel;

use Core\Base\Repositories\Eloquents\EloquentRepository;
use Core\Modules\Product\Repositories\Contracts\CategoryRepositoryInterface;

class EloquentCategoryRepository extends EloquentRepository implements CategoryRepositoryInterface
{
    /**
     * Get model
     * @return string
     */
    public function getModel()
    {
        return CategoryModel::class;
    }

    /**
     * Get categories with tree format
     * @return \Illuminate\Http\Response
     */
    public function getCategoriesTree($id)
    {
        // Call function get categories
        return $this->model->getCategoriesTree($id);
    }

    /**
     * Get categories with tree format
     * @return \Illuminate\Http\Response
     */
    public function getAllCategoriesTree()
    {
        // Call function get categories
        return $this->model->getAllCategoriesTree();
    }

    /**
     * search categories
     * @param  Request $request Request
     * @return Response
     */
    public function searchCategoriesBy($options)
    {
        return $this->model->searchCategoriesBy($options);
    }
}