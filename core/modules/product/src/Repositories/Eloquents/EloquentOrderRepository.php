<?php

namespace Core\Modules\Product\Repositories\Eloquents;

use Core\Modules\Product\Models\OrderModel;
use Core\Base\Repositories\Eloquents\EloquentRepository;
use Core\Modules\Product\Repositories\Contracts\OrderRepositoryInterface;

class EloquentOrderRepository extends EloquentRepository implements OrderRepositoryInterface
{
	/**
     * Get model
     * @return string
     */
    public function getModel()
    {
        return OrderModel::class;
    }

    /**
	 * Export data to excel
	 * @param  Array $data The data input
	 * @return Void       
	 */
    public function export($data) 
    {
    	// Get orders
        $orders = $this->model->export($data);

        return $orders;
    }
}