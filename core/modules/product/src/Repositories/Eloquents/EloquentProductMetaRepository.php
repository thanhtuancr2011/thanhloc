<?php

namespace Core\Modules\Product\Repositories\Eloquents;

use Core\Modules\Product\Models\ProductMetaModel;

use Core\Base\Repositories\Eloquents\EloquentRepository;
use Core\Modules\Product\Repositories\Contracts\ProductMetaRepositoryInterface;

class EloquentProductMetaRepository extends EloquentRepository implements ProductMetaRepositoryInterface
{
	/**
     * Get model
     * @return string
     */
    public function getModel()
    {
        return ProductMetaModel::class;
    }

    /**
	 * Get all product meta with distinct
	 * @return Void 
	 */
	public function getAllDistinct() 
	{
		return ProductMetaModel::select('meta_key')->distinct()->get();
	}
}