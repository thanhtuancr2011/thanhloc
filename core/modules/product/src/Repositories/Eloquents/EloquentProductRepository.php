<?php

namespace Core\Modules\Product\Repositories\Eloquents;

use Core\Modules\Product\Models\ProductModel;

use Core\Base\Repositories\Eloquents\EloquentRepository;
use Core\Modules\Product\Repositories\Contracts\ProductRepositoryInterface;

class EloquentProductRepository extends EloquentRepository implements ProductRepositoryInterface
{
    /**
     * Get model
     * @return string
     */
    public function getModel()
    {
        return ProductModel::class;
    }

    /**
     * @param String $slug
     * @return String $url
     */
    public function getUrlPost($slug)
    {
        $string = '/{category}/{post_slug}.html';

        // Post slug
        if (strpos($string, '{post_slug}') != false) {
            $post = $this->model->where('slug', $slug)->first();
            $string = str_replace('{post_slug}', $slug, $string);
        }

        // Get category
        if ($post && strpos($string, '{category}') != false) {
            $category = $post->categories->first();
            $string = str_replace('{category}', $category->url, $string);
        }

        $string = str_replace('//', '/', $string);
        $string = str_replace('{', '', $string);
        $string = str_replace('}', '', $string);

        return url($string);
    }

    /**
     * @param String $slug
     * @param int $limit
     * @return  PostModel $post
     */
    public function getRelated($slug, $limit = 3)
    {
        // Get product
        $product = $this->model->where('slug', $slug)->first();

        // Isset product
        if ($product) {
            $categories = $product->categories()->pluck('slug');
            if ($categories) {
                $relateProducts = $this->model->where('slug', '!=', $slug)
                    ->whereHas('categories', function ($q) use ($categories) {
                        $q->whereIn('slug', $categories);
                    })->limit($limit)
                    ->orderBy('products.created_at', 'desc')->get();
                return $relateProducts;
            }
        }
        
        return false;
    }

    /**
     * Search product by options
     * @param  Array $option List search options
     * @return Void
     */
    public function searchProductsBy($options)
    {
        return $this->model->searchProductsBy($options);
    }
}