<?php

namespace Core\Modules\Product\Repositories\Eloquents;

use Core\Modules\Product\Models\OrderDetailModel;
use Core\Base\Repositories\Eloquents\EloquentRepository;
use Core\Modules\Product\Repositories\Contracts\OrderDetailRepositoryInterface;

class EloquentOrderDetailMetaRepository extends EloquentRepository implements OrderDetailRepositoryInterface
{
	/**
     * Get model
     * @return string
     */
    public function getModel()
    {
        return OrderDetailModel::class;
    }
}