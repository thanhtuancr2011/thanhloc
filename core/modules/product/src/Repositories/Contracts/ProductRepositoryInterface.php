<?php

namespace Core\Modules\Product\Repositories\Contracts;

interface ProductRepositoryInterface
{
	/**
     * Search product by options
     * @param  Array $option List search options
     * @return Void
     */
    public function searchProductsBy($option);
}


