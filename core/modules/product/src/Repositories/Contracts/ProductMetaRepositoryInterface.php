<?php

namespace Core\Modules\Product\Repositories\Contracts;

interface ProductMetaRepositoryInterface
{
	/**
	 * Get all post meta with distinct
	 * @return Void 
	 */
	public function getAllDistinct();
}


