<?php

namespace Core\Modules\Product\Repositories\Contracts;

interface OrderRepositoryInterface
{
	/**
	 * Export data to excel
	 * @param  Array $data The data input
	 * @return Void       
	 */
    public function export($data);
}


