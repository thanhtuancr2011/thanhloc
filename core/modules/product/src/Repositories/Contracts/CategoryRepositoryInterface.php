<?php

namespace Core\Modules\Product\Repositories\Contracts;

interface CategoryRepositoryInterface
{
	/**
     * Get all categories with tree format
     * @param  Request $request Request
     * @return Response         
     */
    public function getCategoriesTree($id);

    /**
     * Get all categories with tree format
     * @param  Request $request Request
     * @return Response         
     */
    public function getAllCategoriesTree();

    /**
     * search categories
     * @param  Request $request Request
     * @return Response
     */
    public function searchCategoriesBy($options);
}


