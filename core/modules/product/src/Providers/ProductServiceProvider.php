<?php

namespace Core\Modules\Product\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Foundation\AliasLoader;

use Gloudemans\Shoppingcart\Facades\Cart;
use Gloudemans\Shoppingcart\ShoppingcartServiceProvider;

use Core\Modules\Product\Repositories\Contracts\ProductRepositoryInterface;
use Core\Modules\Product\Repositories\Eloquents\EloquentProductRepository;

use Core\Modules\Product\Repositories\Contracts\CategoryRepositoryInterface;
use Core\Modules\Product\Repositories\Eloquents\EloquentCategoryRepository;

use Core\Modules\Product\Repositories\Contracts\OrderRepositoryInterface;
use Core\Modules\Product\Repositories\Eloquents\EloquentOrderRepository;
use Core\Modules\Product\Repositories\Contracts\OrderDetailRepositoryInterface;
use Core\Modules\Product\Repositories\Eloquents\EloquentOrderDetailRepository;

class ProductServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        // Publishes file
        $this->publishes([
            __DIR__.'/../../public/backend' => base_path('public/backend/app/components'),
        ], 'all');

        // List providers
        $this->app->register(ShoppingcartServiceProvider::class);

        // List aliases
        $loader = AliasLoader::getInstance();
        $loader->alias('Cart', Cart::class);

        // Load route
        $this->loadRoutesFrom(__DIR__.'/../../routes/web.php');

        // Load view
        $this->loadViewsFrom(__DIR__ .'/../../views/', 'product');

        // Load migrate
        $this->loadMigrationsFrom(__DIR__.'/../../database/migrations');

        $this->app->bind(
            CategoryRepositoryInterface::class,
            EloquentCategoryRepository::class
        );

        $this->app->bind(
            ProductRepositoryInterface::class,
            EloquentProductRepository::class
        );

        $this->app->bind(
            OrderRepositoryInterface::class,
            EloquentOrderRepository::class
        );

        $this->app->bind(
            OrderDetailRepositoryInterface::class,
            EloquentOrderDetailRepository::class
        );
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        // Register helper
        foreach (glob(__DIR__ . '/../Helpers/*.php') as $filename) {
            require_once $filename;
        }
    }

}
