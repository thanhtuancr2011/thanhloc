<?php

namespace Core\Modules\Post\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;
use Nicolaslopezj\Searchable\SearchableTrait;

use Auth;
use Carbon\Carbon;

use Core\Base\Models\StatusModel;
use Core\Modules\Term\Models\TermModel;
use Core\Modules\User\Models\UserModel;

class PostModel extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'posts';
    /**
     * Searchable rules.
     *
     * @var array
     */
    protected $searchable = [
        'columns' => [
            'posts.title' => 10,
            'posts.content' => 1,
            'posts.excerpt' => 1,
        ]
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'slug', 'excerpt', 'content', 'post_type', 'image_feature', 'status', 'seo_title', 'seo_description', 'primary_keyword', 'sub_keyword', 'no_index', 'no_follow', 'seo_facebook_title', 'seo_facebook_description', 'seo_facebook_image', 'template', 'script', 'related_post', 'score', 'view', 'out_link', 'in_link', 'user_id', 'user_publish_id', 'user_update_id', 'refused_at', 'published_at'];

    /**
     * Many-to-Many relations with the permission model.
     * Named "perms" for backwards compatibility. Also because "perms" is short and sweet.
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany(TermModel::class, 'post_term', 'post_id', 'category_id');
    }

    /**
     * Many-to-Many relations with the permission model.
     * Named "perms" for backwards compatibility. Also because "perms" is short and sweet.
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tags()
    {
        return $this->belongsToMany(TermModel::class, 'post_term', 'post_id', 'tag_id');
    }

    /**
     * Post belong to Users.
     * @author AnhVN <AnhVN@diziweb.com>
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function user()
    {
        return $this->belongsTo(UserModel::class, 'user_id', 'id');
    }

    /**
     * Relationship
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @return Voids
     */
    public function category()
    {
        return $this->hasMany(TermModel::class, 'parent_id');
    }

    /**
     * Relationship
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @return Voids
     */
    public function postMetas()
    {
        return $this->hasMany(PostMetaModel::class, 'post_id');
    }

    /**
     * Get list terms
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Array $data The data input
     * @return Array       The result
     */
    public function items($data)
    {
        if (isset($data['pageOptions'])) {
            $currentPage = $data['pageOptions']['currentPage'];
            $itemsPerPage = $data['pageOptions']['itemsPerPage'];
        }

        // Set current page
        Paginator::currentPageResolver(function () use ($currentPage) {
            return $currentPage;
        });

        // If isset search with category
        if (isset($data['searchOptions']['category'])) {
            $query = TermModel::where('id', $data['searchOptions']['category'])->first()->posts();
        } else {
            $query = $this;
        }

        // Query data
        $paginate = $query->where('post_type', 'LIKE', $data['searchOptions']['type'])->where(function ($q) use ($data) {
            if (isset($data['searchOptions'])) {

                // Search title or content
                if (isset($data['searchOptions']['searchText'])) {
                    $q->where(function ($q1) use ($data) {
                        $q1->where('title', 'LIKE', ('%' . $data['searchOptions']['searchText'] . '%'))
                            ->orWhere('content', 'LIKE', ('%' . $data['searchOptions']['searchText'] . '%'));
                    });
                }

                // Search with status code
                if (isset($data['searchOptions']['status_code'])) {
                    $status = StatusModel::where('code', $data['searchOptions']['status_code'])->first();
                    if ($status)
                        $q->where('status', $status->code);
                }
            }
        })->orderBy('created_at', 'desc')->paginate($itemsPerPage);

        // Total pages
        $totalItems = $paginate->total();
        $totalPages = ceil($totalItems / $itemsPerPage);

        // Get author
        $authors = UserModel::pluck('full_name', 'id')->all();

        // Get post status
        $lstStatus = StatusModel::pluck('name', 'code');

        // Get post categories
        foreach ($paginate->items() as $key => $post) {
            $post->categories;
            $post->post_metas = $post->postMetas()->pluck('meta_value', 'meta_key');
        }

        return [
            'status' => 1,
            'posts' => $paginate->items(),
            'authors' => $authors,
            'lstStatus' => $lstStatus,
            'totalPages' => $totalPages,
            'totalItems' => $totalItems,
        ];
    }

    /**
     * Create post
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Array $data Data input
     * @return Object       Post
     */
    public function storeItem($data)
    {
        // Author
        $data['user_id'] = \Auth::user()->id;

        // Isset image feature
        if (isset($data['image_feature']))
            $data['image_feature'] = json_encode($data['image_feature']);

        // Isset related post
        if (isset($data['related_post']))
            $data['related_post'] = json_encode($data['related_post']);

        // Get status with id
        $status = StatusModel::where('code', $data['status'])->first();

        // Save post
        $post = self::create($data);

        // Save custom fields
        if (isset($data['customFields']) && !empty($data['customFields'])) {

            // Each custom field
            foreach ($data['customFields'] as $key => &$value) {

                // Set meta key
                $value['meta_key'] = str_slug($value['meta_key'], '_');

                // If meta value is array
                if (is_array($value['meta_value'])) {
                    $value['meta_value'] = json_encode($value['meta_value']);
                }
            };

            // Save custom fields
            $post->postMetas()->createMany($data['customFields']);
        }

        // Save categories and tags if post type is post
        if ($data['post_type'] == 'post' || strpos($data['post_type'], 'post_') !== false) {

            $listTagIds = [];

            // Not isset categories
            if (!isset($data['categories']) || empty($data['categories'])) {
                // Get uncategory
                $uncategory = TermModel::where('slug', 'uncategory')->first();
                $data['categories'][] = $uncategory->id;
            }

            // If user choose tags
            if (isset($data['tags']) && !empty($data['tags'])) {
                $listTagIds = $this->saveTagsIfNotExists($data['tags'], $data['post_type']);
            }

            // Save tags for post
            $post->tags()->sync($listTagIds);

            // Save categories for post
            $post->categories()->sync($data['categories']);
        }

        return $post;
    }

    /**
     * Save tags if not exists
     * @param  Array $tags List tags name
     * @return Array       List tags id
     */
    public function saveTagsIfNotExists($tags, $postType)
    {
        // Contain list tag ids
        $listTagIds = [];

        foreach ($tags as $key => $value) {

            // Slug 
            $slug = str_slug($value, '-');

            // Get tags
            $tag = TermModel::where('slug', $slug)->where('taxonomy', 'post_tag')->first();

            // Save tag if not exists
            if (!$tag) {
                $tag = TermModel::create([
                    'name' => $value,
                    'slug' => $slug,
                    'url' => $slug,
                    'taxonomy' => 'post_tag',
                    'description' => '',
                ]);
            }

            $listTagIds[] = $tag->id;
        }

        return $listTagIds;
    }

    /**
     * Update post
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Array $data Data input
     * @return Object       Post
     */
    public function updateItem($data)
    {
        // Isset image feature
        if (isset($data['image_feature']))
            $data['image_feature'] = json_encode($data['image_feature']);

        // Isset related post
        if (isset($data['related_post']))
            $data['related_post'] = json_encode($data['related_post']);

        // User update
        $data['user_update_id'] = Auth::user()->id;

        // Get status with id
        $status = StatusModel::where('code', $data['status'])->first();

        // Update post
        $this->update($data);

        if (isset($data['post_metas']) && isset($data['customFields'])) {

            // Get list meta ids deleted
            $postMetaIds = array_column($data['post_metas'], 'id');
            $postMetaIdsUpdate = array_column($data['customFields'], 'id');
            $listMetaIds = array_diff($postMetaIds, $postMetaIdsUpdate);

            // Delete post metas
            PostMetaModel::whereIn('id', $listMetaIds)->delete();
        }

        // Custom fields
        if (isset($data['customFields']) && !empty($data['customFields'])) {
            // Each field
            foreach ($data['customFields'] as $key => &$value) {

                $value['meta_key'] = str_slug($value['meta_key'], '_');

                // If meta value is array
                if (is_array($value['meta_value'])) {
                    $value['meta_value'] = json_encode($value['meta_value']);
                }
                if (isset($value['id'])) {
                    $postMeta = PostMetaModel::find($value['id']);
                    $postMeta->update($value);
                } else {
                    $this->postMetas()->create($value);
                }
            }
        }

        if ($data['post_type'] == 'post' || $data['post_type'] == 'product' || $data['post_type'] == 'introduce' || strpos($data['post_type'], 'post_') !== false) {

            $listTagIds = [];

            // Not isset categories
            if (!isset($data['categories']) || empty($data['categories'])) {
                // Get uncategory
                $uncategory = TermModel::where('slug', 'uncategory')->first();
                $data['categories'][] = $uncategory->id;
            }

            // If user choose tags
            if (isset($data['tags']) && !empty($data['tags'])) {
                $listTagIds = $this->saveTagsIfNotExists($data['tags'], $data['post_type']);
            }

            // Set tags for post
            $this->tags()->sync($listTagIds);
            
            // Set categories for post
            $this->categories()->sync($data['categories']);
        }

        return $this;
    }

    /**
     * Delete posts
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Array $ids List ids
     * @return Void
     */
    public function deleteItems($ids)
    {
        $status = self::whereIn('id', $ids)->delete();

        return $status;
    }

    /**
     * Search posts with search options
     * @param  Array $option List options search
     * @return Void
     */
    public function searchPostBy($option)
    {
        $postModel = new self;
        if (isset($option['search']) && $option['search']) {
            if (isset($option['type_seach']) && $option['type_seach'] == 'fulltextsearch') {
                $postModel = $postModel->select(DB::raw("*,MATCH(title,content,excerpt) AGAINST ('" . $option['search'] . "' IN BOOLEAN MODE) AS relevance_score"));
            }
        }

        // Get posts with post slug
        if (isset($option['post']) && $option['post']) {
            $postModel = $postModel->where('slug', $option['post']);
        }

        // Get posts with post id
        if (isset($option['post_id']) && $option['post_id']) {
            $postModel = $postModel->where('id', $option['post_id']);
        }

        // set post_type default
        if (!(isset($option['post_type']) && $option['post_type'])) {
            $option['post_type'] = 'post_';
        }

        // check not paginator
        if (!(isset($option['not_paginate']) && $option['not_paginate'])) {
            $option['not_paginate'] = false;
        }

        // Get posts with category slug
        // Get posts with category id
        // Get posts with category name
        if ((isset($option['category']) || isset($option['category_id']) || isset($option['category_name'])) && (isset($option['post_type']) && ($option['post_type'] == 'post' || strpos($option['post_type'], 'post_') !== false))) {
            $postModel = $postModel->whereHas('categories', function ($q) use ($option) {
                if (isset($option['category']) && $option['category']) {
                    $q->where('taxonomy', '!=', 'post_tag')->where('terms.slug', $option['category']);
                }
                if (isset($option['category_name']) && $option['category_name']) {
                    $q->where('taxonomy', '!=', 'post_tag')->where('terms.name', $option['category_name']);
                }
                if (isset($option['category_id']) && $option['category_id']) {
                    $q->where('taxonomy', '!=', 'post_tag')->where('terms.id', $option['category_id']);
                }
            });
        }

        // Get list post with many meta key and meta value
        if (isset($option['post_meta']) && is_array($option['post_meta'])) {
            foreach ($option['post_meta'] as $meta_key => $meta_value) {
                if ($meta_key == 'vinhomes_basic_product_type_value') {
                    $postModel = $postModel->whereHas('postMetas', function ($q) use ($meta_key, $meta_value) {
                        $q->where('meta_key', $meta_key)->where('meta_value', 'LIKE', ('%' . $meta_value . '%'));
                    });
                } else {
                    $postModel = $postModel->whereHas('postMetas', function ($q) use ($meta_key, $meta_value) {
                        $q->where('meta_key', $meta_key)->where('meta_value', $meta_value);
                    });
                }
            }
        } else {
            // Get post with meta key and meta value
            if (isset($option['meta_key']) || isset($option['meta_value'])) {
                $postModel = $postModel->whereHas('postMetas', function ($q) use ($option) {
                    if (isset($option['meta_key']) && $option['meta_key']) {
                        $q->where('meta_key', $option['meta_key']);
                    }
                    if (isset($option['meta_value']) && $option['meta_value']) {
                        $q->where('meta_value', $option['meta_value']);
                    }
                });
            }
        }

        // Get posts with author id
        // Get posts with author name
        if (isset($option['author']) || isset($option['author_name'])) {
            $postModel = $postModel->whereHas('user', function ($q) use ($option) {
                if (isset($option['author']) && $option['author']) {
                    $q->where('id', $option['author']);
                }
                if (isset($option['author_name']) && $option['author_name']) {
                    $q->where('name', $option['author_name']);
                }
            });
        }

        // Get posts with tags
        if (isset($option['tag'])) {
            $postModel = $postModel->whereHas('tags', function ($q) use ($option) {
                $q->where('taxonomy', 'post_tag')->where('slug', $option['tag']);
            });
        }

        // Get posts with post status
        if (!(isset($option['post_status']) && $option['post_status'])) {
            $option['post_status'] = 'publish';
        }
        if (isset($option['post_status']) && $option['post_status']) {
            $statusList = StatusModel::where('name', $option['post_status'])->first();
            if ($statusList) {
                $postModel = $postModel->where('status', $statusList->code);
            }
        }

        // Get post with post_type: post or page
        if (isset($option['post_type']) && $option['post_type']) {
            if ($option['post_type'] == 'post_') {
                $postModel = $postModel->where(function ($q) use ($option) {
                    $q->where('post_type', 'post')->orWhere('post_type', 'like', $option['post_type'] . '%');
                });
            } elseif ($option['post_type'] == 'page_') {
                $postModel = $postModel->where(function ($q) use ($option) {
                    $q->where('post_type', 'page')->orWhere('post_type', 'like', $option['post_type'] . '%');
                });
            } else {
                $postModel = $postModel->where('post_type', $option['post_type']);
            }
        }

        // search data
        if (isset($option['search']) && $option['search']) {
            if (isset($option['type_seach']) && $option['type_seach'] == 'fulltextsearch') {
                $postModel = $postModel->raw(DB::raw("MATCH(title,content,excerpt) AGAINST (" . $option['search'] . " IN BOOLEAN MODE) AS relevance_score"));
                $postModel = $postModel->whereRaw("match(title,content,excerpt) against (? in boolean mode)", [$option['search']]);
                $option['orderby'] = 'relevance_score';
                $option['order'] = 'DESC';
            } else {
                $postModel = $postModel->where(function ($q) use ($option) {
                    $q->where('title', 'like', '%' . $option['search'] . '%')
                        ->orWhere('excerpt', 'like', '%' . $option['search'] . '%')
                        ->orWhere('content', 'like', '%' . $option['search'] . '%');
                });
            }

        }

        // Get posts with order by column
        if (isset($option['orderby']) && $option['orderby']) {
            $postModel = $postModel->orderBy($option['orderby'], $option['order']);
        } else {
            $postModel = $postModel->orderBy('published_at', 'DESC');
        }

        // Set current page and page size
        if (!(isset($option['posts_per_page']) && $option['posts_per_page'])) {
            $option['posts_per_page'] = (get_config('items_per_page') != "") ? get_config('items_per_page') : 5;
        }

        if (!(isset($option['current_page']) && $option['current_page'])) {
            $option['current_page'] = 1;
        }

        // Check pushlish_at
        $postModel=$postModel->where(function ($q){
            $q->whereDate('published_at', '<=', date('Y-m-d H:i:s'))->orWhere('published_at', null);
        });

        //
        if ($option['current_page']) {
            $currentPage = $option['current_page'];
            Paginator::currentPageResolver(function () use ($currentPage) {
                return $currentPage;
            });
        }

        //get post with post meta
        if (!isset($option['is_collection'])) {
            $option['is_collection'] = false;
        }
        if ($option['is_collection']) {
            return $postModel;
        }

        if ($option['not_paginate']) {
            return $postModel->get();
        }

        if (isset($option['with_categories'])) {
            return $postModel->with('categories')->paginate($option['posts_per_page']);
        }

        return $postModel->paginate($option['posts_per_page']);
    }

    /**
     * Get pages
     * @return Void
     */
    public function getPages()
    {
        $pages = self::where('post_type', 'page')->where('status', 99)->get();

        return $pages;
    }

    /**
     * Get image feature of post
     * @param  Array $options Image options
     * @return Void
     */
    public function getFeatureImg($options = [])
    {
        $width = isset($options['width']) ? $options['width'] : 0;
        $height = isset($options['height']) ? $options['height'] : 0;
        if (isset($options['temp'])) {
            if ($options['temp'] == 'thumbnail') {
                $width = setting('thumbnail_width', 150);
                $height = setting('thumbnail_height', 150);
            }
            if ($options['temp'] == 'medium') {
                $width = setting('medium_width', 300);
                $height = setting('medium_height', 300);
            }
            if ($options['temp'] == 'large') {
                $width = setting('large_width', 1024);
                $height = setting('large_width', 1024);
            }
        }
        if ($this->image_feature) {
            $img = json_decode($this->image_feature, true);
            if ($img['src']) {
                return getImageResize($img['src'], $width, $height);
            }
        }

        return getImageResize(null, $width, $height);
        return null;
    }
}
