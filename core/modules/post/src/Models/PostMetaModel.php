<?php

namespace Core\Modules\Post\Models;

use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Config;
use Illuminate\Database\Eloquent\Model;

class PostMetaModel extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'post_meta';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['post_id', 'meta_key', 'meta_value'];

}
