<?php

namespace Core\Modules\Post\Repositories\Eloquents;

use Core\Modules\Post\Models\PostMetaModel;

use Core\Base\Repositories\Eloquents\EloquentRepository;
use Core\Modules\Post\Repositories\Contracts\PostMetaRepositoryInterface;

class EloquentPostMetaRepository extends EloquentRepository implements PostMetaRepositoryInterface
{
	/**
     * Get model
     * @return string
     */
    public function getModel()
    {
        return PostMetaModel::class;
    }

    /**
	 * Get all post meta with distinct
	 * @return Void 
	 */
	public function getAllDistinct() 
	{
		return PostMetaModel::select('meta_key')->distinct()->get();
	}
}