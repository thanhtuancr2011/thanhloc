<?php

namespace Core\Modules\Post\Repositories\Eloquents;

use Core\Modules\Post\Models\PostModel;

use Core\Base\Repositories\Eloquents\EloquentRepository;
use Core\Modules\Term\Repositories\Contracts\TermRepositoryInterface;
use Core\Modules\Post\Repositories\Contracts\PostRepositoryInterface;

class EloquentPostRepository extends EloquentRepository implements PostRepositoryInterface
{
    /**
     * Get model
     * @return string
     */
    public function getModel()
    {
        return PostModel::class;
    }

    /**
     * Search posts by options
     * @param  Array $option List search options
     * @return Void
     */
    public function searchPostsBy($options)
    {
        return $this->model->searchPostBy($options);
    }

    /**
     * Search pages by options
     * @param  Array $option List search options
     * @return Void
     */
    public function searchPagesBy($options)
    {
        return $this->model->searchPagesBy($options);
    }

    /**
     * Search pages
     * @param  Array $option List search options
     * @return Void
     */
    public function getPages()
    {
        return $this->model->getPages();
    }

    /**
     * @param String $slug
     * @return String $url;
     */
    public function getUrlPage($slug)
    {
        return url($slug);
    }

    /**
     * @param String $slug
     * @return String $url
     */
    public function getUrlPost($slug)
    {
        $string = '/{category}/{post_slug}.html';

        // Post slug
        if (strpos($string, '{post_slug}') != false) {
            $post = $this->model->where('slug', $slug)->first();
            $string = str_replace('{post_slug}', $slug, $string);
        }

        // Get category
        if ($post && strpos($string, '{category}') != false) {
            $category = $post->categories->first();
            $string = str_replace('{category}', $category->url, $string);
        }

        $string = str_replace('//', '/', $string);
        $string = str_replace('{', '', $string);
        $string = str_replace('}', '', $string);

        return url($string);
    }

    /**
     * @param String $current_slug
     * @param int $limit
     * @return  PostModel $post
     */
    public function getRelated($current_slug, $limit = 3)
    {
        $currentPost = $this->model->where('slug', $current_slug)->first();
        if ($currentPost) {
            $categories = $currentPost->categories()->pluck('slug');
            if ($categories) {
                $relatePost = $this->model->where('slug', '!=', $current_slug)
                    ->where('posts.status', 99)
                    ->whereHas('categories', function ($q) use ($categories) {
                        $q->whereIn('slug', $categories);
                    })->limit($limit)
                    ->orderBy('posts.created_at', 'desc')->get();
                return $relatePost;
            }
        }
        return false;
    }

    /**
     * @param $limit
     * @param array $args
     * @return mixed
     * @author Sang Nguyen
     */
    public function getPopularPosts($limit, array $args = [])
    {
        $data = $this->model->inRandomOrder()->orderBy('posts.id', 'DESC')
            ->select('posts.*')
            ->where(function ($q) {
                $q->where('post_type', 'like', 'post_news')->orWhere('post_type','event');
            })
            ->limit($limit);
        if (!empty(array_get($args, 'where'))) {
            $data = $data->where($args['where']);
        }
        return $data->get();
    }
}