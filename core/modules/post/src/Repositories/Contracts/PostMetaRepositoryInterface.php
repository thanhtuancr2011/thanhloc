<?php

namespace Core\Modules\Post\Repositories\Contracts;

interface PostMetaRepositoryInterface
{
	/**
	 * Get all post meta with distinct
	 * @return Void 
	 */
	public function getAllDistinct();
}


