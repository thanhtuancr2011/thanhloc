<?php

namespace Core\Modules\Post\Repositories\Contracts;

interface PostRepositoryInterface
{
    /**
     * Search posts by options
     * @param  Array $option List search options
     * @return Void
     */
    public function searchPostsBy($option);

    /**
     * Search pages
     * @param  Array $option List search options
     * @return Void
     */
    public function getPages();

    /**
     * Get url by slug
     * @author  AnhVN <anhvn@diziweb.com>
     * @param  String $slug The data input
     * @return String $url
     */
    public function getUrlPage($slug);

    /**
     * Get url by slug
     * @author  AnhVN <anhvn@diziweb.com>
     * @param  String $slug The data input
     * @return String $url
     */
    public function getUrlPost($slug);

    /**
     * Get url by slug
     * @author  AnhVN <anhvn@diziweb.com>
     * @param  String $current_slug
     * @param  int $limit
     * @return $post
     */
    public function getRelated($current_slug, $limit = 3);

    /**
     * @param $limit
     * @param array $args
     * @return mixed
     * @author Sang Nguyen
     */
    public function getPopularPosts($limit, array $args = []);
}


