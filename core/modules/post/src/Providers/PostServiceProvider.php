<?php

namespace Core\Modules\Post\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Foundation\AliasLoader;

use Core\Modules\Post\Repositories\Contracts\PostRepositoryInterface;
use Core\Modules\Post\Repositories\Eloquents\EloquentPostRepository;

use Core\Modules\Post\Repositories\Contracts\PostMetaRepositoryInterface;
use Core\Modules\Post\Repositories\Eloquents\EloquentPostMetaRepository;

class PostServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        // Publishes file
        $this->publishes([
            __DIR__.'/../../public/backend' => base_path('public/backend/app/components'),
        ], 'all');

        // Load route
        $this->loadRoutesFrom(__DIR__.'/../../routes/web.php');

        // Load view
        $this->loadViewsFrom(__DIR__ .'/../../views/', 'post');

        // Load migrate
        $this->loadMigrationsFrom(__DIR__.'/../../database/migrations');

        $this->app->bind(
            PostRepositoryInterface::class,
            EloquentPostRepository::class
        );

        $this->app->bind(
            PostMetaRepositoryInterface::class,
            EloquentPostMetaRepository::class
        );
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        // Register helper
        foreach (glob(__DIR__ . '/../Helpers/*.php') as $filename) {
            require_once $filename;
        }
    }

}
