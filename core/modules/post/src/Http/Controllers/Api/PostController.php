<?php

namespace Core\Modules\Post\Http\Controllers\Api;


use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;

use Core\Modules\Post\Repositories\Contracts\PostRepositoryInterface;
use Core\Modules\User\Repositories\Contracts\UserRepositoryInterface;

class PostController extends Controller
{
    protected $postRepository;

    public function __construct(PostRepositoryInterface $postRepository, UserRepositoryInterface $userRepository)
    {
        $this->middleware('auth');
        $this->postRepository = $postRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * Get posts
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Request $request Data input
     * @return Void           
     */
    public function posts(Request $request)
    {
    	// Get all data input
    	$data = $request->all();

    	// Call function get all posts
        $category = $this->postRepository->items($data);

        return new JsonResponse($category);
    }

    /**
     * Set data validate
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Array $data The data input
     * @return Void       
     */
    public function dataValidate($data)
    {
        $validate = [
            'rules' => [
                'title' => 'required',
                'slug'  => 'unique:posts,slug'
            ],
            'messages' => [
                'title.required' => 'Bạn chưa nhập tiêu đề',
                'slug.unique'    => 'Tiêu đề đã tồn tại trong hệ thống'
            ]
        ];

        // If update
        if (!empty($data['id'])){

            // Find post
            $post = $this->postRepository->edit($data['id']);

            // If slug input = slug of post edit
            if($post->slug == $data['slug']){
                $validate['rules']['slug'] = 'unique:posts,slug,NULL,id,slug,$post->slug';
            }
        }

        return $validate;
    }

    /**
     * Save post
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Request $request Data input
     * @return Void           
     */
    public function store(Request $request)
    {
        // All data input
        $data = $request->all();

        // Call function set data validate and validate the data input
        $vali = $this->dataValidate($data);
        $data = $this->postRepository->validate($data, $vali['rules'], $vali['messages']);

        // Validate fail
        if (isset($data['errors'])) {
            return new JsonResponse($data);
        }

        // Call function save post
        $post = $this->postRepository->store($data);

        if ($post) {
            $result = ['status' => 1, 'msg' => 'Thêm thành công', 'post' => $post];
        } else {
            $result = ['status' => 0, 'msg' => 'Đã xảy ra lỗi, mời thử lại'];
        }

        return new JsonResponse($result);
    }

    /**
     * Update post
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Request $request Data input
     * @return Void           
     */
    public function update($id, Request $request)
    {
        // All data input
        $data = $request->all();

        // Call function set data validate and validate the data input
        $vali = $this->dataValidate($data);
        $data = $this->postRepository->validate($data, $vali['rules'], $vali['messages']);

        // Validate fail
        if (isset($data['errors'])) {
            return new JsonResponse($data);
        }

        // Call function save post
        $post = $this->postRepository->update($id, $data);

        if ($post) {
            $result = ['status' => 1, 'msg' => 'Cập nhật thành công'];
        } else {
            $result = ['status' => 0, 'msg' => 'Đã xảy ra lỗi, mời thử lại'];
        }

        return new JsonResponse($result);
    }

    /**
     * Delete multiple posts
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Request $request The request data
     * @return Void           
     */
    public function delete(Request $request)
    {

        // All ids
        $ids = $request->all();

        // Call function save post
        $status = $this->postRepository->delete($ids);

        if ($status) {
            $result = ['status' => $status, 'msg' => 'Xóa thành công'];
        } else {
            $result = ['status' => $status, 'msg' => 'Đã xảy ra lỗi, mời thử lại'];
        }

        // Return blogger 
        return new JsonResponse($result);
    }

    /**
     * Clone the post       
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Request $postId The post id
     * @return Void           
     */
    public function clonePost($postId)
    {
        $status = 0;

        // Get post
        $post = $this->postRepository->show($postId);

        if ($post) {

            // Data clone
            $data = $post->toArray();
            $data['title'] = $data['title'] . ' copy';
            unset($data['id']);

            // Call function set data validate and validate the data input
            $vali = $this->dataValidate($data);
            $dataValidate = $this->postRepository->validate($data, $vali['rules'], $vali['messages']);

            // Validate faild
            if (isset($dataValidate['errors'])) {

                $title = $data['title'];
                
                for ($i = 0; $i < 1000; $i++) {

                    $data['title'] = $title . ($i != 0 ? ('-' . $i) : '');
                    $data['slug'] = str_slug($data['title'], '-');

                    // Call function set data validate and validate the data input
                    $vali = $this->dataValidate($data);
                    $dataValidate = $this->postRepository->validate($data, $vali['rules'], $vali['messages']);

                    if (!isset($dataValidate['errors'])) {
                        break;
                    }
                }
            }

            // Convert image feature from json to array
            if (isset($data['image_feature']) && !empty($data['image_feature'])) {
                $data['image_feature'] = json_decode($data['image_feature'], TRUE);
            }

            // Call function save post
            $post = $this->postRepository->store($data);

            if ($post) {
                $result = ['status' => 1, 'msg' => 'Nhân bản thành công', 'post' => $post];
            } else {
                $result = ['status' => 0, 'msg' => 'Đã xảy ra lỗi, mời thử lại'];
            }

            // Return blogger 
            return new JsonResponse($result);
        }
    }
}
