<?php

namespace Core\Modules\Post\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Core\Base\Facades\Base;
use Core\Base\Services\FileService;
use Core\Base\Repositories\Contracts\AssetRepositoryInterface;
use Core\Base\Repositories\Contracts\StatusRepositoryInterface;
use Core\Modules\Term\Repositories\Contracts\TermRepositoryInterface;
use Core\Modules\Post\Repositories\Contracts\PostRepositoryInterface;
use Core\Modules\Post\Repositories\Contracts\PostMetaRepositoryInterface;

class PostController extends Controller
{
    protected $postRepository;
    protected $termRepository;
    protected $assetRepository;
    protected $statusRepository;
    protected $postMetaRepository;

    public function __construct(PostRepositoryInterface $postRepository, TermRepositoryInterface $termRepository, PostMetaRepositoryInterface $postmetaRepository, StatusRepositoryInterface $statusRepository, AssetRepositoryInterface $assetRepository)
    {
        $this->middleware('auth');
        $this->postRepository     = $postRepository;
        $this->termRepository     = $termRepository;
        $this->assetRepository    = $assetRepository;
        $this->statusRepository   = $statusRepository;
        $this->postmetaRepository = $postmetaRepository;
    }

    public function index()
    {
        // Type post
        $type = isset($_GET['type']) ? $_GET['type'] : 'post';

        // Get all status
        $status = $this->statusRepository->all();

        // Get all categories
        $categories = $this->termRepository->getCategoriesTree('', 'category');
        array_pop($categories);
        
        // Get status
        $statusCode = '';
        if (isset($_GET['status']))
            $statusCode = $this->statusRepository->where(['name' => $_GET['status']])->first()->code;

        return view('post::index', compact('type', 'categories', 'statusCode', 'status'));
    }

    public function create()
    {
        // Type post
        $type = $_GET['type'];

        // Get status belong to user
        $status = Base::getStatus();

        // Get model
        $item = $this->postRepository->create();
        $item->categories;
        
        // Call function get tree category
        $categoriesTree = $this->termRepository->getCategoriesTree('', 'category');
        array_pop($categoriesTree);
        
        $maxSizeUpload    = intval(ini_get("upload_max_filesize")); // Max file size
        $mimeContentTypes = FileService::listMimeTypes(); // List mime type

        return view('post::create', compact('item', 'type', 'categoriesTree', 'mimeContentTypes', 'maxSizeUpload', 'status'));
    }

    function isJson($string) 
    {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }

    public function edit($id)
    {
        // Type post
        $type = $_GET['type'];

        // Get model
        $item = $this->postRepository->edit($id);

        // Get status belong to user
        $status = Base::getStatus();
        
        $item->tags; 
        $item->categories;
        $item->image_feature = json_decode($item->image_feature, TRUE); 
        
        // Call function get tree category
        $categoriesTree = $this->termRepository->getCategoriesTree('', 'category');
        array_pop($categoriesTree);

        $maxSizeUpload    = intval(ini_get("upload_max_filesize")); // Max file size
        $mimeContentTypes = FileService::listMimeTypes(); // List mime type
        
        return view('post::create', compact('item', 'type', 'categoriesTree', 'mimeContentTypes', 'maxSizeUpload', 'status'));
    }

    public function library()
    {
        // List mime type
        $mimeContentTypes = FileService::listMimeTypes();

        // Get list folder
        $folders = $this->assetRepository->getFoldersTree();

        return view('post::library', compact('mimeContentTypes', 'folders'));
    }
}
