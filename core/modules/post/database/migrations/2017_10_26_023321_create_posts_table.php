<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 255);
            $table->string('slug', 255);
            $table->longText('excerpt')->nullable();
            $table->longText('content')->nullable();
            $table->string('post_type', 30);
            $table->longText('image_feature')->nullable();
            $table->string('status', 30);
            $table->string('seo_title', 255)->nullable();
            $table->string('seo_description', 255)->nullable();
            $table->string('primary_keyword', 255)->nullable();
            $table->string('sub_keyword', 255)->nullable();
            $table->integer('no_index')->default(0);
            $table->integer('no_follow')->default(0);
            $table->string('seo_facebook_title', 255)->nullable();
            $table->string('seo_facebook_description', 255)->nullable();
            $table->string('seo_facebook_image', 255)->nullable();
            $table->string('template', 255)->nullable();
            $table->longText('script')->nullable();
            $table->longText('related_post')->nullable();
            $table->integer('score')->nullable();
            $table->integer('view')->nullable();
            $table->integer('out_link')->nullable();
            $table->integer('in_link')->nullable();
            $table->integer('user_id')->nullable();
            $table->integer('user_publish_id')->nullable();
            $table->integer('user_update_id')->nullable();
            $table->dateTime('refused_at', 255)->nullable();
            $table->dateTime('published_at', 255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
