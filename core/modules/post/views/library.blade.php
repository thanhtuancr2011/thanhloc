<style type="text/css">
    .glyphicon {
        color: #2196f3;
        margin-right: 5px;
    }
    .glyphicon-folder-open:before {
        content: "\e118";
    }
    .glyphicon-chevron-down:before {
        content: "\e114";
        line-height: 20px;
    }
    .glyphicon-chevron-right:before {
        content: "\e080";
        line-height: 20px;
    }
    .glyphicon-folder-close:before {
        content: "\e117";
    }
    span.fancytree-title {
        border: none!important;
        padding: 0!important;
        margin: 5px 0 0 5px!important;
        border-radius: inherit!important;
    }
    span.fancytree-active .fancytree-title, span.fancytree-selected .fancytree-title {
        padding: 0 5px!important;
    }
    .fancytree-treefocus span.fancytree-active .fancytree-title, 
    span.fancytree-active .fancytree-title, 
    span.fancytree-selected .fancytree-title {
        background: linear-gradient(to bottom, #20a2d2 0, #20a8d8 100%);
        color: #fff;
    }
</style>
<div class="modal-body" style="padding: 15px;" ng-init='mimeContentTypes={{json_encode($mimeContentTypes)}}; folders={{json_encode($folders)}}; baseUrl="{{URL::to('/')}}"'>
    <div style="width: 150px; float: left; padding-right: 0; border: 1px solid #d1d1d1;">
        <div id="tree"></div>
    </div>
    <div style="width: calc(100% - 165px); float: left; padding-right: 0; margin-left: 15px;">
        <ul class="nav nav-tabs" ng-init="typeTab = 'media'">
            <li class="@{{(typeTab == 'media') ? 'active' : ''}}" ng-click="typeTab='media'">
                <a href="javascript:void(0)">Thư viện</a>
            </li>
            <li class="@{{(typeTab == 'upload') ? 'active' : ''}}" ng-click="typeTab='upload'">
                <a href="javascript:void(0)">Tải lên</a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane fade @{{(typeTab == 'media') ? 'in active' : ''}}">
                <div class="innerAll">
                    <div class="col-md-9 left-detail" id="myDIV" style="overflow: scroll;">
                        <div class="col-md-2 grid-view-media" style="height: @{{gridMediaHeight + 'px'}}" ng-repeat="fileGridView in librariesGridView track by $index" ng-click="chooseFile(fileGridView, $event)">
                            <button type="button" class="btn-check-file check-file-active want-active-@{{fileGridView.id}}" tabindex="-1">
                                <i class="fa fa-check" aria-hidden="true"></i>
                            </button>
                            <a class="check-file-active want-active-@{{fileGridView.id}}" href="javascript:void(0)">

                                <!-- When file not image -->
                                <i ng-show="mimeContentTypes[fileGridView.file_name.split('.').pop()]['group'] != 'Image'"
                                   class="thumbnail @{{mimeContentTypes[fileGridView.file_name.split('.').pop()]['class']}} fa-2"
                                   aria-hidden="true"></i>
                                <span ng-show="mimeContentTypes[fileGridView.file_name.split('.').pop()]['group'] != 'Image'"
                                      class="txt-file-name">@{{fileGridView.file_name}}</span>

                                <!-- When file is image -->
                                <img ng-if="mimeContentTypes[fileGridView.file_name.split('.').pop()]['group'] == 'Image' && 
                                              mimeContentTypes[fileGridView.file_name.split('.').pop()]['name'] != 'image/svg+xml' &&
                                              mimeContentTypes[fileGridView.file_name.split('.').pop()]['name'] != 'image/vnd.microsoft.icon' &&
                                              mimeContentTypes[fileGridView.file_name.split('.').pop()]['name'] != 'image/vnd.adobe.photoshop'"
                                     class="thumbnail" ng-src="@{{folderPath}}320-@{{fileGridView.file_name}}"/>
                                <img ng-if="mimeContentTypes[fileGridView.file_name.split('.').pop()]['group'] == 'Image' && 
                                              (mimeContentTypes[fileGridView.file_name.split('.').pop()]['name'] != 'image/svg+xml' ||
                                              mimeContentTypes[fileGridView.file_name.split('.').pop()]['name'] != 'image/vnd.microsoft.icon' ||
                                              mimeContentTypes[fileGridView.file_name.split('.').pop()]['name'] != 'image/vnd.adobe.photoshop')"
                                     class="thumbnail" ng-src="@{{folderPath}}@{{fileGridView.file_name}}"/>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3 right-detail">
                        <div class="innerLR">
                            <div class="" style="margin: 10px 0 20px 0;">
                                <div class="filename" style="color: #666;"><strong>Tên tập tin:</strong> @{{fileShow.file_name}}</div>
                                <div class="filename" style="color: #666;"><strong>Loại tập tin:</strong> @{{fileShow.type}}</div>
                                <div class="filename" style="color: #666;"><strong>Ngày tải lên:</strong> @{{fileShow.created_at}}</div>
                            </div>
                            <form method="POST" action="{{{ URL::to('users') }}}" accept-charset="UTF-8" name="formAddLibrary">   
                                <div class="form-group">

                                    <div class="form-group" ng-class="{true: 'has-error'}[submitted && formAddLibrary.last_name.$invalid]">
                                        <label for="last_name">Đường dẫn</label>
                                        <div class="">
                                            <input class="form-control" readonly type="text" value="@{{fileShow.file_name ? (baseUrl + folderPath + fileShow.file_name) : ''}}">
                                        </div>
                                    </div>

                                    <div class="form-group" ng-class="{true: 'has-error'}[submitted && formAddLibrary.title.$invalid]">
                                        <label for="title">Tiêu đề (*)</label>
                                        <div class="">
                                            <input class="form-control" placeholder="Nhập tiêu đề" type="text" name="title" ng-model="fileShow.title" required="true">
                                            <label class="control-label" ng-show="submitted && formAddLibrary.title.$error.required">
                                                Bạn chưa nhập tiêu đề
                                            </label>
                                        </div>
                                    </div>

                                    <div class="form-group" ng-class="{true: 'has-error'}[submitted && formAddLibrary.excerpt.$invalid]">
                                        <label for="excerpt">Chú thích</label>
                                        <div class="">
                                            <textarea class="form-control" placeholder="Nhập chú thích" type="text" name="excerpt" ng-model="fileShow.excerpt"></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group" ng-class="{true: 'has-error'}[submitted && formAddLibrary.description.$invalid]">
                                        <label for="description">Mô tả</label>
                                        <div class="">
                                            <textarea class="form-control" placeholder="Nhập mô tả" type="text" name="description" ng-model="fileShow.description"></textarea>
                                        </div>
                                    </div>

                                    <div class="alert alert-error alert-danger" ng-show="errors" ng-repeat="error in errors">
                                        @{{error}}
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="clearfix"></div>
                        <div class="modal-footer" style="margin: 10px 0;">
                            <button class="btn btn-default" ng-click="cancel()"><i class="fa fa-times"></i> Hủy</button>
                            <button class="btn btn-primary" ng-click="submit(formAddLibrary.$valid)">
                                <i class="fa fa-check-square-o"></i>
                                <span>Chọn</span>
                            </button>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="tab-pane fade @{{(typeTab == 'upload') ? 'in active' : ''}}">
                <div class="innerAll" style="padding: 20px;">
                    <!-- File upload directive -->
                <file-upload id="uploadDirective" max-size-upload="maxSizeUpload" mime-content-types="mimeContentTypes" ng-model="filesUpload" multiple-file="true"></file-upload>
                </div>
            </div>
        </div>
    </div>
</div>

