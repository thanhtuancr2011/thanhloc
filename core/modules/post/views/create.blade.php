@extends('base::layouts.master')

@section('title')
    {{$type == 'post' ? 'Bài viết' : 'Trang'}}
@endsection

@section('breadcrumb')
    <div class="row">
        <div class="container col-md-12 admin-breadcrumb">
            <div class="breadcrumb">
                <div class="col-md-6 no-padding">
                    <h3>{{$type == 'post' ? 'Bài viết' : 'Trang'}}</h3>
                </div>
                <div class="col-md-6 no-padding content-end">
                    <span class="breadcrumb-item">Trang chủ</span>
                    <span class="breadcrumb-item">
                        <a href="{{URL::to('admin/post?type=' . $type)}}">{{$type == 'post' ? 'Bài viết' : 'Trang'}}</a>
                    </span>
                    <span class="breadcrumb-item active">
                        @if ($item->id) Chỉnh sửa @else Thêm @endif
                    </span>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content')
<div class="card-body create-post-body" ng-controller="CreatePostController">  
    <form method="POST" id="frm-create-post" accept-charset="UTF-8" name="formAddPost" ng-init="postItem={{json_encode($item)}}; categoriesTree={{json_encode($categoriesTree)}}; mimeContentTypes={{json_encode($mimeContentTypes)}}; maxSizeUpload={{$maxSizeUpload}}; status={{json_encode($status)}}; type='{{$type}}'" novalidate="">  
        <div class="row">
            <div class="col-md-9">
                <input type="hidden" name="_token" value="csrf_token()" >
                <div class="form-group">
                    <input ng-show="false" type="text" name="post_type" ng-model="postItem.post_type" ng-init="postItem.post_type = '{{$type}}'" >

                    <div class="form-group" ng-class="{true: 'has-error'}[submitted && formAddPost.title.$invalid]">
                        <label for="title">Tiêu đề (*)</label>
                        <div class="">
                            <input class="form-control" placeholder="Tiêu đề" type="text" name="title" ng-model="postItem.title" required="true">
                            <label class="control-label" ng-show="submitted && formAddPost.title.$error.required">
                                Bạn chưa nhập tiêu đề
                            </label>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="name">Mô tả ngắn</label>
                        <div class="">
                            <textarea class="form-control" placeholder="Mô tả ngắn" type="text" name="excerpt" ng-model="postItem.excerpt"></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="name">Đường dẫn</label>
                        <div class="">
                            <input class="form-control" placeholder="Đường dẫn" type="text" name="slug" ng-model="postItem.slug">
                        </div>
                    </div>

                    <div class="form-group" ng-show="categorySlug">
                        <label for="name">Xem bài viết</label>
                        <div class="">
                            <a href="{{getUrl($item->post_type . '.' . $item->slug)}}"><span id="seo-slug">{{getUrl($item->post_type . '.' . $item->slug)}}</span></a>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="">
                            <button class="btn btn-primary pull-left" ng-click="getModalLibraries('all', 'editor')">
                                <i class="fa fa-plus"></i> Thêm từ thư viện
                            </button>
                        </div>
                        <div class="clearfix"></div>
                    </div>

                    <div class="form-group">
                        <label for="name">Nội dung</label>
                        <div class="">
                            <textarea id="tinymceEditor" ng-model="postItem.content"></textarea>
                        </div>
                    </div>  
                </div>

                <div class="form-group">
                    <ul class="nav nav-tabs" ng-init="typeTab = 'analysis'">
                        <li class="@{{(typeTab == 'analysis') ? 'active' : ''}}" ng-click="typeTab='analysis'">
                            <a href="javascript:void(0)">Phân tích từ khóa</a>
                        </li>
                        <li class="@{{(typeTab == 'shareFB') ? 'active' : ''}}" ng-click="typeTab='shareFB'">
                            <a href="javascript:void(0)">Chia sẻ facebook</a>
                        </li>
                        <li class="@{{(typeTab == 'utm') ? 'active' : ''}}" ng-click="typeTab='utm'">
                            <a href="javascript:void(0)">UTM</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane fade @{{(typeTab == 'analysis') ? 'in active' : ''}}">
                            <div class="form-group">
                                <h5>Xem trước</h5>
                                <div class="content-preview">
                                    <div class="seo-title">@{{postItem.seo_title}}</div>
                                    <div class="seo-slug" ng-show="slugView">
                                        <span ng-show="type=='post'" id="seo-slug">{{ URL::to('') }}/@{{categorySlug}}/@{{slugView}}.html</span>
                                        <span ng-show="type=='page'" id="seo-slug">{{ URL::to('') }}/@{{slugView}}.html</span>
                                    </div>
                                    <div class="seo-descripton" id="seo-descripton">@{{postItem.seo_description}}</div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="seo_title">Tiêu đề SEO</label>
                                <div class="">
                                    <input class="form-control" placeholder="Tiêu đề SEO" type="text" name="seo_title" ng-model="postItem.seo_title">
                                </div>
                            </div>

                            <div class="form-group" ng-class="{true: 'has-error'}[submitted && formAddPost.slug.$invalid]">
                                <label for="slug">
                                    Đường dẫn SEO
                                </label>
                                <div class="">
                                    <input class="form-control" placeholder="Đường dẫn SEO" type="text" name="slug" ng-model="postItem.slug">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="seo_description">Mô tả SEO</label>
                                <div class="">
                                    <textarea class="form-control" placeholder="Mô tả SEO" type="text" name="seo_description" ng-model="postItem.seo_description"></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="primary_keyword">Từ khóa chính</label>
                                <div class="">
                                    <input class="form-control" placeholder="Từ khóa chính" type="text" name="primary_keyword" ng-model="postItem.primary_keyword">
                                    <ul class="error-list col-md-12">
                                        <li class="@{{!keyWordInUrl ? 'err' : 'suc'}}">Từ khóa chính nằm trong đường dẫn</li>
                                        <li class="@{{!keyWordInTitle ? 'err' : 'suc'}}">Từ khóa chính nằm trong tiêu đề</li>
                                        <li class="@{{!keyWordInContent ? 'err' : 'suc'}}">Từ khóa chính nằm trong nội dung</li>
                                        <li class="@{{!keyWordInDescription ? 'err' : 'suc'}}">Từ khóa chính nằm trong mô tả</li>
                                        <li class="@{{!keyWordInFirst150Chracters ? 'err' : 'suc'}}">Từ khóa chính nằm trong 150 kí tự đầu</li>
                                        <li class="@{{!keyWordInLast150Chracters ? 'err' : 'suc'}}">Từ khóa chính nằm trong 150 kí tự cuối</li>
                                        <li class="@{{!appearKeyWordInContent ? 'err' : 'suc'}}">Mật độ từ khoá trong bài: <strong>@{{percentAppear}}%</strong>. Yêu cầu 2 - 5%</li>
                                    </ul>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="form-group">
                                <label for="sub_keyword">Từ khóa phụ</label>
                                <div class="">
                                    <input class="form-control" placeholder="Từ khóa phụ" type="text" name="sub_keyword" ng-model="postItem.sub_keyword">
                                    <ul class="error-list col-md-12">
                                        <li class="@{{!subKeyWordInTitle ? 'err' : 'suc'}}">Từ khóa phụ nằm trong tiêu đề</li>
                                        <li class="@{{!subKeyWordInDescription ? 'err' : 'suc'}}">Từ khóa phụ nằm trong mô tả</li>
                                    </ul>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="form-group">
                                <input type="checkbox" class="css-checkbox" id="no-index" ng-model="postItem.no_index">
                                <label for="no-index" class="css-label lite-blue-check">
                                    <small class="txt-14 no-padding">No index</small>
                                </label>

                                <input type="checkbox" class="css-checkbox" id="no-follow" ng-model="postItem.no_follow">
                                <label style="margin-left: 20px"  for="no-follow" class="css-label lite-blue-check">
                                    <small class="txt-14 no-padding">No follow</small>
                                </label>
                            </div>

                            <div class="form-group" id="evaluate">
                                <div><label style="font-size: 20px">Điểm SEO: </label>
                                    <span id="full-span-score"><span id="score-seo">@{{currentScore}}</span>/@{{totalScore}}</span>
                                </div>
                                <ul class="error-list col-md-12">
                                    <li class="suc">Danh sách liệt kê</li>
                                    <li class="suc">Kiểm tra chính tả</li>
                                    <li class="suc">Có các thẻ H1, H2, H3</li>
                                    <li class="suc">Kiểm tra ảnh thumbnail khi share ảnh lên Facebook</li>
                                    <li class="@{{!keyWordInAltImg ? 'err' : 'suc'}}">Thẻ alt của ảnh trong nội dung chứa từ khóa chính</li>
                                    <li class="@{{!existsInternalLinkInContent ? 'err' : 'suc'}}">Nội dung chứa link trỏ tới các trang trong website</li>
                                    <li class="@{{!notExistsInternalLinkInContent ? 'err' : 'suc'}}">Nội dung chứa link trỏ tới các trang bên ngoài website</li>
                                    <li class="@{{!hasBUITagInContent ? 'err' : 'suc'}}">Từ khóa trong nội dung bài viết được bôi đậm, in nghiêng, gạch chân.</li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="tab-pane fade @{{(typeTab == 'shareFB') ? 'in active' : ''}}">

                            <div class="form-group">
                                <h5>Xem trước</h5>
                                <style type="text/css">
                                    .facebook-pre {
                                        margin: 0 0 10px;
                                        padding: 11px 0;
                                        border-bottom: 1px hidden #fff;
                                        border-radius: 2px;
                                        box-shadow: 0 1px 2px rgba(0,0,0,.2);
                                        font-family: Helvetica, Arial, sans-serif;
                                    }
                                    .facebook-title {
                                        font-size: 18px;
                                        font-weight: 400;
                                        padding: 5px 10px;
                                    }
                                    .facebook-descripton {
                                        padding: 0 10px;
                                        color: #b4b6b8
                                    }
                                    .facebook-slug {
                                        color: #a1a5ac;
                                        font-size: 12px;
                                        padding: 5px 10px;
                                        text-transform: uppercase;
                                    }
                                </style>
                                <div class="content-preview facebook-pre col-md-8" style="padding-top: 0">
                                    <img ng-if="postItem.seo_facebook_image != NULL" ng-src="@{{postItem.seo_facebook_image}}" />
                                    <div class="facebook-title">@{{postItem.seo_facebook_title}}</div>
                                    <div class="facebook-descripton" id="facebook-descripton">@{{postItem.seo_facebook_description}}</div>
                                    <div class="facebook-slug">@{{domainName}}</div>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="form-group">
                                <label for="">Tiêu đề</label>
                                <div class="">
                                    <input class="form-control" placeholder="Tiêu đề" type="text" name="seo_facebook_title" ng-model="postItem.seo_facebook_title">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="">Mô tả</label>
                                <div class="">
                                    <textarea class="form-control" placeholder="Mô tả" type="text" name="seo_facebook_description" ng-model="postItem.seo_facebook_description"></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="">Hình đại diện</label>
                                <div class="">
                                    <input class="form-control" placeholder="Đường dẫn ảnh" type="text" name="seo_facebook_image" ng-model="postItem.seo_facebook_image">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="">
                                    <button class="btn btn-primary pull-left" ng-click="getModalLibraries('image', 'facebook')"><i class="fa fa-plus"></i> Thêm từ thư viện</button>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>

                        <div class="tab-pane fade @{{(typeTab == 'utm') ? 'in active' : ''}}">

                            <ng-form method="" id="frm-utm" accept-charset="UTF-8" name="formAddUtm">
                                <div class="form-group">
                                    <h5>Campaign URL</h5>
                                    <div class="content-preview">
                                        <div class="seo-slug" ng-show="utmItem.campaign_source">
                                            <span id="seo-slug">{{ URL::to('') }}/@{{categorySlug}}/@{{slugView}}.html?utm_source=@{{utmItem.campaign_source | spaceless}}@{{utmItem.campaign_medium ? '&utm_medium=' + (utmItem.campaign_medium | spaceless) : ''}}@{{utmItem.campaign_name ? '&utm_campaign=' + (utmItem.campaign_name | spaceless) : ''}}@{{utmItem.campaign_term ? '&utm_term=' + (utmItem.campaign_term | spaceless) : ''}}@{{utmItem.campaign_content ? '&utm_content=' + (utmItem.campaign_content | spaceless) : ''}}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group" ng-class="{true: 'has-error'}[submittedUtm && formAddUtm.campaign_source.$invalid]">
                                    <label for="campaign_source">
                                        Campaign source (*) <br />
                                        <small>The referrer: (e.g. google, newsletter)</small>
                                    </label>
                                    <div class="">
                                        <input class="form-control" placeholder="" type="text" name="campaign_source" ng-model="utmItem.campaign_source">
                                    </div>
                                </div>
                                <div class="form-group" ng-class="{true: 'has-error'}[submittedUtm && formAddUtm.campaign_medium.$invalid]">
                                    <label for="campaign_medium">
                                        Campaign medium <br />
                                        <small>Marketing medium: (e.g. cpc, banner, email)</small>
                                    </label>
                                    <div class="">
                                        <input class="form-control" placeholder="" type="text" name="campaign_medium" ng-model="utmItem.campaign_medium">
                                    </div>
                                </div>
                                <div class="form-group" ng-class="{true: 'has-error'}[submittedUtm && formAddUtm.campaign_name.$invalid]">
                                    <label for="campaign_name">
                                        Campaign name<br />
                                        <small>Product, promo code, or slogan (e.g. spring_sale)</small>
                                    </label>
                                    <div class="">
                                        <input class="form-control" placeholder="" type="text" name="campaign_name" ng-model="utmItem.campaign_name">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="campaign_term">
                                        Campaign term <br />
                                        <small>Identify the paid keywords</small>
                                    </label>
                                    <div class="">
                                        <input class="form-control" placeholder="" type="text" name="campaign_term" ng-model="utmItem.campaign_term">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="campaign_content">
                                        Campaign content <br />
                                        <small>Use to differentiate ads</small>
                                    </label>
                                    <div class="">
                                        <input class="form-control" placeholder="" type="text" name="campaign_content" ng-model="utmItem.campaign_content">
                                    </div>
                                </div>
                            </ng-form>
                        </div>
                    </div>
                </div>

                <div class="alert alert-error alert-danger" ng-show="errors" ng-repeat="error in errors">
                    @{{error}}
                </div>

                <div class="lst-btn">
                    <button class="btn btn-primary" ng-click="submit(formAddPost.$valid)">
                        @if (!$item->id) 
                            <span><i class="fa fa-plus"></i> Thêm</span>
                        @else
                            <span><i class="fa fa-pencil-square-o"></i> Cập nhật</span>
                        @endif
                    </button>
                </div>
            </div>
            <div class="col-md-3 right-sidebar">
                <div class="category-show">
                    <div class="form-group" ng-class="{true: 'has-error'}[submitted && formAddPost.status.$invalid]">
                        <label for="name">Trạng thái (*)</label>
                        <select class="form-control" style="text-transform: capitalize;" name="status" ng-model="postItem.status" required="true">
                            <option value="">Trạng thái</option>
                            <option ng-repeat="(key, value) in status" value="@{{key}}">@{{value}}</option>
                        </select>
                        <label class="control-label" ng-show="submitted && formAddPost.status.$error.required">
                            Bạn chưa chọn trạng thái
                        </label>
                    </div>

                    <div class="alert alert-error alert-danger" ng-show="errors" ng-repeat="error in errors">
                        @{{error}}
                    </div>

                    <style>
                        .glyphicon-chevron-right:before {
                            content: "\e080";
                        }

                        .glyphicon-chevron-left:before {
                            content: "\e079";
                        }
                    </style>
                    <div class="form-group">
                        <label for="">Ngày đăng</label>
                        <div class="">
                            <input type="text" class="form-control"
                                   name="published_at"
                                   uib-datepicker-popup="@{{format}}"
                                   ng-click="open($event, published_at)" ng-model="postItem.published_at"
                                   is-open="opened[published_at]"
                                   datepicker-options="dateOptions"
                                   placeholder="@{{format}}" close-text="Close"/>
                        </div>
                    </div>
                    <button class="btn btn-primary" ng-click="submit(formAddPost.$valid)">
                        @if (!$item->id) 
                            <span><i class="fa fa-plus"></i> Thêm</span>
                        @else
                            <span><i class="fa fa-pencil-square-o"></i> Cập nhật</span>
                        @endif
                    </button>

                    <div class="clearfix"></div>
                </div>

                @if ($type == 'post' || strpos($type, 'post_'))
                <div class="category-show" ng-class="{true: 'has-error'}[submitted && invalidCategory]">
                    <label for="name">Danh mục</label>
                    <script type="text/ng-template" id="subSelect">
                        <input type="checkbox" class="css-checkbox ckb-cate" id="checkbox@{{value.id}}" value="@{{value.id}}" ng-click="selectCategory(value)">
                        <label for="checkbox@{{value.id}}" class="css-label lite-blue-check" ng-click="chooseCategory(value.id)">
                            <small class="txt-14 no-padding">@{{value.name}}</small>
                        </label>
                        
                        <ul ng-if="value.subFolder" class="collapse in">
                            <li class="category-item" style="padding-bottom: 0px; overflow: visible;" ng-repeat="(key, value) in value.subFolder | orderBy:'id'" ng-include="'subSelect'"></li>
                            <div class="clearfix"></div>
                        </ul>
                        <div class="clearfix"></div>
                    </script>
                    <ul class="select-menu-parrent select-cat">
                        <li class="select-menu-item category-item" ng-repeat="(index, value) in categoriesTree | orderBy:'id'" ng-include="'subSelect'" style="@{{value.subFolder.length > 0 ? 'padding-bottom: 0' : ''}}"></li>
                        <div class="clearfix"></div>
                    </ul>
                    <label class="control-label" ng-show="submitted && invalidCategory">
                        Bạn chưa chọn danh mục
                    </label>
                </div>
                <div class="tag-show">
                    <label for="name">Thêm từ khóa</label>
                    <div class="">
                        <select class="key-words" name="key_word" multiple="" ng-model="postItem.tags"></select>
                        <div class="list-tags">
                            <div class="tagchecklist">
                                <span ng-repeat="tag in listTags track by $index">
                                    <button type="button" id="post_tag-check-num-0" class="ntdelbutton" ng-click="removeTag(tag)">
                                        <span class="remove-tag-icon" aria-hidden="true"></span>
                                    </button>&nbsp;@{{tag}}
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="img-feature">
                    <label for="name">Hình đại diện</label>
                    <div class="image-feature">
                        <img ng-src="@{{postItem.image_feature.src}}" alt="@{{postItem.image_feature.title}}">
                    </div>
                    <div>
                        <a href="javascript:void(0)" ng-click="getModalLibraries('image', 'feature')">
                            <span ng-show="imageFeature">Thay đổi</span>
                            <span ng-show="!imageFeature">Chọn</span>
                            hình đại diện
                        </a>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </form>
</div>

@endsection

@section('script')
    {!! Html::script('backend/app/components/post/PostService.js?v='.getVersionScript()) !!}
    {!! Html::script('backend/app/components/post/PostController.js?v='.getVersionScript()) !!}
    {!! Html::script('backend/app/components/library/LibraryService.js?v='.getVersionScript()) !!}
    {!! Html::script('backend/app/shared/file-upload/FileService.js?v='.getVersionScript()) !!}
    {!! Html::script('backend/app/shared/file-upload/FileUploadDirective.js?v='.getVersionScript()) !!}
    {!! Html::script('backend/app/components/setting/SettingService.js?v='.getVersionScript()) !!}
@endsection


