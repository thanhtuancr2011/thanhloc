<?php

Route::group(['middleware' => ['web', 'status']], function () {

    /* Admin route */
    Route::group(['prefix' => 'admin'], function () {
        // Post route
        Route::get('post/library', 'Core\Modules\Post\Http\Controllers\PostController@library');
        Route::resource('post', 'Core\Modules\Post\Http\Controllers\PostController');
    });

    /* Api route */
    Route::group(['prefix' => 'api'], function () {
        // Post route
        Route::post('post/posts', 'Core\Modules\Post\Http\Controllers\Api\PostController@posts');
        Route::post('post/clone/{id}', 'Core\Modules\Post\Http\Controllers\Api\PostController@clonePost');
        Route::post('post/delete', 'Core\Modules\Post\Http\Controllers\Api\PostController@delete');
        Route::resource('post', 'Core\Modules\Post\Http\Controllers\Api\PostController');
    });

});

