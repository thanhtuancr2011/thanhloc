<div class="modal-header" id="modal-title-file">
    <h5 class="modal-title">Chi tiết</h5>
    <button aria-label="Close" data-dismiss="modal" class="close" type="button" ng-click="cancel()"><span aria-hidden="true">×</span></button>
</div>

<div class="modal-body" style="padding: 0px;" ng-init='fileItem={{json_encode($item)}}; folderPath="{{$folderPath}}"; mimeContentTypes={{json_encode($mimeContentTypes)}}'>
    <div class="innerAll">
        <div class="col-md-9 left-detail" style="overflow: hidden; overflow-y: scroll;">
            <div class="col-md-12" style="margin-top: 20px; padding: 0">
                <!-- When file is image -->
                <form id="form" ng-if="mimeContentTypes[fileItem.file_name.split('.').pop()]['group'] == 'Image'">
                    {{-- <button type="button" class="btn btn-default none-radius" ng-click="applyCrop()"><i class="fa fa-crop" aria-hidden="true"></i></button> --}}
                    <div id="views"></div>
                    {{-- <div class="form-group">
                        <button class="btn btn-default none-radius" ng-click="cancel()">
                            <i class="fa fa-times"></i> Hủy
                        </button>
                        <button class="btn btn-primary none-radius" ng-click="saveImage()" ng-disabled="dsbSaveBtn">
                            <i class="fa fa-pencil-square-o"></i>
                            <span>Cập nhật</span>
                        </button>
                    </div>  --}}     
                </form>
                <!-- When file not image -->
                <i ng-if="mimeContentTypes[fileItem.file_name.split('.').pop()]['group'] != 'Image'" style="display: block; text-align: center;" class="@{{mimeContentTypes[fileItem.file_name.split('.').pop()]['class']}} fa-3" aria-hidden="true"></i>
            </div>
        </div>

        <div class="col-md-3 right-detail">
            <div class="innerLR">
                <div class="" style="margin: 10px 0 20px 0;">
                    <div class="filename">
                        <label for="title">Loại tập tin: </label> 
                        <span style="color: #666;">{{$item->type}}</span>
                    </div>
                    <div class="filename">
                        <label for="title">Ngày tạo: </label> 
                        <span style="color: #666;">{{$item->created_at}}</span>
                    </div>
                </div>
                <form method="POST" action="" accept-charset="UTF-8" name="formAddLibrary">   
                    <div class="form-group">

                        <div class="form-group" ng-class="{true: 'has-error'}[submitted && formAddLibrary.last_name.$invalid]">
                            <label for="last_name">Đường dẫn</label>
                            <div class="">
                                <input class="form-control" readonly type="text" value="@{{ folderPath + fileItem.file_name }}">
                            </div>
                        </div>

                        <div class="form-group" ng-class="{true: 'has-error'}[submitted && formAddLibrary.title.$invalid]">
                            <label for="title">Tên (*)</label>
                            <div class="">
                                <input class="form-control" placeholder="Tên" type="text" name="title" ng-model="fileItem.title" required="true">
                                <label class="control-label" ng-show="submitted && formAddLibrary.title.$error.required">
                                    Bạn chưa nhập tên
                                </label>
                            </div>
                        </div>

                        <div class="form-group" ng-class="{true: 'has-error'}[submitted && formAddLibrary.excerpt.$invalid]">
                            <label for="excerpt">Chú thích</label>
                            <div class="">
                                <textarea class="form-control" placeholder="Chú thích" type="text" name="excerpt" ng-model="fileItem.excerpt"></textarea>
                            </div>
                        </div>

                        <div class="form-group" ng-class="{true: 'has-error'}[submitted && formAddLibrary.description.$invalid]">
                            <label for="description">Mô tả</label>
                            <div class="">
                                <textarea class="form-control" placeholder="Mô tả" type="text" name="description" ng-model="fileItem.description"></textarea>
                            </div>
                        </div>

                        <div class="alert alert-error alert-danger" ng-show="errors" ng-repeat="error in errors">
                            @{{error}}
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="modal-footer">
    <button class="btn btn-default" ng-click="cancel()">
        <i class="fa fa-times"></i> Hủy
    </button>
    <button class="btn btn-primary" ng-click="submit(formAddLibrary.$valid)">
        <i class="fa fa-pencil-square-o"></i>
        <span>Cập nhật</span>
    </button>
</div>
