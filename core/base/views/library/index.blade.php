@extends('base::layouts.master')

@section('title')
    Thư viện
@endsection

@section('breadcrumb')
    <div class="row">
        <div class="container col-md-12 admin-breadcrumb">
            <div class="breadcrumb">
                <div class="col-md-6 no-padding">
                    <h3>Thư viện</h3>
                </div>
                <div class="col-md-6 no-padding content-end">
                    <span class="breadcrumb-item">Trang chủ</span>
                    <span class="breadcrumb-item active">Thư viện</span>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <style type="text/css">
        .glyphicon {
            color: #2196f3;
            margin-right: 5px;
        }
        .glyphicon-folder-open:before {
            content: "\e118";
        }
        .glyphicon-chevron-down:before {
            content: "\e114";
            line-height: 30px;
        }
        .glyphicon-chevron-right:before {
            content: "\e080";
            line-height: 30px;
        }
        .glyphicon-folder-close:before {
            content: "\e117";
        }
        span.fancytree-title {
            border: none!important;
            padding: 0!important;
            margin: 5px 0 0 5px!important;
            border-radius: inherit!important;
        }
        span.fancytree-active .fancytree-title, span.fancytree-selected .fancytree-title {
            padding: 0 5px!important;
        }
        .fancytree-treefocus span.fancytree-active .fancytree-title, 
        span.fancytree-active .fancytree-title, 
        span.fancytree-selected .fancytree-title {
            background: linear-gradient(to bottom, #20a2d2 0, #20a8d8 100%);
            color: #fff;
        }
    </style>
    <div ng-controller="LibraryController" ng-init="showStyle = 'list'; maxSizeUpload={{$maxSizeUpload}}; mimeContentTypes={{json_encode($mimeContentTypes)}}; folders={{json_encode($folders)}}; type='{{$type}}'">
        <div class="card-body">
            <!-- Header title and button -->
            <div class="group-search">
                <form class="frm-search" id="formSearch">
                    <input type="text" class="form-control" ng-model="searchOptions.searchText" ng-show="showStyle=='list'"
                           placeholder="Tìm kiếm..." ng-keypress="searchLibraries($event)"/>
                    <input type="text" class="form-control" ng-model="searchGridOptions.searchText" ng-show="showStyle=='grid'"
                           placeholder="Tìm kiếm..." ng-keypress="searchLibraries($event)"/>
                </form>
                <div class="group-btn">
                    <button type="button" class="btn btn-primary" ng-show="showStyle == 'grid'" ng-init="multipleChooice = false" ng-click="multipleChooice = !multipleChooice">
                        <span ng-show="multipleChooice">Hủy chọn</span>
                        <span ng-show="!multipleChooice">Chọn nhiều</span>
                    </button>

                    <button type="button" class="btn btn-danger" ng-show="showStyle == 'list' && showBtn" ng-click="removeFiles()"><i class="fa fa-trash-o"></i></button>
                    <button type="button" class="btn btn-danger" ng-show="showStyle == 'grid' && showBtnDlt" ng-click="removeFiles()"><i class="fa fa-trash-o"></i></button>
                </div>
                <div class="group-view">
                    <button class="btn @{{showStyle == 'list' ? 'btn-primary' : 'btn-default'}}" ng-click="changeStyle('list')">
                        <i class="icon-list" aria-hidden="true"></i>
                    </button>
                    <button class="btn @{{showStyle == 'grid' ? 'btn-primary' : 'btn-default'}}" ng-click="changeStyle('grid')">
                        <i class="icon-grid" aria-hidden="true"></i>
                    </button>
                </div>
            </div>

            <div class="clearfix"></div>

            <!-- File upload directive -->
            <file-upload id="uploadDirective" max-size-upload="maxSizeUpload" mime-content-types="mimeContentTypes" ng-model="filesUpload" multiple-file="true"></file-upload>

            <style type="text/css">
                .lst-asset .admin-breadcrumb {
                    padding: 0 15px;
                }
                .lst-asset .admin-breadcrumb .breadcrumb .breadcrumb-item {
                    font-size: 16px;
                }
            </style>
            <div class="row lst-asset">
                <div class="container col-md-12 admin-breadcrumb">
                    <div class="breadcrumb" style="border-left: none!important;">
                        <div class="col-md-6 no-padding">
                            <span class="breadcrumb-item @{{$last ? 'active' : ''}}" ng-repeat="directory in directoryPath">@{{directory.data.name}}</span>
                        </div>
                        <div class="col-md-6 no-padding group-btn" style="line-height: 45px; margin-top: 5px; margin-left: 0">
                            @if (Base::can('add.library'))
                            <button type="button" class="btn btn-primary pull-right" ng-click="getModalFolder('add')" title="Thêm thư mục">
                                <i class="fa fa-plus" aria-hidden="true"></i>
                            </button>
                            @endif

                            @if (Base::can('edit.library'))
                            <button type="button" ng-show="activeNode.title != 'root'" class="btn btn-primary pull-right" ng-click="getModalFolder('edit')" title="Chỉnh sửa thư mục">
                                <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                            </button>
                            @endif

                            @if (Base::can('delete.library'))
                            <button type="button" ng-show="activeNode.title != 'root'" class="btn btn-danger pull-right" ng-click="removeFolder()" title="Xóa thư mục">
                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </button>
                            @endif
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 no-padding">
                <div style="width: 250px; float: left; padding-right: 0; border: 1px solid #d1d1d1; margin-top: @{{showStyle == 'grid' ? '10px' : 0}};">
                    <div id="tree"></div>
                </div>
                <div style="width: calc(100% - 265px); float: left; padding-right: 0; margin-left: 15px;">
                    <!-- Table view -->
                    <table class="table table-bordered table-striped" ng-show="showStyle == 'list'">
                        <thead>
                            <th class="percent-5">#</th>
                            <th class="percent-5">
                                <input type="checkbox" class="css-checkbox ckb-all" id="checkbox-all" ng-click="toggleSelection('all')">
                                <label for="checkbox-all" class="css-label lite-blue-check"></label>
                            </th>
                            <th style="width: 40%">Tên tập tin</th>
                            <th>Tác giả</th>
                            <th>Ngày tạo</th>
                            <th></th>
                        </thead>
                        <tbody>
                            <tr ng-repeat="file in libraries track by $index" ng-class="{'bg-tr': $index%2 == 0}">

                                <td class="text-center">
                                    @{{ ($index + 1) + (pageOptions.currentPage - 1) * pageOptions.itemsPerPage }}
                                </td>

                                <td class="text-center cls-chk">
                                    <input type="checkbox" class="css-checkbox ckb" id="checkbox@{{$index}}" value="@{{file.id}}"
                                           ng-click="toggleSelection()">
                                    <label for="checkbox@{{$index}}" class="css-label lite-blue-check"></label>
                                </td>

                                <td>
                                    <i ng-show="mimeContentTypes[file.file_name.split('.').pop()]['group'] != 'Image'"
                                       class="file-upload @{{mimeContentTypes[file.file_name.split('.').pop()]['class']}} fa-3"
                                       aria-hidden="true"></i>
                                    <img ng-if="mimeContentTypes[file.file_name.split('.').pop()]['group'] == 'Image' && 
                                                  mimeContentTypes[file.file_name.split('.').pop()]['name'] != 'image/svg+xml' &&
                                                  mimeContentTypes[file.file_name.split('.').pop()]['name'] != 'image/vnd.microsoft.icon' &&
                                                  mimeContentTypes[file.file_name.split('.').pop()]['name'] != 'image/vnd.adobe.photoshop'"
                                         class="file-upload" ng-src="@{{folderPath}}@{{file.file_name}}"/>

                                    <img ng-if="mimeContentTypes[file.file_name.split('.').pop()]['group'] == 'Image' && 
                                                  (mimeContentTypes[file.file_name.split('.').pop()]['name'] == 'image/svg+xml' ||
                                                  mimeContentTypes[file.file_name.split('.').pop()]['name'] == 'image/vnd.microsoft.icon' ||
                                                  mimeContentTypes[file.file_name.split('.').pop()]['name'] == 'image/vnd.adobe.photoshop')"
                                         class="file-upload" ng-src="@{{folderPath}}@{{file.file_name}}"/>
                                    <div class="text-file">
                                        <div class="file-name">@{{file.file_name}}</div>
                                    </div>
                                </td>

                                <td>@{{authors[file.user_id]}}</td>

                                <td>
                                    @{{formatDate (file.created_at) | date : "yyyy/MM/dd"}}
                                </td>

                                <td class="text-center">
                                    <a href="javascript:void(0)" ng-click="editFile(file.id)" class="btn btn-primary"
                                       title="Chỉnh sửa"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                    <a href="javascript:void(0)" class="btn btn-danger" ng-click="removeFiles(file.id)"
                                       title="Xóa"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <pagination-directive ng-show="showStyle == 'list'"></pagination-directive>

                    <!-- Grid view -->
                    <div ng-show="showStyle == 'grid'">
                        <div class="row">
                            <div class="col-md-2 grid-view-media" style="height: @{{gridMediaHeight + 'px'}}"
                                 ng-repeat="fileGridView in librariesGridView track by $index"
                                 ng-click="chooseFile(fileGridView.id, $event)">
                                <button type="button" class="btn-check-file check-file-active want-active-@{{fileGridView.id}}"
                                        tabindex="-1">
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                </button>
                                <a class="check-file-active want-active-@{{fileGridView.id}}" href="javascript:void(0)">

                                    <!-- When file not image -->
                                    <i ng-show="mimeContentTypes[fileGridView.file_name.split('.').pop()]['group'] != 'Image'"
                                       class="thumbnail @{{mimeContentTypes[fileGridView.file_name.split('.').pop()]['class']}} fa-2"
                                       aria-hidden="true"></i>
                                    <span ng-show="mimeContentTypes[fileGridView.file_name.split('.').pop()]['group'] != 'Image'"
                                          class="txt-file-name">@{{fileGridView.file_name}}</span>

                                    <!-- When file is image -->
                                    <img ng-show="mimeContentTypes[fileGridView.file_name.split('.').pop()]['group'] == 'Image' && 
                                                  mimeContentTypes[fileGridView.file_name.split('.').pop()]['name'] != 'image/svg+xml' &&
                                                  mimeContentTypes[fileGridView.file_name.split('.').pop()]['name'] != 'image/vnd.microsoft.icon' &&
                                                  mimeContentTypes[fileGridView.file_name.split('.').pop()]['name'] != 'image/vnd.adobe.photoshop'"
                                         class="thumbnail" ng-src="@{{folderPath}}@{{fileGridView.file_name}}"/>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                <div class="clearfix"></div>
        </div>
    </div>

@endsection

@section('script')
    {!! Html::script('backend/app/components/library/AssetService.js?v='.getVersionScript()) !!}
    {!! Html::script('backend/app/components/library/LibraryService.js?v='.getVersionScript()) !!}
    {!! Html::script('backend/app/components/library/LibraryController.js?v='.getVersionScript()) !!}
    {!! Html::script('backend/app/shared/file-upload/FileService.js?v='.getVersionScript()) !!}
    {!! Html::script('backend/app/shared/file-upload/FileUploadDirective.js?v='.getVersionScript()) !!}
    {!! Html::script('backend/app/shared/pagination/PaginationDirective.js?v='.getVersionScript()) !!}
@endsection
