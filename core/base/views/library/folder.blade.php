<div class="modal-header">
    @if(!empty($item->id))
        <h4 class="modal-title">Chỉnh sửa</h4>
    @else
        <h4 class="modal-title">Thêm</h4>
    @endif
    <button aria-label="Close" data-dismiss="modal" class="close" type="button" ng-click="cancel()"><span aria-hidden="true">×</span></button>
</div>

<div class="modal-body">
    <div class="innerAll">
        <div class="innerLR">
            <form method="POST" action="{{{ URL::to('users') }}}" accept-charset="UTF-8" name="formAddFolder" ng-init='folderItem={{$item}}'>     
                <div class="form-group">
                    <div class="form-group" ng-class="{true: 'has-error'}[submitted && formAddFolder.name.$invalid]">
                        <label for="name">Tên thư mục (*)</label>
                        <div class="">
                            <input class="form-control" type="text" placeholder="Tên thư mục" name="name" ng-model="folderItem.name" required="true">
                            <label class="control-label" ng-show="submitted && formAddFolder.name.$error.required">
                                Mời nhập tên thư mục
                            </label>
                        </div>
                    </div>
                    <div class="alert alert-error alert-danger" ng-show="errors" ng-repeat="error in errors">
                        @{{error}}
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="modal-footer">
    <button class="btn btn-default" ng-click="cancel()"><i class="fa fa-times"></i> Hủy</button>
    <button class="btn btn-primary" ng-click="submit(formAddFolder.$valid)">
        <span>@if(!empty($item->id))<i class="fa fa-pencil-square-o"></i> Cập nhật @else<i class="fa fa-plus"></i> Thêm @endif</span>
    </button>
</div>
