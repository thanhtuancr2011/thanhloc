<div class="modal-header">
    Slide ảnh
    <button aria-label="Close" data-dismiss="modal" class="close" type="button" ng-click="cancel()"><span aria-hidden="true">×</span></button>
</div>

<div class="modal-body">
    <div class="innerAll">
        <div class="innerLR">
            <form method="POST" action="{{{ URL::to('users') }}}" accept-charset="UTF-8" name="formEditSlide">   
                <div class="form-group">
                    <div class="form-group" ng-class="{true: 'has-error'}[submitted && formEditSlide.title.$invalid]">
                        <label for="title">Tiêu đề (*)</label>
                        <div class="">
                            <input class="form-control" placeholder="Tiêu đề" type="text" name="title" ng-model="item.title" ng-required="true">
                            <label class="control-label" ng-show="submitted && formEditSlide.title.$error.required">
                                Tiêu đề bắt buộc nhập
                            </label>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="title">Loại</label>
                        <div class="">
                            <select class="form-control" name="type" ng-model="item.type" ng-init="item.type = item.type ? item.type : 'image'">
                                <option value="">Lựa chọn</option>
                                <option value="video">Link video</option>
                                <option value="image">Hình ảnh</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group" ng-class="{true: 'has-error'}[submitted && formEditSlide.link.$invalid]">
                        <label for="name">Link</label>
                        <div class="">
                            <input class="form-control" placeholder="Đường dẫn" type="text" name="link" ng-model="item.link" ng-required="true">
                            <label class="control-label" ng-show="submitted && formEditSlide.link.$error.required">
                                Bạn chưa nhập đường dẫn
                            </label>
                        </div>
                    </div>

                    <div class="" ng-if="item.type == 'image'">
                        <div class="form-group">
                            <label for="name">Nội dung</label>
                            <div class="">
                                <textarea class="form-control" id="idContent" ng-init="initTinyMCE('idContent')" name="content" ng-model="item.content"></textarea>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div class="form-group">
                            <label for="name">Hình ảnh</label>
                            <div class="img-feature" ng-show="item.url" style="border: none; background-image: url(@{{item.url}}); background-size: cover; background-position: center center; background-repeat: no-repeat; width: 100%; height: 200px; display: block; box-shadow: 1px 1px 1px #aaa;">
                            </div>
                            <div>
                                <a href="javascript:void(0)" ng-click="getModalLibraries('image', 'feature')">
                                    <span ng-show="item.url">Thay đổi</span>
                                    <span ng-show="!item.url">Chọn</span> hình ảnh
                                </a>
                            </div>
                            <div class="clearfix"></div>
                        </div> 
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="modal-footer">
    <button class="btn btn-default" ng-click="cancel()"><i class="fa fa-times"></i> Hủy </button>
    <button class="btn btn-primary" ng-click="submit(formEditSlide.$valid)">
        <span><i class="fa fa-pencil-square-o"></i> Cập nhật</span>
    </button>
</div>
