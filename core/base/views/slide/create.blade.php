@extends('base::layouts.master')

@section('title')
    Cấu hình banner & slider
@endsection

@section('breadcrumb')
    <div class="row">
        <div class="container col-md-12 admin-breadcrumb">
            <div class="breadcrumb">
                <div class="col-md-6 no-padding">
                    <h3>Cấu hình banner & slider</h3>
                </div>
                <div class="col-md-6 no-padding content-end">
                    <span class="breadcrumb-item">Trang chủ</span>
                    <span class="breadcrumb-item"><a href="{{URL::to('admin/image-slider')}}">Cấu hình banner & slider</a></span>
                    <span class="breadcrumb-item active">
                        {{$item->id ? 'Chỉnh sửa' : 'Thêm'}}
                    </span>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content')
<div class="card-body create-post-body" ng-controller="CreatePostController">  
    <form method="POST" id="frm-create-post" accept-charset="UTF-8" name="formAddPost" ng-init="mimeContentTypes={{json_encode($mimeContentTypes)}}; customFields={{json_encode($customFields)}}; maxSizeUpload={{$maxSizeUpload}}; status={{json_encode($status)}}; type='{{$type}}'; settingItem = {{json_encode($settingItem)}}; postItem = {{json_encode($item)}}" novalidate="">
        
        <div class="row">
            <div class="col-md-9">
                <input type="hidden" name="_token" value="csrf_token()" >
                <div class="form-group">
                    <input ng-show="false" type="text" name="post_type" ng-model="postItem.post_type" ng-init="postItem.post_type = '{{$type}}'" >

                    <div class="form-group" ng-class="{true: 'has-error'}[submitted && formAddPost.title.$invalid]">
                        <label for="title">Tên slide (*)</label>
                        <div class="">
                            <input class="form-control" placeholder="Tên slide" type="text" name="title" ng-model="postItem.title" required="true">
                            <label class="control-label" ng-show="submitted && formAddPost.title.$error.required">
                                Bạn chưa nhập tên cho slide
                            </label>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="name">Mô tả ngắn</label>
                        <div class="">
                            <textarea class="form-control" placeholder="Mô tả ngắn" type="text" name="excerpt" ng-model="postItem.excerpt"></textarea>
                        </div>
                    </div>
                    <div class="form-group" ng-repeat="basicField in customFields.basic" ng-class="{true: 'has-error'}[submitted && formAddPost[basicField['name']].$invalid]">

                        <!-- Label element -->
                        <label ng-if="basicField.element != 'button'" for="seo_title">@{{basicField.label}}</label>

                        <!-- Button element -->
                        <button class="btn btn-primary" ng-if="basicField.element == 'button'"
                                ng-click="getModalLibraries('image', 'customField', false, ('basic.' + basicField.name))">
                            <i class="fa fa-file-image-o" aria-hidden="true"></i> @{{basicField.label}}
                        </button>

                        <!-- Input element -->
                        <input ng-if="basicField.element == 'input'" class="form-control" type="@{{basicField.type}}" name="@{{basicField.name}}" ng-model="basicField.value" placeholder="@{{basicField.label}}">

                        <!--Textarea element -->
                        <textarea ng-if="basicField.element == 'textarea'"
                                ng-init="initTinyMCE('basic', basicField.name)"
                                placeholder="@{{basicField.label}}" 
                                name="@{{basicField.name}}" 
                                class="form-control" id="basic-@{{basicField.name}}"
                                ng-model="basicField.value">
                        </textarea> 

                        <!--Multiple element -->
                        <div class="" ng-if="basicField.type == 'multiple'" ng-repeat="field in basicField.fields">
                            <!-- Input element -->
                            <input ng-if="field.element == 'input'" style="margin-top: 15px;" class="form-control" type="@{{field.type}}" name="@{{field.name}}" ng-model="field.value" placeholder="@{{field.label}}">
                        </div>

                        <!-- Select element -->
                        <select ng-if="basicField.element == 'select'" 
                                name="@{{basicField.name}}" class="form-control" 
                                ng-model="basicField.value" required="true">
                            <option value="">Lựa chọn</option>
                            <option ng-repeat="(key, value) in basicField.options" value="@{{key}}">@{{value}}</option>
                        </select>

                        <!--Select element -->
                        <div class="" ng-if="basicField.type == 'multiple'" ng-repeat="field in basicField.fields">
                            <!-- Input element -->
                            <input ng-if="field.element == 'input'" style="margin-top: 15px;" class="form-control" type="@{{field.type}}" name="@{{field.name}}" ng-model="field.value" placeholder="@{{field.label}}">
                        </div>
                        <label class="control-label" ng-show="submitted && formAddPost[basicField['name']].$error.required">
                            @{{basicField['label']}} bắt buộc nhập
                        </label>
                    </div>
                    <style type="text/css">
                        #sortable {
                            padding: 0;
                        }
                        #sortable .image {
                            height: 120px;
                            padding: 0 10px 0 0;
                            margin-bottom: 10px;
                            cursor: pointer;
                        }
                        #sortable .image:hover .slide-media-container {
                            background-color: rgba(0, 0, 0, 0.5)!important;
                            opacity: 0.2;
                        }
                        .icon {
                            top: 50%;
                            transform: translateX(-50%) translateY(-50%);
                            left: 50%;
                            position: absolute;
                            display: none;
                        }
                        #sortable .image:hover .icon {
                            display: block!important;
                        }
                        .new-slide {
                            height: 85px;
                            display: block;
                            border: 1px dashed #e6e6e6;
                            border-bottom: 0;
                            position: relative;
                        }
                        .slide-link-content {
                            height: 35px;
                            background: #eee;
                        }
                        .slide-link-content span {
                            line-height: 35px;
                        }
                        .btn-add-slide {
                            position: absolute;
                            top: 50%;
                            transform: translateX(-50%) translateY(-50%);
                            left: 50%;
                            color: #e9e9e9;
                        }
                        .new-slide:hover .btn-add-slide {
                            color: #ccc;
                        }
                        
                    </style>
                    <div class="form-group">
                        <div class="col-md-12" id="sortable">
                            <div class="col-md-3 image img-slide" ng-repeat="image in customFields.slide.images track by $index" data-image="@{{image}}">
                                <span class="slide-media-container" style="background-image: url(@{{image.url}}); background-size: cover; background-position: center center; background-repeat: no-repeat; width: 100%; height: 120px; display: block; box-shadow: 1px 1px 1px #aaa;">
                                </span>
                                <div class="icon">
                                    <i class="fa fa-trash-o btn btn-primary" aria-hidden="true" ng-click="removeSlide($index)"></i>
                                    <i class="fa fa-pencil btn btn-primary" aria-hidden="true" ng-click="getModalSlide($index)"></i>
                                </div>
                            </div>
                            <div class="col-md-3 image add-slide" ng-click="getModalSlide()">
                                <div class="new-slide">
                                    <i class="btn-add-slide fa fa-plus fa-2"></i>
                                </div>
                                <div class="slide-link-content text-center">
                                    <span>Thêm ảnh hoặc video</span>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>

                <div class="alert alert-error alert-danger" ng-show="errors" ng-repeat="error in errors">
                    @{{error}}
                </div>

                <div class="lst-btn">
                    <button class="btn btn-primary" ng-click="submit(formAddPost.$valid)">
                        @if (!$item->id) 
                            <span><i class="fa fa-plus"></i> Thêm</span>
                        @else
                            <span><i class="fa fa-pencil-square-o"></i> Cập nhật</span>
                        @endif
                    </button>
                </div>
            </div>
            <div class="col-md-3 right-sidebar">
                <div class="category-show">
                    <div class="form-group" ng-class="{true: 'has-error'}[submitted && formAddPost.status.$invalid]">
                        <label for="name">Trạng thái (*)</label>
                        <select class="form-control" style="text-transform: capitalize;" name="status" ng-model="postItem.status" required="true">
                            <option value="">Chọn trạng thái</option>
                            <option ng-repeat="(key, value) in status" value="@{{key}}">@{{value}}</option>
                        </select>
                        <label class="control-label" ng-show="submitted && formAddPost.status.$error.required">
                            Bạn chưa chọn trạng thái
                        </label>
                    </div>

                    <div class="alert alert-error alert-danger" ng-show="errors" ng-repeat="error in errors">
                        @{{error}}
                    </div>

                    <button class="btn btn-primary" ng-click="submit(formAddPost.$valid)">
                        @if (!$item->id) 
                            <span><i class="fa fa-plus"></i> Thêm</span>
                        @else
                            <span><i class="fa fa-pencil-square-o"></i> Cập nhật</span>
                        @endif
                    </button>

                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </form>
</div>

@endsection

@section('script')

    {!! Html::script('backend/app/components/slide/PostService.js?v='.getVersionScript()) !!}
    {!! Html::script('backend/app/components/slide/PostController.js?v='.getVersionScript()) !!}
    {!! Html::script('backend/app/components/library/LibraryService.js?v='.getVersionScript()) !!}
    {!! Html::script('backend/app/shared/file-upload/FileService.js?v='.getVersionScript()) !!}
    {!! Html::script('backend/app/shared/file-upload/FileUploadDirective.js?v='.getVersionScript()) !!}
    {!! Html::script('backend/app/components/setting/SettingService.js?v='.getVersionScript()) !!}
    
@endsection


