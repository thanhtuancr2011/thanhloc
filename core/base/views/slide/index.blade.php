@extends('base::layouts.master')

@section('title')
    Banner & slider
@endsection

@section('breadcrumb')
    <div class="row">
        <div class="container col-md-12 admin-breadcrumb">
            <div class="breadcrumb">
                <div class="col-md-6 no-padding">
                    <h3>Banner & slider</h3>
                </div>
                <div class="col-md-6 no-padding content-end">
                    <span class="breadcrumb-item">Trang chủ</span>
                    <span class="breadcrumb-item active">Banner & slider</span>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content')

    <div class="card-body" ng-controller="PostController" ng-init="type='{{$type}}'; status={{json_encode($status)}}; categories={{json_encode($categories)}}">
        <!-- Header title and button -->
        <div class="group-search">
            <form class="frm-search" id="formSearch">
                <input type="text" class="form-control" ng-model="searchOptions.searchText" placeholder="Tìm kiếm..." ng-keypress="searchPosts($event)"/>
                <select class="form-control"
                        ng-model="searchOptions.status_code"
                        ng-change="searchPosts('ng-change')"
                        ng-options="option.code as option.name for option in status">
                        <option value="">Trạng thái</option>
                        <option ng-repeat="(key, value) in status" value="@{{value.code}}">@{{value.name}}</option>
                </select>
            </form>
            <div class="group-btn">
                <a href="{{ URL::to('admin/image-slider/create') }}" class="btn btn-primary"><i class="fa fa-plus"></i></a>
                <a href="javascript:void(0)" class="btn btn-danger" ng-show="showBtn" ng-click="removePost()">
                    <i class="fa fa-trash-o"></i>
                </a>
            </div>
        </div>

        <div class="clearfix"></div>

        <table class="table table-bordered table-hover table-striped">
            <thead>
                <th style="text-align: center; width: 5%;">#</th>
                <th style="text-align: center; width: 5%;">
                    <input type="checkbox" class="css-checkbox ckb-all" id="checkbox-all" ng-click="toggleSelection('all')">
                    <label for="checkbox-all" class="css-label lite-blue-check"></label>
                </th>
                <th style="width: 20%">Tên slider</th>
                <th>Trạng thái</th>
                <th>Ngày tạo</th>
                <th></th>
            </thead>
            <tbody>
            <tr ng-repeat="post in posts track by $index" ng-class="{'bg-tr': $index%2 == 0}">

                <td class="text-center">
                    @{{ ($index + 1) + (pageOptions.currentPage - 1) * pageOptions.itemsPerPage }}
                </td>

                <td class="text-center cls-chk">
                    <input type="checkbox" class="css-checkbox ckb" id="checkbox@{{$index}}" value="@{{post.id}}"
                           ng-click="toggleSelection()">
                    <label for="checkbox@{{$index}}" class="css-label lite-blue-check"></label>
                </td>

                <td>@{{post.title}}</td>

                <td>@{{lstStatus[post.status]}}</td>

                <td>
                    @{{formatDate (post.created_at) | date : "yyyy/MM/dd"}}
                </td>

                <td class="text-center">
                    <a href="/admin/image-slider/@{{post.id}}/edit" class="btn btn-primary"
                       title="Chỉnh sửa">
                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                    </a>
                    <a href="javascript:void(0)" class="btn btn-danger" ng-click="removePost(post.id)"
                       title="Xóa">
                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                    </a>
                </td>
            </tr>
            </tbody>
        </table>
        <pagination-directive></pagination-directive>
    </div>

@endsection

@section('script')
    {!! Html::script('backend/app/components/slide/PostService.js?v='.getVersionScript()) !!}
    {!! Html::script('backend/app/components/slide/PostController.js?v='.getVersionScript()) !!}
    {!! Html::script('backend/app/shared/pagination/PaginationDirective.js?v='.getVersionScript()) !!}
@endsection
