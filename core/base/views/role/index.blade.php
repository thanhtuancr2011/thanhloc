@extends('base::layouts.master')

@section('title')
    Phân quyền
@endsection

@section('breadcrumb')
    <div class="row">
        <div class="container col-md-12 admin-breadcrumb">
            <div class="breadcrumb">
                <div class="col-md-6 no-padding">
                    <h3>Phân quyền</h3>
                </div>
                <div class="col-md-6 no-padding content-end">
                    <span class="breadcrumb-item">Trang chủ</span>
                    <span class="breadcrumb-item active">Phân quyền</span>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content')

    <div class="card-body" ng-controller="RoleController">
        <!-- Header title and button -->
        <div class="group-search">
            <form class="frm-search" id="formSearch">
                <input type="text" class="form-control" ng-model="searchOptions.searchText" placeholder="Tìm kiếm..." ng-keypress="searchRoles($event)"/>
            </form>
            <div class="group-btn">

                @if (Base::can('add.role'))
                <a href="{{URL::to('admin/role/create')}}" class="btn btn-primary">
                    <i class="fa fa-plus"></i>
                </a>
                @endif

                @if (Base::can('delete.role'))
                <button type="button" class="btn btn-danger" ng-show="showBtn" ng-click="removeRole()">
                    <i class="fa fa-trash-o"></i>
                </button>
                @endif
            </div>
        </div>

        <div class="clearfix"></div>
        <table class="table table-bordered table-striped">
            <thead>
                <th class="percent-5">#</th>
                <th class="percent-5">
                    <input type="checkbox" class="css-checkbox ckb-all" id="checkbox-all" ng-click="toggleSelection('all')">
                    <label for="checkbox-all" class="css-label lite-blue-check"></label>
                </th>
                <th>Tên quyền</th>
                <th>Mô tả</th>
                <th>Ngày tạo</th>
                <th></th>
            </thead>
            <tbody>
                <tr ng-repeat="role in roles track by $index" ng-class="{'bg-tr': $index%2 == 0}">

                    <td class="text-center">
                        @{{ ($index + 1) + (pageOptions.currentPage - 1) * pageOptions.itemsPerPage }}
                    </td>

                    <td class="text-center cls-chk">
                        <input type="checkbox" class="css-checkbox ckb" id="checkbox@{{$index}}" value="@{{role.id}}"
                               ng-click="toggleSelection()">
                        <label for="checkbox@{{$index}}" class="css-label lite-blue-check"></label>
                    </td>

                    <td>@{{role.name}}</td>

                    <td>@{{role.description}}</td>

                    <td>
                        @{{formatDate (role.created_at) | date : "yyyy/MM/dd"}}
                    </td>

                    <td class="text-center">
                        @if (Base::can('edit.role'))
                        <a href="/admin/role/@{{role.id}}/edit" class="btn btn-primary" title="Chỉnh sửa">
                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                        </a>
                        @endif

                        @if (Base::can('delete.role'))
                        <a href="javascript:void(0)" class="btn btn-danger" ng-click="removeRole(role.id)" title="Xóa">
                           <i class="fa fa-trash-o" aria-hidden="true"></i>
                        </a>
                        @endif
                    </td>
                </tr>
            </tbody>
        </table>
        <pagination-directive></pagination-directive>
    </div>

@endsection

@section('script')
    {!! Html::script('backend/app/components/role/RoleService.js?v='.getVersionScript()) !!}
    {!! Html::script('backend/app/components/role/RoleController.js?v='.getVersionScript()) !!}
    {!! Html::script('backend/app/shared/pagination/PaginationDirective.js?v='.getVersionScript()) !!}
@endsection
