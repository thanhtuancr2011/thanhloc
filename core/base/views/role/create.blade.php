@extends('base::layouts.master')

@section('title')
    {{$item->id ? 'Chỉnh sửa quyền' : 'Thêm quyền'}}
@endsection

@section('breadcrumb')
    <div class="row">
        <div class="container col-md-12 admin-breadcrumb">
            <div class="breadcrumb">
                <div class="col-md-6 no-padding">
                    <h3>Phân quyền</h3>
                </div>
                <div class="col-md-6 no-padding content-end">
                    <span class="breadcrumb-item">Trang chủ</span>
                    <span class="breadcrumb-item"><a href="{{URL::to('admin/role')}}">Quyền</a></span>
                    <span class="breadcrumb-item active">{{$item->id ? 'Chỉnh sửa' : 'Thêm'}}</span>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content')

    <div class="card-body" ng-controller="ModalCreateRoleCtrl">

        <div class="clearfix"></div>
        <form method="POST" accept-charset="UTF-8" name="formAddRole" ng-init='roleItem={{$item}}; permissions={{$permissions}}; status={{$status}}' novalidate="">
            <div class="form-group">
                <div class="form-group" ng-class="{true: 'has-error'}[submitted && formAddRole.name.$invalid]">
                    <label for="name">Tên quyền (*)</label>
                    <div class="">
                        <input class="form-control" placeholder="Tên quyền" type="text" name="name" ng-model="roleItem.name" required="true">
                        <label class="control-label" ng-show="submitted && formAddRole.name.$error.required">
                            Bạn chưa nhập tên quyền
                        </label>
                    </div>
                </div>

                <div class="form-group">
                    <label for="name">Mô tả</label>
                    <div class="">
                        <textarea class="form-control" placeholder="Mô tả" type="text" name="description" ng-model="roleItem.description"></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label for="name">Thêm quyền</label>
                    <table class="table table-bordered table-striped">
                        <thead>
                            <th></th>
                            <th style="width: 100px;" class="text-center">Hiển thị</th>
                            <th style="width: 100px;" class="text-center">Thêm</th>
                            <th style="width: 100px;" class="text-center">Sửa</th>
                            <th style="width: 100px;" class="text-center">Xóa</th>
                        </thead>
                        <tbody>
                            @php
                                $trans = trans('base-lang::role');
                            @endphp
                            <tr ng-repeat="(key, value) in permissions | groupBy: 'table'" ng-class="{'bg-tr': $index%2 == 0}">

                                <td ng-init="trans = {{json_encode($trans)}}">
                                    @{{trans[key] ? trans[key] : key}}
                                </td>

                                <td class="text-center">
                                    <span class="clear" ng-if="lstPers[key]['index']">
                                        <input type="checkbox" class="css-checkbox ckb-per" value="@{{lstPers[key]['index'].id}}" id="checkbox@{{lstPers[key]['index'].id}}">
                                        <label class="css-label lite-blue-check check-box-cat" for="checkbox@{{lstPers[key]['index'].id}}"></label>
                                    </span>
                                </td>

                                <td class="text-center">
                                    <span class="clear" ng-if="lstPers[key]['add']">
                                        <input type="checkbox" class="css-checkbox ckb-per" value="@{{lstPers[key]['add'].id}}" id="checkbox@{{lstPers[key]['add'].id}}">
                                        <label class="css-label lite-blue-check check-box-cat" for="checkbox@{{lstPers[key]['add'].id}}"></label>
                                    </span>
                                </td>

                                <td class="text-center">
                                    <span class="clear" ng-if="lstPers[key]['edit']">
                                        <input type="checkbox" class="css-checkbox ckb-per" value="@{{lstPers[key]['edit'].id}}" id="checkbox@{{lstPers[key]['edit'].id}}">
                                        <label class="css-label lite-blue-check check-box-cat" for="checkbox@{{lstPers[key]['edit'].id}}"></label>
                                    </span>
                                </td>

                                <td class="text-center">
                                    <span class="clear" ng-if="lstPers[key]['delete']">
                                        <input type="checkbox" class="css-checkbox ckb-per" value="@{{lstPers[key]['delete'].id}}" id="checkbox@{{lstPers[key]['delete'].id}}">
                                        <label class="css-label lite-blue-check check-box-cat" for="checkbox@{{lstPers[key]['delete'].id}}"></label>
                                    </span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div class="form-group">
                    <label for="name">Trạng thái duyệt bài viết</label>
                    <div class="list-status">
                        <span ng-repeat="sts in status" >
                            <input type="checkbox" class="css-checkbox ckb-status" value="@{{sts.id}}" id="checkboxStatus@{{sts.id}}">
                            <label class="css-label lite-blue-check check-box-cat" for="checkboxStatus@{{sts.id}}" style="text-transform: capitalize">
                                @{{sts.name}}
                            </label>
                        </span>
                    </div>
                </div>

                <div class="alert alert-error alert-danger" ng-show="errors" ng-repeat="error in errors">
                    @{{error}}
                </div>
            </div>
            <div class="clearfix"></div>
            <button class="btn btn-primary pull-right" ng-click="submit(formAddRole.$valid)">
                <span>
                    @if(!empty($item->id) && Base::can('edit.role'))
                        @if (Base::can('edit.role'))
                            <i class="fa fa-pencil-square-o"></i> Cập nhật
                        @endif
                    @else
                        @if (Base::can('add.role'))
                            <i class="fa fa-plus"></i> Thêm
                        @endif
                    @endif
                </span>
            </button>
        </form>
        <div class="clearfix"></div>
    </div>

@endsection

@section('script')
    {!! Html::script('backend/app/components/role/RoleService.js?v='.getVersionScript()) !!}
    {!! Html::script('backend/app/components/role/RoleController.js?v='.getVersionScript()) !!}
@endsection
