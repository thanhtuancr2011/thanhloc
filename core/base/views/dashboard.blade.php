@extends('base::layouts.master')

@section('title')
    Dashboard
@endsection

@section('breadcrumb')
    <div class="row">
        <div class="container col-md-12 admin-breadcrumb">
            <div class="breadcrumb">
                <div class="col-md-6 no-padding">
                    <h3>Dashboard</h3>
                </div>
                <div class="col-md-6 no-padding content-end">
                    <span class="breadcrumb-item">Trang chủ</span>
                    <span class="breadcrumb-item active">Dashboard</span>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content')
<style type="text/css">
    .card-body {
        background: #ecf0f5;
    }
    .info-box {
        display: block;
        min-height: 90px;
        background: #fff;
        width: 100%;
        box-shadow: 0 1px 1px rgba(0,0,0,0.1);
        border-radius: 2px;
        margin-bottom: 15px;
    }
    .bg-aqua, .callout.callout-info, .alert-info, .label-info, .modal-info .modal-body {
        background-color: #00c0ef !important;
    }
    .info-box-icon {
        border-top-left-radius: 2px;
        border-top-right-radius: 0;
        border-bottom-right-radius: 0;
        border-bottom-left-radius: 2px;
        display: block;
        float: left;
        height: 90px;
        width: 90px;
        text-align: center;
        font-size: 45px;
        line-height: 90px;
        background: rgba(0,0,0,0.2);
    }
    .info-box-content {
        padding: 5px 10px;
        margin-left: 90px;
    } 
    .info-box-text {
        text-transform: uppercase;
    }
    .progress-description, .info-box-text {
        display: block;
        font-size: 14px;
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
    }
    .info-box-number {
        display: block;
        font-weight: bold;
        font-size: 18px;
    }

    .info-box small {
        font-size: 14px;
    } 

.box {
    position: relative;
    border-radius: 3px;
    background: #ffffff;
    border-top: 3px solid #d2d6de;
    margin-bottom: 20px;
    width: 100%;
    box-shadow: 0 1px 1px rgba(0,0,0,0.1);
}

.bg-green {
    background-color: #00a65a !important;
} 

.bg-yellow {
    background-color: #f39c12 !important;
} 

.box.box-info {
    border-top-color: #00c0ef;
}


.box-header:before, .box-body:before, .box-footer:before, .box-header:after, .box-body:after, .box-footer:after {
    content: " ";
    display: table;
}

.box-header.with-border {
    border-bottom: 1px solid #f4f4f4;
}

.box.box-primary {
    border-top-color: #3c8dbc;
}

.box-title {
    padding-left: 15px;
}

.box-header .box-title {
    display: inline-block;
    border: none;
    font-size: 18px;
    margin: 0;
    line-height: 1;
}

.box-header>.box-tools {
    position: absolute;
    right: 10px;
    top: 5px;
}

.box-body {
    border-top-left-radius: 0;
    border-top-right-radius: 0;
    border-bottom-right-radius: 3px;
    border-bottom-left-radius: 3px;
    padding: 10px;
}

.table-responsive {
    min-height: .01%;
    overflow-x: auto;
}

.no-margin {
    margin: 0 !important;
}

.table>tbody>tr>td, .table>tfoot>tr>td {
    border-top: 1px solid #f4f4f4;
    padding-left: 5px;
}

.table thead th {
    border: none;
    border-bottom: 2px solid #c2cfd6;
    padding: 5px;
}

.box-footer {
    border-top-left-radius: 0;
    border-top-right-radius: 0;
    border-bottom-right-radius: 3px;
    border-bottom-left-radius: 3px;
    border-top: 1px solid #f4f4f4;
    padding: 10px;
    background-color: #fff;
}

.btn.btn-flat {
    border-radius: 0;
    -webkit-box-shadow: none;
    -moz-box-shadow: none;
    box-shadow: none;
    border-width: 1px;
}

.btn-info {
    background-color: #00c0ef;
    border-color: #00acd6;
    color: #fff;
}

.label {
    display: inline;
    padding: .2em .6em .3em;
    font-size: 75%;
    font-weight: 700;
    line-height: 1;
    color: #fff;
    text-align: center;
    white-space: nowrap;
    vertical-align: baseline;
    border-radius: .25em;
}

.box-header.with-border {
    border-bottom: 1px solid #f4f4f4;
}

.products-list {
    list-style: none;
    margin: 0;
    padding: 0;
}

.product-list-in-box>.item {
    box-shadow: none;
    border-radius: 0;
    border-bottom: 1px solid #f4f4f4;
    padding: 10px 0 10px 0;
    background: #fff;
}

.product-list-in-box>.item:last-child {
    border-bottom: none;
}

.products-list>.item:before, .products-list>.item:after {
    content: " ";
    display: table;
}

.products-list .product-img {
    float: left;
}

.products-list .product-img img {
    width: 42px;
    height: 51px;
}

.products-list .product-info {
    margin-left: 60px;
}

.products-list .product-title {
    font-weight: 600;
}

.bg-yellow, .callout.callout-warning, .alert-warning, .label-warning, .modal-warning .modal-body {
    background-color: #f39c12 !important;
}

.products-list .product-description {
    display: block;
    color: #999;
    overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
}

</style>
    <div class="card-body">
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-aqua"><i class="icon-social-dropbox" aria-hidden="true"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Sản phẩm</span>
                        <span class="info-box-number">{{ number_format($totalProduct , 0, ',', ',') }}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-red"><i class="fa fa-newspaper-o" aria-hidden="true"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Bài viết</span>
                        <span class="info-box-number">{{ number_format($totalPost , 0, ',', ',') }}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <!-- fix for small devices only -->
            <div class="clearfix visible-sm-block"></div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-green"><i class="fa fa-shopping-cart" aria-hidden="true"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Đơn hàng</span>
                        <span class="info-box-number">{{ number_format($totalOrder , 0, ',', ',') }}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-yellow"><i class="icon-people" aria-hidden="true"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Khách hàng</span>
                        <span class="info-box-number">{{ number_format($totalCustomer , 0, ',', ',') }}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
        </div>
        <div class="row">
            <!-- Left col -->
            <div class="col-md-8">
                <!-- TABLE: LATEST ORDERS -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Đơn hàng mới nhất</h3>
                        <div class="box-tools pull-right">
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table no-margin">
                                <thead>
                                    <tr>
                                        <th>Mã đơn</th>
                                        <th>Kiểu thanh toán</th>
                                        <th>Trạng thái</th>
                                        <th>Tổng tiền</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($orders as $order)
                                    <tr>
                                        <td>
                                            <a target="_blank" href="/admin/order/{{ $order->id }}/edit">#{{ $order->id }}</a>
                                        </td>
                                        <td>{{ $order->payment_method ? 'Chuyển khoản' : 'COD' }}</td>
                                        <td>
                                            <span class="label {{ $order->status ? 'label-success' : 'label-warning' }}">
                                                {{ $order->status ? 'Đã hoành thành' : 'Đang xử lý' }}
                                            </span>
                                        </td>
                                        <td>
                                            <div class="sparkbar" data-color="#00a65a" data-height="20">
                                                {{ number_format($order->subtotal , 0, ',', ',') }} ₫
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer clearfix">
                        <a href="/admin/order" target="_blank" class="btn btn-sm btn-info btn-flat pull-right">Xem tất cả</a>
                    </div>
                    <!-- /.box-footer -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
            <div class="col-md-4">
                <!-- PRODUCT LIST -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Sản phẩm mới nhất</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <ul class="products-list product-list-in-box">
                            @foreach ($products as $product)
                            <li class="item">
                                <div class="product-img">
                                    @if (isset($product->image_feature))
                                    <img src="{{ $product->image_feature->src }}" alt="{{ $product->title }}">
                                    @endif
                                </div>
                                <div class="product-info">
                                    <a href="{{ $product->url }}" class="{{ $product->title }}" target="_blank">
                                        {{ $product->title }}
                                        <span class="label label-warning pull-right">
                                            {{ number_format(!empty($product->sale_price) ? $product->sale_price : $product->price , 0, ',', ',') }} ₫
                                        </span>
                                    </a>
                                    <span class="product-description">
                                        {{ $product->excerpt }}
                                    </span>
                                </div>
                                <div class="clearfix"></div>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer text-center">
                        <a href="/admin/product" target="_blank" class="uppercase">Xem tất cả sản phẩm</a>
                    </div>
                    <!-- /.box-footer -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
    </div>  
@endsection

@section('script')
    <script type="text/javascript">
        $('.container-fluid').removeClass('hidden');
    </script>
@endsection
