@extends('base::layouts.master')

@section('title')
    Danh sách liên hệ
@endsection

@section('breadcrumb')
    <div class="row">
        <div class="container col-md-12 admin-breadcrumb">
            <div class="breadcrumb">
                <div class="col-md-6 no-padding">
                    <h3>Danh sách liên hệ</h3>
                </div>
                <div class="col-md-6 no-padding content-end">
                    <span class="breadcrumb-item">Trang chủ</span>
                    <span class="breadcrumb-item active">Danh sách liên hệ</span>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="card-body" ng-controller="ContactController">

        <!-- Header title and button -->
        <div class="group-search">
            <form class="frm-search" id="formSearch">
                <input type="text" class="form-control" ng-model="searchOptions.searchText"
                       placeholder="Tìm kiếm..."
                       ng-keypress="searchContacts($event)"/>
            </form>
            <div class="group-btn">
                <button type="button" class="btn btn-danger" ng-show="showBtn" ng-click="removeContacts()"><i class="fa fa-trash-o"></i></button>
            </div>
            <div class="group-btn">
                <button type="button" class="btn btn-default" ng-show="true" ng-click="getExport()">Xuất dữ liệu</button>
            </div>
        </div>

        <div class="clearfix"></div>

        <table class="table table-bordered table-hover table-striped">
            <thead>
            <th style="text-align: center; width: 5%;">#</th>
            <th style="text-align: center; width: 5%;">
                <input type="checkbox" class="css-checkbox ckb-all" id="checkbox-all" ng-click="toggleSelection('all')">
                <label for="checkbox-all" class="css-label lite-blue-check"></label>
            </th>
            <th>Họ tên</th>
            <th>Email</th>
            <th>Số điện thoại</th>
            <th>Địa chỉ</th>
            <th>Nội dung</th>
            <th>Kiểu liên hệ</th>
            <th>Ngày tạo</th>
            <th></th>
            </thead>
            <tbody>
            <tr ng-repeat="contact in contacts track by $index" ng-class="{'bg-tr': $index%2 == 0}">

                <td class="text-center">
                    @{{ ($index + 1) + (pageOptions.currentPage - 1) * pageOptions.itemsPerPage }}
                </td>

                <td class="text-center cls-chk">
                    <input type="checkbox" class="css-checkbox ckb" id="checkbox@{{$index}}" value="@{{contact.id}}"
                           ng-click="toggleSelection()">
                    <label for="checkbox@{{$index}}" class="css-label lite-blue-check"></label>
                </td>

                <td>@{{contact.full_name}}</td>

                <td>@{{contact.email}}</td>

                <td>@{{contact.phone_number}}</td>

                <td>@{{contact.address}}</td>

                <td>@{{contact.content}}</td>

                <td>@{{contactTypes[contact.type]}}</td>

                <td>
                    @{{formatDate (contact.created_at) | date : "yyyy/MM/dd"}}
                </td>

                <td class="text-center">
                    <a href="javascript:void(0)" class="btn btn-danger" ng-click="removeContacts(contact.id)"
                       title="Xóa">
                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                    </a>
                </td>
            </tr>
            </tbody>
        </table>
        <pagination-directive></pagination-directive>
    </div>

@endsection

@section('script')
    {!! Html::script('/backend/app/components/contact/ContactService.js?v='.getVersionScript()) !!}
    {!! Html::script('/backend/app/components/contact/ContactController.js?v='.getVersionScript()) !!}
    {!! Html::script('/backend/app/shared/pagination/PaginationDirective.js?v='.getVersionScript()) !!}
@endsection
