<?php

return [
	'notification' => 'Thông báo',
	'items_per_page' => 'Số lượt hiển thị trên một trang',
	'first' => 'Đầu',
	'last' => 'Cuối',
	'confirm_delete' => 'Bạn có chắc muốn xóa?',
	'confirm_clone' => 'Bạn có muốn nhân bản?',
	'drag_drop_to_upload' => "Kéo & thả, click để upload tập tin",
	'maximum_file_upload' => 'Kích thước tập tin tải lên tối đa ',
	'cancel' => 'Hủy',
	'delete' => 'Xóa',
	'clone' => 'Nhân bản',
];