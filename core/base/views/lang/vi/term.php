<?php

return [
	'home' => 'Trang chủ',
    'content' => 'Nội dung',
    'category' => 'Chuyên mục',
    'tag' => 'Tags',
    'placeholder_search' => 'Tìm kiếm...',
    'name' => 'Tên',
    'url' => 'Đường dẫn',
    'seo_title' => 'Tiêu đề SEO',
    'seo_description' => 'Mô tả SEO',
    'category_parent' => 'Chuyên mục cha',
    'description' => 'Mô tả',
    'created_date' => 'Ngày tạo',
    'cancel' => 'Hủy',
    'add' => 'Thêm',
    'update' => 'Cập nhật',
    'edit' => 'Chỉnh sửa',
    'delete' => 'Xóa',
    'add_success' => 'Thêm thành công',
    'update_success' => 'Cập nhật thành công',
    'delete_success' => 'Xóa thành công',
    'error_occurred' => 'Đã xảy ra lỗi. Mời thử lại',
    'invalid' => ':input không chính xác',
    'required' => ':input bắt buộc nhập',
    'exists' => ':input đã tồn tại trong hệ thống'
];