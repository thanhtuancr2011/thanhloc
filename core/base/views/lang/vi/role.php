<?php

return [
	'users' => 'Quản lý tài khoản',
	'roles' => 'Quản lý quyền',
	'menus' => 'Quản lý menu',
	'libraries' => 'Quản lý thư viện',
	'terms' => 'Quản lý danh mục & từ khóa',
	'posts' => 'Quản lý bài viết & trang',
	'categories' => 'Quản lý danh mục sản phẩm',
	'products' => 'Quản lý sản phẩm',
	'orders' => 'Quản lý đơn hàng',
	'settings' => 'Quản lý cấu hình',
];