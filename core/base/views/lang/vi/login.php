<?php

return [
    'login' => 'Đăng nhập',
    'sign_in_account' => 'Đăng nhập vào tài khoản',
    'email' => 'Email',
    'password' => 'Mật khẩu',
    'remember_me' => 'Ghi nhớ đăng nhập',
    'forgot_password' => 'Quên mật khẩu?',
];