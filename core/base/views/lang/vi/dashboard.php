<?php

return [
    'dashboard' => 'Bảng điều khiển',
    'home' => 'Trang chủ',
    'articles' => 'Bài viết',
    'pending_articles' => 'Bài viết chờ duyệt',
    'publish_articles' => 'Bài viết đã đăng',
    'standard_seo_articles' => 'Bài viết chuẩn SEO',
];