<?php

return [
	'home' => 'Home',
    'setting' => 'Setting',
    'basic_infomation' => 'Basic infomation',
    'page_infomation' => 'Page infomation',
    'page_title' => 'Page title',
    'page_description' => 'Page description',
    'keyword' => 'Keyword',
    'keyword_separated' => 'Keywords are separated by commas',
    'logo' => 'Logo',
    'marketing' => 'Marketing',
    'google' => 'Google',
    'facebook' => 'Facebook',
    'change_logo' => 'Change logo',
    'add_logo' => 'Add logo',
    'thumbnail_size' => 'Thumbnail size',
    'medium_size' => 'Medium size',
    'large_size' => 'Large size',
    'diziweb' => 'Diziweb',
    'width' => 'Width',
    'height' => 'Height',
    'phone_number' => 'Phone number',
    'keywords' => 'Keywords',
    'email' => 'Email',
    'image' => 'Image',
    'update' => 'Update',
    'content' => 'Content',
    'email_config' => 'Email config',
    'email_driver' => 'Email driver',
    'email_host' => 'Email host',
    'email_port' => 'Email port',
    'email_username' => 'Email username',
    'email_password' => 'Email password',
    'email_encryption' => 'Email encryption',
    'place_first' => 'Attached to the first position of the tag :tag',
    'place_last' => 'Attached to the last position of the tag :tag',
    'analytic_config' => 'Analytics display configuration on dashboard',
    'pagination'=>'Pagination'
];