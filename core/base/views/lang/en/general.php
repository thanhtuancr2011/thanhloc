<?php

return [
	'notification' => 'Notification',
	'items_per_page' => 'Number of items per page',
	'first' => 'First',
	'last' => 'Last',
	'confirm_delete' => 'Are you sure you want to delete?',
	'confirm_clone' => 'Are you sure you want to clone?',
	'drag_drop_to_upload' => 'Drag & drop, click to upload file',
	'maximum_file_upload' => 'Maximum file upload',
	'cancel' => 'Cancel',
	'delete' => 'Delete',
	'clone' => 'Clone',
];