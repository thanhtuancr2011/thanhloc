<?php

return [
    'dashboard' => 'Dashboard',
    'home' => 'Home',
    'articles' => 'Articles',
    'pending_articles' => 'Pending articles',
    'publish_articles' => 'Publish articles',
    'standard_seo_articles' => 'Standard SEO articles',
];