<?php

return [
	'home' => 'Home',
    'content' => 'Content',
    'category' => 'Category',
    'tag' => 'Tag',
    'placeholder_search' => 'Search...',
    'name' => 'Name',
    'url' => 'Url',
    'seo_title' => 'SEO title',
    'seo_description' => 'SEO description',
    'category_parent' => 'Category parent',
    'description' => 'Description',
    'created_date' => 'Created date',
    'cancel' => 'Cancel',
    'add' => 'Add',
    'update' => 'Update',
    'edit' => 'Edit',
    'delete' => 'Delete',
    'add_success' => 'Add new is success',
    'update_success' => 'Update is success',
    'delete_success' => 'Delete is success',
    'error_occurred' => 'Error! An error occurred. Please try again later.',
    'invalid' => ':input is invalid',
    'required' => ':input is required field',
    'exists' => ':input is exists in system'
];