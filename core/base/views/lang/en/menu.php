<?php

return [
	'home' => 'Home',
    'content' => 'Content',
    'menu' => 'Menu',
    'placeholder_search' => 'Search...',
    'name' => 'Name',
    'level' => 'Level',
    'created_date' => 'Created date',
    'add_menu_item' => 'Add menu item',
    'url' => 'Url',
    'cancel' => 'Cancel',
    'add' => 'Add',
    'update' => 'Update',
    'edit' => 'Edit',
    'delete' => 'Delete',
    'add_success' => 'Add new is success',
    'update_success' => 'Update is success',
    'delete_success' => 'Delete is success',
    'required' => ':input is required field',
    'exists' => ':input is exists in system',
    'level_range' => 'Level from :from to :to',
    'page' => 'Page',
    'category' => 'Category',
    'custom_link' => 'Custom Link',
    'add_to_menu' => 'Add to menu',
    'display_name' => 'Display name',
    'invalid' => ':input is invalid',
    'error_occurred' => 'Error! An error occurred. Please try again later.',
];