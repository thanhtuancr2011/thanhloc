<?php

return [
    'login' => 'Login',
    'sign_in_account' => 'Sign in to your account',
    'email' => 'Email',
    'password' => 'Password',
    'remember_me' => 'Remember me',
    'forgot_password' => 'Forgot password?',
];