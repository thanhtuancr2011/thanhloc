
@extends('base::layouts.master')

@section('title')
    Menu
@endsection

@section('breadcrumb')
    <div class="row">
        <div class="container col-md-12 admin-breadcrumb">
            <div class="breadcrumb">
                <div class="col-md-6 no-padding">
                    <h3>Menu</h3>
                </div>
                <div class="col-md-6 no-padding content-end">
                    <span class="breadcrumb-item">Trang chủ</span>
                    <span class="breadcrumb-item active">Menu</span>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="card-body" ng-controller="MenuController">
        <!-- Header title and button -->
        <div class="group-search">
            <form class="frm-search" id="formSearch">
                <input type="text" class="form-control" ng-model="searchOptions.searchText" placeholder="Tìm kiếm..." ng-keypress="searchMenus($event)"/>
            </form>
            <div class="group-btn">
                @if (Base::can('add.menu'))
                <button type="button" class="btn btn-primary" ng-click="getModalMenu()"><i class="fa fa-plus"></i></button>
                @endif

                @if (Base::can('delete.menu'))
                <button type="button" class="btn btn-danger" ng-show="showBtn" ng-click="removeMenu()"><i class="fa fa-trash-o"></i></button>
                @endif
            </div>
        </div>

        <div class="clearfix"></div>
        <table class="table table-bordered table-striped">
            <thead>
                <th class="percent-5">#</th>
                <th class="percent-5">
                    <input type="checkbox" class="css-checkbox ckb-all" id="checkbox-all" ng-click="toggleSelection('all')">
                    <label for="checkbox-all" class="css-label lite-blue-check"></label>
                </th>
                <th>Tên menu</th>
                <th>Cấp menu</th>
                <th>Ngày tạo</th>
                <th></th>
            </thead>
            <tbody>
            <tr ng-repeat="menu in menus track by $index" ng-class="{'bg-tr': $index%2 == 0}">

                <td class="text-center">
                    @{{ ($index + 1) + (pageOptions.currentPage - 1) * pageOptions.itemsPerPage }}
                </td>

                <td class="text-center cls-chk">
                    <input type="checkbox" class="css-checkbox ckb" id="checkbox@{{$index}}" value="@{{menu.id}}"
                           ng-click="toggleSelection()">
                    <label for="checkbox@{{$index}}" class="css-label lite-blue-check"></label>
                </td>

                <td>@{{menu.name}}</td>

                <td>@{{menu.depth}}</td>

                <td>
                    @{{formatDate (menu.created_at) | date : "yyyy/MM/dd"}}
                </td>

                <td class="text-center">
                    @if (Base::can('edit.menu'))
                    <a href="{{ URL::to('admin/menu') }}/@{{menu.id}}" class="btn btn-success" title="Menu chi tiết">
                        <i class="fa fa-th-list" aria-hidden="true"></i>
                    </a>
                    @endif

                    @if (Base::can('edit.menu'))
                    <a href="javascript:void(0)" class="btn btn-primary" ng-click="getModalMenu(menu.id)" title="Chỉnh sửa">
                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                    </a>
                    @endif 

                    @if (Base::can('delete.menu'))
                    <a href="javascript:void(0)" class="btn btn-danger" ng-click="removeMenu(menu.id)" title="Xóa">
                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                    </a>
                    @endif
                </td>
            </tr>
            </tbody>
        </table>
        <pagination-directive></pagination-directive>
    </div>

@endsection

@section('script')
    {!! Html::script('backend/app/components/menu/MenuService.js?v='.getVersionScript()) !!}
    {!! Html::script('backend/app/components/menu/MenuController.js?v='.getVersionScript()) !!}
    {!! Html::script('backend/app/shared/pagination/PaginationDirective.js?v='.getVersionScript()) !!}
@endsection
