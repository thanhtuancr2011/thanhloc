<div class="modal-header">
    <h4 class="modal-title">Chỉnh sửa</h4>
    <button aria-label="Close" data-dismiss="modal" class="close" type="button" ng-click="cancel()"><span aria-hidden="true">×</span></button>
</div>

<div class="modal-body">
    <div class="innerAll">
        <div class="innerLR">
            <form method="POST" accept-charset="UTF-8" name="formAddMenuItem" ng-init='menuItem={{$menuItem}};'>
                <input type="hidden" name="_token" value="csrf_token()" />
                <div class="form-group">
                    <div class="form-group" ng-class="{true: 'has-error'}[submitted && formAddMenuItem.title.$invalid]">
                        <label for="name">Tiêu đề (*)</label>
                        <div class="">
                            <input class="form-control" placeholder="Tiêu đề" type="text" name="title" ng-model="menuItem.title" required="true">
                            <label class="control-label" ng-show="submitted && formAddMenuItem.title.$error.required">
                                Bạn chưa nhập tiêu đề
                            </label>
                        </div>
                    </div>
                    <div class="form-group" ng-class="{true: 'has-error'}[submitted && formAddMenuItem.url.$invalid]" ng-if="menuItem.url != ''">
                        <label for="name">Đường dẫn (*)</label>
                        <input class="form-control" placeholder="Đường dẫn" type="text" name="url" ng-model="menuItem.url" required ng-pattern="/^(?!-)([\w\d\/\:\.\-])+$/">
                        <label class="control-label" ng-show="submitted && formAddMenuItem.url.$error.required">
                            Bạn chưa nhập đường dẫn
                        </label>
                        <label class="control-label" ng-show="submitted && formAddMenuItem.url.$error.pattern">
                            Đường dẫn không hợp lệ
                        </label>
                    </div>

                    <div class="alert alert-error alert-danger" ng-show="errors" ng-repeat="error in errors">
                        @{{error}}
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="modal-footer">
    <button class="btn btn-default" ng-click="cancel()"><i class="fa fa-times"></i> Hủy</button>
    <button class="btn btn-primary" ng-click="submit(formAddMenuItem.$valid)">
        <span><i class="fa fa-pencil-square-o"></i> Cập nhật</span>
    </button>
</div>
