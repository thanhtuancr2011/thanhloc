<div class="modal-header">
    @if(!empty($item->id))
        <h4 class="modal-title">Chỉnh sửa</h4>
    @else
        <h4 class="modal-title">Thêm</h4>
    @endif
    <button aria-label="Close" data-dismiss="modal" class="close" type="button" ng-click="cancel()"><span aria-hidden="true">×</span></button>
</div>

<div class="modal-body">
    <div class="innerAll">
        <div class="innerLR">
            <form method="POST" accept-charset="UTF-8" name="formAddMenu" ng-init='menuItem={{$item}};'>
                <input type="hidden" name="_token" value="csrf_token()" />
                <div class="form-group">

                    <div class="form-group" ng-class="{true: 'has-error'}[submitted && formAddMenu.name.$invalid]">
                        <label for="name">Tên menu (*)</label>
                        <div class="">
                            <input class="form-control" placeholder="Tên menu" type="text" name="name" ng-model="menuItem.name" required="true">
                            <label class="control-label" ng-show="submitted && formAddMenu.name.$error.required">
                                {{trans('dizi-core-lang::menu.required', ['input' => trans('dizi-core-lang::menu.name')])}}
                            </label>
                        </div>
                    </div>

                    <div class="form-group" ng-class="{true: 'has-error'}[submitted && formAddMenu.depth.$invalid]">
                        <label for="depth">Cấp menu (*)</label>
                        <div class="">
                            <input class="form-control" placeholder="Cấp menu từ 1 đến 5" ng-pattern="/^[1-5]$/" type="text" name="depth" ng-model="menuItem.depth" required="true">
                            <label class="control-label" ng-show="submitted && formAddMenu.depth.$error.required">
                                Bạn chưa nhập số cấp cho menu
                            </label>
                            <label class="control-label" ng-show="submitted && formAddMenu.depth.$error.pattern">
                                Số cấp menu từ 1 đến 5
                            </label>
                        </div>
                    </div>

                    <div class="alert alert-error alert-danger" ng-show="errors" ng-repeat="error in errors">
                        @{{error}}
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="modal-footer">
    <button class="btn btn-default" ng-click="cancel()"><i class="fa fa-times"></i> Hủy</button>
    <button class="btn btn-primary" ng-click="submit(formAddMenu.$valid)">
        <span>
            @if(!empty($item->id) || Base::can('edit.menu'))
                <i class="fa fa-pencil-square-o"></i> Chỉnh sửa
            @else
                @if (Base::can('add.menu'))
                    <i class="fa fa-plus"></i> Thêm
                @endif
            @endif
        </span>
    </button>
</div>
