@extends('base::layouts.master')

@section('title')
    Chi tiết menu
@endsection

@section('breadcrumb')
    <div class="row">
        <div class="container col-md-12 admin-breadcrumb">
            <div class="breadcrumb">
                <div class="col-md-6 no-padding">
                    <h3>Chi tiết menu</h3>
                </div>
                <div class="col-md-6 no-padding content-end">
                    <span class="breadcrumb-item">Trang chủ</span>
                    <span class="breadcrumb-item"><a href="{{URL::to('admin/menu')}}">Menu</a></span>
                    <span class="breadcrumb-item active">{{$item->name}}</span>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content')
<div class="card-body" style="padding: 0" ng-controller="MenuItemController" ng-init="menu={{json_encode($item)}}; pages={{json_encode($pages)}}; categories={{json_encode($categories)}}; terms={{json_encode($terms)}}; menuItems={{json_encode($menuItems)}}">  
    <div class="col-md-12" style="padding: 15px;">
        <div class="col-md-3" id="leftGroup">
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-primary">
                    <div class="panel-heading" role="tab" id="headingOne" data-toggle="collapse" data-target="#post" ng-init="showPost = false" ng-click="showPost = !showPost">
                        <a href="javascript:void(0)">
                            Trang
                            <i class="fa @{{showPost ? 'fa-minus' : 'fa-plus'}} pull-right" aria-hidden="true"></i>
                        </a>
                    </div>
                    <div id="post" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                        <div class="panel-body">
                            <div class="group-data">
                                <div ng-repeat="page in pages">
                                    <input type="checkbox" class="css-checkbox ckb-post" id="checkbox-post-@{{page.id}}" value="@{{page.id}}" ng-click="toggleSelection()">
                                    <label for="checkbox-post-@{{page.id}}" class="css-label lite-blue-check"><small class="txt-14 no-padding">@{{page.title}}</small></label>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="padding-10">
                                <button class="btn btn-primary btn-xs pull-right" ng-click="addMenuItem('page')">
                                    <strong>Thêm vào menu</strong>
                                </button>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-primary">
                    <div class="panel-heading" role="tab" id="headingOne" data-toggle="collapse" data-target="#term" ng-init="showCategory = false" ng-click="showCategory = !showCategory">
                        <a href="javascript:void(0)">
                            Danh mục tin tức
                            <i class="fa @{{showCategory ? 'fa-minus' : 'fa-plus'}} pull-right" aria-hidden="true"></i>
                        </a>
                    </div>
                    <div id="term" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                        <div class="panel-body">
                            <div class="group-data">
                                <script type="text/ng-template" id="subSelect1">
                                    <input type="checkbox" class="css-checkbox ckb-cate" id="checkbox@{{value.id}}" value="@{{value.id}}" ng-click="selectCategory(value)">
                                    <label for="checkbox@{{value.id}}" class="css-label lite-blue-check" ng-click="toggleSelection()">
                                        <small class="txt-14 no-padding">@{{value.name}}</small>
                                    </label>
                                    
                                    <ul ng-if="value.subFolder" class="collapse in">
                                        <li class="category-item" style="padding-bottom: 0px; overflow: visible;" ng-repeat="(key, value) in value.subFolder | orderBy:'id'" ng-include="'subSelect1'"></li>
                                        <div class="clearfix"></div>
                                    </ul>
                                    <div class="clearfix"></div>
                                </script>
                                <ul class="select-menu-parrent select-cat">
                                    <li class="select-menu-item category-item" ng-repeat="(index, value) in terms | orderBy:'id'" ng-include="'subSelect1'" style="@{{value.subFolder.length > 0 ? 'padding-bottom: 0' : ''}}"></li>
                                    <div class="clearfix"></div>
                                </ul>
                            </div>
                            <div class="clearfix"></div>
                            <div class="padding-10">
                                <button class="btn btn-primary btn-xs pull-right" ng-click="addMenuItem('category_news')">
                                    <strong>Thêm vào menu</strong>
                                </button>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-primary">
                    <div class="panel-heading" role="tab" id="headingOne" data-toggle="collapse" data-target="#category" ng-init="showCategory = false" ng-click="showCategory = !showCategory">
                        <a href="javascript:void(0)">
                            Danh mục sản phẩm
                            <i class="fa @{{showCategory ? 'fa-minus' : 'fa-plus'}} pull-right" aria-hidden="true"></i>
                        </a>
                    </div>
                    <div id="category" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                        <div class="panel-body">
                            <div class="group-data">
                                <script type="text/ng-template" id="subSelect">
                                    <input type="checkbox" class="css-checkbox ckb-cate" id="checkbox@{{value.id}}" value="@{{value.id}}" ng-click="selectCategory(value)">
                                    <label for="checkbox@{{value.id}}" class="css-label lite-blue-check" ng-click="toggleSelection()">
                                        <small class="txt-14 no-padding">@{{value.name}}</small>
                                    </label>
                                    
                                    <ul ng-if="value.subFolder" class="collapse in">
                                        <li class="category-item" style="padding-bottom: 0px; overflow: visible;" ng-repeat="(key, value) in value.subFolder | orderBy:'id'" ng-include="'subSelect'"></li>
                                        <div class="clearfix"></div>
                                    </ul>
                                    <div class="clearfix"></div>
                                </script>
                                <ul class="select-menu-parrent select-cat">
                                    <li class="select-menu-item category-item" ng-repeat="(index, value) in categories | orderBy:'id'" ng-include="'subSelect'" style="@{{value.subFolder.length > 0 ? 'padding-bottom: 0' : ''}}"></li>
                                    <div class="clearfix"></div>
                                </ul>
                            </div>
                            <div class="clearfix"></div>
                            <div class="padding-10">
                                <button class="btn btn-primary btn-xs pull-right" ng-click="addMenuItem('category_product')">
                                    <strong>Thêm vào menu</strong>
                                </button>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-primary">
                    <div class="panel-heading" role="tab" id="headingOne" data-toggle="collapse" data-target="#customLink" ng-init="showCustomLink = false" ng-click="showCustomLink = !showCustomLink">
                        <a href="javascript:void(0)">
                            Đường dẫn tùy chỉnh
                            <i class="fa @{{showCustomLink ? 'fa-minus' : 'fa-plus'}} pull-right" aria-hidden="true"></i>
                        </a>
                    </div>
                    <div id="customLink" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                        <div class="panel-body" style="padding: 10px;">
                            <form method="POST" accept-charset="UTF-8" name="formAddMenuItem">
                                <div class="form-group" ng-class="{true: 'has-error'}[submitted && formAddMenuItem.title.$invalid]">
                                    <label>Tên hiển thị (*)</label>
                                    <div class="">
                                        <input class="form-control" placeholder="Tên hiển thị" type="text" name="title" ng-model="menuItem.title" required="true">
                                        <label class="control-label" ng-show="submitted && formAddMenuItem.title.$error.required">
                                            Bạn chưa nhập tên hiển thị
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group" ng-class="{true: 'has-error'}[submitted && formAddMenuItem.url.$invalid]">
                                    <label>Đường dẫn (*)</label>
                                    <div class="">
                                        <input class="form-control" placeholder="Đường dẫn" type="text" name="url" ng-model="menuItem.url" required ng-pattern="/^(?!-)([\w\d\/\:\.\-\#])+$/">
                                        <label class="control-label" ng-show="submitted && formAddMenuItem.url.$error.required">
                                            Bạn chưa nhập đường dẫn
                                        </label>
                                        <label class="control-label" ng-show="submitted && formAddMenuItem.url.$error.pattern">
                                            Đường dẫn không đúng định dạng    
                                        </label>
                                    </div>
                                </div>
                            </form>
                            <button class="btn btn-primary btn-xs pull-right" ng-click="addMenuItem('customLink', formAddMenuItem.$valid)">
                                <strong>Thêm vào menu</strong>
                            </button>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-9 show-lst-menu">
            <div class="card-header" style="background: #f5f5f5;">
                <i class="fa fa-list font-1xl"></i>@{{menu.name}}
                <div class="clearfix"></div>
            </div>
            <!-- Nested node template -->
            <script type="text/ng-template" id="nodes_renderer.html">
                <div ui-tree-handle>
                    @{{node.title}}
                    <a class="pull-right btn btn-default cls-remove" data-nodrag="" ng-click="removeMenuItem(this)">
                        <span class="fa fa-times"></span>
                    </a>
                    <a style="margin-right: 10px;" class="pull-right btn btn-default cls-remove" data-nodrag="" ng-click="getModalChangeItem(node.id)">
                        <span class="fa fa-pencil-square-o"></span>
                    </a>
                    <div class="clearfix"></div>
                </div>
                <ol ui-tree-nodes ng-model="node.nodes">
                    <li ng-repeat="node in node.nodes" ui-tree-node ng-include="'nodes_renderer.html'"></li>
                </ol>
            </script>
            <div ui-tree="treeOptions" ng-if="menuItems.length > 0">
                <ol ui-tree-nodes ng-model="menuItems" id="tree-root">
                    <li ng-repeat="node in menuItems" ui-tree-node ng-include="'nodes_renderer.html'"></li>
                </ol>
            </div>
            <div class="clearfix"></div>
        </div>

    </div>
</div>

@endsection

@section('script')
    {!! Html::script('backend/app/components/menu/MenuService.js?v='.getVersionScript()) !!}
    {!! Html::script('backend/app/components/menu/MenuController.js?v='.getVersionScript()) !!}
    {!! Html::script('backend/app/shared/pagination/PaginationDirective.js?v='.getVersionScript()) !!}
@endsection
