<!DOCTYPE html>

<html lang="{{ config('app.locale') }}" ng-app="myApp" ng-controller="BaseController">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Admin | @yield('title')</title>

    @yield('css')

    <!-- Styles -->
    @include('base::shared.css')
    {!! Html::script('bower_components/jquery/dist/jquery.min.js')!!}
    {!! Html::script('bower_components/jquery-ui/jquery-ui.min.js')!!}

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto|Roboto+Condensed" rel="stylesheet">
</head>

<body class="app header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden">

    <!-- Main header -->
    @include('base::shared.header')

    <div class="app-body">

        <!-- Main left sidebar -->
        @include('base::shared.sidebar')

        <!-- Main content -->
        <main class="main">
            <!-- Main breadcrumb -->
            @yield('breadcrumb')
            <!-- Main Content -->
            <div class="container-fluid hidden">
                <div id="ui-view">
                    <div class="animated fadeIn">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 master-col">
                                <div class="card master">
                                    @yield('content')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <!-- Main right sidebar -->
        @include('base::shared.aside')
    </div>

    <footer class="app-footer">
        <span><a href="">Copyright</a> &copy;2018.</span>
        <span class="ml-auto">Designed by <a href="">Thành Tuấn</a></span>
    </footer>

</body>

<!-- Scripts -->
@include('base::shared.script')

<!-- Message -->
{{-- {!! Messenger::getHtml() !!} --}}

</html>
