@extends('base::layouts.master')

@section('title')
    Cấu hình
@endsection

@section('breadcrumb')
    <div class="row">
        <div class="container col-md-12 admin-breadcrumb">
            <div class="breadcrumb">
                <div class="col-md-6 no-padding">
                    <h3>Cấu hình</h3>
                </div>
                <div class="col-md-6 no-padding content-end">
                    <span class="breadcrumb-item">Trang chủ</span>
                    <span class="breadcrumb-item active">Cấu hình</span>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="card-body" ng-controller="SettingController" ng-init="item={{json_encode($item)}}; mimeContentTypes={{ json_encode($mimeContentTypes) }}; maxSizeUpload={{$maxSizeUpload}};">
        <div class="col-md-12 no-padding">
            <div class="col-md-3" id="leftGroup">
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-primary">
                        <div class="panel-heading" role="tab" id="headingOne" data-toggle="collapse"
                             data-target="#general"
                             ng-init="showGeneral = true; tabShow = 'general'" ng-click="showGeneral = !showGeneral">
                            <a href="javascript:void(0)">
                                Thông tin cơ bản
                                <i class="fa @{{showGeneral ? 'fa-minus' : 'fa-plus'}} pull-right" aria-hidden="true"></i>
                            </a>
                        </div>
                        <div id="general" class="panel-collapse collapse in" role="tabpanel"
                             aria-labelledby="headingOne"
                             aria-expanded="false">
                            <div class="panel-body" style="padding: 0">
                                <div class="group-data" style="padding: 0">
                                    <div class="item-menu-left @{{tabShow == 'general' ? 'active' : ''}}"
                                         ng-click="tabShow='general'">
                                        Thông tin trang
                                    </div>
                                    <div class="item-menu-left @{{tabShow == 'logo' ? 'active' : ''}}"
                                         ng-click="tabShow='logo'">
                                        Logo/Favicon
                                    </div>
                                    <div class="item-menu-left @{{tabShow == 'email' ? 'active' : ''}}"
                                         ng-click="tabShow='email'">
                                        Địa chỉ email
                                    </div>
                                    <div class="item-menu-left @{{tabShow == 'media' ? 'active' : ''}}"
                                         ng-click="tabShow='media'">
                                        Hình ảnh
                                    </div>
                                    <div class="item-menu-left @{{tabShow == 'social_link' ? 'active' : ''}}"
                                         ng-click="tabShow='social_link'">
                                        Social link
                                    </div>
                                    <div class="item-menu-left @{{tabShow == 'company' ? 'active' : ''}}"
                                         ng-click="tabShow='company'">
                                        Thông tin công ty
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-9 show-lst-menu">
                <div class="card-body">
                    <!--Content-->
                    <div class="tab-content border-none">
                        <div class="tab-pane" style="display: @{{tabShow == 'general' ? 'block' : 'none'}};">
                            <div class="form-group">
                                <label for="">Tiêu đề trang</label>
                                <div class="">
                                    <input class="form-control" type="text" name="title" ng-model="item.title">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="">Mô tả trang</label>
                                <div class="">
                                    <textarea class="form-control" type="text" name="description" ng-model="item.description"></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="">
                                    Từ khóa <br/ >
                                    <small>Ngăn cách nhau bằng dấu phẩy</small>
                                </label>
                                <div class="">
                                    <input class="form-control" type="text" name="keyword" ng-model="item.keyword">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="">
                                    Địa chỉ
                                </label>
                                <div class="">
                                    <input class="form-control" type="text" name="address" ng-model="item.address">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="">
                                    Số điện thoại
                                </label>
                                <div class="">
                                    <input class="form-control" type="text" name="phone" ng-model="item.phone">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="">
                                    Số fax
                                </label>
                                <div class="">
                                    <input class="form-control" type="text" name="fax" ng-model="item.fax">
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" style="display: @{{tabShow == 'logo' ? 'block' : 'none'}};">
                            <div class="img-feature">
                                <div class="col-md-6">
                                    <div class="image-feature">
                                        <img ng-src="@{{item.logo_src ? item.logo_src : '/images/product-no-image.png'}}" alt="">
                                    </div>
                                    <div style="margin-top: 10px">
                                        <a class="btn btn-primary" href="javascript:void(0)" ng-click="getModalLibraries('logo_src')">
                                            <span ng-if="item.logo_src">Thay đổi logo</span>
                                            <span ng-if="!item.logo_src">Thêm logo</span>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="image-feature">
                                        <img ng-src="@{{item.favicon_src ? item.favicon_src : '/images/product-no-image.png'}}" alt="">
                                    </div>
                                    <div style="margin-top: 10px">
                                        <a class="btn btn-primary" href="javascript:void(0)" ng-click="getModalLibraries('favicon_src')">
                                            <span ng-if="item.favicon_src">Thay đổi favicon</span>
                                            <span ng-if="!item.favicon_src">Thêm favicon</span>
                                        </a>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>

                        <div class="tab-pane" style="display: @{{tabShow == 'email' ? 'block' : 'none'}};">
                            <div class="form-group">
                                <label for="">Địa chỉ email</label>
                                <div class="">
                                    <input class="form-control" placeholder="Địa chỉ email" type="text" name="email" ng-model="item.email">
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane col-md-12" style="display: @{{tabShow == 'media' ? 'block' : 'none'}};"
                             ng-init="@if (!isset($item['thumbnail_width'])) item.thumbnail_width=150 @endif
                             @if (!isset($item['thumbnail_height'])) ;item.thumbnail_height=150 @endif
                             @if (!isset($item['medium_width'])) ;item.medium_width=300 @endif
                             @if (!isset($item['medium_height'])) ;item.medium_height=300 @endif
                             @if (!isset($item['large_width'])) ;item.large_width=1024 @endif
                             @if (!isset($item['large_height'])) ;item.large_height=1024 @endif">
                            <div class="form-group">
                                <div class="col-md-3"><label for="">Cỡ nhỏ</label></div>
                                <div class="col-md-9 row">
                                    <div class="col">
                                        <label for="">Chiều rộng</label>
                                        <input type="text" class="form-control" placeholder="Chiều rộng"
                                               ng-model="item.thumbnail_width">
                                    </div>
                                    <div class="col">
                                        <label for="">Chiều cao</label>
                                        <input type="text" class="form-control" placeholder="Chiều cao"
                                               ng-model="item.thumbnail_height">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-3"><label for="">Cỡ vừa</label></div>
                                <div class="col-md-9 row">
                                    <div class="col">
                                        <label for="">Chiều rộng</label>
                                        <input type="text" class="form-control" placeholder="Chiều rộng"
                                               ng-model="item.medium_width">
                                    </div>
                                    <div class="col">
                                        <label for="">Chiều cao</label>
                                        <input type="text" class="form-control" placeholder="Chiều cao"
                                               ng-model="item.medium_height">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-3"><label for="">Cỡ lớn</label></div>
                                <div class="col-md-9 row">
                                    <div class="col">
                                        <label for="">Chiều rộng</label>
                                        <input type="text" class="form-control" placeholder="Chiều rộng"
                                               ng-model="item.large_width">
                                    </div>
                                    <div class="col">
                                        <label for="">Chiều cao</label>
                                        <input type="text" class="form-control" placeholder="Chiều cao" ng-model="item.large_height">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div class="tab-pane" style="display: @{{tabShow == 'robot' ? 'block' : 'none'}};">
                            <div class="form-group">
                                <label for="">{{trans('dizi-core-lang::setting.content')}}</label>
                                <div class="">
                                    <textarea class="form-control" rows="7" placeholder="Nhập nội dung" type="text"
                                              name="robot_txt" ng-model="item.robot_txt"></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" style="display: @{{tabShow == 'social_link' ? 'block' : 'none'}};">
                            <div class="form-group">
                                <label for="">Kênh youtube</label>
                                <div class="">
                                    <input class="form-control" placeholder="Kênh youtube" type="text" name="youtube_channel" ng-model="item.youtube_channel">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="">Fanpage facebook</label>
                                <div class="">
                                    <input class="form-control" placeholder="Fanpage facebook" type="text" name="facebook_fanpage" ng-model="item.facebook_fanpage">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="">Link twitter</label>
                                <div class="">
                                    <input class="form-control" placeholder="Link twitter" type="text" name="twitter" ng-model="item.twitter">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="">Link google +</label>
                                <div class="">
                                    <input class="form-control" placeholder="Link google +" type="text" name="google_plus" ng-model="item.google_plus">
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" style="display: @{{tabShow == 'company' ? 'block' : 'none'}};">
                            <div class="form-group">
                                <label for="">Tên công ty</label>
                                <div class="">
                                    <input class="form-control" placeholder="Tên công ty" type="text" name="company_name" ng-model="item.company_name">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="">Địa chỉ</label>
                                <div class="">
                                    <input class="form-control" placeholder="Địa chỉ" type="text" name="company_address" ng-model="item.company_address">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="">Di động</label>
                                <div class="">
                                    <input class="form-control" placeholder="Di động" type="text" name="company_phone" ng-model="item.company_phone">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="">Điện thoại bàn</label>
                                <div class="">
                                    <input class="form-control" placeholder="Điện thoại bàn" type="text" name="company_telephone" ng-model="item.company_telephone">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="">Email</label>
                                <div class="">
                                    <input class="form-control" placeholder="Email" type="text" name="company_email" ng-model="item.company_email">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="">Website</label>
                                <div class="">
                                    <input class="form-control" placeholder="Website" type="text" name="company_website" ng-model="item.company_website">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 form-group">
                            <button class="btn btn-primary pull-right" ng-click="submit()">
                                <span><i class="fa fa-pencil-square-o"></i> Cập nhật</span>
                            </button>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>

@endsection

@section('script')
    {!! Html::script('backend/app/components/post/PostService.js?v='.getVersionScript()) !!}
    {!! Html::script('backend/app/components/library/LibraryService.js?v='.getVersionScript()) !!}
    {!! Html::script('backend/app/components/setting/SettingService.js?v='.getVersionScript()) !!}
    {!! Html::script('backend/app/components/setting/SettingController.js?v='.getVersionScript()) !!}
    {!! Html::script('backend/app/shared/file-upload/FileService.js?v='.getVersionScript()) !!}
    {!! Html::script('backend/app/shared/file-upload/FileUploadDirective.js?v='.getVersionScript()) !!}
@endsection


