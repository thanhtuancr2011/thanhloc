@extends('base::layouts.master')

@section('title')
    Cấu hình
@endsection

@section('breadcrumb')
    <div class="row">
        <div class="container col-md-12 admin-breadcrumb">
            <div class="breadcrumb">
                <div class="col-md-6 no-padding">
                    <h3>Cấu hình</h3>
                </div>
                <div class="col-md-6 no-padding content-end">
                    <span class="breadcrumb-item">Trang chủ</span>
                    <span class="breadcrumb-item active">Cấu hình</span>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <style type="text/css">
        .CodeMirror {border: 1px solid #c2cfd6; font-size:13px}
    </style>

    <div class="card-body" ng-controller="SettingController" ng-init="item={{json_encode($item)}}; tabShow='{{$type}}'; mimeContentTypes={{ json_encode($mimeContentTypes) }}; maxSizeUpload={{$maxSizeUpload}};">
        <div class="col-md-12">
            <div class="col-md-3" id="leftGroup">
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-primary">

                        <div class="panel-heading" role="tab" id="headingOne" data-toggle="collapse"
                             data-target="#analytics"
                             ng-init="showGeneral = true; tabShow = 'sitemaps'" ng-click="showGeneral = !showGeneral">
                            <a href="javascript:void(0)">
                                Cấu hình google
                            </a>
                        </div>

                        <div id="google" class="panel-collapse collapse in" role="tabpanel"
                             aria-labelledby="headingOne"
                             aria-expanded="false">
                            <div class="panel-body" style="padding: 0">
                                <div class="group-data" style="padding: 0">
                                    {{-- <div class="item-menu-left @{{tabShow == 'analytics' ? 'active' : ''}}" ng-click="changeTab('analytics')">
                                        Google analytics
                                    </div>
                                    <div class="item-menu-left @{{tabShow == 'tag_manager' ? 'active' : ''}}" ng-click="changeTab('tag_manager')">
                                        Tag manager
                                    </div>
                                    <div class="item-menu-left @{{tabShow == 'webmaster' ? 'active' : ''}}" ng-click="changeTab('webmaster')">
                                        Webmaster tools
                                    </div>
                                    <div class="item-menu-left @{{tabShow == 'remarketing' ? 'active' : ''}}" ng-click="changeTab('remarketing')">
                                        Google remarketing
                                    </div> --}}
                                    <div class="item-menu-left @{{tabShow == 'sitemaps' ? 'active' : ''}}" ng-click="changeTab('sitemaps')">
                                        Sitemap
                                    </div>
                                    <div class="item-menu-left @{{tabShow == 'robot' ? 'active' : ''}}" ng-click="changeTab('robot')">
                                        Robots.txt
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-md-9 show-lst-menu">
                <div class="card-body">
                    <!--Google-->
                    <div class="tab-content border-none">
                        <div class="" style="display: @{{tabShow == 'tag_manager' ? 'block' : 'none'}};">
                            <div class="form-group">
                                <label for="">
                                    Google tags manager in header<br/>
                                    <small>{{trans('dizi-core-lang::setting.place_first', ['tag' => '&lt;head&gt;'])}} </small>
                                </label>
                                <div class="">
                                    <textarea class="form-control" rows="7" id="google_tags_manager_header" name="google_tags_manager_header">@{{item.google_tags_manager_header}}</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="">
                                    Google tags manager in body<br/>
                                    <small>{{trans('dizi-core-lang::setting.place_first', ['tag' => '&lt;body&gt;'])}}</small>
                                </label>
                                <div class="">
                                    <textarea class="form-control" rows="7" id="google_tags_manager_body" name="google_tags_manager_body">@{{item.google_tags_manager_body}}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-content border-none">
                        <div class="" style="display: @{{tabShow == 'webmaster' ? 'block' : 'none'}};">
                            <div class="form-group">
                                <label for="">
                                    Google webmaster tool<br/>
                                    <small>{{trans('dizi-core-lang::setting.place_last', ['tag' => '&lt;head&gt;'])}}</small>
                                </label>
                                <div class="">
                                    <textarea class="form-control" rows="7" id="google_webmaster_tool" name="google_webmaster_tool">@{{item.google_webmaster_tool}}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-content border-none">
                        <div class="" style="display: @{{tabShow == 'remarketing' ? 'block' : 'none'}};">
                            <div class="form-group">
                                <label for="">
                                    Script remarketing google<br/>
                                    <small>{{trans('dizi-core-lang::setting.place_last', ['tag' => '&lt;head&gt;'])}}</small>
                                </label>
                                <div class="">
                                    <textarea class="form-control" rows="7" id="script_remarketing_google" name="script_remarketing_google">@{{item.script_remarketing_google}}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-content border-none">
                        <div class="" style="display: @{{tabShow == 'analytics' ? 'block' : 'none'}};">
                            <div class="form-group">
                                <label for="">
                                    Google analytics<br/>
                                    <small>{{trans('dizi-core-lang::setting.place_last', ['tag' => '&lt;head&gt;'])}}</small>
                                </label>
                                <div class="">
                                    <textarea class="form-control" rows="7" id="google_analytics" name="google_analytics">@{{item.google_analytics}}</textarea>
                                </div>
                            </div>
                            <p style="font-size: 20px;">{{trans('dizi-core-lang::setting.analytic_config')}}</p>
                            <div class="form-group">
                                <label for="">View ID</label>
                                <div class="">
                                    <input class="form-control" type="text" name="view_id" ng-model="item.view_id">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="">Service account credentials json file</label>
                                <div class="">
                                    <input class="form-control" name="service_account_credentials_json" ng-model="item.service_account_credentials_json" disabled="">
                                    <div style="margin-top: 10px"><a class="btn btn-primary" href="javascript:void(0)" ng-click="getModalLibraries('service_account_credentials_json')">Upload service account credentials json</a></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="">Cache lifetime in minutes</label>
                                <div class="">
                                    <input class="form-control" type="text" name="cache_lifetime_in_minutes" ng-model="item.cache_lifetime_in_minutes">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-content border-none">
                        <div class="" style="display: @{{tabShow == 'sitemaps' ? 'block' : 'none'}};">
                            <div class="form-group">
                                Sitemap: <span class="sitemap-url">
                                    <a href="{{route('sitemaps')}}" target="_blank">{{route('sitemaps')}}</a></span>
                            </div>
                        </div>
                    </div>
                    <div class="tab-content border-none">
                        <div class="" style="display: @{{tabShow == 'robot' ? 'block' : 'none'}};">
                            <div class="form-group">
                                <label for="">Nội dung</label>
                                <div class="">
                                    <textarea class="form-control" rows="7" 
                                              placeholder="Nội dung" 
                                              type="text" name="robot_txt" ng-model="item.robot_txt">
                                    </textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 form-group">
                    <button class="btn btn-primary pull-right" ng-click="submit()">
                        <span><i class="fa fa-pencil-square-o"></i> Cập nhật</span>
                    </button>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
@endsection

@section('script')
    {!! Html::script('backend/app/components/post/PostService.js?v='.getVersionScript()) !!}
    {!! Html::script('backend/app/components/library/LibraryService.js?v='.getVersionScript()) !!}
    {!! Html::script('backend/app/components/setting/SettingService.js?v='.getVersionScript()) !!}
    {!! Html::script('backend/app/components/setting/SettingController.js?v='.getVersionScript()) !!}
    {!! Html::script('backend/app/shared/file-upload/FileService.js?v='.getVersionScript()) !!}
    {!! Html::script('backend/app/shared/file-upload/FileUploadDirective.js?v='.getVersionScript()) !!}
@endsection


