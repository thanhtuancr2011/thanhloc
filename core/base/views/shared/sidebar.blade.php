<div class="sidebar">
    <nav class="sidebar-nav">
        <ul class="nav">
            <li class="nav-item">
                <a class="nav-link" href="{{ URL::to('admin') }}">
                    <i class="icon-speedometer"></i> Dashboard
                </a>
            </li>

            @if (Base::can('index.role'))
            <li class="nav-item nav-dropdown">
                <a class="nav-link" href="{{ URL::to('admin/role') }}">
                    <i class="icon-lock-open"></i> Quyền
                </a>
            </li>
            @endif

            @if (Base::can('index.user'))
            <li class="nav-item nav-dropdown">
                <a class="nav-link nav-dropdown-toggle" href="javascript:void(0)">
                    <i class="icon-people" aria-hidden="true"></i> Tài khoản
                </a>
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL::to('admin/user?type=admin') }}">
                            <i class="fa fa-certificate"></i> Quản trị
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL::to('admin/user?type=customer') }}">
                            <i class="fa fa-certificate"></i> Khách hàng
                        </a>
                    </li>
                </ul>
            </li>
            @endif

            @if (Base::can('index.menu'))
            <li class="nav-item nav-dropdown">
                <a class="nav-link" href="{{ URL::to('admin/menu') }}">
                    <i class="fa fa-list"></i> Menu
                </a>
            </li>
            @endif

            @if (Base::can('index.library'))
            <li class="nav-item nav-dropdown">
                <a class="nav-link" href="{{ URL::to('admin/library') }}">
                    <i class="icon-camera"></i> Thư viện
                </a>
            </li>
            @endif

            @if (Base::can('index.tag') || Base::can('index.category') || Base::can('index.post') || 
                Base::can('index.page') || Base::can('index.slide'))
            <li class="nav-item nav-dropdown">
                <a class="nav-link nav-dropdown-toggle" href="javascript:void(0)">
                    <i class="fa fa-newspaper-o" aria-hidden="true"></i> Nội dung
                </a>
                <ul class="nav-dropdown-items">
                    @if (Base::can('index.tag'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL::to('admin/term?taxonomy=post_tag') }}">
                            <i class="fa fa-certificate"></i> Từ khóa
                        </a>
                    </li>
                    @endif

                    @if (Base::can('index.category'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL::to('admin/term?taxonomy=category') }}">
                            <i class="fa fa-certificate"></i> Danh mục
                        </a>
                    </li>
                    @endif

                    @if (Base::can('index.post'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL::to('admin/post?type=post') }}">
                            <i class="fa fa-certificate"></i> Bài viết
                        </a>
                    </li>
                    @endif

                    @if (Base::can('index.page'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL::to('admin/post?type=page') }}">
                            <i class="fa fa-certificate"></i> Trang
                        </a>
                    </li>
                    @endif

                    @if (Base::can('index.slide'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL::to('admin/image-slider') }}">
                            <i class="fa fa-certificate"></i> Banner & slide
                        </a>
                    </li>
                    @endif
                </ul>
            </li>
            @endif

            @if (Base::can('index.setting'))
            {!! Theme::partial('admin.sidebar') !!}
            @endif

            @if (Base::can('index.productcategory') || Base::can('index.product'))
            <li class="nav-item nav-dropdown">
                <a class="nav-link nav-dropdown-toggle" href="javascript:void(0)">
                    <i class="icon-social-dropbox" aria-hidden="true"></i> Sản phẩm
                </a>
                <ul class="nav-dropdown-items">
                    @if (Base::can('index.productcategory'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL::to('admin/category') }}">
                            <i class="fa fa-certificate"></i> Danh mục
                        </a>
                    </li>
                    @endif

                    @if (Base::can('index.product'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL::to('admin/product') }}">
                            <i class="fa fa-certificate"></i> Sản phẩm
                        </a>
                    </li>
                    @endif
                </ul>
            </li>
            @endif

            @if (Base::can('index.order'))
            <li class="nav-item nav-dropdown">
                <a class="nav-link" href="{{ URL::to('admin/order') }}">
                    <i class="fa fa-shopping-cart" aria-hidden="true"></i> Đơn hàng
                </a>
            </li>
            @endif

            @if (Base::can('index.setting'))
            <li class="nav-item nav-dropdown">
                <a class="nav-link" href="{{ URL::to('admin/setting/google') }}">
                    <i class="fa fa-google"></i> Google
                </a>
            </li>
            <li class="nav-item nav-dropdown">
                <a class="nav-link" href="{{ URL::to('admin/setting') }}">
                    <i class="icon-settings"></i> Cấu hình
                </a>
            </li>
            @endif
        </ul>
    </nav>
    <button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div>