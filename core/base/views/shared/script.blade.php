
{!! Html::script('bower_components/angular/angular.min.js')!!}  
{!! Html::script('bower_components/angular-resource/angular-resource.js')!!}
{!! Html::script('bower_components/angular-sanitize/angular-sanitize.min.js')!!}
{!! Html::script('bower_components/angular-bootstrap/ui-bootstrap.min.js')!!}
{!! Html::script('bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js')!!}
{!! Html::script('bower_components/bootstrap/dist/js/bootstrap.min.js')!!}
{!! Html::script('bower_components/ngImgCrop/source/js/init.js')!!}
{!! Html::script('bower_components/ngImgCrop/source/js/ng-img-crop.js')!!}
{!! Html::script('bower_components/ngImgCrop/compile/minified/ng-img-crop.js')!!}
{!! Html::script('bower_components/angular-xeditable/dist/js/xeditable.js') !!}
{!! Html::script('bower_components/select2/dist/js/select2.full.min.js') !!}
{!! Html::script('bower_components/jquery.fancytree/dist/jquery.fancytree-all.min.js') !!}
{!! Html::script('bower_components/angular-drag-and-drop-lists/angular-drag-and-drop-lists.min.js') !!}
{!! Html::script('bower_components/remarkable-bootstrap-notify/bootstrap-notify.min.js') !!}
{!! Html::script('bower_components/tinymce/tinymce.min.js') !!}
{!! Html::script('bower_components/angular-filter/dist/angular-filter.min.js') !!}
{!! Html::script('bower_components/angular-ui-tree/dist/angular-ui-tree.js') !!}
{!! Html::script('bower_components/Jcrop/js/jquery.Jcrop.min.js') !!}
{!! Html::script('bower_components/jquery-mask-plugin/src/jquery.mask.js') !!}
{{-- {!! Html::script('bower_components/PACE/pace.min.js') !!} --}}

{!! Html::script('bower_components/codemirror/lib/codemirror.js') !!}
{!! Html::script('bower_components/codemirror/mode/javascript/javascript.js') !!}
{!! Html::script('bower_components/codemirror/addon/selection/active-line.js') !!}
{!! Html::script('bower_components/codemirror/addon/edit/matchbrackets.js') !!}

<!-- upload js  -->
{!! Html::script('bower_components/ng-file-upload/ng-file-upload-all.min.js')!!}
{!! Html::script('bower_components/ng-file-upload/ng-file-upload-shim.min.js')!!}


<!-- Export excel  -->
{!! Html::script('bower_components/alasql/dist/alasql.min.js')!!}
{!! Html::script('bower_components/js-xlsx/dist/xlsx.core.min.js')!!}

<!-- My script file -->
{!! Html::script('backend/js/HoldOn.min.js')!!}
{!! Html::script('backend/js/Sortable.min.js')!!}
{!! Html::script('backend/js/app.js?v='.getVersionScript()) !!}

<!-- My Angular js File -->
{!! Html::script('backend/app/app.js?v='.getVersionScript()) !!}
{!! Html::script('backend/app/config.js?v='.getVersionScript()) !!}
{!! Html::script('backend/app/base.js?v='.getVersionScript()) !!}

<script type="text/javascript">
    window.baseUrl = '{{URL::to("")}}';
</script>

<!-- Core JavaScript -->
@yield('script')



    