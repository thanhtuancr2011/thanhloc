<header class="app-header navbar">
    @if (Auth::user())
    <button class="navbar-toggler mobile-sidebar-toggler d-lg-none mr-auto" type="button"> 
        <span class="navbar-toggler-icon"></span> 
    </button> 
    <a class="navbar-brand" href="#"></a> 
    <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button"> 
        <span class="navbar-toggler-icon"></span> 
    </button> 
    <ul class="nav navbar-nav ml-auto">
        <li class="nav-item d-md-down-none">
            <a class="nav-link" href="{{URL::to('')}}" target="_blank">
                <i class="fa fa-globe" aria-hidden="true"></i> Xem website
            </a>
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                <img src="{{ !\Auth::user()->avatar ? asset('/backend/avatars/160x160_avatar_default.png') : asset('/backend/avatars/' . \Auth::user()->avatar) }}" class="img-avatar img-circles" alt="{{ Auth::user()->email }}"> 
                <span class="d-md-down-none">{{ \Auth::user()->full_name }} </span> 
            </a> 
            <div class="dropdown-menu dropdown-menu-right">
                <a class="dropdown-item first" href="{{ URL::to('auth/logout') }}">
                    <i class="fa fa-sign-out" aria-hidden="true"></i> Đăng xuất
                </a>
            </div>
        </li>
    </ul>
    @endif
</header>