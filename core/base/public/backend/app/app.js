
var defaultModules = 
[
    'xeditable',
    'ui.bootstrap',
    'angular.filter',
    'ui.tree',
    'ngResource',
    'ngSanitize',
    'ngImgCrop',
    'ngFileUpload',
    'dndLists',
    'appBase',
    'appUser',
    'appRole',
    'appPermission',
    'appTerm',
    'appLibrary',
    'appCustomer',
    'appPost',
    'appMenu',
    'appSetting',
    'appOrganization'
];

if(typeof modules != 'undefined'){
    defaultModules = defaultModules.concat(modules);
}

// Editable options
angular.module('myApp', defaultModules).run(['editableOptions',function(editableOptions) {
    editableOptions.theme = 'bs3';
}]);

// Slug value
function slugValue (text, slugInput) {

    // Nếu text is null
    if (text == null) return text; 
    
    //Đổi chữ hoa thành chữ thường
    slug = text.toLowerCase();
    
    //Đổi ký tự có dấu thành không dấu
    slug = slug.replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, 'a');
    slug = slug.replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, 'e');
    slug = slug.replace(/i|í|ì|ỉ|ĩ|ị/gi, 'i');
    slug = slug.replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, 'o');
    slug = slug.replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, 'u');
    slug = slug.replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, 'y');
    slug = slug.replace(/đ/gi, 'd');

    // Xóa các ký tự đặt biệt
    slug = slug.replace(/\`|\~|\!|\@|\#|\||\$|\%|\^|\&|\*|\(|\)|\+|\=|\,|\.|\/|\?|\>|\<|\'|\"|\:|\;|\“|\”|_/gi, '');

    // Đổi nhiều kí tự khoảng trắng thành 1 kí tự
    slug = slug.replace(/\s+/g, ' ');
    slug = slug.replace(/\s/gi, slugInput);

    // Đổi nhiều ký tự gạch ngang liên tiếp thành 1 ký tự gạch ngang
    // Phòng trường hợp người nhập vào quá nhiều ký tự trắng
    slug = slug.replace(/\-+/gi, slugInput);
    
    // Xóa các ký tự gạch ngang ở đầu và cuối
    slug = '@' + slug + '@';
    slug = slug.replace(/\@\-|\-\@|\@/gi, '');

    // In slug ra textbox có id “slug”
    return slug;
}

// Show loading
var optionsHoldOn = {
    theme:"sk-bounce",
    message:'',
    backgroundColor:"#000",
    textColor:"white"
};

// Config ajax with token
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

var Notify = function(message) {
    setTimeout(function() {
        $.notify({
            // options
            icon: 'glyphicon glyphicon-info-sign',
            title: '<span style="color: #08aba7">Thông báo: <br /></span>',
            message: message,
            url: 'javascript:void(0)',
            target: '_blank'
        },{
            // settings
            element: 'body',
            position: null,
            type: "info custom-notify",
            allow_dismiss: true,
            newest_on_top: false,
            showProgressbar: false,
            placement: {
                from: "top",
                align: "right"
            },
            offset: {
                x: 30,
                y: 10
            },
            spacing: 10,
            z_index: 1060,
            delay: 5000,
            timer: 1000,
            url_target: '_blank',
            mouse_over: null,
            animate: {
                enter: 'animated fadeInRight',
                exit: 'animated fadeOutRight'
            },
            onShow: null,
            onShown: null,
            onClose: null,
            onClosed: null,
            icon_type: 'class',
        });
    }, 500);
}

/**
 * Get url of item
 * @param  {String} slug The item slug
 * @param  {String} url  The url
 * @return {Void}      
 */
function getUrl(slug, url) {
    $.ajax({
        type: 'POST',
        url: "/api/get-url",
        data: {slug: slug, url: url},
        success: function(resultData) { 
            console.log(resultData, 'resultData');
            return resultData;
        }
    });
}

// Create Base64 Object
var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9+/=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/rn/g,"n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}
