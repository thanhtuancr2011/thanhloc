var pagination = angular.module('myApp');
pagination.directive("paginationDirective", ['$timeout', '$filter', '$http', '$rootScope', function($timeout, $filter, $http, $rootScope) {
    return {
        require: '?ngModel',
        restrict: 'EA',
        replace: true,
        templateUrl: baseUrl + '/backend/app/shared/pagination/views/pagination.html?v=' + new Date().getTime(),
        link: function($scope, element, attrs, ngModel) {

            // Translate text
            $scope.transLast = window.transLast;
            $scope.transFirst = window.transFirst;
            $scope.transItemsPerPage = window.transItemsPerPage;

            /**
             * Onload when controller call to set page options
             * @author Thanh Tuan <thanhtuancr2011@gmail.com>
             * @param  {Object} event The event 
             * @param  {Object} data) The page options
             * @return {Void}       
             */
            $scope.$on('pageOptions',function(event, data) {
                $scope.currentPage  = data.currentPage;
                $scope.itemsPerPage = data.itemsPerPage;
                $scope.totalPages   = data.totalPages;
                $scope.totalItems   = data.totalItems;
            });

            /**
             * Set page options
             * @author Thanh Tuan <thanhtuancr2011@gmail.com>
             * @param {Integer} currentPage  The current page
             * @param {Integer} itemsPerPage The items per page
             */
            $scope.setPageOptions = function(currentPage, itemsPerPage) {
                $scope.pageOptions.currentPage  = (currentPage !== null) ? currentPage : $scope.pageOptions.currentPage;
                $scope.pageOptions.itemsPerPage = (itemsPerPage !== null) ? itemsPerPage : $scope.pageOptions.itemsPerPage;
                $rootScope.$broadcast('setPageOptions', $scope.pageOptions);
            }
        }
    }
}])
.filter('range', function() {
    return function(input, total) {
        total = parseInt(total);
        for (var i = 0; i < total; i++) {
            input.push(i);
        }
        return input;
    };
});
