roleApp.controller('RoleController', ['$scope', '$uibModal', '$filter', 'RoleService', '$timeout', '$rootScope', function ($scope, $uibModal, $filter, RoleService, $timeout, $rootScope) {

	// When js didn't  loaded then hide table term
	$('.container-fluid').removeClass('hidden');
	HoldOn.open(optionsHoldOn);
	
	/**
	 * Get list items
	 * @author Thanh Tuan <thanhtuancr2011@gmail.com>
	 * @return {Void} 
	 */
	$scope.getItems = function() {

		// Reset all checkbox
		$scope.toggleSelection('none');

		RoleService.getItems({
			pageOptions: $scope.pageOptions,
			searchOptions: $scope.searchOptions,
		}).then(function (data) {
			if (data.status == 1) {

				// Set terms value
				$scope.roles = angular.copy(data.roles);
				$scope.pageOptions.totalPages = data.totalPages;
				$scope.pageOptions.totalItems = data.totalItems;
				$rootScope.$broadcast('pageOptions', $scope.pageOptions);

				// Show loading
				HoldOn.close();
			}
		});
	}

	$timeout(function() {
		// Call function get items
		$scope.getItems();
	});

	/**
     * Search roles
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @return {Void}                 */
    $scope.searchRoles = function(keyEvent) {

    	if (keyEvent == 'submit' || keyEvent.which === 13) {
    		HoldOn.open(optionsHoldOn);
	    	$scope.getItems();
    	}
    } 
	
	$scope.getModalRole = function(id) {
		
		// When create
		var template = '/admin/role/create?v='+ new Date().getTime();

		// When update
		if(typeof id != 'undefined'){
			template = '/admin/role/'+ id + '/edit?v=' + new Date().getTime();
		}

		var modalInstance = $uibModal.open({
		    animation: true,
		    templateUrl: window.baseUrl + template,
		    controller: 'ModalCreateRoleCtrl',
		    size: null,
		    resolve: {
		    }
		});

		/* After create or edit role then reset role and reload ng-table */
		modalInstance.result.then(function (data) {
			$scope.getItems();
		}, function () {

		   });
	};

	/**
	 * Delete the role
	 * @author Thanh Tuan <thanhtuancr2011@gmail.com>
	 * @param  {Integer} id   The category id
	 * @param  {String}  size Type of modal
	 * @return {Void}     
	 */
	$scope.removeRole = function(id){

		if (angular.isDefined(id)) {
			var roleIds = [id];
		} else {
			var roleIds = $scope.listIdsSelected;
		}
		
		var template = '/backend/app/components/role/views/delete.html?v=' + new Date().getTime();
		var modalInstance = $uibModal.open({
		    animation: $scope.animationsEnabled,
		    templateUrl: window.baseUrl + template,
		    controller: 'ModalDeleteRoleCtrl',
		    size: 'sm',
		    resolve: {
		    	roleIds: function(){
		            return roleIds;
		        }
		    }
		});

		modalInstance.result.then(function (data) {
			$scope.getItems();
		}, function () {});
	};

	/**
     * Onload when controller call to set page options
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  {Object} event The event 
     * @param  {Object} data) The page options
     * @return {Void}       
     */
    $scope.$on('setPageOptions',function(event, data) {
        HoldOn.open(optionsHoldOn);
        $scope.pageOptions = data;
        $scope.getItems();
    });

}])
.controller('ModalCreateRoleCtrl', ['$scope', 'RoleService', '$timeout', '$filter', function ($scope, RoleService, $timeout, $filter) {

	// When js didn't  loaded then hide table term
	$('.container-fluid').removeClass('hidden');
	HoldOn.open(optionsHoldOn);

	$timeout(function() {

		// Each permissions
		$scope.lstPers = [];
		angular.forEach($scope.permissions, function(value, key) {
			if (angular.isUndefined($scope.lstPers[value.table])) 
				$scope.lstPers[value.table] = [];
			var strSlug = value.slug.split('.');
			if (angular.isUndefined($scope.lstPers[value.table][strSlug[0]]))
				$scope.lstPers[value.table][strSlug[0]] = [];
			$scope.lstPers[value.table][strSlug[0]] = value;
		});

		$timeout(function() {
			// When edit
			if (angular.isDefined($scope.roleItem.id)) {
				$.map($scope.roleItem.permissions, function(permission) {
					$('#checkbox' + permission.id).prop('checked', true);
				});
				$.map($scope.roleItem.status, function(status) {
					$('#checkboxStatus' + status.id).prop('checked', true);
				});
			}
		})

		HoldOn.close();
	});

	// When role click add or edit role
	$scope.submit = function (validate) {
		console.log(validate, 'validate');
		// Validate
		$scope.submitted = true;
		if (!validate) return;

		// Loading
		HoldOn.open(optionsHoldOn);

		// Set permission
		$scope.roleItem.permissions = $('.ckb-per:checked').map(function() {
		    return parseInt($(this).val());
		}).get();

		// Set status
		$scope.roleItem.status = $('.ckb-status:checked').map(function() {
		    return parseInt($(this).val());
		}).get();

		RoleService.createRoleProvider($scope.roleItem).then(function (data) {

			// Close loading
			HoldOn.close();

			if (data.status == -1) {
				$scope.errors = data.errors;
			} else {
				if (!angular.isDefined($scope.roleItem.id)) {
					console.log(data, 'data');
					window.location.href = window.baseUrl + '/admin/role/' + data.role.id + '/edit'; 
				} else {
					Notify(data.msg);
				}
			}
		})
	};

	/* When role click cancel then close modal popup */
	$scope.cancel = function () {
	    $uibModalInstance.dismiss('cancel');
	};
}])
.controller('ModalDeleteRoleCtrl', ['$scope', '$uibModalInstance', 'roleIds', 'RoleService', function ($scope, $uibModalInstance, roleIds, RoleService) {
	
	// When category click update the post for category
	$scope.submit = function () {

		// Loading
		HoldOn.open(optionsHoldOn);

		RoleService.deleteRoles(roleIds).then(function (data) {
			
			// Close loading
			HoldOn.close();

			$uibModalInstance.close();
			Notify(data.msg);
		});
	};

	// When category click cancel then close modal popup
	$scope.cancel = function () {
	    $uibModalInstance.dismiss('cancel');
	};
}]);