var menuApp = angular.module('appMenu', []);
menuApp.factory('MenuResource',['$resource', function ($resource){
    return $resource('/api/menu/:method/:id', {'method':'@method','id':'@id'}, {
        add: {method: 'post'},
        save:{method: 'post'},
        update:{method: 'put'}
    });
}])
.service('MenuService', ['MenuResource', '$q', function (MenuResource, $q) {
    var that = this;

    /**
     * Get items
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @return {Void} 
     */
    this.getItems = function(data) {
        var defer = $q.defer(); 
        var temp  = new MenuResource(data);
        temp.$save({method: 'menus'}, function success(data) {
            defer.resolve(data);
        }, function error(reponse) {
            defer.resolve(reponse.data);
        });
        return defer.promise;  
    }

    /**
     * Get items
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @return {Void} 
     */
    this.getMenuItems = function(data) {
        var defer = $q.defer(); 
        var temp  = new MenuResource();
        temp.$get({id: data['id'], method: 'menu-items'}, function success(data) {
            defer.resolve(data);
        }, function error(reponse) {
            defer.resolve(reponse.data);
        });
        return defer.promise;  
    }

    /**
     * Function create new menu
     * @author Thanh Tuan <tuannt@acro.vn>
     * @param  {Array} data The data input
     * @return {Void}      
     */
	this.createMenuProvider = function(data){

        // If isset id of menu then call function edit menu
        if(typeof data['id'] != 'undefined') {
            return that.editMenuProvider(data);
        }

		var defer = $q.defer(); 
        var temp  = new MenuResource(data);

        temp.$save({}, function success(data) {
            // Resolve result 
            defer.resolve(data);
        }, function error(reponse) {
            // Resolve result 
        	defer.resolve(reponse.data);
        });
        return defer.promise;  
	};

    /**
     * The function edit menu
     * @author Thanh Tuan <tuannt@acro.vn>
     * @param  {Array} data The data input 
     * @return {Void}      
     */
    this.editMenuProvider = function(data){

        var defer = $q.defer(); 
        var temp  = new MenuResource(data);

        // Update menu successfull 
        temp.$update({id: data['id']}, function success(data) {
            defer.resolve(data);
        }, function error(reponse) {
            // If create menu is error 
            defer.resolve(reponse.data);
        });
        
        return defer.promise;  
    };

    /**
     * The function edit menu
     * @author Thanh Tuan <tuannt@acro.vn>
     * @param  {Array} data The data input 
     * @return {Void}      
     */
    this.addMenuItems = function(data) {
        var defer = $q.defer(); 
        var temp  = new MenuResource(data);

        // Update menu successfull 
        temp.$save({id: data['id'], method: 'add-menu-item'}, function success(data) {
            defer.resolve(data);
        }, function error(reponse) {
            // If create menu is error 
            defer.resolve(reponse.data);
        });
        
        return defer.promise;  
    };

    /**
     * The function delete menu item
     * @author Thanh Tuan <tuannt@acro.vn>
     * @param  {Array} data The data input 
     * @return {Void}      
     */
    this.deleteMenuItem = function(data) {
        var defer = $q.defer(); 
        var temp  = new MenuResource(data);

        // Update menu successfull 
        temp.$save({id: data['id'], method: 'delete-menu-item'}, function success(data) {
            defer.resolve(data);
        }, function error(reponse) {
            // If create menu is error 
            defer.resolve(reponse.data);
        });
        
        return defer.promise;  
    };

    /**
     * The function edit menu
     * @author Thanh Tuan <tuannt@acro.vn>
     * @param  {Array} data The data input 
     * @return {Void}      
     */
    this.changeMenuItemPosition = function(data) {
        var defer = $q.defer(); 
        var temp  = new MenuResource(data);

        // Update menu successfull 
        temp.$save({id: data['id'], method: 'change-menu-item-position'}, function success(data) {
            defer.resolve(data);
        }, function error(reponse) {
            // If create menu is error 
            defer.resolve(reponse.data);
        });
        
        return defer.promise;  
    };

    /**
     * Delete the menus
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  {Integer} id The menu ids
     * @return {Void}    
     */
    this.deleteMenus = function (menuIds) {

        var defer = $q.defer(); 
        var temp  = new MenuResource(menuIds);

        // Delete categories is successfull
        temp.$save({method: 'delete'}, function success(data) {
            defer.resolve(data);
        },
        
        // If delete category is error
        function error(reponse) {
            defer.resolve(reponse.data);
        });
        return defer.promise;  
    }

}]);
