menuApp.controller('MenuController', ['$scope', '$uibModal', '$filter', 'MenuService', '$timeout', '$rootScope', function ($scope, $uibModal, $filter, MenuService, $timeout, $rootScope) {

	// When js didn't  loaded then hide table term
	$('.container-fluid').removeClass('hidden');
	HoldOn.open(optionsHoldOn);
	
	/**
	 * Get list items
	 * @author Thanh Tuan <thanhtuancr2011@gmail.com>
	 * @return {Void} 
	 */
	$scope.getItems = function() {

		// Reset all checkbox
		$scope.toggleSelection('none');

		MenuService.getItems({
			pageOptions: $scope.pageOptions,
			searchOptions: $scope.searchOptions,
		}).then(function (data) {
			if (data.status == 1) {

				// Set terms value
				$scope.menus = angular.copy(data.menus);
				$scope.pageOptions.totalPages = data.totalPages;
				$scope.pageOptions.totalItems = data.totalItems;
				$rootScope.$broadcast('pageOptions', $scope.pageOptions);

				// Show loading
				HoldOn.close();
			}
		});
	}

	$timeout(function() {
		// Call function get items
		$scope.getItems();
	});

	/**
     * Search menus
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @return {Void}                 */
    $scope.searchMenus = function(keyEvent) {

    	if (keyEvent == 'submit' || keyEvent.which === 13) {
    		HoldOn.open(optionsHoldOn);
	    	$scope.getItems();
    	}
    } 
	
	$scope.getModalMenu = function(id) {
		
		// When create
		var template = '/admin/menu/create?v='+ new Date().getTime();

		// When update
		if(typeof id != 'undefined'){
			template = '/admin/menu/'+ id + '/edit?v=' + new Date().getTime();
		}

		var modalInstance = $uibModal.open({
		    animation: true,
		    templateUrl: window.baseUrl + template,
		    controller: 'ModalCreateMenuCtrl',
		    size: null,
		    resolve: {
		    }
		});

		/* After create or edit menu then reset menu and reload ng-table */
		modalInstance.result.then(function (data) {
			$scope.getItems();
		}, function () {

		   });
	};

	/**
     * Onload when controller call to set page options
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  {Object} event The event 
     * @param  {Object} data) The page options
     * @return {Void}       
     */
    $scope.$on('setPageOptions',function(event, data) {
        HoldOn.open(optionsHoldOn);
        $scope.pageOptions = data;
        $scope.getItems();
    });

	/**
	 * Delete the menu
	 * @author Thanh Tuan <thanhtuancr2011@gmail.com>
	 * @param  {Integer} id   The category id
	 * @param  {String}  size Type of modal
	 * @return {Void}     
	 */
	$scope.removeMenu = function(id){

		if (angular.isDefined(id)) {
			var menuIds = [id];
		} else {
			var menuIds = $scope.listIdsSelected;
		}
		
		var template = '/backend/app/components/menu/views/delete.html?v=' + new Date().getTime();
		var modalInstance = $uibModal.open({
		    animation: $scope.animationsEnabled,
		    templateUrl: window.baseUrl + template,
		    controller: 'ModalDeleteMenuCtrl',
		    size: 'sm',
		    resolve: {
		    	menuIds: function(){
		            return menuIds;
		        }
		    }
		});

		modalInstance.result.then(function (data) {
			$scope.getItems();
		}, function () {});
	};

}])
.controller('ModalCreateMenuCtrl', ['$scope', '$uibModalInstance', 'MenuService', '$timeout', '$filter', function ($scope, $uibModalInstance, MenuService, $timeout, $filter) {

	// When menu click add or edit menu
	$scope.submit = function (validate) {

		// Validate
		$scope.submitted = true;
		if (!validate) return;

		// Loading
		HoldOn.open(optionsHoldOn);

		MenuService.createMenuProvider($scope.menuItem).then(function (data) {

			// Close loading
			HoldOn.close();

			if (data.status == -1) {
				$scope.errors = data.errors;
			} else {
				$uibModalInstance.close();
				Notify(data.msg);
			}
		})
	};

	// When menu click cancel then close modal popup 
	$scope.cancel = function () {
	    $uibModalInstance.dismiss('cancel');
	};
}])
.controller('ModalDeleteMenuCtrl', ['$scope', '$uibModalInstance', 'menuIds', 'MenuService', function ($scope, $uibModalInstance, menuIds, MenuService) {
	
	// When category click update the post for category
	$scope.submit = function () {

		// Loading
		HoldOn.open(optionsHoldOn);

		MenuService.deleteMenus(menuIds).then(function (data) {
			
			// Close loading
			HoldOn.close();

			$uibModalInstance.close();
			Notify(data.msg);
		});
	};

	// When category click cancel then close modal popup
	$scope.cancel = function () {
	    $uibModalInstance.dismiss('cancel');
	};
}])
.controller('MenuItemController', ['$scope', 'MenuService', '$timeout', function ($scope, MenuService, $timeout) {

	// When js didn't  loaded then hide table term
	$('.container-fluid').removeClass('hidden');

	$scope.treeOptions = {
	    accept: function (sourceNodeScope, destNodesScope, destIndex) {
	    	if (destNodesScope.$nodeScope == null || 
	    		destNodesScope.$nodeScope.$modelValue.level < $scope.menu.depth) {
	    		$scope.draged = true;
		    	return true;
	    	}
        },
        itemRemoved: function (scope, modelData, sourceIndex) {
            console.log("Removed");
        },
        itemAdded: function (scope, modelData, destIndex) {
            console.log("Added");
        },
        itemMoved: function (sourceScope, modelData, sourceIndex, destScope, destIndex) {
            console.log("Moved");
        },
        orderChanged: function (scope, modelData, sourceIndex, destIndex) {
             console.log("orderChanged");
        },
        itemClicked: function (modelData) {
            // console.log("clicked");
        },
        dragStart: function(event) {
        	// console.log('dragStart');
        },
        removed: function(node, event) {
        	return;
        },
        dragStop: function(event) {
        	// If not drag
        	if (!$scope.draged) return;

        	// If dest node is null or node level > menu level
        	if (event.dest.nodesScope.$nodeScope != null &&
        		event.dest.nodesScope.$nodeScope.$modelValue.level >= $scope.menu.depth) {
	        	return;
        	}

        	$timeout(function() {
        		// Close loading
				HoldOn.open(optionsHoldOn);

	        	MenuService.changeMenuItemPosition({
		        		id: $scope.menu.id,
		        		menuItems: $scope.menuItems
	        		}).then(function (data) {

	        		MenuService.getMenuItems({id: $scope.menu.id}).then(function (data) {
						$scope.menuItems = data.menuItems;
					});

					// Close loading
					HoldOn.close();
				});

	        	$scope.draged = false;
    		});
		}
	};

	/**
	 * When user click remove menu item
	 * @param  {Object} node Menu item
	 * @return {Void}      
	 */
	$scope.removeMenuItem = function(node) {
		$timeout(function() {
        		
    		// Close loading
			HoldOn.open(optionsHoldOn);

        	MenuService.deleteMenuItem({
	        		id: $scope.menu.id,
	        		menuItem: node.$modelValue
        		}).then(function (data) {

				// Close loading
				HoldOn.close();
				Notify(data.msg);

				MenuService.getMenuItems({id: $scope.menu.id}).then(function (data) {
					$scope.menuItems = data.menuItems;
				});
			});
		});
	}

	/**
	 * Add menu item
	 * @param {String}  type     Type of menu item
	 * @param {Boolean} validate Validate value
	 */
	$scope.addMenuItem = function (type, validate) {

		if (type == 'category') {

			// Get list ids selected
	    	$scope.dataSave = [];
	    	$scope.dataSave = $('.ckb-cat:checked').map(function() {
			    return parseInt($(this).val());
			}).get();

	    	// Category ids
			if ($scope.dataSave.length == 0) return;
		}

		if (type == 'page') {

			// Get list ids selected
	    	$scope.dataSave = [];
	    	$scope.dataSave = $('.ckb-post:checked').map(function() {
			    return parseInt($(this).val());
			}).get();

	    	// Category ids
			if ($scope.dataSave.length == 0) return;
		}

		if (type == 'customLink') {

			// Validate
			$scope.submitted = true;
			if (!validate) return;

			$scope.submitted = false;
			$scope.dataSave = $scope.menuItem;
		}

		$timeout(function() {

			// Loading
			HoldOn.open(optionsHoldOn);

			MenuService.addMenuItems({id: $scope.menu.id, type: type, data: $scope.dataSave}).then(function (data) {
				
				// Set data save
				$scope.dataSave = [];

				// Close loading
				HoldOn.close();
				Notify(data.msg);

				if (type == 'category' || type == 'post') {
					$('input[type=checkbox]').prop('checked', false);
				}

				if (type == 'customLink') {
					$scope.menuItem = {};
				}

				MenuService.getMenuItems({id: $scope.menu.id}).then(function (data) {
					$scope.menuItems = data.menuItems;
				});
			});
		})
	}
}]);