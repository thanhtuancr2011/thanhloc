libraryApp.controller('LibraryController', ['$scope', '$uibModal', '$filter', 'LibraryService', 'FileService', '$timeout', '$rootScope', function ($scope, $uibModal, $filter, LibraryService, FileService, $timeout, $rootScope) {

	// When js didn't  loaded then hide table term
	$('.container-fluid').removeClass('hidden');
	HoldOn.open(optionsHoldOn);

	/**
	 * Load file when user scroll
	 * author Thanh Tuan <thanhtuancr2011@gmail.com>
	 * @return {Void}   
	 */
	$(window).scroll(function() {
	   	if($(window).scrollTop() + $(window).height() == $(document).height() 
	   	   && $scope.pageGridOptions.currentPage < $scope.pageGridOptions.totalPages
	   	   && $scope.showStyle == 'grid') {
	   		$scope.pageGridOptions.currentPage++;
	   		$scope.getGridItems();
	   	}
	});

	// Set page options and search options of grid
	$scope.pageGridOptions = {};
	$scope.pageGridOptions.totalPages   = 0;
	$scope.pageGridOptions.totalItems   = 0;
	$scope.pageGridOptions.currentPage  = 1;
    $scope.pageGridOptions.itemsPerPage = 18;
	$scope.searchGridOptions = {};
	
	/**
	 * Get list items
	 * @author Thanh Tuan <thanhtuancr2011@gmail.com>
	 * @return {Void} 
	 */
	$scope.getListItems = function() {

		// Reset all checkbox
		$scope.toggleSelection('none');

		LibraryService.getItems({
			pageOptions: $scope.pageOptions,
			searchOptions: $scope.searchOptions,
		}).then(function (data) {
			if (data.status == 1) {

				// Set page options
				$scope.pageOptions.totalPages = data.totalPages;
				$scope.pageOptions.totalItems = data.totalItems;
				$rootScope.$broadcast('pageOptions', $scope.pageOptions);

				// Set libraries value
				$scope.authors = angular.copy(data.authors);
				$scope.libraries = angular.copy(data.libraries);

				// Show loading
				HoldOn.close();
			}
		});
	}

	/**
	 * Get grid items
	 * @author Thanh Tuan <thanhtuancr2011@gmail.com>
	 * @return {Void} 
	 */
	$scope.getGridItems = function(event = null) {

		LibraryService.getItems({
			pageOptions: $scope.pageGridOptions,
			searchOptions: $scope.searchGridOptions,
		}).then(function (data) {
			if (data.status == 1) {

				// Set page options
				$scope.pageGridOptions.totalPages = data.totalPages;
				$scope.pageGridOptions.totalItems = data.totalItems;

				// Set default value
				if (angular.isUndefined($scope.librariesGridView)) {
					$scope.librariesGridView = [];
				}

				// Set libraries value
				$scope.librariesGridView = $scope.librariesGridView.concat(angular.copy(data.libraries));

				// Set height
				$timeout(function() {
					$scope.gridMediaHeight = $('.grid-view-media').width();
				});

				// Show loading
				HoldOn.close();
			}
		});
	}

	/**
	 * When user click on file
	 * @param  {Void} e Element
	 * @return {Void}   
	 */
	$scope.chooseFile = function(fileId, e) {

		if ($scope.multipleChooice) {

			// Active class check
			if ($('.want-active-' + fileId).hasClass('active')) {
				$('.want-active-' + fileId).removeClass('active');
			} else {
				$('.want-active-' + fileId).addClass('active');
			}

			// Check file id is exists in array
			if ($scope.choosenIds.indexOf(fileId) == -1) {
				$scope.choosenIds.push(fileId);
			} else {
				$scope.choosenIds.splice($scope.choosenIds.indexOf(fileId), 1);
			}

			if ($scope.choosenIds.length > 0) {
				$scope.showBtnDlt = true;
			}

		} else {
			$scope.editFile(fileId);
		}
	}

	/**
	 * When user change style show
	 * @param  {String} newVal  Type style show
	 * @param  {String} oldVal  Type style show
	 * @return {Void}         
	 */
	$scope.$watch('showStyle', function(newVal, oldVal) {
		HoldOn.open(optionsHoldOn);
		switch(newVal) {
		    case 'list':
		        $scope.getListItems();
		        break;
		    case 'grid':
		    	// Reset all choose file
				$scope.choosenIds = [];
				$scope.showBtnDlt = false;
				$scope.librariesGridView = [];
				$scope.multipleChooice = false;
				$('.check-file-active').removeClass('active');
		        $scope.getGridItems();
		        break;
		}
	});

	/**
	 * When user change style show
	 * @param  {String} newVal  Type style show
	 * @param  {String} oldVal  Type style show
	 * @return {Void}         
	 */
	$scope.$watch('multipleChooice', function(newVal, oldVal) {
		if (newVal == false) {
			$scope.choosenIds = [];
			$scope.showBtnDlt = false;
			$scope.multipleChooice = false;
			$('.check-file-active').removeClass('active');
		}
	});

	/**
     * Onload when controller call to set page options
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  {Object} event The event 
     * @param  {Object} data) The page options
     * @return {Void}       
     */
    $scope.$on('setPageOptions',function(event, data) {
        HoldOn.open(optionsHoldOn);
        $scope.pageOptions = data;
        $scope.getListItems();
    });

    /**
     * Onload when file upload finished
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  {Object} event The event 
     * @param  {Object} data) The page options
     * @return {Void}       
     */
    $scope.$on('allUploadFinish',function(event, data) {
    	if ($scope.showStyle == 'list') {
	     	HoldOn.open(optionsHoldOn);
	        $scope.getListItems();
    	}
    });

    /**
     * Onload when file upload finished
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  {Object} event The event 
     * @param  {Object} data) The page options
     * @return {Void}       
     */
    $scope.$on('uploadFinish',function(event, data) {
    	if ($scope.showStyle == 'grid') {
    		$scope.librariesGridView = [data.file].concat($scope.librariesGridView);
    	} 
    });

	/**
     * Search roles
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @return {Void}                 
	 */
    $scope.searchLibraries = function(keyEvent) {
    	if (keyEvent == 'submit' || keyEvent.which === 13) {
    		HoldOn.open(optionsHoldOn);
	    	$scope.getListItems();
    	}
    } 

    /**
	 * Edit file
	 * @author Thanh Tuan <thanhtuancr2011@gmail.com>
	 * @param  {Integer} id   The file id
	 * @return {Void}     
	 */
	$scope.editFile = function(id) {
		template = '/admin/library/'+ id + '/edit?v=' + new Date().getTime();
        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: window.baseUrl + template,
            controller: 'ModelEditFileCtrl',
            size: 'lg',
            resolve: {
            	fileId: function(){
                    return id;
                }
            }
        });

        /* After create or edit user then reset user and reload ng-table */
        modalInstance.result.then(function (data) {
        	if ($scope.showStyle == 'list') {
				$scope.getListItems();
			} else {
				$scope.librariesGridView = [];
				$scope.getGridItems();
			}
        }, function () {});
	};

    /**
	 * Delete file
	 * @author Thanh Tuan <thanhtuancr2011@gmail.com>
	 * @param  {Integer} id   The file id
	 * @return {Void}     
	 */
	$scope.removeFiles = function(id){

		if ($scope.showStyle == 'list') {
			if (angular.isDefined(id)) {
				var fileIds = [id];
			} else {
				var fileIds = $scope.listIdsSelected;
			}
		} else {
			var fileIds = $scope.choosenIds;
		}
		
		var template = '/backend/app/components/library/views/delete.html?v=' + new Date().getTime();
		var modalInstance = $uibModal.open({
		    animation: $scope.animationsEnabled,
		    templateUrl: window.baseUrl + template,
		    controller: 'ModalDeleteFileCtrl',
		    size: 'sm',
		    resolve: {
		    	fileIds: function(){
		            return fileIds;
		        }
		    }
		});

		modalInstance.result.then(function (data) {
			if ($scope.showStyle == 'list') {
				$scope.getListItems();
			} else {
				$scope.librariesGridView = $scope.librariesGridView.filter( function( item ) {
				    return ($scope.choosenIds.indexOf(item.id) == -1);
				});
				$timeout(function() {
					$scope.choosenIds = [];
					$scope.showBtnDlt = false;
					$('.check-file-active').removeClass('active');
				})
			}
		}, function () {});
	};	

}]).controller('ModalDeleteFileCtrl', ['$scope', '$uibModalInstance', 'fileIds', 'LibraryService', function ($scope, $uibModalInstance, fileIds, LibraryService) {
	
	// When category click update the post for category
	$scope.submit = function () {

		// Loading
		HoldOn.open(optionsHoldOn);

		LibraryService.deleteFiles(fileIds).then(function (data) {
			
			// Close loading
			HoldOn.close();

			$uibModalInstance.close();
			Notify(data.msg);
		});
	};

	// When category click cancel then close modal popup
	$scope.cancel = function () {
	    $uibModalInstance.dismiss('cancel');
	};
}]).controller('ModelEditFileCtrl', ['$scope', '$uibModalInstance', 'fileId', 'LibraryService', '$timeout', function ($scope, $uibModalInstance, fileId, LibraryService, $timeout) {
    
	$timeout(function() {
		$('.left-detail').css('height', $(window).height() - (60 + 57 + 66));
		$('.right-detail').css('height', $(window).height() - (60 + 57 + 66));
		$('.left-detail').find('img').css('max-width', $('.left-detail').width());
		$('.left-detail').find('img').css('max-height', $('.left-detail').height() - 200);

		var cropMaxWidth = 400;
		var cropMaxHeight = 400;
		var canvas;
		var context;
		var jcropApi;

		$scope.prefSize;

		$timeout(function() {
			if ($scope.mimeContentTypes[$scope.fileItem.file_name.split('.').pop()]['group'] == 'Image') {
				var request = new XMLHttpRequest();
				request.open('GET', window.baseUrl + $scope.fileItem.url, true);
				request.responseType = 'blob';
				request.onload = function() {
				    var reader = new FileReader();
				    reader.readAsDataURL(request.response);
				    reader.onload =  function(e){
				    	$scope.image = new Image();
				    	$scope.image.onload = validateImage;
		            	$scope.image.src = e.target.result;
		            	$timeout(function() {
							restartJcrop();
						})
				    };
				};
				request.send();
			}
		});

		function validateImage() {
		    if (canvas != null) {
		        $scope.image = new Image();
		        $scope.image.onload = restartJcrop;
		        $scope.image.src = canvas.toDataURL('image/png');
		    } else restartJcrop();
		}

		function dataURLtoBlob(dataURL) {
			var BASE64_MARKER = ";base64,";
		    if (dataURL.indexOf(BASE64_MARKER) == -1) {
		        var parts = dataURL.split(",");
		        var contentType = parts[0].split(":")[1];
		        var raw = decodeURIComponent(parts[1]);

		        return new Blob([raw], {type: contentType});
		    }

		    var parts = dataURL.split(BASE64_MARKER);
		    var contentType = parts[0].split(":")[1];
		    var raw = window.atob(parts[1]);
		    var rawLength = raw.length;

		    var uInt8Array = new Uint8Array(rawLength);

		    for (var i = 0; i < rawLength; ++i) {
		        uInt8Array[i] = raw.charCodeAt(i);
		    }

		    return new Blob([uInt8Array], {type: contentType});
		}

		function restartJcrop() {
		    if (jcropApi != null) {
		        jcropApi.destroy();
		    }
		    $("#views").empty();
		    $("#views").append("<canvas id=\"canvas\">");
		    canvas  = $("#canvas")[0];

		    context = canvas.getContext("2d");
		    canvas.width = $scope.image.width;
		    canvas.height = $scope.image.height;
		    context.drawImage($scope.image, 0, 0);

		    $("#canvas").Jcrop({
		        onSelect: selectCanvas,
		        onRelease: clearCanvas,
		        boxWidth: cropMaxWidth,
		        boxHeight: cropMaxHeight
		    }, function() {
		        jcropApi = this;
		    });
		    clearCanvas();
		}

		function clearCanvas() {
		    $scope.prefSize = {
		        x: 0,
		        y: 0,
		        w: canvas.width,
		        h: canvas.height,
		    };
		    $scope.$apply();
		}

		function selectCanvas(coords) {
		    $scope.prefSize = {
		        x: Math.round(coords.x),
		        y: Math.round(coords.y),
		        w: Math.round(coords.w),
		        h: Math.round(coords.h)
		    };
		    $scope.$apply();
		}

		$scope.applyCrop = function() { 
		    canvas.width = $scope.prefSize.w;
		    canvas.height = $scope.prefSize.h;
		    context.drawImage(
		    	$scope.image, 
		    	$scope.prefSize.x, 
		    	$scope.prefSize.y, $scope.prefSize.w, 
		    	$scope.prefSize.h, 
		    	0, 
		    	0, 
		    	canvas.width, 
		    	canvas.height
		    );
		    $scope.dsbSaveBtn = false;
		    validateImage();
		}

		// function applyScale(scale) {
		//     if (scale == 1) return;
		//     canvas.width = canvas.width * scale;
		//     canvas.height = canvas.height * scale;
		//     context.drawImage(image, 0, 0, canvas.width, canvas.height);
		//     validateImage();
		// }

		// function applyRotate() {
		//     canvas.width = image.height;
		//     canvas.height = image.width;
		//     context.clearRect(0, 0, canvas.width, canvas.height);
		//     context.translate(image.height / 2, image.width / 2);
		//     context.rotate(Math.PI / 2);
		//     context.drawImage(image, -image.width / 2, -image.height / 2);
		//     validateImage();
		// }

		// function applyHflip() {
		//     context.clearRect(0, 0, canvas.width, canvas.height);
		//     context.translate(image.width, 0);
		//     context.scale(-1, 1);
		//     context.drawImage(image, 0, 0);
		//     validateImage();
		// }

		// function applyVflip() {
		//     context.clearRect(0, 0, canvas.width, canvas.height);
		//     context.translate(0, image.height);
		//     context.scale(1, -1);
		//     context.drawImage(image, 0, 0);
		//     validateImage();
		// }

		/**
		 * Upload image cropped
		 * @param  {Object} e The object element
		 * @return {Void}           
		 */
		$scope.saveImage = function() {
		    // Add file blob to the form data
		    var blob = dataURLtoBlob(canvas.toDataURL('image/png'));
		    formData = new FormData($(this)[0]);	
		    formData.append("cropped_image", blob, $scope.fileItem.file_name);

		    // Loading
			HoldOn.open(optionsHoldOn);

		    $.ajax({
		        url: window.baseUrl + '/api/library/crop',
		        type: "POST",
		        data: formData,
		        contentType: false,
		        cache: false,
		        processData: false,
		        success: function(data) {

		        	// Close loading
					HoldOn.close();

		        	if (data.status == 1) {
		        		$scope.fileName = data.file;
		        		$uibModalInstance.close();
						Notify(data.msg);
		        	}
		        },
		        error: function(data) {
		            // Close loading
					HoldOn.close();
					Notify(data.responseJSON.msg);
		        },
		        complete: function(data) {}
		    });
		}
	});

    $scope.submit = function (validate) {

        // Validate
		$scope.submitted = true;
		if (!validate) return;

		// Loading
		HoldOn.open(optionsHoldOn);

		LibraryService.editLibraryProvider($scope.fileItem).then(function (data) {

			// Close loading
			HoldOn.close();

			if (data.status == -1) {
				$scope.errors = data.errors;
			} else {
				$uibModalInstance.close();
				Notify(data.msg);
			}
		})
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);