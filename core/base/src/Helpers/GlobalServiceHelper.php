<?php

use Core\Modules\Base\Site\Http\Controllers\SiteController;

function getVersionScript()
{
    return strtotime(date('Y-m-d H:i:s'));
}

function getVersionCss()
{
    return strtotime(date('Y-m-d H:i:s'));
}

function d($data)
{
    echo "<pre>";
    print_r($data);
}

function httpGet($url)
{
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $output = curl_exec($ch);

    curl_close($ch);
    return $output;
}

function httpPost($url, $params)
{
    $postData = '';

    $postData = rtrim(convertArrayToString($params), '&');
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_POST, count($postData));
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);

    $output = curl_exec($ch);

    curl_close($ch);
    return $output;
}

function imageSlideTypes()
{
    return [
        'basic' => [
            'type' => [
                'name' => 'type',
                'label' => 'Loại slide',
                'element' => 'select',
                'required' => true,
                'options' => [
                    'slide' => 'Slide',
                    'galery' => 'Gallery'
                ]
            ]
        ],
        'slide' => [
            'type' => 'multiple',
            'label' => 'Hình ảnh slide',
            'images' => [
            ]
        ]
    ];
}