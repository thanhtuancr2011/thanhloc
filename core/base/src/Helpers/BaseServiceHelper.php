<?php

namespace Core\Base\Helpers;

use Illuminate\Support\Facades\Request;
use Illuminate\Validation\UnauthorizedException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

use Core\Base\Models\PermissionModel;
use Core\Modules\User\Models\UserModel;

class BaseServiceHelper
{
    protected $permissionsLoaded = false;
    protected $permissions = [];
    protected $postRepository;

    public function can($permission)
    {
        $this->loadPermissions();

        // Check if permission exist
        $exist = $this->permissions->where('slug', $permission)->first();
    
        // Get user
        $user = $this->getUser();
        
        // If user is supper admin then show all role
        if ($user->type == 'super_admin') {
            return true;
        }

        // If user has role is super admin  
        if (isset($user->role) && $user->role->slug == "super.admin") {
            return true;
        }

        if ($exist) {

            // If input permission not in array permissions
            if (!in_array($permission, $this->permissions->pluck('slug')->toArray()) && $user->role->slug != "super.admin") {
                return false;
            }

            if ($user->role->slug == "super.admin") {
                return true;
            }

            if ($user == null || !$user->hasPermission($permission)) {
                return false;
            }

            return true;
        }

        return false;
    }

    public function canOrFail($permission)
    {
        if (!$this->can($permission)) {
            throw new UnauthorizedException(null);
        }

        return true;
    }

    public function canOrAbort($permission, $statusCode = 403)
    {
        if (!$this->can($permission)) {
            return abort($statusCode);
        }

        return true;
    }

    public function canOrRedirect($permission)
    {
        if (!$this->can($permission)) {
            return redirect('admin/dashboard')->send();
        }

        return true;
    }

    public function getStatus()
    {
        $user = $this->getUser();
        $status = $user->getStatus();
        return $status;
    }

    public function checkStatus($status_code)
    {
        $user = $this->getUser();
        $status = $user->hasStatus($status_code);
        return $status;
    }

    public function checkStatusOrAbort($status_code, $statusCode = 403)
    {
        if (!$this->checkStatus($status_code)) {
            return abort($statusCode);
        }

        return true;
    }

    public function getRequest()
    {
        $request = \Illuminate\Support\Facades\Request::segments();
        return $request;

    }

    protected function loadPermissions()
    {
        if (!$this->permissionsLoaded) {
            $this->permissionsLoaded = true;
            $this->permissions = PermissionModel::all();
        }
    }

    protected function getUser($id = null)
    {
        if (is_null($id)) {
            $id = auth()->check() ? auth()->user()->id : null;
        }

        if (is_null($id)) {
            return;
        }

        if (!isset($this->users[$id])) {
            $this->users[$id] = UserModel::find($id);
        }

        return $this->users[$id];
    }
}