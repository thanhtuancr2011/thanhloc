<?php

namespace Core\Base\Models;

use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Config;
use Illuminate\Database\Eloquent\Model;

class FormModel extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'forms';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['full_name', 'email', 'phone_number', 'address', 'content', 'type'];

    /**
     * Get list menu
     * @param  Array $data List data search and option paginate
     * @return Array       List data result and option paginate
     */
    public function items($data)
    {
        if (isset($data['pageOptions'])) {
            $currentPage = $data['pageOptions']['currentPage'];
            $itemsPerPage = $data['pageOptions']['itemsPerPage'];
        }

        // Set current page
        Paginator::currentPageResolver(function () use ($currentPage) {
            return $currentPage;
        });

        if (isset($data['searchOptions']['type'])) {
            $query = self::where('type', 'LIKE', $data['searchOptions']['type']);
        } else {
            $query = $this;
        }

        // Query data
        $paginate = $query->where(function ($q) use ($data) {
            // Search with text
            if (isset($data['searchOptions']['searchText'])) {
                $q->where(function ($q1) use ($data) {
                    $q1->where('email', 'LIKE', ('%' . $data['searchOptions']['searchText'] . '%'))
                        ->orWhere('full_name', 'LIKE', ('%' . $data['searchOptions']['searchText'] . '%'))
                        ->orWhere('phone_number', 'LIKE', ('%' . $data['searchOptions']['searchText'] . '%'))
                        ->orWhere('address', 'LIKE', ('%' . $data['searchOptions']['searchText'] . '%'))
                        ->orWhere('content', 'LIKE', ('%' . $data['searchOptions']['searchText'] . '%'));
                });
            }
        })->orderBy('created_at', 'desc');
        if (!isset($data['is_pagination'])) {
            $data['is_pagination'] = true;
        }
        if ($data['is_pagination']) {
            $paginate = $paginate->paginate($itemsPerPage)->toArray();

            $totalItems = $paginate['total'];
            $totalPages = ceil($totalItems / $itemsPerPage);
        } else {
            $paginate = $paginate->get()->toArray();

            $totalItems = 0;
            $totalPages = 0;
        }


        return [
            'status' => 1,
            'contacts' => ($data['is_pagination']) ? $paginate['data'] : $paginate,
            'totalPages' => $totalPages,
            'totalItems' => $totalItems,
        ];
    }

    /**
     * Create new contact
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Array $data Data input
     * @return Object
     */
    public function storeItem($data)
    {
        if (!isset($data['type'])) {
            $data['type'] = 'contact';
        }

        // Create new contact
        $contact = self::create($data);

        return $contact;
    }

    /**
     * Delete contacts
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Array $ids List ids
     * @return Void
     */
    public function deleteItems($ids)
    {
        $status = self::whereIn('id', $ids)->delete();
        return $status;
    }
}
