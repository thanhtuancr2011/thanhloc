<?php

namespace Core\Base\Models;

use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Config;
use Illuminate\Database\Eloquent\Model;
use Ultraware\Roles\Models\Role;

use Core\Base\Models\StatusModel;

class RoleModel extends Role
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'roles';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'slug', 'description', 'level'];

    /**
     * Many-to-Many relations with the permission model.
     * Named "perms" for backwards compatibility. Also because "perms" is short and sweet.
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function permissions()
    {
        return $this->belongsToMany(Config::get('roles.models.permission'), Config::get('roles.permission_role_table'), 'role_id', 'permission_id');
    }

    /**
     * Many-to-Many relations with the status model.
     * Named status
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function status()
    {
        return $this->belongsToMany(StatusModel::class, 'status_role', 'role_id', 'status_id');
    }

    /**
     * Get list roles
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Array $data The data input
     * @return Array       The result
     */
    public function items($data)
    {
        if (isset($data['pageOptions'])) {
            $currentPage = $data['pageOptions']['currentPage'];
            $itemsPerPage = $data['pageOptions']['itemsPerPage'];
        }

        // Set current page
        Paginator::currentPageResolver(function () use ($currentPage) {
            return $currentPage;
        });

        // If isset search data
        if (isset($data['searchOptions']['searchText'])) {
            $paginate = self::where('name', 'LIKE', '%' . $data['searchOptions']['searchText'] . '%')
                ->orderBy('created_at', 'desc')->where('slug', '!=', 'super.admin')
                ->paginate($itemsPerPage)->toArray();
        } else {
            $paginate = self::orderBy('created_at', 'desc')->where('slug', '!=', 'super.admin')
                            ->paginate($itemsPerPage)->toArray();
        }

        $totalItems = $paginate['total'];
        $totalPages = ceil($totalItems / $itemsPerPage);

        return [
            'status' => 1,
            'roles' => $paginate['data'],
            'totalPages' => $totalPages,
            'totalItems' => $totalItems,
        ];
    }

    /**
     * Create role
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Array $data Data input
     * @return Object       Role
     */
    public function storeItem($data)
    {
        // Slug
        $data['slug'] = str_slug($data['name'], '-');

        // Create new role
        $role = self::create($data);

        // syns permissions
        $role->permissions()->sync($data['permissions']);

        // syns status
        $role->status()->sync($data['status']);

        return $role;
    }

    /**
     * Update role
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Array $data Data input
     * @return Object       Role
     */
    public function updateItem($data)
    {
        // Slug
        $data['slug'] = str_slug($data['name'], '.');

        // Update role
        $this->update($data);
        
        // syns permissions
        $this->permissions()->sync($data['permissions']);

        // syns status
        $this->status()->sync($data['status']);

        return $this;
    }

    /**
     * Delete roles
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Array $ids List ids
     * @return Void
     */
    public function deleteItems($ids)
    {
        $status = self::whereIn('id', $ids)->delete();
        $this->permissions()->whereIn('role_id', $ids)->delete();
        return $status;
    }
}
