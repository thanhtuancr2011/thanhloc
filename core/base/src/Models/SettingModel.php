<?php

namespace Core\Base\Models;

use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Config;
use Illuminate\Database\Eloquent\Model;

class SettingModel extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'setting';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['key', 'value'];

    /**
     * Create setting
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Array $data Data input
     * @return Object       setting
     */
    public function storeItem($data)
    {
        $status = 0;
        
        foreach ($data as $key => $value) {

            // Find with key
            $item = self::where('key', $key)->first();

            // If value is array
            if (is_array($value)) $value = json_encode($value);

            if (empty($item)) {
                $status = self::create(['key' => $key, 'value' => !empty($value) ? $value : '']);
            } else {
                $item->value = $value;
                $status = $item->update();
            }

            // Save file robots.txt
            if ($key == 'robot_txt') {
                $status = \Storage::disk('public_local')->put('robots.txt', $value);
            }
        }

        return $status;
    }

    /**
     * Get setting config
     * @param  String $key The config key
     * @return Void        The value config
     */
    public function getConfig($key)
    {
        if (!$key) {
            return "";
        }
        $item = self::where('key', $key)->first();
        return isset($item->value) ? $item->value : '';
    }
}
