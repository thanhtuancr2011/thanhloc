<?php 

namespace Core\Base\Models;

use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Config;
use Ultraware\Roles\Models\Permission;

class PermissionModel extends Permission
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'permissions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'slug', 'description', 'table'];

    /**
     * Many-to-Many relations with role model.
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany(Config::get('roles.models.role'), Config::get('roles.permission_role_table'), 'permission_id', 'role_id');
    }

    /**
     * Get list permissions
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Array $data The data input
     * @return Array       The result
     */
    public function items($data) 
    {
        if (isset($data['pageOptions'])) {
            $currentPage = $data['pageOptions']['currentPage'];
            $itemsPerPage = $data['pageOptions']['itemsPerPage'];
        }

        if (isset($data['searchOptions'])) {

            // Seach datas 
            $searchOptions = [];

            // If search with email input
            if (isset($data['searchOptions']['name'])) {
                if ($data['searchOptions']['name'][0] == '"' && substr($data['searchOptions']['name'], -1) == '"') {
                    $searchOptions[] = ['name', 'like', substr($data['searchOptions']['name'], 1, -1)];
                } else {
                    $searchOptions[] = ['name', 'like', ('%' . $data['searchOptions']['name'] . '%')];
                }
            }
        }

        // Set current page
        Paginator::currentPageResolver(function() use ($currentPage) {
           return $currentPage;
        });
        $paginate = self::where($searchOptions)->paginate($itemsPerPage)->toArray();

        $totalItems = $paginate['total'];
        $totalPages = ceil($totalItems / $itemsPerPage);

        return [
            'status' => 1, 
            'permissions' => $paginate['data'], 
            'totalPages'  => $totalPages, 
            'totalItems'  => $totalItems, 
        ];
    }

    /**
     * Create permission
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Array $data  Data input 
     * @return Object       Permission
     */
    public function storeItem($data)
    {
        // Slug
        $data['slug'] = str_slug($data['name'], '.');

        // Create new permission
        $permission = self::create($data);

        return $permission;
    }

    /**
     * Update permission
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Array $data  Data input 
     * @return Object       Permission
     */
    public function updateItem($data)
    {
        // Slug
        $data['slug'] = str_slug($data['name'], '.');

        // Update permission
        $this->update($data);
        
        return $this;
    }

    /**
     * Delete permissions
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Array $ids List ids
     * @return Void      
     */
    public function deleteItems($ids)
    {
        $status = self::whereIn('id', $ids)->delete(); 
        return $status;
    }
}
