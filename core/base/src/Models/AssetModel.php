<?php

namespace Core\Base\Models;

use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Config;
use Illuminate\Database\Eloquent\Model;

use Core\Base\Services\FileService;

class AssetModel extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'assets';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'slug', 'parent_id', 'path', 'user_id'];

    /**
     * Relationship asset
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @return Voids
     */
    public function childs()
    {
        return $this->hasMany('Core\Base\Models\AssetModel', 'parent_id');
    }

    /**
     * Get folders with tree format
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @return Array   Hierachy folders
     */
    public function getFoldersTree()
    {
        // Get all folders
        $folders = self::orderBy('name', 'asc')->get()->toArray();

        foreach ($folders as &$folder) {
            // Update value folder
            $folder['children'] = [];
            $folder['folder'] = true;
            $folder['key'] = $folder['id'];
            $folder['title'] = $folder['name'];
            $referenceFolders[$folder['id']] = $folder;
        }

        // Put a folder to property children of a parent folder that it should belong to
        foreach ($folders as &$folder) {
            if (!empty($folder['parent_id'])) {
                $referenceFolders[$folder['parent_id']]['children'][] = &$referenceFolders[$folder['id']];
            }
        }

        if (isset($referenceFolders)) {
            // Get root folders
            $hierachyFolders = $referenceFolders;
            foreach ($hierachyFolders as $key => $hierachyFolder) {
                // Get tree
                if (!empty($hierachyFolder['parent_id']) && ($hierachyFolder['parent_id'] != '0')) {
                    unset($hierachyFolders[$key]);
                }
            }
        }
            
        return array_values($hierachyFolders);
    }

    /**
     * Get parent folders name
     * @param  Integer $parentId       The parent folder id
     * @param  String  &$directoryName The directory name
     * @return Void                    
     */
    public function recursiveGetParentFoldersName($parentId, &$directoryName = '')
    {
        if ($parentId != '0') {
            $folder = self::find($parentId);
            if ($folder) {
                if (!($folder->slug == 'root' && $folder->parent_id == 0))
                    $directoryName = $folder->name . '/' . $directoryName;
                $this->recursiveGetParentFoldersName($folder->parent_id, $directoryName);
            }
        }

        return $directoryName;
    }

    /**
     * Create folder
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Array $data Data input
     * @return Object       Role
     */
    public function storeItem($data)
    {
        $status = 0;

        // User id
        $data['user_id'] = \Auth::user()->id;

        // Set name and slug
        $data['slug'] = $data['name'] = str_slug($data['name'], '-');

        // Get directory name
        $directoryName = $this->recursiveGetParentFoldersName($data['parent_id']);

        // Create folder
        $folder = self::create($data);

        // Create folder
        if ($folder) {
            $status = FileService::makeDirectory($directoryName . $data['name'], 'storage');
        }

        return $folder;
    }

    /**
     * Update folder
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Array $data Data input
     * @return Object       Folder
     */
    public function updateItem($data)
    {
        // Set name and slug
        $data['slug'] = $data['name'] = str_slug($data['name'], '-');

        // Get directory name
        $directoryName = $this->recursiveGetParentFoldersName($data['parent_id']);

        // Rename folder
        FileService::renameDirectory($directoryName . $this->slug, $directoryName . $data['slug'], 'storage');

        // Update folder
        $this->update($data);

        return $this;
    }

    public function recursiveGetChildsFolder($folderId, &$childsFolderIds)
    {
        $childsFolderIds[] = $folderId;

        $childsFolder = self::where('parent_id', $folderId)->get();
        if (count($childsFolder) > 0) {
            foreach ($childsFolder as $key => $value) {
                $this->recursiveGetChildsFolder($value->id, $childsFolderIds);
            }
        } 
    }

    /**
     * Delete folder
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Array $folderId The folder id
     * @return Void
     */
    public function deleteItems($folderId)
    {
        // Get all child folder
        $folderIds = [];
        $this->recursiveGetChildsFolder($folderId, $folderIds);

        // Get directory name
        $directoryName = $this->recursiveGetParentFoldersName($folderId);

        // Delete directory
        $status = FileService::deleteDirectory($directoryName, 'storage');

        // Delete folder
        if ($status) {
            $status = self::whereIn('id', $folderIds)->delete();
        }

        return $status;
    }
}
