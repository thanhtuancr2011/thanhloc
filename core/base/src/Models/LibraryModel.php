<?php  

namespace Core\Base\Models;

use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Config;
use Illuminate\Database\Eloquent\Model;
use Intervention\Image\Facades\Image;

use Core\Base\Services\FileService;

use Core\Base\Models\AssetModel;
use Core\Modules\User\Models\UserModel;

class LibraryModel extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'libraries';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'url', 'excerpt', 'description', 'type', 'asset_id', 'file_name', 'user_id'];

    /**
     * Get the folder that owns the comment.
     */
    public function folder()
    {
        return $this->belongsTo(AssetModel::class, 'asset_id');
    }

    /**
     * Get list libraries
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Array $data The data input
     * @return Array       The result
     */
    public function items($data) 
    {
        if (isset($data['pageOptions'])) {
            $currentPage = $data['pageOptions']['currentPage'];
            $itemsPerPage = $data['pageOptions']['itemsPerPage'];
        }

        // Set current page
        Paginator::currentPageResolver(function () use ($currentPage) {
            return $currentPage;
        });

        // If isset search data
        if (isset($data['searchOptions']['searchText'])) {
            $fileModel = self::where('asset_id', $data['searchOptions']['asset_id'])
                ->where(function ($query) use ($data) {
                    $query->where('file_name', 'LIKE', '%' . $data['searchOptions']['searchText'] . '%');
                })->orderBy('created_at', 'desc');
        } else {
            $fileModel = self::where('asset_id', $data['searchOptions']['asset_id'])
                ->orderBy('created_at', 'desc');
        }

        // Search by type
        if (isset($data['searchOptions']['group']) && !empty($data['searchOptions']['group']) && $data['searchOptions']['group'] != 'all') {
            $lstFileTypes = [];
            foreach (FileService::listMimeTypes() as $key => $type) {
                if (str_slug($type['group']) == str_slug($data['searchOptions']['group'])) {
                    $lstFileTypes[] = $type['name'];
                }
            }
            $fileModel->whereIn('type', $lstFileTypes);
        }

        // Convert to array
        if (isset($data['no_paginate'])) {
            $paginate = $fileModel->toArray();
        } else {
            $paginate = $fileModel->paginate($itemsPerPage)->toArray();
        }

        // Get folder
        $folder = AssetModel::find($data['searchOptions']['asset_id']);

        $assetModel = new AssetModel;
        $directoryName = $assetModel->recursiveGetParentFoldersName($folder->id);

        // Path folder
        $folderPath = '/storage' . '/' . $directoryName;

        // Paginage
        $totalItems = $paginate['total'];
        $totalPages = ceil($totalItems/$itemsPerPage);

        // List authors
        $authors = UserModel::whereIn('id', array_column($paginate['data'], 'user_id'))->pluck('full_name', 'id');

        return [
            'status' => 1, 
            'authors'    => $authors, 
            'libraries'  => $paginate['data'], 
            'totalPages' => $totalPages, 
            'totalItems' => $totalItems, 
            'folderPath' => $folderPath
        ];
    }

    /**
     * Check file name is exists
     * author  Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  String $fileName The file name
     * @param  String $ext      The file extendsion
     * @return String $fileNameResult File name
     */
    public function checkFileExist($fileName, $ext, $folderId)
    {
        $fileNameReslt = '';

        for ($i = 0; $i < 100; $i++) {

            // Set file name check
            if ($i == 0) {
                $fileCheck = $fileName;
            } else {
                $fileCheck = $fileName . '-' . $i;
            }

            // Check file name is exists
            $file = self::where('file_name', 'LIKE', $fileCheck . '.' . $ext)->where('asset_id', $folderId)->first();

            if (empty($file)) {
                $fileNameReslt = $fileCheck;
                break;
            }
        }

        return $fileNameReslt;
    }

    /**
     * Create new
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Array $data  Data input 
     * @return Object       
     */
    public function storeItem($data)
    {
        $status = 0;

        // Folder upload file
        $folderId = $_GET['folder'];

        // Init model
        $assetModel = new AssetModel;

        // Find folder
        $folder = AssetModel::find($folderId);

        if (!empty($folder)) {

            $directoryName = $assetModel->recursiveGetParentFoldersName($folder->id);

            // File upload
            $fileUpload = $data['file'];

            // Upload fail 
            if(empty($fileUpload) || !$fileUpload['tmp_name']){
                return $status;
            }

            $ext       = pathinfo($fileUpload['name'], PATHINFO_EXTENSION);     // File extension
            $fileName  = pathinfo($fileUpload['name'], PATHINFO_FILENAME);      // File name
            $storeDisk = 'storage';                                             // storeDisk

            // Format file name
            $fileName = str_slug($fileName, '-');
            
            // Check file name is exists
            $fileName = $this->checkFileExist($fileName, $ext, $folderId) . '.' . $ext;

            // Contain list image widths
            $arrayWidths = [320, 767, 980, 1024, 1336];

            try {

                // Save file
                $status = FileService::save($fileName, file_get_contents($fileUpload['tmp_name']), false, '/' . $directoryName, null, $storeDisk);
                
                // Get list mimeContentTypes
                $mimeContentTypes = FileService::listMimeTypes();

                // Image type
                $imageType = FileService::mimeContentType($fileName);

                // Contain array types image deny
                $denyTypes = ['image/svg+xml', 'image/vnd.microsoft.icon', 'image/vnd.adobe.photoshop'];

                // Check file is image
                $isImage = (isset($mimeContentTypes[$ext]) && $mimeContentTypes[$ext]['group'] == 'Image' && !in_array($imageType, $denyTypes)) ? true : false;

                // Save image success
                if ($status) {

                    // Create file
                    $file = self::create([
                        'excerpt'     => '',
                        'description' => '',
                        'title'       => $fileName,
                        'file_name'   => $fileName,
                        'asset_id'    => $folder->id,
                        'type'        => FileService::mimeContentType($fileName),
                        'user_id'     => \Auth::user()->id
                    ]);

                    // Resize image
                    if (!empty($file) && $isImage) {
                        foreach ($arrayWidths as $key => $value) {
                            $image = Image::make(storage_path() . '/app/public/' . $directoryName . $fileName);
                            $image->resize(intval($value), null, function ($constraint) {
                                $constraint->aspectRatio();
                            });   
                            $image->save(storage_path('app/public/' . $directoryName . $value . '-' . $fileName));
                        }
                    }
                }
            } catch (Exception $e){
                $status = 0;
            }      
        } else {
            $status = -1;
        }

        // If upload fail
        if ($status == 0) {
            $result = ['status' => $status, 'msg' => 'Đã xảy ra lỗi, mời thử lại.'];
        } elseif ($status) {
            $result = ['status' => $status, 'msg' => 'Tải lên thành công.', 'file' => $file];
        } else {
            $result = ['status' => $status, 'msg' => 'Chưa chọn thư mục để upload file'];
        }

        return $result;
    }

    /**
     * Store image
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Array $data  Data input 
     * @return Object       
     */
    public function storeFile($data)
    {
        $status = 0;

        // Init model
        $assetModel = new AssetModel;

        // Find folder
        $folder = $assetModel->where('slug', $data['folder'])->first();

        if (!empty($folder)) {

            // Get directory name
            $directoryName = $assetModel->recursiveGetParentFoldersName($folder->id);

            $ext       = pathinfo($data['fileName'], PATHINFO_EXTENSION);     // File extension
            $fileName  = pathinfo($data['fileName'], PATHINFO_FILENAME);      // File name
            $storeDisk = 'storage';     

            // Check file exists
            $file = LibraryModel::where('file_name', $data['fileName'])->where('asset_id', $folder->id)->first();

            if (!$file) {
                // Contain list image widths
                $arrayWidths = [320, 767, 980, 1024, 1336];

                try {
                    // Save file
                    if (FileService::save($data['fileName'], file_get_contents($data['url']), false, $folder->path . '/', null, $storeDisk)) {
                        $status = 1;
                    }

                    // Save image success
                    if ($status) {
                    
                        // Get list mimeContentTypes
                        $mimeContentTypes = FileService::listMimeTypes();

                        // Image type
                        $imageType = FileService::mimeContentType($data['fileName']);

                        // Contain array types image deny
                        $denyTypes = ['image/svg+xml', 'image/vnd.microsoft.icon', 'image/vnd.adobe.photoshop'];

                        // Check file is image
                        $isImage = (isset($mimeContentTypes[$ext]) && $mimeContentTypes[$ext]['group'] == 'Image' && !in_array($imageType, $denyTypes)) ? true : false;

                        // User 
                        $userAdmin = UserModel::first();

                        // Create file
                        $fileSaved = self::create([
                            'excerpt'     => '',
                            'description' => '',
                            'title'       => $data['fileName'],
                            'file_name'   => $data['fileName'],
                            'asset_id'    => $folder->id,
                            'type'        => FileService::mimeContentType($data['fileName']),
                            'user_id'     => $userAdmin->id
                        ]);

                        // Resize image
                        if (!empty($fileSaved) && $isImage) {
                            foreach ($arrayWidths as $key => $value) {
                                $image = Image::make(storage_path() . '/app/public/' . $directoryName . '/' . $data['fileName']);
                                $image->resize(intval($value), null, function ($constraint) {
                                    $constraint->aspectRatio();
                                });   
                                $image->save(storage_path('app/public/' . $directoryName . '/' . $value . '-' . $data['fileName']));
                            }
                        }
                    }
                } catch (Exception $e){
                    $status = 0;
                } 
            } else {
                $status = 2;
            }    
        } else {
            $status = -1;
        }

        // If upload fail
        if ($status == 0) {
            $result = ['status' => $status, 'msg' => 'Đã xảy ra lỗi, mời thử lại.'];
        } elseif ($status == -1) {
            $result = ['status' => $status, 'msg' => 'Chưa chọn thư mục để upload file'];
        } elseif ($status == 2) {
            $result = ['status' => $status, 'msg' => 'File đã tồn tại trong hệ thống'];
        } else {
            $result = ['status' => $status, 'msg' => 'Tải lên thành công.', 'file' => $file];
        }

        return $result;
    }

    /**
     * Crop image
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Array $data  Data input 
     * @return Object       
     */
    public function cropImage($data)
    {
        $status = 0;

        // File Id
        $fileId = $_GET['fileId'];

        // File upload
        $fileUpload = $data;
        
        // Upload fail 
        if (empty($fileUpload) || !$fileUpload['tmp_name']){
            return $status;
        }

        $ext       = pathinfo($fileUpload['name'], PATHINFO_EXTENSION);     // File extension
        $fileName  = pathinfo($fileUpload['name'], PATHINFO_FILENAME);      // File name
        $storeDisk = 'storage';                                             // storeDisk
        
        // Get file 
        $fileCrop = self::where('file_name', $data['name'])->first();

        // Check file name is exists
        $fileName = $this->checkFileExist($fileName, $ext);
        $fileName .= '.' . $ext;

        try {
            $status = FileService::save($fileName, file_get_contents($fileUpload['tmp_name']), false, $folder->path . '/', null, $storeDisk);
            if ($status) {
                $file = $fileCrop->update([
                    'excerpt'     => '',
                    'description' => '',
                    'file_name'   => $fileName,
                    'title'       => $fileName,
                    'asset_id'    => $folder->id,
                    'url'         => $folder->path . '/' . $fileName,
                    'type'        => FileService::mimeContentType($fileName)
                ]);
            }
        } catch (Exception $e){
            $status = 0;
        }

        // If upload fail
        if ($status == 0) {
            $result = ['status' => $status, 'msg' => 'Đã xảy ra lỗi, mời thử lại.'];
        } else {
            $result = ['status' => $status, 'msg' => 'Cắt ảnh thành công.', 'file' => $fileCrop];
        }

        return $result;
    }

    /**
     * Update item
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Array $data  Data input 
     * @return Object       
     */
    public function updateItem($data)
    {
        // Update
        $this->update($data);
        
        return $this;
    }

    /**
     * Delete items
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Array $ids List ids
     * @return Void      
     */
    public function deleteItems($ids)
    {
        $status = 0;

        $storeDisk = 'storage';  

        // Init model
        $assetModel = new AssetModel;

        // Contain list image widths
        $arrayWidths = [320, 767, 980, 1024, 1336];

        // Get file and get list file names
        $files = self::whereIn('id', $ids)->get();
        
        // File first
        $folder = AssetModel::find($files->first()->asset_id);

        // Get list mimeContentTypes
        $mimeContentTypes = FileService::listMimeTypes();

        // Get directory name
        $directoryName = $assetModel->recursiveGetParentFoldersName($folder->id);
        $directoryName = empty($directoryName) ? '/' : $directoryName;

        // Each file
        foreach ($files as $key => $file) {

            // Delete file
            $status = FileService::deleteFiles($directoryName . $file->file_name, $storeDisk);

            if ($status) {

                // File ext
                $ext = pathinfo($file->file_name, PATHINFO_EXTENSION);

                // Image type
                $imageType = FileService::mimeContentType($file->file_name);

                // Contain array types image deny
                $denyTypes = ['image/svg+xml', 'image/vnd.microsoft.icon', 'image/vnd.adobe.photoshop', 'image/tiff'];

                // Check file is image
                $isImage = (isset($mimeContentTypes[$ext]) && $mimeContentTypes[$ext]['group'] == 'Image' && !in_array($imageType, $denyTypes)) ? true : false;

                if ($isImage) {
                    foreach ($arrayWidths as $key => $width) {
                        $status = FileService::deleteFiles($directoryName . $width . '-' . $file->file_name, $storeDisk);
                    }
                }

                // Delete file
                $status = $file->delete();
            }
        }

        return $status;
    }
}
