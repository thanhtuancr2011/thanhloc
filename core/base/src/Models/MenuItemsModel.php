<?php

namespace Core\Base\Models;

use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Config;
use Illuminate\Database\Eloquent\Model;

class MenuItemsModel extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'menu_items';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'url', 'level', 'icon_class', 'parent_id', 'order', 'route', 'parameters', 'menu_id'];

    /**
     * Relationship menu items
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @return Voids 
     */
    public function childs()
    {
        return $this->hasMany(MenuItemsModel::class, 'parent_id');
    }
}
