<?php

namespace Core\Base\Models\En;

use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Config;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\View;
use Illuminate\Support\HtmlString;

use Core\Base\Models\En\MenuItemsModel;
use Core\Modules\Term\Models\En\TermModel;
use Core\Modules\Post\Models\En\PostModel;


class MenuModel extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'menus_en';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'slug', 'depth'];

    /**
     * Get list libraries
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Array $data The data input
     * @return Array       The result
     */
    /**
     * Relationship items
     * @author AnhVN <johnavu.it@gmail.com>
     */
    public function menuItems()
    {
        return $this->hasMany(MenuItemsModel::class, 'menu_id');
    }

    /**
     * Get list menu
     * @param  Array $data List data search and option paginate
     * @return Array       List data result and option paginate
     */
    public function items($data)
    {
        if (isset($data['pageOptions'])) {
            $currentPage = $data['pageOptions']['currentPage'];
            $itemsPerPage = $data['pageOptions']['itemsPerPage'];
        }

        if (isset($data['searchOptions'])) {

            // Seach datas 
            $searchOptions = [];

            // If search with title
            if (isset($data['searchOptions']['title'])) {
                if ($data['searchOptions']['title'][0] == '"' && substr($data['searchOptions']['title'], -1) == '"') {
                    $searchOptions[] = ['title', 'like', substr($data['searchOptions']['title'], 1, -1)];
                } else {
                    $searchOptions[] = ['title', 'like', ('%' . $data['searchOptions']['title'] . '%')];
                }
            }
        }

        // Set current page
        Paginator::currentPageResolver(function () use ($currentPage) {
            return $currentPage;
        });
        $paginate = self::where($searchOptions)->orderBy('created_at', 'desc')->paginate($itemsPerPage)->toArray();

        $totalItems = $paginate['total'];
        $totalPages = ceil($totalItems / $itemsPerPage);

        return [
            'status' => 1,
            'menus' => $paginate['data'],
            'totalPages' => $totalPages,
            'totalItems' => $totalItems,
        ];
    }

    /**
     * Create new
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Array $data Data input
     * @return Object
     */
    public function storeItem($data)
    {
        // Slug
        $data['slug'] = str_slug($data['name'], '-');

        // Create new menu
        $menu = self::create($data);

        return $menu;
    }

    /**
     * Update item
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Array $data Data input
     * @return Object
     */
    public function updateItem($data)
    {
        // Update
        $this->update($data);

        return $this;
    }

    /**
     * Get max order column
     * author  Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Integer $menuId The menu id
     * @return Void
     */
    public function getMaxOrderColumn($menuId)
    {
        // Get max order column
        $maxOrder = MenuItemsModel::where('parent_id', 0)->where('menu_id', $menuId)->max('order');

        if ($maxOrder === NULL) {
            $maxOrder = 0;
        } else {
            $maxOrder += 1;
        }

        return $maxOrder;
    }

    /**
     * Add menu items
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Array $data Data input
     * @return Object
     */
    public function addMenuItems($data)
    {
        $menuItem = [];

        if ($data['type'] == 'category') {

            // Get categories
            $categories = TermModel::whereIn('id', $data['data'])->get();

            foreach ($categories as $key => $category) {

                // Get max order column
                $maxOrder = $this->getMaxOrderColumn($this->id);

                $menuItem[] = MenuItemsModel::create([
                    'title' => $category->name,
                    'url' => '',
                    'route' => 'category.' . $category->slug,
                    'order' => $maxOrder,
                    'menu_id' => $this->id
                ]);
            }
        }

        if ($data['type'] == 'page') {

            // Get pages
            $pages = PostModel::whereIn('id', $data['data'])->get();

            foreach ($pages as $key => $page) {

                // Get max order column
                $maxOrder = $this->getMaxOrderColumn($this->id);

                $menuItem[] = MenuItemsModel::create([
                    'title' => $page->title,
                    'url' => '',
                    'route' => 'page.' . $page->slug,
                    'order' => $maxOrder,
                    'menu_id' => $this->id
                ]);
            }
        }

        if ($data['type'] == 'customLink') {

            // Get max order column
            $maxOrder = $this->getMaxOrderColumn($this->id);

            $menuItem[] = MenuItemsModel::create([
                'title' => $data['data']['title'],
                'url' => $data['data']['url'],
                'order' => $maxOrder,
                'menu_id' => $this->id
            ]);
        }

        return $menuItem;
    }

    /**
     * Delete menu item
     * @param  Array $data The data input
     * @return Void
     */
    public function deleteMenuItem($data)
    {
        $status = 0;

        // Find menu Item delete
        $menuItemDelete = MenuItemsModel::find($data['menuItem']['id']);

        // Menu id of menu item delete
        $menuId = $menuItemDelete->menu_id;

        // If menu item hasn't child
        if (empty($data['menuItem']['nodes'])) {

            // Find all node has same parent node and order > curent node
            $menuItems = MenuItemsModel::where('parent_id', $menuItemDelete->parent_id)
                ->where('order', '>', $menuItemDelete->order)
                ->orderBy('order', 'asc')->get();

            // Each menu item
            foreach ($menuItems as $key => $value) {
                $value['order'] = $value['order'] - 1;
                $value['parent_id'] = $data['menuItem']['parent_id'];

                $status = $value->update();
            }
        } else {

            // Get menu item child
            $menuItemsChild = MenuItemsModel::where('parent_id', $data['menuItem']['id'])
                ->orderBy('order', 'asc')->get();

            // Find all node has same parent node and order > curent node
            $menuItems = MenuItemsModel::where('parent_id', $menuItemDelete->parent_id)
                ->where('order', '>', $menuItemDelete->order)
                ->orderBy('order', 'asc')->get();

            foreach ($menuItemsChild as $key => $value) {
                $value['level'] = $data['menuItem']['level'];
                $value['order'] = $data['menuItem']['order'] + $key;
                $value['parent_id'] = $data['menuItem']['parent_id'];

                // If item has child node
                // Call function change all child level of item
                if (!empty($value->childs)) {
                    $this->recursiveChangeLevel($value->childs);
                }

                $maxOrder = $value['order'];
                $status = $value->update();
            }

            // Each menu item
            foreach ($menuItems as $k => $v) {
                $maxOrder += 1;
                $v['order'] = $maxOrder;
                $status = $v->update();
            }
        }

        // Delete menu items
        $status = $menuItemDelete->delete();

        return $status;
    }

    /**
     * Recursive change all child level
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Array $menuItems List menu items
     * @return Void
     */
    public function recursiveChangeLevel($menuItems)
    {
        $status = 0;
        foreach ($menuItems as $key => $value) {
            $value->level = $value->level - 1;
            $status = $value->update();
            if (!empty($value->childs)) {
                $this->recursiveChangeLevel($value->childs);
            }
        }

        return $status;
    }

    /**
     * Change menu item position
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Array $data Data input
     * @return Object
     */
    public function changeMenuItemPosition($data)
    {
        $status = 0;
        $status = $this->recursiveChangeMenuItemPosition($parentId = 0, $level = 1, $data['menuItems']);

        return $status;
    }

    /**
     * Recursive change menu item position and menu level
     * @param  Integer $parentId Parent id
     * @param  Object $level Level menu item
     * @param  Object $menuItems List menu items
     * @return Void
     */
    public function recursiveChangeMenuItemPosition($parentId, $level, $menuItems)
    {
        foreach ($menuItems as $key => $value) {

            $menuItem = MenuItemsModel::find($value['id']);
            $menuItem->order = $key;
            $menuItem->level = $level;
            $menuItem->parent_id = $parentId;
            $status = $menuItem->update();

            if (!empty($value['nodes'])) {
                $this->recursiveChangeMenuItemPosition($value['id'], $menuItem->level + 1, $value['nodes']);
            }
        }

        return $status;
    }

    /**
     * Delete items
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Array $ids List ids
     * @return Void
     */
    public function deleteItems($ids)
    {
        $status = self::whereIn('id', $ids)->delete();
        return $status;
    }

    public function getMenuItemsTree()
    {
        // Contain data output
        $result = [];

        // Get all menuItems
        $menuItems = $this->menuItems()->orderBy('order', 'asc')->get()->toArray();

        if (!empty($menuItems)) {
            foreach ($menuItems as &$menuItem) {
                $menuItem['nodes'] = [];
                $referenceMenuItems[$menuItem['id']] = $menuItem;
            }

            // Put a folder to property subFolder of a parent folder that it should belong to
            foreach ($menuItems as &$menuItem) {
                if (!empty($menuItem['parent_id'])) {
                    $referenceMenuItems[$menuItem['parent_id']]['nodes'][] = &$referenceMenuItems[$menuItem['id']];
                }
            }

            // Get root folders
            $hierachyMenuItems = $referenceMenuItems;
            foreach ($hierachyMenuItems as $key => $hierachyMenuItem) {
                // Get tree
                if (!empty($hierachyMenuItem['parent_id']) && ($hierachyMenuItem['parent_id'] != '0')) {
                    unset($hierachyMenuItems[$key]);
                }
            }

            // Add null category
            $menuItems = array_values($hierachyMenuItems);
        }

        return $menuItems;
    }

    /**
     * Display menu 
     * @param  String $menuName The menu name
     * @param  String $type     View name
     * @param  array  $options  Options value
     * @return Void           
     */
    public static function display($menuName, $type = null, array $options = [])
    {
        $menu = static::where('slug', '=', $menuName)->first();

        if (!$menu) 
            return false;

        $menuItem = $menu->getMenuItemsTree();

        if ($type == null || !view()->exists($type)) 
            $type = 'dizi-site::menu.default';

        return new HtmlString(View::make($type, ['items' => $menuItem, 'type' => $type, 'options' => $options])->render());
    }
}
