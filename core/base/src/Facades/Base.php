<?php 

namespace Core\Base\Facades;

use Illuminate\Support\Facades\Facade;

class Base extends Facade
{
    public static function getFacadeAccessor()
    {
        return 'base';
    }
}