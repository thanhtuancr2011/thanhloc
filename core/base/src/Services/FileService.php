<?php

namespace Core\Base\Services;

use File;
use Crypt;
use Storage;

use Illuminate\Http\Response;
use Illuminate\Contracts\Routing\ResponseFactory;

class FileService
{
    static $paths = [
        'default' => 'files/',
        'user'    => 'users/'
    ];

    const CIPHER = MCRYPT_RIJNDAEL_128; // Rijndael-128 is AES
    const MODE   = MCRYPT_MODE_CBC;
    
    /**
     * Save file
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  String  $fileName File name
     * @param  Object  $file File file
     * @param  boolean $crypt    Is crypt
     * @param  string  $folder   Folder name to contain file
     * @param  string  $keyPath  Location disk to contain file
     * @param  string  $disk     [description]
     * 
     * @return boolean           Status
     */
    public static function save($fileName, $file, $crypt = false, $folder = null, $keyPath = null, $disk = null)
    {
        // Crypt file
        if ($crypt) {
            $file = Crypt::encrypt($file);
        }

        $status = Storage::disk($disk)->put($folder . $fileName, $file);

        return $status;
    }

    /**
     * Get file
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  String $filename File name
     * @param  String $folder   Folder name
     * @param  String $keyPath  Location file name
     * 
     * @return Object           Image
     */
    public static function get($filename, $folder, $keyPath)
    {
        $contents = Storage::get(self::$paths[$keyPath].$folder.$filename);

        return $contents;
    }

    /**
     * Delete folder
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  String $folder   Folder name
     * @param  String $disk     Location disk
     * 
     * @return Void           
     */
    public static function deleteDirectory($folder, $disk = 'null')
    {
        $status = Storage::disk($disk)->deleteDirectory($folder);
        return $status;
    }

    /**
     * Get all files in directory
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  String $folder   Folder name
     * @param  String $disk     Location disk
     * @return Void           
     */
    public static function allFiles($folder, $disk = 'null')
    {
        $files = Storage::disk($disk)->allFiles($folder);
        return $files;
    }

    /**
     * Make folder
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  String $folder   Folder name
     * @param  String $disk     Location disk
     * 
     * @return Void           
     */
    public static function makeDirectory($folder, $disk = 'null')
    {
        $status = Storage::disk($disk)->makeDirectory($folder, 0777, true);
        return $status;
    }

    /**
     * Move or rename folder
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  String $folder   Folder name
     * @param  String $disk     Location disk
     * 
     * @return Void           
     */
    public static function renameDirectory($folderNameFrom, $folderNameTo, $disk = 'null')
    {
        $status = Storage::disk($disk)->move($folderNameFrom, $folderNameTo);
        return $status;
    }

    /**
     * Delete files
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  String $filesName   List files name delete
     * @param  String $disk        Location disk
     * 
     * @return Void           
     */
    public static function deleteFiles($filesName, $disk = 'null')
    {
        $status = Storage::disk($disk)->delete($filesName);
        return $status;
    }

    /**
     * View vile
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  String  $filename File name
     * @param  boolean $crypt    Is crypt
     * @param  String  $folder   Folder name
     * @param  String  $keyPath  Location contain file
     * @return Void           
     */
    public static function view($filename, $crypt, $folder, $keyPath)
    {
        $contents = self::get($filename, $folder, $keyPath);

        $payload = json_decode(base64_decode($contents), true);

        if($crypt && $payload){
            $contents = Crypt::decrypt($contents);
        }

        return (new Response($contents, 200))->header('Content-Type', self::mimeContentType($filename));
    }

    /**
     * Get error messages upload
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Integer $code Code number
     * @return Void       
     */
    public static function codeToMessage($code)
    {
        switch ($code) {
            case UPLOAD_ERR_INI_SIZE:
                $message = 'Kích thước tập tin tải lên tối đa '. ini_get("upload_max_filesize");
                break;
            case UPLOAD_ERR_FORM_SIZE:
                $message = "Tệp đã tải lên vượt quá chỉ thị MAX_FILE_SIZE đã được chỉ định trong biểu mẫu HTML";
                break;
            case UPLOAD_ERR_PARTIAL:
                $message = "Tệp đã tải lên chỉ được tải lên một phần";
                break;
            case UPLOAD_ERR_NO_FILE:
                $message = "Không có tệp nào được tải lên";
                break;
            case UPLOAD_ERR_NO_TMP_DIR:
                $message = "Không tìm thấy thư mục tmp";
                break;
            case UPLOAD_ERR_CANT_WRITE:
                $message = "Không thể lưu file vào thư mục";
                break;
            case UPLOAD_ERR_EXTENSION:
                $message = "Tệp tải lên đã bị dừng do tiện ích mở rộng";
                break;
            default:
                $message = "Lỗi không xác định";
                break;
        }
        return $message;
    }

    /**
     * Down load file
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  String  $filename File name
     * @param  boolean $crypt   Crypt
     * @param  String  $folder   Folder name
     * @param  String  $keyPath  Location contain file
     * @return Void           
     */
    public function download($filename, $crypt, $folder, $keyPath)
    {
        // Get file
        $contents = self::get($filename, $folder, $keyPath);

        $payload = json_decode(base64_decode($contents), true);

        if($payload){
            $contents = Crypt::decrypt($contents);
            Storage::put(self::$paths[$keyPath].$folder.$filename, $contents);
        }

        return response()->download(storage_path().'/app/'.self::$paths[$keyPath].$folder.$filename);
    }

    /**
     * List mime type
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>   
     * @return Array List mime types
     */
    public static function listMimeTypes()
    {
        $mimeTypes = [

            // Plain text
            'txt'  => ['name' => 'text/plain', 'group' => 'Plain Text', 'class' => 'fa fa-file-text-o'],
            'htm'  => ['name' => 'text/html', 'group' => 'Plain Text', 'class' => 'fa fa-file-text-o'],
            'html' => ['name' => 'text/html', 'group' => 'Plain Text', 'class' => 'fa fa-file-text-o'],
            'php'  => ['name' => 'text/html', 'group' => 'Plain Text', 'class' => 'fa fa-file-text-o'],
            'css'  => ['name' => 'text/css', 'group' => 'Plain Text', 'class' => 'fa fa-file-text-o'],
            'js'   => ['name' => 'application/javascript', 'group' => 'Plain Text', 'class' => 'fa fa-file-text-o'],
            'json' => ['name' => 'application/json', 'group' => 'Plain Text', 'class' => 'fa fa-file-text-o'],
            'xml'  => ['name' => 'application/xml', 'group' => 'Plain Text', 'class' => 'fa fa-file-text-o'],
            'swf'  => ['name' => 'application/x-shockwave-flash', 'group' => 'Plain Text', 'class' => 'fa fa-file-text-o'],

            // Image
            'png'  => ['name' => 'image/png', 'group' => 'Image', 'class' => 'fa fa-picture-o'],
            'jpe'  => ['name' => 'image/jpeg', 'group' => 'Image', 'class' => 'fa fa-picture-o'],
            'jpeg' => ['name' => 'image/jpeg', 'group' => 'Image', 'class' => 'fa fa-picture-o'],
            'jpg'  => ['name' => 'image/jpeg', 'group' => 'Image', 'class' => 'fa fa-picture-o'],
            'gif'  => ['name' => 'image/gif', 'group' => 'Image', 'class' => 'fa fa-picture-o'],
            'bmp'  => ['name' => 'image/bmp', 'group' => 'Image', 'class' => 'fa fa-picture-o'],
            'ico'  => ['name' => 'image/vnd.microsoft.icon', 'group' => 'Image', 'class' => 'fa fa-picture-o'],
            'tiff' => ['name' => 'image/tiff', 'group' => 'Image', 'class' => 'fa fa-picture-o'],
            'tif'  => ['name' => 'image/tiff', 'group' => 'Image', 'class' => 'fa fa-picture-o'],
            'svg'  => ['name' => 'image/svg+xml', 'group' => 'Image', 'class' => 'fa fa-picture-o'],
            'svgz' => ['name' => 'image/svg+xml', 'group' => 'Image', 'class' => 'fa fa-picture-o'],
            'psd'  => ['name' => 'image/vnd.adobe.photoshop', 'group' => 'Image', 'class' => 'fa fa-picture-o'],

            // Archives
            'zip' => ['name' => 'image/zip', 'group' => 'Archive', 'class' => 'fa fa-file-archive-o'],
            'rar' => ['name' => 'image/x-rar-compressed', 'group' => 'Archive', 'class' => 'fa fa-file-archive-o'],
            'cab' => ['name' => 'image/vnd.ms-cab-compressed', 'group' => 'Archive', 'class' => 'fa fa-file-archive-o'],

            // Audio
            'mp3' => ['name' => 'audio/mpeg', 'group' => 'Audio', 'class' => 'fa fa-file-audio-o'],

            // video
            'qt'  => ['name' => 'video/quicktime', 'group' => 'Video', 'class' => 'fa fa-file-video-o'],
            'mov' => ['name' => 'video/quicktime', 'group' => 'Video', 'class' => 'fa fa-file-video-o'],
            'flv' => ['name' => 'video/x-flv', 'group' => 'Video', 'class' => 'fa fa-file-video-o'],
            'avi' => ['name' => 'video/x-msvideo', 'group' => 'Video', 'class' => 'fa fa-file-video-o'],
            'mp4' => ['name' => 'video/mp4', 'group' => 'Video', 'class' => 'fa fa-file-video-o'],

            // Pdf
            'pdf' => ['name' => 'application/pdf', 'group' => 'Pdf', 'class' => 'fa fa-file-pdf-o'],

            // Doc
            'doc'  => ['name' => 'application/msword', 'group' => 'Doc', 'class' => 'fa fa-file-word-o'],
            'docx' => ['name' => 'application/msword', 'group' => 'Doc', 'class' => 'fa fa-file-word-o'],

            // Excel
            'xls' => ['name' => 'application/vnd.ms-excel', 'group' => 'Excel', 'class' => 'fa fa-file-excel-o'],
            'xlsx' => ['name' => 'application/vnd.ms-excel', 'group' => 'Excel', 'class' => 'fa fa-file-excel-o'],

            // Power point
            'ppt' => ['name' => 'application/vnd.ms-powerpoint', 'group' => 'Ppt', 'class' => 'fa fa-file-powerpoint-o'],
            'pptx' => ['name' => 'application/vnd.ms-powerpoint', 'group' => 'Ppt', 'class' => 'fa fa-file-powerpoint-o']
        ];

        return $mimeTypes;
    }

    /**
     * Get mime type 
     * @param  String $filename The name file
     * @return String           The mime type
     */
    public static function mimeContentType($filename) {
        $mimeTypes = self::listMimeTypes();
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        return @$mimeTypes[$ext]['name'];
    }

    /**
     * Save avatar
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Object $content Image content
     * @param  Int $userId     User id
     * @return String          File name
     */
    public static function saveAvatar($content, $userId)
    {
        $disk = Storage::disk('avatar');
        $time = time();

        $fileName = substr(Crypt::encrypt( uniqid(). $time.'-avatar-'.$userId), 6, 16).'.png';
        try {
            // Storage::disk('local_avatar')->put($fileName, $content);
            Storage::disk('avatar')->put($fileName, $content);
            $image = \Image::make(public_path().'/admin/avatars/'.$fileName);
            $image->crop($image->width(), $image->width(), 0)
            ->save(public_path().'/admin/avatars/'.$fileName);
            $image->resize(160, 160)
                ->save(public_path().'/admin/avatars/160x160_'.$fileName);
            $image->resize(100, 100)
                ->save(public_path().'/admin/avatars/100x100_'.$fileName);
            $image->resize(50, 50)
                ->save(public_path().'/admin/avatars/50x50_'.$fileName);
            $image->resize(30, 30)
                ->save(public_path().'/admin/avatars/30x30_'.$fileName);
        } catch(Exeption $e){
            return array('error' => $e->getMessage());
        }

        return $fileName;
    }

    /**
     * Delete avatar of user
     * @author Thanh Tuan <tuan@httsolution.com>
     * @param  String $filename File name
     * @return Void           
     */
    public static function deleteAvatar($filename)
    {
        $exists = Storage::disk('avatar')->exists($filename);
        if($exists){
            Storage::disk('avatar')->delete($filename);
        }
    }

}