<?php

namespace Core\Base\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;

use Core\Base\Models\RoleModel;
use Core\Base\Models\AssetModel;
use Core\Base\Models\StatusModel;
use Core\Base\Services\FileService;
use Core\Base\Models\PermissionModel;
use Core\Modules\Term\Models\TermModel;
use Core\Modules\User\Models\UserModel;
use Core\Modules\Product\Models\CategoryModel;

class InstallCommand extends Command
{
    /**
     * The console command name.
     * @var string
     */
    protected $signature = 'create:data {function}';

    /**
     * The console command description.
     * @var string
     */
    protected $description = 'Create first data';

    /**
     * Create new user supper admin
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @return void
     */
    public function createUser()
    {
        // Contain data ouput
        $result = [];

        // Ask 
        $result['fullName'] = $this->ask('Nhập họ tên');
        $result['email'] = $this->ask('Nhập địa chỉ email của bạn');
        $result['password'] = $this->secret('Nhập mật khẩu');
        $result['rePassword'] = $this->secret('Xác nhận mật khẩu');

        if ($result['password'] != $result['rePassword']) {

            $this->info('Xác nhận mật khẩu chưa đúng, mời nhập lại!');

            // Ask
            $result['password'] = $this->secret('Nhập mật khẩu');
            $result['rePassword'] = $this->secret('Xác nhận mật khẩu');
        }

        // Find the supper admin role
        $supperAdminRole = RoleModel::where('slug', 'super.admin')->first();

        // Create role if not exists
        if (empty($supperAdminRole)) {
            $supperAdminRole = RoleModel::create([
                'name' => 'SupperAdmin',
                'slug' => 'super.admin',
                'description' => 'SupperAdmin role', // optional
            ]);
        }

        // Find user
        $user = UserModel::where('email', $result['email'])->first();

        if (empty($user)) {
            // Create user and assign role is supper admin
            $superAdmin = UserModel::create([
                'full_name' => $result['fullName'],
                'email' => $result['email'],
                'avatar' => '',
                'password' => bcrypt($result['password']),
                'type' => 'super_admin',
                'role_id' => $supperAdminRole->id,
                'status' => 1,
                'remember_token' => str_random(40),
            ]);
        } else {
            $this->info('--- Email ' . $result['email'] . ' đã tồn tại trong hệ thống. ---');
        }
    }  

    public function createUncategory()
    {
        // Find and create category uncategory
        $category = TermModel::where('slug', 'uncategory')->first();

        if (empty($category)) {
            $category = TermModel::create([
                'name' => 'Uncategory',
                'slug' => 'uncategory',
                'url' => 'uncategory',
                'taxonomy' => 'category',
                'ancestor_ids' => json_encode([0]),
                'description' => 'Uncategory',
            ]);
        }

        // Find and create category uncategory
        $category = CategoryModel::where('slug', 'uncategory')->first();

        if (empty($category)) {
            $category = CategoryModel::create([
                'name' => 'Uncategory',
                'slug' => 'uncategory',
                'url' => 'uncategory',
                'ancestor_ids' => json_encode([0]),
                'description' => 'Uncategory',
            ]);
        }
    } 

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->info('--- Bắt đầu ---');

        $function = $this->argument('function');

        switch ($function) {
            case 'permission':
                // Create permissions
                $this->createPermissions();
                break;
            case 'uncategory':
                // Create uncategory
                $this->createUncategory();
                break;
            case 'user':
                // Create user super admin
                $this->createUser();
                break;
            case 'folder':
                // Create root folder
                $this->createFolder();
                break;
            case 'status':
                // Create list post status
                $this->createStatus();
                break;
            default:
        }

        $this->info('--- Thành công ---');
    }

    /**
     * Create the root folder
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @return Void
     */
    public function createFolder()
    {
        // Find user
        $user = UserModel::first();

        // Find upload folder
        $rootFolder = AssetModel::where('slug', 'root')->first();

        // If isset root folder or not exists user
        if (empty($rootFolder) && !empty($user)) {

            $rootFolder = AssetModel::create([
                'name' => 'root',
                'slug' => 'root',
                'parent_id' => 0,
                'user_id' => $user->id
            ]);
            
            FileService::makeDirectory('', 'storage');
        }
    }

    /**
     * Create list status
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @return Void
     */
    public function createStatus() 
    {
        $status = [
            [
                'name' => 'draft',
                'code' => 0
            ],
            [
                'name' => 'pending',
                'code' => 1
            ],
            [
                'name' => 'refuse',
                'code' => 2
            ],
            [
                'name' => 'publish',
                'code' => 99
            ]
        ];

        foreach ($status as $key => $stt) {
            $exists = StatusModel::where('name', $stt['name'])->first();
            if (!$exists) 
                StatusModel::create($stt);
        }
    }

    /**
     * Create list permission
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @return Void
     */
    public function createPermissions()
    {
        $permissions = [
            // Role
            [
                'name' => 'index.role',
                'slug' => 'index.role',
                'description' => 'Xem quyền',
                'table' => 'roles'
            ],

            [
                'name' => 'add.role',
                'slug' => 'add.role',
                'description' => 'Thêm quyền',
                'table' => 'roles'
            ],

            [
                'name' => 'edit.role',
                'slug' => 'edit.role',
                'description' => 'Sửa quyền',
                'table' => 'roles'
            ],

            [
                'name' => 'delete.role',
                'slug' => 'delete.role',
                'description' => 'Xóa quyền',
                'table' => 'roles'
            ],

            // Account
            [
                'name' => 'index.user',
                'slug' => 'index.user',
                'description' => 'Xem tài khoản',
                'table' => 'users'
            ],

            [
                'name' => 'add.user',
                'slug' => 'add.user',
                'description' => 'Thêm tài khoản',
                'table' => 'users'
            ],

            [
                'name' => 'edit.user',
                'slug' => 'edit.user',
                'description' => 'Sửa tài khoản',
                'table' => 'users'
            ],

            [
                'name' => 'delete.user',
                'slug' => 'delete.user',
                'description' => 'Xóa tài khoản',
                'table' => 'users'
            ],

            // Menu
            [
                'name' => 'index.menu',
                'slug' => 'index.menu',
                'description' => 'Xem menu',
                'table' => 'menus'
            ],

            [
                'name' => 'add.menu',
                'slug' => 'add.menu',
                'description' => 'Thêm menu',
                'table' => 'menus'
            ],

            [
                'name' => 'edit.menu',
                'slug' => 'edit.menu',
                'description' => 'Sửa menu',
                'table' => 'menus'
            ],

            [
                'name' => 'delete.menu',
                'slug' => 'delete.menu',
                'description' => 'Xóa menu',
                'table' => 'menus'
            ],

            // Library
            [
                'name' => 'index.library',
                'slug' => 'index.library',
                'description' => 'Xem menu',
                'table' => 'libraries'
            ],

            [
                'name' => 'add.library',
                'slug' => 'add.library',
                'description' => 'Thêm menu',
                'table' => 'libraries'
            ],

            [
                'name' => 'edit.library',
                'slug' => 'edit.library',
                'description' => 'Sửa menu',
                'table' => 'libraries'
            ],

            [
                'name' => 'delete.library',
                'slug' => 'delete.library',
                'description' => 'Xóa menu',
                'table' => 'libraries'
            ],

            // Post tag
            [
                'name' => 'index.tag',
                'slug' => 'index.tag',
                'description' => 'Xem từ khóa',
                'table' => 'terms'
            ],

            [
                'name' => 'add.tag',
                'slug' => 'add.tag',
                'description' => 'Thêm từ khóa',
                'table' => 'terms'
            ],

            [
                'name' => 'edit.tag',
                'slug' => 'edit.tag',
                'description' => 'Sửa từ khóa',
                'table' => 'terms'
            ],

            [
                'name' => 'delete.tag',
                'slug' => 'delete.tag',
                'description' => 'Xóa từ khóa',
                'table' => 'terms'
            ],

            // Post category
            [
                'name' => 'index.category',
                'slug' => 'index.category',
                'description' => 'Xem danh mục',
                'table' => 'terms'
            ],

            [
                'name' => 'add.category',
                'slug' => 'add.category',
                'description' => 'Thêm danh mục',
                'table' => 'terms'
            ],

            [
                'name' => 'edit.category',
                'slug' => 'edit.category',
                'description' => 'Sửa danh mục',
                'table' => 'terms'
            ],

            [
                'name' => 'delete.category',
                'slug' => 'delete.category',
                'description' => 'Xóa danh mục',
                'table' => 'terms'
            ],

            // Post 
            [
                'name' => 'index.post',
                'slug' => 'index.post',
                'description' => 'Xem bài viết',
                'table' => 'posts'
            ],

            [
                'name' => 'add.post',
                'slug' => 'add.post',
                'description' => 'Thêm bài viết',
                'table' => 'posts'
            ],

            [
                'name' => 'edit.post',
                'slug' => 'edit.post',
                'description' => 'Sửa bài viết',
                'table' => 'posts'
            ],

            [
                'name' => 'delete.post',
                'slug' => 'delete.post',
                'description' => 'Xóa bài viết',
                'table' => 'posts'
            ],

            // Page 
            [
                'name' => 'index.page',
                'slug' => 'index.page',
                'description' => 'Xem trang',
                'table' => 'posts'
            ],

            [
                'name' => 'add.page',
                'slug' => 'add.page',
                'description' => 'Thêm trang',
                'table' => 'posts'
            ],

            [
                'name' => 'edit.page',
                'slug' => 'edit.page',
                'description' => 'Sửa trang',
                'table' => 'posts'
            ],

            [
                'name' => 'delete.page',
                'slug' => 'delete.page',
                'description' => 'Xóa trang',
                'table' => 'posts'
            ],

            // Banner & slider 
            [
                'name' => 'index.slide',
                'slug' => 'index.slide',
                'description' => 'Xem banner & slide',
                'table' => 'posts'
            ],

            [
                'name' => 'add.slide',
                'slug' => 'add.slide',
                'description' => 'Thêm banner & slide',
                'table' => 'posts'
            ],

            [
                'name' => 'edit.slide',
                'slug' => 'edit.slide',
                'description' => 'Sửa banner & slide',
                'table' => 'posts'
            ],

            [
                'name' => 'delete.slide',
                'slug' => 'delete.slide',
                'description' => 'Xóa banner & slide',
                'table' => 'posts'
            ],

            // Product category 
            [
                'name' => 'index.product_category',
                'slug' => 'index.product_category',
                'description' => 'Xem danh mục sản phẩm',
                'table' => 'categories'
            ],

            [
                'name' => 'add.product_category',
                'slug' => 'add.product_category',
                'description' => 'Thêm danh mục sản phẩm',
                'table' => 'categories'
            ],

            [
                'name' => 'edit.product_category',
                'slug' => 'edit.product_category',
                'description' => 'Sửa danh mục sản phẩm',
                'table' => 'categories'
            ],

            [
                'name' => 'delete.product_category',
                'slug' => 'delete.product_category',
                'description' => 'Xóa danh mục sản phẩm',
                'table' => 'categories'
            ],

            // Product 
            [
                'name' => 'index.product',
                'slug' => 'index.product',
                'description' => 'Xem sản phẩm',
                'table' => 'products'
            ],

            [
                'name' => 'add.product',
                'slug' => 'add.product',
                'description' => 'Thêm sản phẩm',
                'table' => 'products'
            ],

            [
                'name' => 'edit.product',
                'slug' => 'edit.product',
                'description' => 'Sửa sản phẩm',
                'table' => 'products'
            ],

            [
                'name' => 'delete.product',
                'slug' => 'delete.product',
                'description' => 'Xóa sản phẩm',
                'table' => 'products'
            ],

            // Order 
            [
                'name' => 'index.order',
                'slug' => 'index.order',
                'description' => 'Xem đơn hàng',
                'table' => 'orders'
            ],

            [
                'name' => 'add.order',
                'slug' => 'add.order',
                'description' => 'Thêm đơn hàng',
                'table' => 'orders'
            ],

            [
                'name' => 'edit.order',
                'slug' => 'edit.order',
                'description' => 'Sửa đơn hàng',
                'table' => 'orders'
            ],

            [
                'name' => 'delete.order',
                'slug' => 'delete.order',
                'description' => 'Xóa đơn hàng',
                'table' => 'orders'
            ],

            // Setting 
            [
                'name' => 'index.setting',
                'slug' => 'index.setting',
                'description' => 'Xem cấu hình',
                'table' => 'settings'
            ],

            [
                'name' => 'edit.setting',
                'slug' => 'edit.setting',
                'description' => 'Sửa cấu hình',
                'table' => 'settings'
            ]
        ];

        foreach ($permissions as $key => $permission) {
            $exists = PermissionModel::where('slug', $permission['slug'])->first();
            if (!$exists) 
                PermissionModel::create($permission);
        }
    }
}