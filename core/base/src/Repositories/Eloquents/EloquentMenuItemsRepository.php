<?php

namespace Core\Base\Repositories\Eloquents;

use Core\Base\Models\MenuItemsModel;
use Core\Base\Repositories\Contracts\MenuItemsRepositoryInterface;

class EloquentMenuItemsRepository extends EloquentRepository implements MenuItemsRepositoryInterface
{
    /**
     * Get model
     * @return string
     */
    public function getModel()
    {
        return MenuItemsModel::class;
    }

    /**
     * Edit menu items
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Request $request Request
     * @param  Int $id Id of menu
     * @return \Illuminate\Http\Response
     */
    public function editMenuItems($data)
    {
        // Get menu item
        $menuItem = $this->model->findOrFail($data['id']);

        // Call function update profile
        return $menuItem->update($data);
    }
}