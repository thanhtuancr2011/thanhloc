<?php

namespace Core\Base\Repositories\Eloquents;

use Core\Base\Models\RoleModel;
use Core\Base\Repositories\Eloquents\EloquentRepository;
use Core\Base\Repositories\Contracts\RoleRepositoryInterface;

class EloquentRoleRepository extends EloquentRepository implements RoleRepositoryInterface
{
	/**
     * Get model
     * @return string
     */
    public function getModel()
    {
        return RoleModel::class;
    }

    /**
     * Get role is not power
     * @return Void 
     */
    public function getRoleNotPower()
    {
        return RoleModel::where('slug', 'not.power')->first();
    }
}