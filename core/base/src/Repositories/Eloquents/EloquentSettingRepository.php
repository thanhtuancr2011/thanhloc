<?php

namespace Core\Base\Repositories\Eloquents;

use Core\Base\Models\SettingModel;

use Core\Base\Repositories\Eloquents\EloquentRepository;
use Core\Base\Repositories\Contracts\SettingRepositoryInterface;

class EloquentSettingRepository extends EloquentRepository implements SettingRepositoryInterface
{
    /**
     * Get model
     * @return string
     */
    public function getModel()
    {
        return SettingModel::class;
    }

    /**
     * Get setting is not power
     * @return Void
     */
    public function getItem()
    {
        $item = $this->model->pluck('value', 'key');

        $item->robot_txt = \Storage::disk('public_local')->get('robots.txt');

        return $item;
    }

    /**
     * Get config with key
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  String $key Setting key
     * @return Void
     */
    public function getConfig($key)
    {
        return $this->model->getConfig($key);
    }
}