<?php

namespace Core\Base\Repositories\Eloquents;

use Core\Base\Models\StatusModel;
use Core\Base\Repositories\Eloquents\EloquentRepository;
use Core\Base\Repositories\Contracts\StatusRepositoryInterface;

class EloquentStatusRepository extends EloquentRepository implements StatusRepositoryInterface
{
    /**
     * Get model
     * @return string
     */
    public function getModel()
    {
        return StatusModel::class;
    }
}