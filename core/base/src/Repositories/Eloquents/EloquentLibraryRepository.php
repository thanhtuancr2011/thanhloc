<?php

namespace Core\Base\Repositories\Eloquents;

use Core\Base\Models\LibraryModel;
use Core\Base\Repositories\Eloquents\EloquentRepository;
use Core\Base\Repositories\Contracts\LibraryRepositoryInterface;

class EloquentLibraryRepository extends EloquentRepository implements LibraryRepositoryInterface
{
	/**
     * Get model
     * @return string
     */
    public function getModel()
    {
        return LibraryModel::class;
    }

    /**
     * Crop image
     * @param Array $data File upload
     */
    public function cropImage($data) 
    {
        return $this->model->cropImage($data);
    }

    /**
	 * Store image 
	 * @param Array $data File upload
	 */
    public function storeFile($data)
    {
    	return $this->model->storeFile($data);
    }
}