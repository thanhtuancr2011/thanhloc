<?php

namespace Core\Base\Repositories\Eloquents;

use Core\Base\Models\PermissionModel;

use Core\Base\Repositories\Eloquents\EloquentRepository;
use Core\Base\Repositories\Contracts\PermissionRepositoryInterface;

class EloquentPermissionRepository extends EloquentRepository implements PermissionRepositoryInterface
{
	/**
     * Get model
     * @return string
     */
    public function getModel()
    {
        return PermissionModel::class;
    }
}