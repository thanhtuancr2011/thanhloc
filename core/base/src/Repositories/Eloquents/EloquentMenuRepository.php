<?php

namespace Core\Base\Repositories\Eloquents;

use Core\Base\Models\MenuModel;
use Core\Base\Repositories\Contracts\MenuRepositoryInterface;

class EloquentMenuRepository extends EloquentRepository implements MenuRepositoryInterface
{
    /**
     * Get model
     * @return string
     */
    public function getModel()
    {
        return MenuModel::class;
    }

    /**
     * Add menu items
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Request $request Request
     * @param  Int $id Id of menu
     * @return \Illuminate\Http\Response
     */
    public function addMenuItems($id, $data)
    {
        // Get menu
        $menu = $this->model->findOrFail($id);

        // Call function update profile
        return $menu->addMenuItems($data);
    }

    /**
     * Delete menu item
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Request $request Request
     * @param  Int $id Id of menu
     * @return \Illuminate\Http\Response
     */
    public function deleteMenuItem($id, $data)
    {
        // Get menu
        $menu = $this->model->findOrFail($id);

        // Call function update profile
        return $menu->deleteMenuItem($data);
    }

    /**
     * Change menu item postion
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Request $request Request
     * @param  Int $id Id of menu
     * @return \Illuminate\Http\Response
     */
    public function changeMenuItemPosition($id, $data)
    {
        // Get menu
        $menu = $this->model->findOrFail($id);

        // Call function update profile
        return $menu->changeMenuItemPosition($data);
    }

    /**
     * Get all menu items with tree format
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Request $request Request
     * @return Response
     */
    public function getMenuItemsTree($id)
    {
        // Get menu
        $menu = $this->model->findOrFail($id);

        return $menu->getMenuItemsTree();
    }

    /**
     * @param String $slug
     * @return Array
     */
    public function display($slug)
    {
        $menu = $this->model->where('slug', '=', $slug)->first();

        if (!$menu)
            return false;

        $menuItem = $menu->getMenuItemsTree();

        return $menuItem;
    }
}