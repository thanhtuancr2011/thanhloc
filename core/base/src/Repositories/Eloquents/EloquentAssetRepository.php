<?php

namespace Core\Base\Repositories\Eloquents;

use Core\Base\Models\AssetModel;
use Core\Base\Repositories\Eloquents\EloquentRepository;
use Core\Base\Repositories\Contracts\AssetRepositoryInterface;

class EloquentAssetRepository extends EloquentRepository implements AssetRepositoryInterface
{
    /**
     * Get model
     * @return string
     */
    public function getModel()
    {
        return AssetModel::class;
    }

    /**
     * Get folders with tree format
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @return \Illuminate\Http\Response
     */
    public function getFoldersTree()
    {
        // Call function get folders
        return $this->model->getFoldersTree();
    }

    /**
     * Get folder path
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Request $request Request
     * @return Response         
     */
    public function getFolderPath($folderId) 
    {
        return $this->model->recursiveGetParentFoldersName($folderId);
    }
}