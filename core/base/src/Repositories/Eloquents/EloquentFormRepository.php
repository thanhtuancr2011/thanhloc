<?php

namespace Core\Base\Repositories\Eloquents;

use Core\Base\Models\FormModel;
use Core\Base\Repositories\Eloquents\EloquentRepository;
use Core\Base\Repositories\Contracts\FormRepositoryInterface;

class EloquentFormRepository extends EloquentRepository implements FormRepositoryInterface
{
    /**
     * Get model
     * @return string
     */
    public function getModel()
    {
        return FormModel::class;
    }
}