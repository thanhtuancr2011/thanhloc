<?php

namespace Core\Base\Repositories\Eloquents;

use Core\Base\Repositories\Contracts\RepositoryInterface;

abstract class EloquentRepository implements RepositoryInterface
{
    /**
     * @var \Illuminate\Database\Eloquent\Model
     */
    protected $model;
    
    public function __construct()
    {
        $this->setModel();
    }

    /**
     * Get the model    
     * @return Void
     */
    abstract public function getModel();

    /**
     * Scope date
     * @param  Object $query The query
     * @param  String $type  Type search
     * @return Void        
     */
    public function scopeDate($query, $type)
    {
        return $query->where('created_date', $type);
    }

    /**
     * Set the model
     * @return Void 
     */
    public function setModel()
    {
        $this->model = app()->make(
            $this->getModel()
        );
    }

    /**
     * Get all data
     * @return Void 
     */
    public function all()
    {
        return $this->model->get();
    }

    /**
     * Create view
     * @param  Array $data The data input
     * @return Void
     */
    public function create()
    {
        return $this->model;
    }

    /**
     * Edit view
     * @param  Array $data The data input
     * @return Void
     */
    public function edit($id)
    {
        return $this->model->find($id);
    }

    /**
     * Get data with conditions
     * @param  Array $data The conditions
     * @return Void
     */
    public function where($conditions)
    {
        return $this->model->where($conditions);
    }

    /**
     * Show view
     * @param  Array $data The data input
     * @return Void
     */
    public function show($id)
    {
        return $this->model->find($id);
    }

    /**
     * Get items
     * @param  Array $data The data input
     * @return Void
     */
    public function items($data)
    {
        return $this->model->items($data);
    }

    /**
     * Create a item
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store($data)
    {
        return $this->model->storeItem($data);
    }

    /**
     * Update a item.
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, $data)
    {
        $item = $this->model->find($id);
        return $item->updateItem($data);
    }

    /**
     * Delete item
     * @param  Request $request The request data
     * @return Void
     */
    public function delete($ids)
    {
        return $this->model->deleteItems($ids);
    }

    /**
     * Validate the data input
     * @param  Array $data The data input
     * @param  Array $rules List rules
     * @param  Array $messages Customize messages
     * @return Void
     */
    public function validate($data, $rules, $messages)
    {
        // Validate
        $validator = \Validator::make($data, $rules, $messages);

        // If validate faild
        if ($validator->fails()) {

            // Get errors
            $errors = $validator->errors();

            // Format errors
            $errorsList = [];
            foreach ($errors->toArray() as $key => $value) {
                array_push($errorsList, $value[0]);
            }

            return ['status' => -1, 'errors' => $errorsList];
        }

        return $data;
    }
}