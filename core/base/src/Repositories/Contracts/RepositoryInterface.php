<?php

namespace Core\Base\Repositories\Contracts;

interface RepositoryInterface
{
    /**
     * Create view
     * @param  Array $data The data input
     * @return Void
     */
    public function create();

    /**
     * Edit view
     * @param  Array $data The data input
     * @return Void
     */
    public function edit($id);

    /**
     * Show view
     * @param  Array $data The data input
     * @return Void
     */
    public function show($id);

    /**
     * Get with conditions
     * @param  Array $data The conditions
     * @return Void
     */
    public function where($conditions);

    /**
     * Get all
     * @author AnhVN <thanhtuancr2011@gmail.com>
     * @param  Array $data The data input
     * @return Void
     */
    public function all();

    /**
     * Get items
     * @param  Array $data The data input
     * @return Void
     */
    public function items($data);

    /**
     * Create a item
     * @param  Array $data The data input
     * @return \Illuminate\Http\Response
     */
    public function store($data);

    /**
     * Update a item.
     * @param  Array $data The data input
     * @param  Integer $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, $data);

    /**
     * Delete items
     * @param  Request $id The data id
     * @return Void
     */
    public function delete($ids);

    /**
     * Validate the data input
     * @param  Array $data The data input
     * @param  Array $rules List rules
     * @param  Array $messages Customize messages
     * @return Void
     */
    public function validate($data, $rules, $messages);


}