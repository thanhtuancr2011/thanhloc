<?php


namespace Core\Base\Repositories\Contracts;


interface MenuRepositoryInterface
{
    /**
     * Add menu Utems
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Request $request Request
     * @return Response
     */
    public function addMenuItems($id, $data);

    /**
     * Get all menu items with tree format
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Request $request Request
     * @return Response
     */
    public function getMenuItemsTree($id);

    /**
     * Change menu item position
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Request $request Request
     * @return Response
     */
    public function changeMenuItemPosition($id, $data);

    /**
     * Delete menu item
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Request $request Request
     * @return Response
     */
    public function deleteMenuItem($id, $data);

    /**
     * get menu item of menu
     * @author AnhVN <anhvn@diziweb.com>
     * @param  String $slug
     * @return Array
     */
    public function display($slug);
}


