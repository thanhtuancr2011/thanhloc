<?php


namespace Core\Base\Repositories\Contracts;

interface MenuItemsRepositoryInterface
{
	/**
     * Edit menu Items
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Request $request Request
     * @return Response
     */
    public function editMenuItems($data);
}


