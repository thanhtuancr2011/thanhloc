<?php

namespace Core\Base\Repositories\Contracts;

interface LibraryRepositoryInterface
{
	/**
	 * Crop image
	 * @param Array $data File upload
	 */
    public function cropImage($data);

    /**
	 * Store image 
	 * @param Array $data File upload
	 */
    public function storeFile($data);
}


