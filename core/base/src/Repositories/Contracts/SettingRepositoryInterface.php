<?php

namespace Core\Base\Repositories\Contracts;

interface SettingRepositoryInterface
{
	/**
     * Get all setting
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @return Response         
     */
    public function getItem();

    /**
     * Get config with key
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  String $key Setting key
     * @return Void      
     */
    public function getConfig($key);
}


