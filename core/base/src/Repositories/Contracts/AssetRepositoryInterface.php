<?php

namespace Core\Base\Repositories\Contracts;

interface AssetRepositoryInterface
{
	/**
     * Get all folders with tree format
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Request $request Request
     * @return Response         
     */
    public function getFoldersTree();

    /**
     * Get folder path
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Request $request Request
     * @return Response         
     */
    public function getFolderPath($folderId);
}


