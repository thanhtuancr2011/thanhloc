<?php

namespace Core\Base\Http\Controllers\Api;


use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

use Watson\Sitemap\Facades\Sitemap;
use Core\Base\Repositories\Contracts\SettingRepositoryInterface;

class SettingController extends Controller
{
    protected $settingRepository;

    public function __construct(SettingRepositoryInterface $settingRepository)
    {
        $this->middleware('auth');
        $this->settingRepository = $settingRepository;
    }

    /**
     * Save role
     * @param  Request $request Data input
     * @return Void
     */
    public function store(Request $request)
    {
        // All data input
        $data = $request->all();

        // Call function save setting
        $setting = $this->settingRepository->store($data);

        if ($setting) {
            $result = ['status' => 1, 'msg' => 'Lưu thành công.'];
        } else {
            $result = ['status' => 0, 'msg' => 'Đã xảy ra lỗi. Mời thử lại'];
        }

        return new JsonResponse($result);
    }


    /**
     * Create site map
     * @return Array Result
     */
    public function createSitemap()
    {
        // Get all categories
        $categories = get_category();

        // Get posts
        $posts = get_post(['posts_per_page' => 0]);

        // Each category
        foreach ($categories as $category) {
            Sitemap::addTag(getUrl($category->taxonomy.'.'.$category->slug), $category->updated_at, 'daily', '0.8');
        }

        // Each post
        foreach ($posts as $post) {
            $tag = Sitemap::addTag(getUrl($post->post_type.'.'.$post->slug), $post->updated_at, 'daily', '0.8');
            $img = json_decode($post->image_feature, true);
            $src = isset($img['src']) ? $img['src'] : '';
            $alt = isset($img['alt']) ? $img['alt'] : '';
            if ($src)
                $tag->addImage(url($src), $alt);
        }

        // Save site máp
        $status = Storage::disk('public_local')->put('sitemaps.xml', Sitemap::xml());

        if ($status) {
            $result = ['status' => 1, 'msg' => 'Tạo sitemaps thành công.'];
        } else {
            $result = ['status' => 0, 'msg' => 'Đã xảy ra lỗi. Mời thử lại'];
        }

        return new JsonResponse($result);
    }
}
