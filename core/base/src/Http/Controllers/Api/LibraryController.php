<?php

namespace Core\Base\Http\Controllers\Api;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;

use Core\Base\Facades\Base;
use Core\Base\Services\FileService;
use Core\Base\Repositories\Contracts\LibraryRepositoryInterface;
use Core\Base\Repositories\Contracts\AssetRepositoryInterface;

class LibraryController extends Controller
{
    protected $libraryRepository;
    protected $assetRepository;

    public function __construct(LibraryRepositoryInterface $libraryRepository, AssetRepositoryInterface $assetRepository)
    {
        $this->middleware('auth');
        $this->libraryRepository = $libraryRepository;
        $this->assetRepository   = $assetRepository;
    }

    /**
     * Get libraries
     * @param  Request $request Data input
     * @return Void           
     */
    public function libraries(Request $request)
    {
        // Check permission
        Base::canOrAbort('index.library', 404);

        // Get all data input
        $data = $request->all();
        
        // Call function get all libraries
        $result = $this->libraryRepository->items($data);

        return new JsonResponse($result);
    }

    /**
     * Save role
     * @param  Request $request Data input
     * @return Void           
     */
    public function store(Request $request)
    {
        // Check permission
        Base::canOrAbort('add.library', 404);

        // Check error
        if (empty($_FILES['file'])) {
            $message = 'Kích thước upload tối đa là '. ini_get("upload_max_filesize");
            return new JsonResponse(['status' => 0, 'msg' => $message], 422);
        } elseif ($_FILES['file']['error'] > 0) {
            $error = FileService::codeToMessage($_FILES['file']['error']);
            return new JsonResponse(['status' => 0, 'msg' => $error], 422);
        }

        // File upload
        $data = $_FILES;

        // Call function save
        $result = $this->libraryRepository->store($data);

        return new JsonResponse($result);
    }

    /**
     * Set data validate
     * @param  Array $data The data input
     * @return Void       
     */
    public function dataValidate($data)
    {
        $validate = [
            'rules' => [
                'title' => 'required',
            ],
            'messages' => [
                'title.required' => 'Bạn chưa nhập tên',
            ]
        ];

        return $validate;
    }

    /**
     * Update role
     * @param  Request $request Data input
     * @return Void           
     */
    public function update($id, Request $request)
    {
        // Check permission
        Base::canOrAbort('edit.library', 404);

        // All data input
        $data = $request->all();

        // Call function set data validate and validate the data input
        $vali = $this->dataValidate($data);
        $data = $this->libraryRepository->validate($data, $vali['rules'], $vali['messages']);

        // Validate faild
        if (isset($data['errors'])) {
            return new JsonResponse($data);
        }

        // Call function update
        $library = $this->libraryRepository->update($id, $data);

        if ($library) {
            $result = ['status' => 1, 'library' => $library, 'msg' => 'Cập nhật thành công'];
        } else {
            $result = ['status' => 0, 'msg' => 'Đã xảy ra lỗi, mời thử lại!'];
        }

        return new JsonResponse($result);
    }

    /**
     * Delete multiple roles
     * @param  Request $request The request data
     * @return Void           
     */
    public function delete(Request $request)
    {
        // Check permission
        Base::canOrAbort('delete.library', 404);

        // All ids
        $ids = $request->all();

        // Call function delete
        $status = $this->libraryRepository->delete($ids);

        if ($status) {
            $result = ['status' => $status, 'msg' => 'Xóa thành công'];
        } else {
            $result = ['status' => $status, 'msg' => 'Đã xảy ra lỗi, mời thử lại!'];
        }

        // Return blogger 
        return new JsonResponse($result);
    }

    /**
     * Crop image
     * @param  Request $request The request data
     * @return Void           
     */
    public function cropImage(Request $request)
    {
        // Check permission
        Base::canOrAbort('edit.library', 404);

        // Check error
        if (empty($_FILES['cropped_image'])) {
            $message = 'Kích thước tối đa tập tin là: ' . ini_get("upload_max_filesize");
            return new JsonResponse(['status' => 0, 'msg' => $message], 422);
        } elseif ($_FILES['cropped_image']['error'] > 0) {
            $error = FileService::codeToMessage($_FILES['cropped_image']['error']);
            return new JsonResponse(['status' => 0, 'msg' => $error], 422);
        }

        // File upload
        $data = $_FILES['cropped_image'];

        // Call function save
        $result = $this->libraryRepository->cropImage($data);

        return new JsonResponse($result);
    }
}
