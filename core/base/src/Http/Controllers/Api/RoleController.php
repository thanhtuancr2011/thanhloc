<?php

namespace Core\Base\Http\Controllers\Api;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;

use Core\Base\Facades\Base;
use Core\Base\Repositories\Contracts\RoleRepositoryInterface;

class RoleController extends Controller
{
    protected $roleRepository;

    public function __construct(RoleRepositoryInterface $roleRepository)
    {
        $this->middleware('auth');
        $this->roleRepository = $roleRepository;
    }

    /**
     * Get roles
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Request $request Data input
     * @return Void           
     */
    public function roles(Request $request)
    {
        // Check permission
        Base::canOrAbort('index.role', 404);

    	// Get all data input
    	$data = $request->all();

    	// Call function get all roles
        $role = $this->roleRepository->items($data);

        return new JsonResponse($role);
    }

    /**
     * Set data validate
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Array $data The data input
     * @return Void       
     */
    public function dataValidate($data)
    {
        $validate = [
            'rules' => [
                'name' => 'required',
                'slug' => 'unique:roles,slug'
            ],
            'messages' => [
                'name.required' => 'Bạn chưa nhập quyền',
                'slug.unique'   => 'Quyền đã tồn tại trong hệ thống'
            ]
        ];

        // If update
        if (!empty($data['id'])){

            // Find role
            $role = $this->roleRepository->edit($data['id']);

            // If slug input = slug of category edit
            if($role->slug == $data['slug']){
                $validate['rules']['slug'] = 'unique:roles,slug,NULL,id,slug,$role->slug';
            }
        }

        return $validate;
    }

    /**
     * Save role
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Request $request Data input
     * @return Void           
     */
    public function store(Request $request)
    {
        // Check permission
        Base::canOrAbort('add.role', 404);

        // All data input
        $data = $request->all();
        $data['slug'] = str_slug($data['name'], '.');

        // Call function set data validate and validate the data input
        $vali = $this->dataValidate($data);
        $data = $this->roleRepository->validate($data, $vali['rules'], $vali['messages']);

        // Validate fail
        if (isset($data['errors'])) {
            return new JsonResponse($data);
        }

        // Call function save role
        $role = $this->roleRepository->store($data);

        if ($role) {
            $result = ['status' => 1, 'msg' => 'Thêm thành công', 'role' => $role];
        } else {
            $result = ['status' => 0, 'msg' => 'Đã xảy ra lỗi, mời thử lại'];
        }

        return new JsonResponse($result);
    }

    /**
     * Update role
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Request $request Data input
     * @return Void           
     */
    public function update($id, Request $request)
    {
        // Check permission
        Base::canOrAbort('index.role', 404);

        // All data input
        $data = $request->all();
        $data['slug'] = str_slug($data['name'], '.');

        // Call function set data validate and validate the data input
        $vali = $this->dataValidate($data);
        $data = $this->roleRepository->validate($data, $vali['rules'], $vali['messages']);

        // Validate fail
        if (isset($data['errors'])) {
            return new JsonResponse($data);
        }

        // Call function save role
        $role = $this->roleRepository->update($id, $data);

        if ($role) {
            $result = ['status' => 1, 'msg' => 'Cập nhật thành công'];
        } else {
            $result = ['status' => 0, 'msg' => 'Đã xảy ra lỗi, mời thử lại'];
        }

        return new JsonResponse($result);
    }

    /**
     * Delete multiple roles
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Request $request The request data
     * @return Void           
     */
    public function delete(Request $request)
    {
        // Check permission
        Base::canOrAbort('delete.role', 404);

        // All ids
        $ids = $request->all();

        // Call function save role
        $status = $this->roleRepository->delete($ids);

        if ($status) {
            $result = ['status' => $status, 'msg' => 'Xóa thành công'];
        } else {
            $result = ['status' => $status, 'msg' => 'Đã xảy ra lỗi, mời thử lại'];
        }

        // Return blogger 
        return new JsonResponse($result);
    }
}
