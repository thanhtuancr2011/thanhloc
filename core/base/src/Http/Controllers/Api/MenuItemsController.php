<?php

namespace Core\Base\Http\Controllers\Api;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;

use Core\Base\Repositories\Contracts\MenuItemsRepositoryInterface;


class MenuItemsController extends Controller
{
    protected $menuItemsRepository;

    public function __construct(MenuItemsRepositoryInterface $menuItemsRepository)
    {
        $this->middleware('auth');
        $this->menuItemsRepository = $menuItemsRepository;
    }
}
