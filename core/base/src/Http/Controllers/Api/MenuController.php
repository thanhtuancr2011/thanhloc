<?php

namespace Core\Base\Http\Controllers\Api;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;

use Core\Base\Facades\Base;
use Core\Base\Repositories\Contracts\MenuRepositoryInterface;
use Core\Base\Repositories\Contracts\MenuItemsRepositoryInterface;

class MenuController extends Controller
{
    protected $menuRepository;
    protected $menuItemsRepository;

    public function __construct(MenuRepositoryInterface $menuRepository, MenuItemsRepositoryInterface $menuItemsRepository)
    {
        $this->middleware('auth');
        $this->menuRepository = $menuRepository;
        $this->menuItemsRepository = $menuItemsRepository;
    }

    /**
     * Get menus
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Request $request Data input
     * @return Void           
     */
    public function menus(Request $request)
    {
        // Check permission
        Base::canOrAbort('index.menu', 404);

    	// Get all data input
    	$data = $request->all();

    	// Call function get all menus
        $menu = $this->menuRepository->items($data);

        return new JsonResponse($menu);
    }

    /**
     * Set data validate
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Array $data The data input
     * @return Void       
     */
    public function dataValidate($data)
    {
        $validate = [
            'rules' => [
                'name'  => 'required',
                'depth' => 'required',
                'slug'  => 'unique:menus,slug'
            ],
            'messages' => [
                'name.required'  => 'Bạn chưa nhập tên cho menu',
                'depth.required' => 'Bạn chưa nhập số cấp cho menu',
                'slug.unique'    => 'Menu đã tồn tại trong hệ thống'
            ]
        ];

        // If update
        if (!empty($data['id'])){

            // Find menu
            $menu = $this->menuRepository->edit($data['id']);

            // If slug input = slug of category edit
            if($menu->slug == $data['slug']){
                $validate['rules']['slug'] = 'unique:menus,slug,NULL,id,slug,$menu->slug';
            }
        }

        return $validate;
    }

    /**
     * Save menu
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Request $request Data input
     * @return Void           
     */
    public function store(Request $request)
    {
        // Check permission
        Base::canOrAbort('add.menu', 404);

        // All data input
        $data = $request->all();
        $data['slug'] = str_slug($data['name'], '-');

        // Call function set data validate and validate the data input
        $vali = $this->dataValidate($data);
        $data = $this->menuRepository->validate($data, $vali['rules'], $vali['messages']);

        // Validate fail
        if (isset($data['errors'])) {
            return new JsonResponse($data);
        }

        // Call function save menu
        $menu = $this->menuRepository->store($data);

        if ($menu) {
            $result = ['status' => 1, 'msg' => 'Thêm thành công'];
        } else {
            $result = ['status' => 0, 'msg' => 'Đã xảy ra lỗi, mời thử lại'];
        }

        return new JsonResponse($result);
    }

    /**
     * Update menu
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Request $request Data input
     * @return Void           
     */
    public function update($id, Request $request)
    {
        // Check permission
        Base::canOrAbort('edit.menu', 404);

        // All data input
        $data = $request->all();
        $data['slug'] = str_slug($data['name'], '-');

        // Call function set data validate and validate the data input
        $vali = $this->dataValidate($data);
        $data = $this->menuRepository->validate($data, $vali['rules'], $vali['messages']);

        // Validate fail
        if (isset($data['errors'])) {
            return new JsonResponse($data);
        }

        // Call function save menu
        $menu = $this->menuRepository->update($id, $data);

        if ($menu) {
            $result = ['status' => 1, 'msg' => 'Cập nhật thành công'];
        } else {
            $result = ['status' => 0, 'msg' => 'Đã xảy ra lỗi, mời thử lại'];
        }

        return new JsonResponse($result);
    }

    /**
     * Delete multiple menus
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Request $request The request data
     * @return Void           
     */
    public function delete(Request $request)
    {
        // Check permission
        Base::canOrAbort('delete.menu', 404);

        // All ids
        $ids = $request->all();

        // Call function save menu
        $status = $this->menuRepository->delete($ids);

        if ($status) {
            $result = ['status' => $status, 'msg' => 'Xóa thành công'];
        } else {
            $result = ['status' => $status, 'msg' => 'Đã xảy ra lỗi, mời thử lại'];
        }

        // Return menu 
        return new JsonResponse($result);
    }

    /**
     * Add menu item
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Request $request Data input
     * @return Void           
     */
    public function addMenuItems($id, Request $request)
    {
        // Check permission
        Base::canOrAbort('edit.menu', 404);

        // All data input
        $data = $request->all();

        // Call function save menu
        $menuItems = $this->menuRepository->addMenuItems($id, $data);

        if (!empty($menuItems)) {
            $result = ['status' => 1, 'msg' => 'Thêm thành công', 'menuItems' => $menuItems];
        } else {
            $result = ['status' => 0, 'msg' => 'Đã xảy ra lỗi, mời thử lại'];
        }

        return new JsonResponse($result);
    }

    /**
     * Delete menu item
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Request $request Data input
     * @return Void           
     */
    public function deleteMenuItem($id, Request $request)
    {
        // Check permission
        Base::canOrAbort('edit.menu', 404);

        // All data input
        $data = $request->all();

        // Call function save menu
        $menu = $this->menuRepository->deleteMenuItem($id, $data);

        if ($menu) {
            $result = ['status' => 1, 'msg' => 'Xóa thành công'];
        } else {
            $result = ['status' => 0, 'msg' => 'Đã xảy ra lỗi, mời thử lại'];
        }

        return new JsonResponse($result);
    }

    /**
     * Change menu item position
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Request $request Data input
     * @return Void           
     */
    public function changeMenuItemPosition($id, Request $request)
    {
        // Check permission
        Base::canOrAbort('edit.menu', 404);

        // All data input
        $data = $request->all();

        // Call function save menu
        $menu = $this->menuRepository->changeMenuItemPosition($id, $data);

        if ($menu) {
            $result = ['status' => 1, 'msg' => 'Cập nhật thành công'];
        } else {
            $result = ['status' => 0, 'msg' => 'Đã xảy ra lỗi, mời thử lại'];
        }

        return new JsonResponse($result);
    }

    /**
     * Add menu item
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Request $request Data input
     * @return Void           
     */
    public function editMenuItems($id, Request $request)
    {
        $status = 0;

        // Check permission
        Base::canOrAbort('edit.menu', 404);
        
        // All data input
        $data = $request->all();

        // Call function save menu
        $status = $this->menuItemsRepository->editMenuItems($data);

        if ($status) {
            $result = ['status' => 1, 'msg' => 'Cập nhật thành công'];
        } else {
            $result = ['status' => 0, 'msg' => 'Đã xảy ra lỗi, mời thử lại'];
        }

        return new JsonResponse($result);
    }

    public function getMenuItems($id)
    {
        // Check permission
        Base::canOrAbort('edit.menu', 404);

        // Call function get tree menu items
        $menuItems = $this->menuRepository->getMenuItemsTree($id);

        if ($menuItems) {
            $result = ['status' => 1, 'msg' => 'Success.', 'menuItems' => $menuItems];
        } else {
            $result = ['status' => 0, 'msg' => 'Đã xảy ra lỗi, mời thử lại'];
        }

        return new JsonResponse($result);
    }
}
