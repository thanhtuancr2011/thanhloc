<?php

namespace Core\Base\Http\Controllers\Api;

use Auth;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\App;
use App\Http\Controllers\Controller;

use Core\Base\Facades\Base;
use Core\Base\Repositories\Contracts\FormRepositoryInterface;

class FormController extends Controller
{
    protected $formRepository;

    protected $excel;

    protected $dataSendEmail = [];

    public function __construct(FormRepositoryInterface $formRepository)
    {
        $this->middleware('auth')->except('store');
        $this->formRepository = $formRepository;
    }

    public function contacts(Request $request)
    {
        // Get all data input
        $data = $request->all();
        if (isset($data['is_export']) && $data['is_export']) {
            $data['is_pagination'] = false;
            $result = $this->formRepository->items($data);
            $arr = [
                'id' => 'ID',
                'full_name' => 'Họ và tên',
                'email' => 'Email',
                'phone_number' => 'Số điện thoại',
                'address' => 'Địa chỉ',
                'content' => 'Nội dung',
                'type' => 'Loại',
                'created_at' => "Ngày gửi"
            ];

            $export = [];
            foreach ($result['contacts'] as $d) {
                $temp = [];
                foreach ($arr as $k => $r) {
                    $temp[$r] = ($d[$k])?$d[$k]:'';
                }
                $export[] = $temp;
            }
            $result['contacts']=$export;
            return new JsonResponse($result);
            
        } else {
            // Call function get all libraries
            $result = $this->formRepository->items($data);
            return new JsonResponse($result);
        }
    }

    /**
     * Set data validate
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Array $data The data input
     * @return Void
     */
    public function dataValidate($data)
    {
        if ($data['type'] == 'newsletter') {
            $validate = [
                'rules' => [
                    'email' => "required|email|uniqueEmailAndType:{$data['type']}",
                ],
                'messages' => [
                    'email.required' => 'Bạn chưa nhập email',
                    'email.unique_email_and_type' => 'Email đã tồn tại trong hệ thống',
                ]
            ];
        } else {
            $validate = [
                'rules' => [
                    'email' => 'required|email',
                ],
                'messages' => [
                    'email.required' => 'Email đã tồn tại trong hệ thống',
                ]
            ];
        }

        return $validate;
    }

    public function configMail()
    {
        \Config::set('mail.driver', get_config('mail_drive', 'smtp'));
        \Config::set('mail.host', get_config('mail_host', 'smtp.mailgun.org'));
        \Config::set('mail.port', get_config('mail_port', 587));
        \Config::set('mail.username', get_config('mail_username', ''));
        \Config::set('mail.password', get_config('mail_password', ''));
        \Config::set('mail.encryption', get_config('mail_encryption', 'tls'));
    }
    
    /**
     * Save contact
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Request $request Data input
     * @return Void
     */
    public function store(Request $request)
    {
        // All data input
        $data = $request->all();
        
        \Validator::extend('uniqueEmailAndType', function ($attribute, $value, $parameters, $validator) {
            $count = \DB::table('forms')->where('email', $value)
                                        ->where('type', $parameters[0])
                                        ->count();
            return $count === 0;
        });

        // Call function set data validate and validate the data input
        $vali = $this->dataValidate($data);
        $data = $this->formRepository->validate($data, $vali['rules'], $vali['messages']);

        // Validate fail
        if (isset($data['errors'])) {
            return new JsonResponse($data);
        }

        // Call function save contact
        $contact = $this->formRepository->store($data);

        if ($contact) {

            // Return form
            $result = ['status' => 1, 'msg' => 'Đăng kí thành công'];
        } else {
            $result = ['status' => 0, 'msg' => 'Đã xảy ra lỗi, mời thử lại'];
        }

        return new JsonResponse($result);
    }

    /**
     * Delete multiple roles
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Request $request The request data
     * @return Void
     */
    public function delete(Request $request)
    {
        // Check permission
        Base::canOrAbort('delete.forms', 404);

        // All ids
        $ids = $request->all();

        // Call function delete
        $status = $this->formRepository->delete($ids);

        if ($status) {
            $result = ['status' => $status, 'msg' => 'Xóa thành công'];
        } else {
            $result = ['status' => $status, 'msg' => 'Đã xảy ra lỗi, mời thử lại'];
        }

        // Return blogger 
        return new JsonResponse($result);
    }
}
