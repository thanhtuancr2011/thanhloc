<?php

namespace Core\Base\Http\Controllers\Api;


use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;

use Core\Base\Repositories\Contracts\LibraryRepositoryInterface;
use Core\Base\Repositories\Contracts\AssetRepositoryInterface;

class AssetController extends Controller
{
    protected $libraryRepository;
    protected $assetRepository;

    public function __construct(LibraryRepositoryInterface $libraryRepository, AssetRepositoryInterface $assetRepository)
    {
        $this->middleware('auth');
        $this->libraryRepository = $libraryRepository;
        $this->assetRepository   = $assetRepository;
    }

    /**
     * Set data validate
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Array $data The data input
     * @return Void       
     */
    public function dataValidate($data)
    {
        if (!isset($data['id'])) {
            $validateSlug = Rule::unique('assets')->where(function ($query) use ($data) {
                return $query->where('slug', $data['slug'])->where('parent_id', $data['parent_id']);
            });
        } else {
            $validateSlug = Rule::unique('assets')->ignore($data['id'], 'id')->where(function ($query) use ($data) {
                return $query->where('slug', $data['slug'])->where('parent_id', $data['parent_id']);
            });
        }

        $validate = [
            'rules' => [
                'name' => 'required',
                'slug' => $validateSlug
            ],
            'messages' => [
                'name.required' => 'Mời nhập tên thư mục',
                'slug.unique'   => 'Thư mục đã tồn tại'
            ]
        ];

        return $validate;
    }

    /**
     * Save role
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Request $request Data input
     * @return Void           
     */
    public function store(Request $request)
    {
        // All data input
        $data = $request->all();
        $data['slug'] = str_slug($data['name'], '.');

        // Call function set data validate and validate the data input
        $vali = $this->dataValidate($data);
        $data = $this->assetRepository->validate($data, $vali['rules'], $vali['messages']);

        // Validate fail
        if (isset($data['errors'])) {
            return new JsonResponse($data);
        }

        // Call function save asset
        $asset = $this->assetRepository->store($data);

        if ($asset) {
            $result = ['status' => 1, 'msg' => 'Thêm thành công', 'asset' => $asset];
        } else {
            $result = ['status' => 0, 'msg' => 'Đã xảy ra lỗi, mời thử lại'];
        }

        return new JsonResponse($result);
    }

    /**
     * Update asset
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Request $request Data input
     * @return Void           
     */
    public function update($id, Request $request)
    {
        // All data input
        $data = $request->all();
        $data['slug'] = str_slug($data['name'], '.');

        // Call function set data validate and validate the data input
        $vali = $this->dataValidate($data);
        $data = $this->assetRepository->validate($data, $vali['rules'], $vali['messages']);

        // Validate fail
        if (isset($data['errors'])) {
            return new JsonResponse($data);
        }

        // Call function save asset
        $asset = $this->assetRepository->update($id, $data);

        if ($asset) {
            $result = ['status' => 1, 'msg' => 'Cập nhật thành công', 'asset' => $asset];
        } else {
            $result = ['status' => 0, 'msg' => 'Đã xảy ra lỗi, mời thử lại'];
        }

        return new JsonResponse($result);
    }

    /**
     * Delete multiple roles
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Request $request The request data
     * @return Void           
     */
    public function deleteFolder($folderId)
    {
        // Call function save role
        $status = $this->assetRepository->delete($folderId);

        if ($status) {
            $result = ['status' => $status, 'msg' => 'Xóa thành công'];
        } else {
            $result = ['status' => $status, 'msg' => 'Đã xảy ra lỗi, mời thử lại'];
        }

        // Return blogger 
        return new JsonResponse($result);
    }
}
