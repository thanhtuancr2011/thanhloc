<?php

namespace Core\Base\Http\Controllers;

use Auth;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Core\Base\Facades\Base;
use Core\Base\Repositories\Contracts\FormRepositoryInterface;


class FormController extends Controller
{
    protected $formRepository;

    public function __construct(FormRepositoryInterface $formRepository)
    {
        $this->middleware('auth');
        $this->formRepository = $formRepository;
    }

    public function index()
    {                                                      
        return view('base::form.index');
    }
}
