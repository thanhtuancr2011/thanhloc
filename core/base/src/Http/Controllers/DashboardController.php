<?php

namespace Core\Base\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Core\Modules\User\Repositories\Contracts\UserRepositoryInterface;
use Core\Modules\Post\Repositories\Contracts\PostRepositoryInterface;
use Core\Modules\Product\Repositories\Contracts\OrderRepositoryInterface;
use Core\Modules\Product\Repositories\Contracts\CategoryRepositoryInterface;
use Core\Modules\Product\Repositories\Contracts\ProductRepositoryInterface;

class DashboardController extends Controller
{
    protected $userRepository;
    protected $postRepository;
	protected $orderRepository;
	protected $productRepository;
    protected $categoryRepository;

	public function __construct(ProductRepositoryInterface $productRepository, OrderRepositoryInterface $orderRepository, CategoryRepositoryInterface $categoryRepository, PostRepositoryInterface $postRepository, UserRepositoryInterface $userRepository)
    {
        $this->middleware('auth');
        $this->userRepository     = $userRepository;
        $this->postRepository     = $postRepository;
        $this->orderRepository    = $orderRepository;
        $this->productRepository  = $productRepository;
        $this->categoryRepository = $categoryRepository;
    }

    public function index()
    {
    	// Get lasted orders
    	$orders = $this->orderRepository->create()->orderBy('created_at', 'desc')->limit(5)->get();

    	// Get lasted products
    	$products = $this->productRepository->create()->orderBy('created_at', 'desc')->limit(5)->get();
    	foreach ($products as $key => &$product) {
    		$product->image_feature = json_decode($product->image_feature);
    		$product->url = $product->categories()->first()->url . '/' . $product->slug . '.html';
    	}

    	// Get total product
    	$totalProduct = $this->productRepository->create()->count();

    	// Get total post
    	$totalPost = $this->postRepository->create()->count();

    	// Get total order
    	$totalOrder = $this->orderRepository->create()->count();

    	// Get total customer
    	$totalCustomer = $this->userRepository->create()->where('type', 'customer')->count();

        return view('base::dashboard', compact('orders', 'products', 'totalProduct', 'totalPost', 'totalOrder', 'totalCustomer'));
    }
}
