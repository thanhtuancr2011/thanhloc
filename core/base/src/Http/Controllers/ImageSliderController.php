<?php

namespace Core\Base\Http\Controllers;

use Auth;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Core\Base\Facades\Base;
use Core\Base\Services\FileService;
use Core\Base\Repositories\Contracts\StatusRepositoryInterface;
use Core\Base\Repositories\Contracts\SettingRepositoryInterface;
use Core\Modules\Term\Repositories\Contracts\TermRepositoryInterface;
use Core\Modules\Post\Repositories\Contracts\PostRepositoryInterface;
use Core\Modules\Post\Repositories\Contracts\PostMetaRepositoryInterface;

class ImageSliderController extends Controller
{
    protected $postRepository;
    protected $termRepository;
    protected $statusRepository;

    public function __construct(PostRepositoryInterface $postRepository, TermRepositoryInterface $termRepository, StatusRepositoryInterface $statusRepository, PostMetaRepositoryInterface $postmetaRepository, SettingRepositoryInterface $settingRepository)
    {
        $this->middleware('auth');
        $this->postRepository   = $postRepository;
        $this->termRepository   = $termRepository;
        $this->statusRepository = $statusRepository;
        $this->settingRepository  = $settingRepository;
        $this->postmetaRepository = $postmetaRepository;
    }

    public function index()
    {
        // Type post
        $type = 'image_slider';

        // get status of user
        $status = $this->statusRepository->all();

        // Get all categories
        $categories = $this->termRepository->getAllCategories();

        return view('base::slide.index', compact('type', 'status', 'categories'));
    }

    public function create()
    {
        // Type post
        $type = 'image_slider';

        // get status of user
        $status = Base::getStatus();

        // Get model
        $item = $this->postRepository->create();
        $item->categories;

        // Get setting
        $settingItem = $this->settingRepository->getItem();

        $maxSizeUpload    = intval(ini_get("upload_max_filesize")); // Max file size
        $mimeContentTypes = FileService::listMimeTypes(); // List mime type

        // Get list meta fields
        $customFields = imageSlideTypes();

        return view('base::slide.create', compact('item', 'type', 'mimeContentTypes', 'maxSizeUpload', 'status', 'settingItem', 'customFields'));
    }

    function isJson($string) {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }

    public function edit($id)
    {
        // Type post
        $type = 'image_slider';

        // Get model
        $item = $this->postRepository->edit($id);

        // get status of user
        $status = Base::getStatus();

        // Get settingItem
        $settingItem = $this->settingRepository->getItem();

        $item->tags;       // Post tags
        $item->categories; // Post categories
        $item->image_feature = json_decode($item->image_feature, TRUE); // Post image feature

        // List postmeta
        $postMeta = $item->postMetas->pluck('meta_value', 'meta_key')->toArray();

        // Get list meta fields
        $customFields = imageSlideTypes();
        $customFields['basic']['type']['value'] = $postMeta['basic_type'];
        $customFields['slide']['images'] = json_decode($postMeta['slide_images'], TRUE);

        $maxSizeUpload    = intval(ini_get("upload_max_filesize")); // Max file size
        $mimeContentTypes = FileService::listMimeTypes(); // List mime type

        return view('base::slide.create', compact('item', 'type', 'mimeContentTypes', 'maxSizeUpload', 'status', 'customFields', 'settingItem'));
    }

    public function detail()
    {
        return view('base::slide.edit');
    }

    public function library()
    {
        // List mime type
        $mimeContentTypes = FileService::listMimeTypes();

        // Get list folder
        $folders = $this->assetRepository->getFoldersTree();

        return view('post::library', compact('mimeContentTypes', 'folders'));
    }
}
