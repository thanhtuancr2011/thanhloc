<?php

namespace Core\Base\Http\Controllers;

use Auth;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Core\Base\Services\FileService;
use Core\Base\Repositories\Contracts\LibraryRepositoryInterface;
use Core\Base\Repositories\Contracts\AssetRepositoryInterface;


class AssetController extends Controller
{
    protected $libraryRepository;
    protected $assetRepository;

    public function __construct(LibraryRepositoryInterface $libraryRepository, AssetRepositoryInterface $assetRepository)
    {
        $this->middleware('auth');
        $this->assetRepository   = $assetRepository;
        $this->libraryRepository = $libraryRepository;
    }

    public function index()
    {
    }

    public function create()
    {   
        // Get model 
        $item = $this->assetRepository->create();

        return view('base::library.folder', compact('item'));
    }

    public function edit($id)
    {
        // Get model 
        $item = $this->assetRepository->edit($id);
        return view('base::library.folder', compact('item'));
    }
}
