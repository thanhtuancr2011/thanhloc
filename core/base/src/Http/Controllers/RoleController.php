<?php

namespace Core\Base\Http\Controllers;

use Auth;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Core\Base\Facades\Base;
use Core\Base\Repositories\Contracts\RoleRepositoryInterface;
use Core\Base\Repositories\Contracts\StatusRepositoryInterface;
use Core\Base\Repositories\Contracts\PermissionRepositoryInterface;

class RoleController extends Controller
{
    protected $roleRepository;
    protected $statusRepository;
    protected $permissionRepository;

    public function __construct(RoleRepositoryInterface $roleRepository, PermissionRepositoryInterface $permissionRepository, StatusRepositoryInterface $statusRepository)
    {
        $this->middleware('auth');
        $this->roleRepository = $roleRepository;
        $this->statusRepository = $statusRepository;
        $this->permissionRepository = $permissionRepository;
    }

    public function index()
    {
        // Check permission
        Base::canOrRedirect('index.role');

        return view('base::role.index');
    }

    public function create()
    {
        // Check permission
        Base::canOrRedirect('add.role');

        // Get model role
        $item = $this->roleRepository->create();

        // Get all permission
        $permissions = $this->permissionRepository->all();

        // Get all status
        $status = $this->statusRepository->all();

        return view('base::role.create', compact('item', 'permissions', 'status'));
    }

    public function edit($id)
    {
        // Check permission
        Base::canOrRedirect('edit.role');

        // Get list role and list permission belong to role
        $item = $this->roleRepository->edit($id);

        // Get status
        $item->status;

        // Get permission belong to role
        $item->permissions;

        // Get all permissions
        $permissions = $this->permissionRepository->all();

        // Get all post status
        $status = $this->statusRepository->all();
        
        return view('base::role.create', compact('item', 'permissions', 'status'));
    }
}
