<?php

namespace Core\Base\Http\Controllers;

use Auth;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Core\Base\Facades\Base;
use Core\Base\Repositories\Contracts\MenuRepositoryInterface;
use Core\Base\Repositories\Contracts\MenuItemsRepositoryInterface;
use Core\Modules\Post\Repositories\Contracts\PostRepositoryInterface;
use Core\Modules\Term\Repositories\Contracts\TermRepositoryInterface;
use Core\Modules\Product\Repositories\Contracts\CategoryRepositoryInterface;


class MenuController extends Controller
{
    protected $menuRepository;
    protected $postRepository;
    protected $termRepository;
    protected $categoryRepository;
    protected $menuItemsRepository;

    public function __construct(MenuRepositoryInterface $menuRepository, PostRepositoryInterface $postRepository, TermRepositoryInterface $termRepository, MenuItemsRepositoryInterface $menuItemsRepository, categoryRepositoryInterface $categoryRepository)
    {
        $this->middleware('auth');
        $this->menuRepository = $menuRepository;
        $this->postRepository = $postRepository;
        $this->termRepository = $termRepository;
        $this->categoryRepository = $categoryRepository;
        $this->menuItemsRepository = $menuItemsRepository;
    }

    public function index()
    {
        // Check permission
        Base::canOrRedirect('index.menu');

        return view('base::menu.index');
    }

    public function create()
    {
        // Check permission
        Base::canOrRedirect('add.menu');

        $item = $this->menuRepository->create();

        return view('base::menu.create', compact('item'));
    }

    public function edit($id)
    {
        // Check permission
        Base::canOrRedirect('edit.menu');

        $item = $this->menuRepository->edit($id);

        return view('base::menu.create', compact('item'));
    }

    public function show($id)
    {
        // Check permission
        Base::canOrRedirect('edit.menu');

        // Get menu
        $item = $this->menuRepository->edit($id);

        // Call function get tree menu items
        $menuItems = $this->menuRepository->getMenuItemsTree($id);

        // Get all page
        $pages = $this->postRepository->getPages();

        // Get all categories
        $terms = $this->termRepository->getAllCategoriesTree();
        array_pop($terms);

        // Get all categories product
        $categories = $this->categoryRepository->getAllCategoriesTree();
        array_pop($categories);
        
        return view('base::menu.menu-item', compact('item', 'pages', 'terms', 'categories', 'menuItems'));
    }

    public function editMenuItem($id)
    {
        // Check permission
        Base::canOrRedirect('edit.menu');

        $menuItem = $this->menuItemsRepository->edit($id);

        return view('base::menu.menu-item-edit', compact('menuItem'));
    }
}
