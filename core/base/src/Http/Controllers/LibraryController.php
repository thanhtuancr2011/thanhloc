<?php

namespace Core\Base\Http\Controllers;

use Auth;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Core\Base\Facades\Base;
use Core\Base\Services\FileService;
use Core\Base\Repositories\Contracts\LibraryRepositoryInterface;
use Core\Base\Repositories\Contracts\AssetRepositoryInterface;


class LibraryController extends Controller
{
    protected $libraryRepository;
    protected $assetRepository;

    public function __construct(LibraryRepositoryInterface $libraryRepository, AssetRepositoryInterface $assetRepository)
    {
        $this->middleware('auth');
        $this->libraryRepository = $libraryRepository;
        $this->assetRepository   = $assetRepository;
    }

    public function index()
    {
        // Check permission
        Base::canOrRedirect('index.library');

        $maxSizeUpload    = intval(ini_get("upload_max_filesize")); // Max file size
        $mimeContentTypes = FileService::listMimeTypes();   // List mime type

        // Type show
        $type = isset($_GET['type']) ? $_GET['type'] : 'list';

        // Get list folder
        $folders = $this->assetRepository->getFoldersTree();

        return view('base::library.index', compact('maxSizeUpload', 'mimeContentTypes', 'folders', 'type'));
    }

    public function create()
    {   
    }

    public function edit($id)
    {
        // Check permission
        Base::canOrRedirect('edit.library');

        // Get file
        $item = $this->libraryRepository->edit($id);

        // Get folder
        $folder = $item->folder;
        $folderPath = url('/storage') . '/' . $this->assetRepository->getFolderPath($folder->id);

        // List mime type
        $mimeContentTypes = FileService::listMimeTypes(); 

        return view('base::library.create', compact('item', 'mimeContentTypes', 'folderPath'));
    }
}
