<?php

namespace Core\Base\Http\Controllers;

use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Request;

use Core\Base\Services\FileService;
use Core\Base\Repositories\Contracts\SettingRepositoryInterface;

class SettingController extends Controller
{
    protected $settingRepository;

    public function __construct(SettingRepositoryInterface $settingRepository)
    {
        $this->middleware('auth');
        $this->settingRepository = $settingRepository;
    }

    public function index()
    {
        // Get item
        $item = $this->settingRepository->getItem();

        // If not isset site map file
        if (file_exists(public_path() . '/sitemaps.xml')) {
            $item['sitemap'] = true;
        } else {
            $item['sitemap'] = false;
        }

        // List mime type
        $maxSizeUpload    = intval(ini_get("upload_max_filesize")); // Max file size
        $mimeContentTypes = FileService::listMimeTypes(); // List mime type

        // Return 
        return view('base::setting.index', compact('item', 'mimeContentTypes', 'maxSizeUpload'));
    }

    public function getGoogle()
    {
        $request = Request::all();

        if (isset($request['type'])) {
            $type = $request['type'];
        } else {
            $type = 'analytics';
        }

        // Get item
        $item = $this->settingRepository->getItem();

        // If not isset site map file
        if (file_exists(public_path() . '/sitemaps.xml')) {
            $item['sitemap'] = true;
        } else {
            $item['sitemap'] = false;
        }

        // List mime type
        $maxSizeUpload    = intval(ini_get("upload_max_filesize")); // Max file size
        $mimeContentTypes = FileService::listMimeTypes(); // List mime type

        return view('base::setting.google', compact('item', 'type', 'maxSizeUpload', 'mimeContentTypes'));
    }

}
