<?php

namespace Core\Base\Providers;

use Collective\Html\FormFacade;
use Collective\Html\HtmlFacade;
use Collective\Html\HtmlServiceProvider;

use Intervention\Image\Facades\Image;
use Intervention\Image\ImageServiceProvider;

use Laravel\Socialite\Facades\Socialite;
use Laravel\Socialite\SocialiteServiceProvider;

use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\ExcelServiceProvider;

use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Session;
use Ultraware\Roles\RolesServiceProvider;
use Illuminate\Contracts\Debug\ExceptionHandler;
use Barryvdh\Debugbar\ServiceProvider as DebugbarServiceProvider;

use Core\Base\Exceptions\Handler;
use Core\Modules\User\Middleware\Status;
use Core\Base\Helpers\BaseServiceHelper;
use Core\Base\Facades\Base as BaseFacade;
use Barryvdh\Debugbar\Facade as DebugbarFacade;

use Core\Modules\User\Providers\UserServiceProvider;
use Core\Modules\Term\Providers\TermServiceProvider;
use Core\Modules\Post\Providers\PostServiceProvider;
use Core\Modules\Base\Site\Providers\SiteServiceProvider;
use Core\Modules\Base\Menu\Providers\MenuServiceProvider;
use Core\Modules\Product\Providers\ProductServiceProvider;
use Core\Modules\Base\Theme\Providers\ThemeServiceProvider;
use Core\Modules\Base\Setting\Providers\SettingServiceProvider;
use Core\Modules\Base\Theme\Providers\ThemeManagerServiceProvider;

use Core\Base\Repositories\Contracts\RoleRepositoryInterface;
use Core\Base\Repositories\Eloquents\EloquentRoleRepository;

use Core\Base\Repositories\Contracts\StatusRepositoryInterface;
use Core\Base\Repositories\Eloquents\EloquentStatusRepository;

use Core\Base\Repositories\Contracts\AssetRepositoryInterface;
use Core\Base\Repositories\Eloquents\EloquentAssetRepository;

use Core\Base\Repositories\Contracts\LibraryRepositoryInterface;
use Core\Base\Repositories\Eloquents\EloquentLibraryRepository;

use Core\Base\Repositories\Contracts\SettingRepositoryInterface;
use Core\Base\Repositories\Eloquents\EloquentSettingRepository;

use Core\Base\Repositories\Contracts\MenuRepositoryInterface;
use Core\Base\Repositories\Eloquents\EloquentMenuRepository;

use Core\Base\Repositories\Contracts\MenuItemsRepositoryInterface;
use Core\Base\Repositories\Eloquents\EloquentMenuItemsRepository;

use Core\Base\Repositories\Contracts\PermissionRepositoryInterface;
use Core\Base\Repositories\Eloquents\EloquentPermissionRepository;

use Core\Base\Repositories\Contracts\FormRepositoryInterface;
use Core\Base\Repositories\Eloquents\EloquentFormRepository;


class BaseServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        if (env('APP_ENV') == 'production') {
            \URL::forceScheme('https');
        }

        // Publishes file
        $this->publishes([
            __DIR__.'/../../public/' => base_path('public/'),
        ], 'all');

        // Load migrate
        $this->loadMigrationsFrom(__DIR__.'/../../database/migrations');

        // Load middle ware
        $this->app['router']->middlewareGroup('status', [Status::class]);

        // List providers
        $this->app->register(ImageServiceProvider::class);
        $this->app->register(HtmlServiceProvider::class);
        $this->app->register(ExcelServiceProvider::class);
        $this->app->register(DebugbarServiceProvider::class);

        $this->app->register(UserServiceProvider::class);
        $this->app->register(TermServiceProvider::class);
        $this->app->register(PostServiceProvider::class);
        $this->app->register(MenuServiceProvider::class);
        $this->app->register(ProductServiceProvider::class);
        $this->app->register(ThemeManagerServiceProvider::class);
        $this->app->register(ThemeServiceProvider::class);
        $this->app->register(SiteServiceProvider::class);
        $this->app->register(SettingServiceProvider::class);
        $this->app->register(SocialiteServiceProvider::class);

        // Hander error
        $this->app->singleton(ExceptionHandler::class, Handler::class);

        // List aliases
        $loader = AliasLoader::getInstance();
        $loader->alias('Image', Image::class);
        $loader->alias('Form', FormFacade::class);
        $loader->alias('Html', HtmlFacade::class);
        $loader->alias('Excel', Excel::class);
        $loader->alias('Base', BaseFacade::class);
        $loader->alias('Socialite', Socialite::class);
        $loader->alias('Debugbar', DebugbarFacade::class);

        // Customize config
        config(
            [
                // Contain avatar
                'filesystems.disks.avatar' => [
                    'driver' => 'local',
                    'root' => public_path() . '/admin/avatars',
                ],

                // Contain avatar
                'filesystems.disks.public_local' => [
                    'driver' => 'local',
                    'root' => public_path() . '/',
                ],

                // Contain media files
                'filesystems.disks.storage' => [
                    'driver' => 'local',
                    'root' => storage_path() . '/app/public',
                ],

                // Contain media files
                'filesystems.disks.upload' => [
                    'driver' => 'local',
                    'root' => public_path() . '/upload',
                ],

                // Set role and permission model
                'roles.separator' => '.',
                'roles.models' => [
                    'role' => 'Core\Base\Models\RoleModel',
                    'permission' => 'Core\Base\Models\PermissionModel',
                ],

                'roles.role_user_table' => 'role_user',
                'roles.permission_user_table' => 'permission_user',
                'roles.permission_role_table' => 'permission_role',
                'roles.type_permission_table' => 'type_permission',
            ]
        );

        // Load view
        $this->loadViewsFrom(__DIR__.'/../../views/', 'base');

        // Load route
        $this->loadRoutesFrom(__DIR__.'/../../routes/web.php');

        // Init namspace translate for dizi core
        $this->loadTranslationsFrom(__DIR__ . '/../../views/lang/', 'base-lang');

        $this->app->bind(
            MenuRepositoryInterface::class,
            EloquentMenuRepository::class
        );

        $this->app->bind(
            MenuItemsRepositoryInterface::class,
            EloquentMenuItemsRepository::class
        );

        $this->app->bind(
            SettingRepositoryInterface::class,
            EloquentSettingRepository::class
        );

        $this->app->bind(
            AssetRepositoryInterface::class,
            EloquentAssetRepository::class
        );

        $this->app->bind(
            LibraryRepositoryInterface::class,
            EloquentLibraryRepository::class
        );

        $this->app->bind(
            RoleRepositoryInterface::class,
            EloquentRoleRepository::class
        );

        $this->app->bind(
            PermissionRepositoryInterface::class,
            EloquentPermissionRepository::class
        );

        $this->app->bind(
            StatusRepositoryInterface::class,
            EloquentStatusRepository::class
        );

        $this->app->bind(
            FormRepositoryInterface::class,
            EloquentFormRepository::class
        );
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        // Register helper
        foreach (glob(__DIR__ . '/../Helpers/*.php') as $filename) {
            require_once $filename;
        }

        // Trait
        $this->app->bind('base', function () {
            return new BaseServiceHelper();
        });

        // Register command
        $this->commands(['Core\Base\Commands\InstallCommand']);
    }

}
