<?php

use App\Http\Requests;
use Illuminate\Http\Request;

Route::group(['middleware' => ['web', 'status']], function () {

    /* Admin route */
    Route::group(['prefix' => 'admin'], function () {

        Route::get('/', 'Core\Base\Http\Controllers\DashboardController@index');

        // Asset route
        Route::resource('asset', 'Core\Base\Http\Controllers\AssetController');

        // Library route
        Route::resource('library', 'Core\Base\Http\Controllers\LibraryController');

        // Setting route
        Route::get('setting/google', 'Core\Base\Http\Controllers\SettingController@getGoogle');
        Route::get('setting', 'Core\Base\Http\Controllers\SettingController@index');

        // Menu route
        Route::get('menu/edit-menu-item/{id}', 'Core\Base\Http\Controllers\MenuController@editMenuItem');
        Route::resource('menu', 'Core\Base\Http\Controllers\MenuController');

        // Route image slider
        Route::get('image-slider/detail', 'Core\Base\Http\Controllers\ImageSliderController@detail');
        Route::resource('image-slider', 'Core\Base\Http\Controllers\ImageSliderController');

        // Role route
        Route::resource('role', 'Core\Base\Http\Controllers\RoleController');

        // Route form
        Route::resource('form', 'Core\Base\Http\Controllers\FormController');
    });

    /* Api route */
    Route::group(['prefix' => 'api'], function () {

        // Asset route
        Route::get('asset/{id}', 'Core\Base\Http\Controllers\Api\AssetController@deleteFolder');
        Route::resource('asset', 'Core\Base\Http\Controllers\Api\AssetController');

	    // Library route
        Route::post('library/libraries', 'Core\Base\Http\Controllers\Api\LibraryController@libraries');
        Route::post('library/crop', 'Core\Base\Http\Controllers\Api\LibraryController@cropImage');
        Route::post('library/delete', 'Core\Base\Http\Controllers\Api\LibraryController@delete');
        Route::resource('library', 'Core\Base\Http\Controllers\Api\LibraryController');

        // Setting route
        Route::get('setting/sitemaps', 'Core\Base\Http\Controllers\Api\SettingController@createSitemap');
        Route::resource('setting', 'Core\Base\Http\Controllers\Api\SettingController');

        // Role route
        Route::post('role/roles', 'Core\Base\Http\Controllers\Api\RoleController@roles');
        Route::post('role/delete', 'Core\Base\Http\Controllers\Api\RoleController@delete');
        Route::resource('role', 'Core\Base\Http\Controllers\Api\RoleController');

        // Menu route
        Route::post('menu/menus', 'Core\Base\Http\Controllers\Api\MenuController@menus');
        Route::post('menu/delete', 'Core\Base\Http\Controllers\Api\MenuController@delete');
        Route::get('menu/menu-items/{id}', 'Core\Base\Http\Controllers\Api\MenuController@getMenuItems');
        Route::post('menu/add-menu-item/{id}', 'Core\Base\Http\Controllers\Api\MenuController@addMenuItems');
        Route::post('menu/edit-menu-item/{id}', 'Core\Base\Http\Controllers\Api\MenuController@editMenuItems');
        Route::post('menu/delete-menu-item/{id}', 'Core\Base\Http\Controllers\Api\MenuController@deleteMenuItem');
        Route::post('menu/change-menu-item-position/{id}', 'Core\Base\Http\Controllers\Api\MenuController@changeMenuItemPosition');
        Route::resource('menu', 'Core\Base\Http\Controllers\Api\MenuController');

        // Route form
        Route::post('form/delete', 'Core\Base\Http\Controllers\Api\FormController@delete');
        Route::post('form/contacts', 'Core\Base\Http\Controllers\Api\FormController@contacts');
        Route::resource('form', 'Core\Base\Http\Controllers\Api\FormController');
    });

});

