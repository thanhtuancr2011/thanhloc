/*****
 * CONFIGURATION
 */
// Active ajax page loader
$.ajaxLoad = true;

//required when $.ajaxLoad = true
$.defaultPage = 'main.html';
$.subPagesDirectory = 'views/';
$.page404 = 'views/pages/404.html';
$.mainContent = $('#ui-view');

//Main navigation
$.navigation = $('nav > ul.nav');

$.panelIconOpened = 'icon-arrow-up';
$.panelIconClosed = 'icon-arrow-down';

//Default colours
$.brandPrimary = '#20a8d8';
$.brandSuccess = '#4dbd74';
$.brandInfo = '#63c2de';
$.brandWarning = '#f8cb00';
$.brandDanger = '#f86c6b';

$.grayDark = '#2a2c36';
$.gray = '#55595c';
$.grayLight = '#818a91';
$.grayLighter = '#d1d4d7';
$.grayLightest = '#f8f9fa';

'use strict';

/****
 * AJAX LOAD
 * Load pages asynchronously in ajax mode
 */

if ($.ajaxLoad) {

    // var paceOptions = {
    //     elements: false,
    //     restartOnRequestAfter: false
    // };

    // var url = location.hash.replace(/^#/, '');

    // if (url != '') {
    //     setUpUrl(url);
    // } else {
    //     setUpUrl($.defaultPage);
    // }

    // $(document).on('click', '.nav a[href!="#"]', function(e) {
    //     if ($(this).parent().parent().hasClass('nav-tabs') || $(this).parent().parent().hasClass('nav-pills')) {
    //         e.preventDefault();
    //     } else if ($(this).attr('target') == '_top') {
    //         e.preventDefault();
    //         var target = $(e.currentTarget);
    //         window.location = (target.attr('href'));
    //     } else if ($(this).attr('target') == '_blank') {
    //         e.preventDefault();
    //         var target = $(e.currentTarget);
    //         window.open(target.attr('href'));
    //     } else {
    //         e.preventDefault();
    //         var target = $(e.currentTarget);
    //         setUpUrl(target.attr('href'));
    //     }
    // });

    // $(document).on('click', 'a[href="#"]', function(e) {
    //     e.preventDefault();
    // });
}

/****
 * MAIN NAVIGATION
 */

$(document).ready(function ($) {

    // Add class .active to current link - AJAX Mode off
    $.navigation.find('a').each(function () {
        var cUrl = String(window.location).split('?')[0];
        if (cUrl.substr(cUrl.length - 1) == '#') {
            cUrl = cUrl.slice(0, -1);
        }
        var cUrl_full = String(window.location);
        if ($($(this))[0].href == cUrl || $($(this))[0].href == cUrl_full) {
            $(this).addClass('active');

            $(this).parents('ul').add(this).each(function () {
                $(this).parent().addClass('open');
            });
        }
    });

    // Dropdown Menu
    $.navigation.on('click', 'a', function (e) {

        // If click link
        if ($(this).attr('href') !== 'javascript:void(0)') {
            window.location.href = $(this).attr('href');
        }

        if ($.ajaxLoad) {
            e.preventDefault();
        }

        if ($(this).hasClass('nav-dropdown-toggle')) {
            $(this).parent().toggleClass('open');
            resizeBroadcast();
        }
    });

    function resizeBroadcast() {

        var timesRun = 0;
        var interval = setInterval(function () {
            timesRun += 1;
            if (timesRun === 5) {
                clearInterval(interval);
            }
            window.dispatchEvent(new Event('resize'));
        }, 62.5);
    }

    /* ---------- Main Menu Open/Close, Min/Full ---------- */
    $('.sidebar-toggler').click(function () {
        $('body').toggleClass('sidebar-hidden');
        resizeBroadcast();
    });

    $('.sidebar-minimizer').click(function () {
        $('body').toggleClass('sidebar-minimized');
        resizeBroadcast();
    });

    $('.brand-minimizer').click(function () {
        $('body').toggleClass('brand-minimized');
    });

    $('.aside-menu-toggler').click(function () {
        $('body').toggleClass('aside-menu-hidden');
        resizeBroadcast();
    });

    $('.mobile-sidebar-toggler').click(function () {
        $('body').toggleClass('sidebar-mobile-show');
        resizeBroadcast();
    });

    $('.sidebar-close').click(function () {
        $('body').toggleClass('sidebar-opened').parent().toggleClass('sidebar-opened');
    });

    /* ---------- Disable moving to top ---------- */
    $('a[href="#"][data-top!=true]').click(function (e) {
        e.preventDefault();
    });

});