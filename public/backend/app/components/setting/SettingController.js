settingApp.controller('SettingController', ['$scope', '$uibModal', '$filter', 'SettingService', '$timeout', '$rootScope', function ($scope, $uibModal, $filter, SettingService, $timeout, $rootScope) {

	// When js didn't  loaded then hide table term
	$('.container-fluid').removeClass('hidden');
	HoldOn.open(optionsHoldOn);

	// Code mirror options
	$scope.codeMirrorOptions = {
		lineNumbers: true,
	    mode: "javascript",
	    styleActiveLine: true,
	    matchBrackets: true,
	    theme: 'xq-dark'
	}

	/**
	 * Set element code mirror
	 * return     {Void} 
	 */
	$scope.setElementCodeMirror = function() {
		if (angular.isUndefined($scope.editorGoogleAnalytics) && $scope.tabShow == 'analytics') {
			$scope.editorGoogleAnalytics = CodeMirror.fromTextArea(document.getElementById("google_analytics"), $scope.codeMirrorOptions);
		}

		if (angular.isUndefined($scope.editorGoogleTagsManagerHeader) && angular.isUndefined($scope.editorGoogleTagsManagerBody) && $scope.tabShow == 'tag_manager') {
			$scope.editorGoogleTagsManagerHeader = CodeMirror.fromTextArea(document.getElementById("google_tags_manager_header"), $scope.codeMirrorOptions);
			$scope.editorGoogleTagsManagerBody = CodeMirror.fromTextArea(document.getElementById("google_tags_manager_body"), $scope.codeMirrorOptions);
		}

		if (angular.isUndefined($scope.editorGoogleWebMasterTool) && $scope.tabShow == 'webmaster') {
			$scope.editorGoogleWebMasterTool = CodeMirror.fromTextArea(document.getElementById("google_webmaster_tool"), $scope.codeMirrorOptions);
		}

		if (angular.isUndefined($scope.editorGoogleRemarketing) && $scope.tabShow == 'remarketing') {
			$scope.editorGoogleRemarketing = CodeMirror.fromTextArea(document.getElementById("script_remarketing_google"), $scope.codeMirrorOptions);
		}

		if (angular.isUndefined($scope.editorFacebookFanpage) && $scope.tabShow == 'fanpage') {
			$scope.editorFacebookFanpage = CodeMirror.fromTextArea(document.getElementById("fanpage"), $scope.codeMirrorOptions);
		}

		if (angular.isUndefined($scope.editorFacebookRemarketing) && $scope.tabShow == 'remarketing_fb') {
			$scope.editorFacebookRemarketing = CodeMirror.fromTextArea(document.getElementById("script_remarketing_facebook"), $scope.codeMirrorOptions);
		}

		if (angular.isUndefined($scope.editorPhoneCta) && $scope.tabShow == 'cta') {
			$scope.editorPhoneCta = CodeMirror.fromTextArea(document.getElementById("phone_cta"), $scope.codeMirrorOptions);
		}

		if (angular.isUndefined($scope.editorLiveChat) && $scope.tabShow == 'livechat') {
			$scope.editorLiveChat = CodeMirror.fromTextArea(document.getElementById("livechat"), $scope.codeMirrorOptions);
		}
	}

	$timeout(function() {
		$scope.setElementCodeMirror();
		$('.select-2').select2({
            placeholder: 'Chọn',
            width: '100%',
            tokenSeparators: [',']
        });

        if (angular.isDefined($scope.item.dizi_homeproject_feature)) {
            $('#dizi_homeproject_feature').select2().val($scope.item.dizi_homeproject_feature).trigger("change");
        }
		HoldOn.close();
	});

	$scope.changeTab = function(tabName) {
		$scope.tabShow = tabName;
		$timeout(function() {
			$scope.setElementCodeMirror();
		})
	}

	/**
	 * Show libraries model
	 * @param  {String} type      Type view file
	 * @param  {String} _function Function name use
	 * @return {Void}           
	 */
	$scope.getModalLibraries = function(_function) {

		// When update
		template = '/admin/post/library?v=' + new Date().getTime();

		var modalInstance = $uibModal.open({
		    animation: true,
		    templateUrl: window.baseUrl + template,
		    controller: 'ModalLibrariesCtrl',
		    size: 'lg',
		    resolve: {
		    	_function: function() {
		    		return _function;
		    	},
		    	maxSizeUpload: function() {
		    		return $scope.maxSizeUpload;
		    	}
		    }
		});

		modalInstance.result.then(function (data) {
			$scope.item[_function] = data[0].url;
		}, function () {});
	};

	$scope.submit = function() {

		// Loading
		HoldOn.open(optionsHoldOn);

		if (angular.isDefined($scope.editorGoogleAnalytics)) {
			$scope.item.google_analytics = $scope.editorGoogleAnalytics.getValue();
		}

		if (angular.isDefined($scope.editorGoogleTagsManagerHeader)) {
			$scope.item.google_tags_manager_header = $scope.editorGoogleTagsManagerHeader.getValue();
		}

		if (angular.isDefined($scope.editorGoogleTagsManagerBody)) {
			$scope.item.google_tags_manager_body = $scope.editorGoogleTagsManagerBody.getValue();
		}

		if (angular.isDefined($scope.editorGoogleWebMasterTool)) {
			$scope.item.google_webmaster_tool = $scope.editorGoogleWebMasterTool.getValue();
		}

		if (angular.isDefined($scope.editorGoogleRemarketing)) {
			$scope.item.script_remarketing_google = $scope.editorGoogleRemarketing.getValue();
		}

		if (angular.isDefined($scope.editorFacebookFanpage)) {
			$scope.item.fanpage = $scope.editorFacebookFanpage.getValue();
		}

		if (angular.isDefined($scope.editorFacebookRemarketing)) {
			$scope.item.script_remarketing_facebook = $scope.editorFacebookRemarketing.getValue();
		}

		if (angular.isDefined($scope.editorPhoneCta)) {
			$scope.item.phone_cta = $scope.editorPhoneCta.getValue();
		}

		if (angular.isDefined($scope.editorLiveChat)) {
			$scope.item.livechat = $scope.editorLiveChat.getValue();
		}

		SettingService.createSettingProvider($scope.item).then(function (data) {

			// Close loading
			HoldOn.close();

			if (data.status == -1) {
				$scope.errors = data.errors;
			} else {
				Notify(data.msg);
			}
		})
	}

    $scope.createSitemap = function() {

        // Loading
        HoldOn.open(optionsHoldOn);


        SettingService.createSitemap().then(function (data) {

            // Close loading
            HoldOn.close();

            if (data.status == -1) {
                $scope.errors = data.errors;
            } else {
                Notify(data.msg);
				$scope.showSiteMap = true;
            }
        })
    }
}])
.controller('ModalLibrariesCtrl', ['$scope', '$uibModalInstance', '$timeout', 'LibraryService', '_function', 'maxSizeUpload', '$rootScope', function ($scope, $uibModalInstance, $timeout, LibraryService, 
	_function, maxSizeUpload, $rootScope) {

	// Loading
	HoldOn.open(optionsHoldOn);
	
	$timeout(function() {

		$scope.maxSizeUpload = maxSizeUpload;

		$('.modal-content').css('height', ($(window).height() - 40) + 'px');
		$('.left-detail').css('height', ($(window).height() - 115) + 'px');
		$('.right-detail').css('height', ($(window).height() - 115) + 'px');
		$('.modal.fade .modal-dialog').css({'width': '100%', 'height': '100%', 'top': 0, 'padding': '20px'});
		$('.modal-body .tab-pane').css({'padding': '0'});

		/**
		 * Load file when user scroll
		 * @return {Void}   
		 */
		document.getElementById("myDIV").onscroll = function(e) {scrollDiv(e)};
		function scrollDiv(e) {
			var element = event.target;
			if (element.scrollHeight - element.scrollTop === element.clientHeight
				&& $scope.pageOptions.currentPage < $scope.pageOptions.totalPages) {
				$scope.pageOptions.currentPage++;
		        $scope.getItems();
		    }
		}

		$("#tree").fancytree({
	        source: $scope.folders,
	        activate: function(event, data) {

	            // Set scope node active
	            $scope.activeNode = data.node;

	            // Get directory path
	            $scope.directoryPath = data.node.getParentList(includeRoot = false, includeSelf = true);

	            // Search data
	            $scope.searchOptions.asset_id = data.node.key;

	            // Active and expand node
	            data.node.setActive();
	            data.node.setExpanded();

	            // Reset list files
	            $scope.librariesGridView = [];

	            if (!$scope.$$phase) {
	                $scope.$apply();
	            }

	            $scope.getItems();

	            // Set folder upload file
                $rootScope.$broadcast('userChooseFolder', data.node.key);
	        },
	        extensions: ["glyph"],
	        glyph: {
	            // preset: "awesome4",
	            map: {
	                _addClass: "glyphicon",
	                checkbox: "fa-square-o",
	                checkboxSelected: "fa-check-square-o",
	                checkboxUnknown: "fa-square",
	                dragHelper: "glyphicon-chevron-right",
	                dropMarker: "fa-long-arrow-right",
	                error: "fa-warning",
	                expanderClosed: "glyphicon-chevron-right",
	                expanderLazy: "glyphicon-chevron-right",
	                expanderOpen: "glyphicon-chevron-down",
	                loading: "fa-spinner fa-pulse",
	                nodata: "fa-meh-o",
	                noExpander: "",
	                radio: "fa-circle-thin",
	                radioSelected: "fa-circle",
	                doc: "fa-file",
	                docOpen: "fa-file",
	                folder: "glyphicon-folder-close",
	                folderOpen: "glyphicon-folder-open"
	            }
	        },
	    });

	    var tree = $('#tree').fancytree("getTree");
	    $timeout(function() {
	        var node = tree.getNodeByKey(tree.getFirstChild().key.toString());
	        node.setActive();
	        node.setExpanded();
	    });
	});

	// List colected
	$scope.listIdsSelected = [];

	/**
	 * When user click on file
	 * @param  {Void} e Element
	 * @return {Void}   
	 */
	$scope.chooseFile = function(file, e) {

		$scope.fileShow = file;

		var active = false;

		// Active class check
		if ($('.want-active-' + file.id).hasClass('active'))
			active = true;

		// Remove all active
		$('.check-file-active').removeClass('active');
		
		if (active == true) return;

		$scope.currentSelected = [file];
		$('.want-active-' + file.id).addClass('active');
	}

	$scope.pageOptions = {};
	$scope.pageOptions.totalPages   = 0;
	$scope.pageOptions.totalItems   = 0;
	$scope.pageOptions.currentPage  = 1;
	$scope.pageOptions.itemsPerPage = 30;

	$scope.searchOptions = {};
	$scope.searchOptions.type = 'all';

	/**
	 * Get list items
	 * @author Thanh Tuan <thanhtuancr2011@gmail.com>
	 * @return {Void} 
	 */
	$scope.getItems = function(event = null) {

		LibraryService.getItems({
			pageOptions: $scope.pageOptions,
			searchOptions: $scope.searchOptions,
		}).then(function (data) {
			
			if (data.status == 1) {

				// Folder path
				$scope.folderPath = data.folderPath;

				if (angular.isUndefined($scope.librariesGridView)) {
					$scope.librariesGridView = [];
				}

				// Set page options
				$scope.pageOptions.totalPages = data.totalPages;
				$scope.pageOptions.totalItems = data.totalItems;

				$scope.librariesGridView = $scope.librariesGridView.concat(angular.copy(data.libraries));

				// Set height
				$timeout(function() {
					$scope.gridMediaHeight = $('.grid-view-media').width();
				});

				// Show loading
				HoldOn.close();
			}
		});
	}

	/**
     * Onload when file upload finished
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  {Object} event The event 
     * @param  {Object} data) The page options
     * @return {Void}       
     */
    $scope.$on('uploadFinish',function(event, data) {
    	console.log(data, 'data');
		$scope.librariesGridView = [data.file].concat($scope.librariesGridView);
    });

	// When post click add or edit post
	$scope.submit = function (validate) {
		if (angular.isDefined($scope.currentSelected) && $scope.currentSelected.length > 0) {
			angular.forEach($scope.currentSelected, function(value, key) {
				value.url = $scope.folderPath + value.file_name;
			});
			$timeout(function() {
				$uibModalInstance.close($scope.currentSelected);
			});
		}
	};

	/* When post click cancel then close modal popup */
	$scope.cancel = function () {
	    $uibModalInstance.dismiss('cancel');
	};
}]);