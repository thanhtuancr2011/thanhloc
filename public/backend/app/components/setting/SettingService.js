var settingApp = angular.module('appSetting', []);
settingApp.factory('SettingResource',['$resource', function ($resource){
    return $resource('/api/setting/:method/:id', {'method':'@method','id':'@id'}, {
        add: {method: 'post'},
        save:{method: 'post'},
        update:{method: 'put'}
    });
}])
.service('SettingService', ['SettingResource', '$q', function (SettingResource, $q) {
    var that = this;

    /**
     * Function create new setting
     * @author Thanh Tuan <tuannt@acro.vn>
     * @param  {Array} data The data input
     * @return {Void}      
     */
	this.createSettingProvider = function(data){

		var defer = $q.defer(); 
        var temp  = new SettingResource(data);

        temp.$save({}, function success(data) {
            // Resolve result 
            defer.resolve(data);
        }, function error(reponse) {
            // Resolve result 
        	defer.resolve(reponse.data);
        });
        return defer.promise;  
	};
	this.createSitemap = function(){
        var defer = $q.defer();
        var temp  = new SettingResource();

        temp.$get({method:'sitemaps'}, function success(data) {
            // Resolve result
            defer.resolve(data);
        }, function error(reponse) {
            // Resolve result
            defer.resolve(reponse.data);
        });
        return defer.promise;
    }

}]);
