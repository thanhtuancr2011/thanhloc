var orderApp = angular.module('orderApp', []);
orderApp.factory('OrderResource',['$resource', function ($resource){
    return $resource('/api/order/:method/:id', {'method':'@method','id':'@id'}, {
        add: {method: 'post'},
        save:{method: 'post'},
        update:{method: 'put'}
    });
}])
.service('OrderService', ['OrderResource', '$q', function (OrderResource, $q) {
    var that = this;

    /**
     * Get items
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @return {Void} 
     */
    this.getItems = function(data) {
        var defer = $q.defer(); 
        var temp  = new OrderResource(data);
        temp.$save({method: 'items'}, function success(data) {
            defer.resolve(data);
        }, function error(reponse) {
            defer.resolve(reponse.data);
        });
        return defer.promise;  
    }

    /**
     * The function edit order
     * @author Thanh Tuan <tuannt@acro.vn>
     * @param  {Array} data The data input 
     * @return {Void}      
     */
    this.editOrderProvider = function(data) {

        var defer = $q.defer(); 
        var temp  = new OrderResource(data);

        // Update order successfull 
        temp.$update({id: data[0]['order_id']}, function success(data) {
            defer.resolve(data);
        }, function error(reponse) {
            // If create order is error 
            defer.resolve(reponse.data);
        });
        
        return defer.promise;  
    };

    /**
     * The function edit order
     * @author Thanh Tuan <tuannt@acro.vn>
     * @param  {Array} data The data input 
     * @return {Void}      
     */
    this.exportExcel = function(data) {
        
        var defer = $q.defer(); 
        var temp  = new OrderResource(data);

        // Update order successfull 
        temp.$save({method: 'export'}, function success(data) {
            defer.resolve(data);
        }, function error(reponse) {
            // If create order is error 
            defer.resolve(reponse.data);
        });
        
        return defer.promise;  
    };

    /**
     * Delete the orders
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  {Integer} id The order ids
     * @return {Void}    
     */
    this.deleteOrders = function (ids) {

        var defer = $q.defer(); 
        var temp  = new OrderResource(ids);

        // Delete orders is successfull
        temp.$save({method: 'delete'}, function success(data) {
            defer.resolve(data);
        },
        
        // If delete order is error
        function error(reponse) {
            defer.resolve(reponse.data);
        });
        return defer.promise;  
    }

    /**
     * Change order status
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  {Integer} id The order ids
     * @return {Void}    
     */
    this.changeStatus = function (order) {

        var defer = $q.defer(); 
        var temp  = new OrderResource();

        // Delete categories is successfull
        temp.$save({method: 'change-status', id: order}, function success(data) {
            defer.resolve(data);
        },
        
        // If delete category is error
        function error(reponse) {
            defer.resolve(reponse.data);
        });
        return defer.promise;  
    }

}]);
