orderApp.controller('OrderController', ['$scope', '$uibModal', '$filter', 'OrderService', '$timeout', '$rootScope', function ($scope, $uibModal, $filter, OrderService, $timeout, $rootScope) {

	// When js didn't  loaded then hide table category
	$('.container-fluid').removeClass('hidden');
	HoldOn.open(optionsHoldOn);

	// setup variable for date picker
	$scope.format = 'dd-MM-yyyy';

	// Date picker popup
	$scope.dateOptions = {
	    formatYear: 'yy',
	    // minDate: new Date(),
	    startingDay: 1
  	};

	/* Open calendar when create page*/
	$scope.open = function($event, name) {
    	$event.preventDefault();
    	$event.stopPropagation();
    	$scope.opened = {};
    	$scope.opened[name] = true;
  	};

	/**
	 * Get list items
	 * @author Thanh Tuan <thanhtuancr2011@gmail.com>
	 * @return {Void} 
	 */
	$scope.getItems = function() {

		// Reset all checkbox
		$scope.toggleSelection('none');
		
		OrderService.getItems({
			pageOptions: $scope.pageOptions,
			searchOptions: $scope.searchOptions,
		}).then(function (data) {
			if (data.status == 1) {

				// Set orders value
				$scope.orders = angular.copy(data.orders);
				$scope.users = angular.copy(data.users);
				$scope.pageOptions.totalPages = data.totalPages;
				$scope.pageOptions.totalItems = data.totalItems;
				$rootScope.$broadcast('pageOptions', $scope.pageOptions);

				// Show loading
				HoldOn.close();
			}
		});
	}

	$timeout(function() {
		$scope.searchOptions.created_at_from = new Date();
		$scope.searchOptions.created_at_to = new Date();
		$scope.getItems();
	});

	$scope.exportExcel = function() {

		HoldOn.open(optionsHoldOn);

		OrderService.exportExcel({
			pageOptions: $scope.pageOptions,
			searchOptions: $scope.searchOptions,
		}).then(function (data) {
			if (data.status == 1) {
				window.location.href = '/admin/order/download';
			} else {
				Notify(data.msg);
			}
			HoldOn.close();
		});
	}

	/**
     * Onload when controller call to set page options
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  {Object} event The event 
     * @param  {Object} data) The page options
     * @return {Void}       
     */
    $scope.$on('setPageOptions',function(event, data) {
        HoldOn.open(optionsHoldOn);
        $scope.pageOptions = data;
        $scope.getItems();
    });

	/**
     * Search orders
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @return {Void}                 
	 */
    $scope.searchOrders = function(keyEvent) {
    	if (keyEvent == 'submit' || keyEvent == 'ng-change' || keyEvent.which === 13) {
    		HoldOn.open(optionsHoldOn);
	    	$scope.getItems();
    	}
    } 

	/**
	 * Delete the category
	 * @author Thanh Tuan <thanhtuancr2011@gmail.com>
	 * @param  {Integer} id   The category id
	 * @return {Void}     
	 */
	$scope.removeOrder = function(id){

		if (angular.isDefined(id)) {
			var orderIds = [id];
		} else {
			var orderIds = $scope.listIdsSelected;
		}
		
		var template = '/backend/app/components/order/views/delete.html?v=' + new Date().getTime();
		var modalInstance = $uibModal.open({
		    animation: true,
		    templateUrl: window.baseUrl + template,
		    controller: 'ModalDeleteOrderCtrl',
		    size: 'sm',
		    resolve: {
		    	orderIds: function(){
		            return orderIds;
		        }
		    }
		});

		modalInstance.result.then(function (data) {
			$scope.getItems();
		}, function () {});
	};

	/**
	 * Change status for order
	 * @author Thanh Tuan <thanhtuancr2011@gmail.com>
	 * @param  {Integer} status   The status
	 * @return {Void}     
	 */
	$scope.changeStatus = function(orderId){
		
		var template = '/backend/app/components/order/views/status.html?v=' + new Date().getTime();
		var modalInstance = $uibModal.open({
		    animation: true,
		    templateUrl: window.baseUrl + template,
		    controller: 'ModalChangeStatusOrderCtrl',
		    size: 'sm',
		    resolve: {
		    	orderId: function(){
		            return orderId;
		        }
		    }
		});

		modalInstance.result.then(function (data) {
			$scope.getItems();
		}, function () {});
	};

}]).controller('ModalDeleteOrderCtrl', ['$scope', '$uibModalInstance', 'orderIds', 'OrderService', function ($scope, $uibModalInstance, orderIds, OrderService) {
	
	// When category click update the category for category
	$scope.submit = function () {

		// Loading
		HoldOn.open(optionsHoldOn);

		OrderService.deleteOrders(orderIds).then(function (data) {
			
			// Close loading
			HoldOn.close();

			$uibModalInstance.close();
			Notify(data.msg);
		});
	};

	// When category click cancel then close modal ppopup
	$scope.cancel = function () {
	    $uibModalInstance.dismiss('cancel');
	};
}]).controller('OrderDetailController', ['$scope', 'OrderService', '$timeout', function ($scope, OrderService, $timeout) {

	// When js didn't  loaded then hide table category
	$('.container-fluid').removeClass('hidden');
	HoldOn.open(optionsHoldOn);

	$timeout(function() {
		HoldOn.close();
	});

	$scope.submit = function(validate) {

		// Validate
		$scope.submitted = true;
		if (!validate) return;

		OrderService.editOrderProvider($scope.orders).then(function (data) {
			
			// Close loading
			HoldOn.close();
			Notify(data.msg);
		});
	}

}]).controller('ModalChangeStatusOrderCtrl', ['$scope', '$uibModalInstance', 'orderId', 'OrderService', function ($scope, $uibModalInstance, orderId, OrderService) {
	
	// When category click update the post for category
	$scope.submit = function () {

		// Loading
		HoldOn.open(optionsHoldOn);

		OrderService.changeStatus(orderId).then(function (data) {
			HoldOn.close();
			$uibModalInstance.close();
			Notify(data.msg);
		});
	};

	// When category click cancel then close modal popup
	$scope.cancel = function () {
	    $uibModalInstance.dismiss('cancel');
	};
}]);