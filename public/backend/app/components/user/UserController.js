userApp.controller('UserController', ['$scope', '$uibModal', '$filter', 'UserService', '$timeout', '$rootScope', function ($scope, $uibModal, $filter, UserService, $timeout, $rootScope) {

	// When js didn't  loaded then hide table term
	$('.container-fluid').removeClass('hidden');
	HoldOn.open(optionsHoldOn);
	
	/**
	 * Get list items
	 * @author Thanh Tuan <thanhtuancr2011@gmail.com>
	 * @return {Void} 
	 */
	$scope.getItems = function() {

		// Reset all checkbox
		$scope.toggleSelection('none');

		UserService.getItems({
			pageOptions: $scope.pageOptions,
			searchOptions: $scope.searchOptions,
		}).then(function (data) {
			if (data.status == 1) {

				// Set terms value
				$scope.users   = angular.copy(data.users);
				$scope.pageOptions.totalPages = data.totalPages;
				$scope.pageOptions.totalItems = data.totalItems;
				$rootScope.$broadcast('pageOptions', $scope.pageOptions);

				// Show loading
				HoldOn.close();
			}
		});
	}

	$timeout(function() {
		// Call function get items
		$scope.searchOptions.type = $scope.type;
		$scope.getItems();
	});

	/**
     * Search users
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @return {Void}                 */
    $scope.searchUsers = function(keyEvent) {

    	if (keyEvent == 'submit' || keyEvent.which === 13) {
    		HoldOn.open(optionsHoldOn);
	    	$scope.getItems();
    	}
    } 
	
	$scope.getModalUser = function(id) {
		
		// When create
		var template = '/admin/user/create?type=' + $scope.type + '&v='+ new Date().getTime();

		// When update
		if(typeof id != 'undefined'){
			template = '/admin/user/'+ id + '/edit?type=' + $scope.type + '&v=' + new Date().getTime();
		}

		var modalInstance = $uibModal.open({
		    animation: true,
		    templateUrl: window.baseUrl + template,
		    controller: 'ModalCreateUserCtrl',
		    size: null,
		    resolve: {
		    }
		});

		/* After create or edit user then reset user and reload ng-table */
		modalInstance.result.then(function (data) {
			$scope.getItems();
		}, function () {});
	};

	/**
	 * Delete the user
	 * @author Thanh Tuan <thanhtuancr2011@gmail.com>
	 * @param  {Integer} id   The category id
	 * @param  {String}  size Type of modal
	 * @return {Void}     
	 */
	$scope.removeUser = function(id){

		if (angular.isDefined(id)) {
			var userIds = [id];
		} else {
			var userIds = $scope.listIdsSelected;
		}
		
		var template = '/backend/app/components/user/views/delete.html?v=' + new Date().getTime();
		var modalInstance = $uibModal.open({
		    animation: true,
		    templateUrl: window.baseUrl + template,
		    controller: 'ModalDeleteUserCtrl',
		    size: 'sm',
		    resolve: {
		    	userIds: function(){
		            return userIds;
		        }
		    }
		});

		modalInstance.result.then(function (data) {
			$scope.getItems();
		}, function () {});
	};

	/**
     * Onload when controller call to set page options
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  {Object} event The event 
     * @param  {Object} data) The page options
     * @return {Void}       
     */
    $scope.$on('setPageOptions',function(event, data) {
        HoldOn.open(optionsHoldOn);
        $scope.pageOptions = data;
        $scope.getItems();
    });

	/**
	 * Change status for user
	 * @author Thanh Tuan <thanhtuancr2011@gmail.com>
	 * @param  {Integer} status   The status
	 * @return {Void}     
	 */
	$scope.changeStatus = function(userId){
		
		var template = '/backend/app/components/user/views/status.html?v=' + new Date().getTime();
		var modalInstance = $uibModal.open({
		    animation: true,
		    templateUrl: window.baseUrl + template,
		    controller: 'ModalChangeStatusUserCtrl',
		    size: 'sm',
		    resolve: {
		    	userId: function(){
		            return userId;
		        }
		    }
		});

		modalInstance.result.then(function (data) {
			$scope.getItems();
		}, function () {});
	};

}])
.controller('ModalCreateUserCtrl', ['$scope', '$uibModalInstance', 'UserService', '$timeout', function ($scope, $uibModalInstance, UserService, $timeout) {
	
	// $timeout(function() {
	// 	if (angular.isDefined($scope.userItem.role_id)) {
	// 		$("#list-role").select2().val($scope.userItem.role_id).trigger("change")
	// 	} else {
	// 		$('#list-role').select2();
	// 	}
	// });

	// When user click add or edit user
	$scope.submit = function (validate) {

		// Validate
		$scope.submitted = true;
		if (!validate) return;

		// Loading
		HoldOn.open(optionsHoldOn);

		UserService.createUserProvider($scope.userItem).then(function (data) {

			// Close loading
			HoldOn.close();

			if (data.status == -1) {
				$scope.errors = data.errors;
			} else {
				$uibModalInstance.close();
				Notify(data.msg);
			}
		});
	};

	/* When user click cancel then close modal popup */
	$scope.cancel = function () {
	    $uibModalInstance.dismiss('cancel');
	};
}])
.controller('ModalDeleteUserCtrl', ['$scope', '$uibModalInstance', 'userIds', 'UserService', function ($scope, $uibModalInstance, userIds, UserService) {

	// When category click update the post for category
	$scope.submit = function () {

		// Loading
		HoldOn.open(optionsHoldOn);

		UserService.deleteUsers(userIds).then(function (data) {
			HoldOn.close();
			$uibModalInstance.close();
			Notify(data.msg);
		});
	};

	// When category click cancel then close modal popup
	$scope.cancel = function () {
	    $uibModalInstance.dismiss('cancel');
	};
}])
.controller('ModalChangeStatusUserCtrl', ['$scope', '$uibModalInstance', 'userId', 'UserService', function ($scope, $uibModalInstance, userId, UserService) {
	
	// When category click update the post for category
	$scope.submit = function () {

		// Loading
		HoldOn.open(optionsHoldOn);

		UserService.changeStatus(userId).then(function (data) {
			HoldOn.close();
			$uibModalInstance.close();
			Notify(data.msg);
		});
	};

	// When category click cancel then close modal popup
	$scope.cancel = function () {
	    $uibModalInstance.dismiss('cancel');
	};
}]);