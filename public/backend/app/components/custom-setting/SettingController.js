settingApp.controller('SettingController', ['$scope', '$uibModal', '$filter', 'SettingService', '$timeout', '$rootScope', function ($scope, $uibModal, $filter, SettingService, $timeout, $rootScope) {

    // When js didn't  loaded then hide table term
    $('.container-fluid').removeClass('hidden');
    HoldOn.open(optionsHoldOn);

    $scope.initSelect = function(fieldName) {
        $timeout(function() {
            let $select2 = $("select#" + fieldName).select2();

            // List keywords
            $select2.on("select2:select", function(e) { 
                var value = $(e.currentTarget).val();
                $timeout(function() {
                    if ($scope.fieldsHome[fieldName].value.indexOf(value[value.length - 1]) == -1) {
                        $scope.fieldsHome[fieldName].value.push(value[value.length -1]);
                    } else {
                        $scope.fieldsHome[fieldName].value.splice($scope.fieldsHome[fieldName].value.indexOf(value[value.length - 1]), 1);
                    }
                    $('#' + fieldName).val(null).trigger('change');
                })

            });
        })
    }

    $scope.removeItem = function(id, type) {
        var index = $scope.fieldsHome[type].value.findIndex(index => id == index);
        $scope.fieldsHome[type].value.splice(index, 1);
    }

    $timeout(function() {
        HoldOn.close();
    });

    /**
     * Show libraries model
     * @param  {String} type      Type view file
     * @param  {String} _function Function name use
     * @return {Void}
     */
    $scope.getModalLibraries = function(_function, key) {
        // When update
        template = '/admin/post/library?v=' + new Date().getTime();

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: window.baseUrl + template,
            controller: 'ModalLibrariesCtrl',
            size: 'lg',
            resolve: {
                _function: function() {
                    return _function;
                },
                maxSizeUpload: function() {
                    return $scope.maxSizeUpload;
                }
            }
        });

        modalInstance.result.then(function (data) {
            if (angular.isDefined(key)) {
                $scope.item[key] = data[0].url;
            }
        }, function () {});
    };

    $scope.submit = function(validate) {

        // Init item
        $scope.item = {};

        angular.forEach($scope.fieldsHome, function(field, key) {
            if (angular.isDefined($scope.fieldsHome[key].value)) {
                $scope.item[key] = $scope.fieldsHome[key].value;
            }
        });

        // Loading
        HoldOn.open(optionsHoldOn);

        SettingService.createSettingProvider($scope.item).then(function (data) {

            // Close loading
            HoldOn.close();

            if (data.status == -1) {
                $scope.errors = data.errors;
            } else {
                Notify(data.msg);
            }
        })
    }
}]).controller('ModalLibrariesCtrl', ['$scope', '$uibModalInstance', 'PostService', '$timeout', 'LibraryService', '_function', 'maxSizeUpload', function ($scope, $uibModalInstance, PostService, $timeout, LibraryService,
                                                                                                                                                           _function, maxSizeUpload) {
        // Loading
        HoldOn.open(optionsHoldOn);

        $timeout(function() {

            $scope.maxSizeUpload = maxSizeUpload;

            $('.modal-content').css('height', ($(window).height() - 40) + 'px');
            $('.left-detail').css('height', ($(window).height() - 115) + 'px');
            $('.right-detail').css('height', ($(window).height() - 115) + 'px');
            $('.modal.fade .modal-dialog').css({'width': '100%', 'height': '100%', 'top': 0, 'padding': '20px'});
            $('.modal-body .tab-pane').css({'padding': '0'});

            /**
             * Load file when user scroll
             * @return {Void}
             */
            document.getElementById("myDIV").onscroll = function(e) {scrollDiv(e)};
            function scrollDiv(e) {
                var element = event.target;
                if (element.scrollHeight - element.scrollTop === element.clientHeight
                    && $scope.pageOptions.currentPage < $scope.pageOptions.totalPages) {
                    $scope.pageOptions.currentPage++;
                    $scope.getItems();
                }
            }
        });

        // List colected
        $scope.listIdsSelected = [];

        /**
         * When user click on file
         * @param  {Void} e Element
         * @return {Void}
         */
        $scope.chooseFile = function(file, e) {

            $scope.fileShow = file;

            var active = false;

            // Active class check
            if ($('.want-active-' + file.id).hasClass('active'))
                active = true;

            // Remove all active
            $('.check-file-active').removeClass('active');

            if (active == true) return;

            $scope.currentSelected = [file];
            $('.want-active-' + file.id).addClass('active');
        }

        $scope.pageOptions = {};
        $scope.pageOptions.totalPages   = 0;
        $scope.pageOptions.totalItems   = 0;
        $scope.pageOptions.currentPage  = 1;
        $scope.pageOptions.itemsPerPage = 60;

        $scope.searchOptions = {};

        /**
         * Get list items
         * @author Thanh Tuan <thanhtuancr2011@gmail.com>
         * @return {Void}
         */
        $scope.getItems = function(event = null) {

            LibraryService.getItems({
                pageOptions: $scope.pageOptions,
                searchOptions: $scope.searchOptions,
            }).then(function (data) {
                if (data.status == 1) {

                    if (angular.isUndefined($scope.librariesGridView)) {
                        $scope.librariesGridView = [];
                    }

                    // Set page options
                    $scope.pageOptions.totalPages = data.totalPages;
                    $scope.pageOptions.totalItems = data.totalItems;

                    $scope.librariesGridView = $scope.librariesGridView.concat(angular.copy(data.libraries));

                    // Set height
                    $timeout(function() {
                        $scope.gridMediaHeight = $('.grid-view-media').width();
                    });

                    // Show loading
                    HoldOn.close();
                }
            });
        }

        /**
         * Onload when file upload finished
         * @author Thanh Tuan <thanhtuancr2011@gmail.com>
         * @param  {Object} event The event
         * @param  {Object} data) The page options
         * @return {Void}
         */
        $scope.$on('uploadFinish',function(event, data) {
            $scope.librariesGridView = [data.file].concat($scope.librariesGridView);
        });

        $timeout(function() {
            $scope.getItems();
        });

        // When post click add or edit post
        $scope.submit = function (validate) {
            if (angular.isDefined($scope.currentSelected) && $scope.currentSelected.length > 0) {
                $uibModalInstance.close($scope.currentSelected);
            }
        };

        // When post click cancel then close modal popup
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }]);