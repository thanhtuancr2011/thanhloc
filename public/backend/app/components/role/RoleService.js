var roleApp = angular.module('appRole', []);
roleApp.factory('RoleResource',['$resource', function ($resource){
    return $resource('/api/role/:method/:id', {'method':'@method','id':'@id'}, {
        add: {method: 'post'},
        save:{method: 'post'},
        update:{method: 'put'}
    });
}])
.service('RoleService', ['RoleResource', '$q', function (RoleResource, $q) {
    var that = this;

    /**
     * Get items
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @return {Void} 
     */
    this.getItems = function(data) {
        var defer = $q.defer(); 
        var temp  = new RoleResource(data);
        temp.$save({method: 'roles'}, function success(data) {
            defer.resolve(data);
        }, function error(reponse) {
            defer.resolve(reponse.data);
        });
        return defer.promise;  
    }

    /**
     * Function create new role
     * @author Thanh Tuan <tuannt@acro.vn>
     * @param  {Array} data The data input
     * @return {Void}      
     */
	this.createRoleProvider = function(data){

        // If isset id of role then call function edit role
        if(typeof data['id'] != 'undefined') {
            return that.editRoleProvider(data);
        }

		var defer = $q.defer(); 
        var temp  = new RoleResource(data);

        temp.$save({}, function success(data) {
            // Resolve result 
            defer.resolve(data);
        }, function error(reponse) {
            // Resolve result 
        	defer.resolve(reponse.data);
        });
        return defer.promise;  
	};

    /**
     * The function edit role
     * @author Thanh Tuan <tuannt@acro.vn>
     * @param  {Array} data The data input 
     * @return {Void}      
     */
    this.editRoleProvider = function(data){

        var defer = $q.defer(); 
        var temp  = new RoleResource(data);

        // Update role successfull 
        temp.$update({id: data['id']}, function success(data) {
            defer.resolve(data);
        }, function error(reponse) {
            // If create role is error 
            defer.resolve(reponse.data);
        });
        
        return defer.promise;  
    };

    /**
     * Delete the roles
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  {Integer} id The role ids
     * @return {Void}    
     */
    this.deleteRoles = function (roleIds) {

        var defer = $q.defer(); 
        var temp  = new RoleResource(roleIds);

        // Delete categories is successfull
        temp.$save({method: 'delete'}, function success(data) {
            defer.resolve(data);
        },
        
        // If delete category is error
        function error(reponse) {
            defer.resolve(reponse.data);
        });
        return defer.promise;  
    }

}]);
