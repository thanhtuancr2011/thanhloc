var postApp = angular.module('appPost', []);
postApp.factory('PostResource',['$resource', function ($resource){
    return $resource('/api/post/:method/:id', {'method':'@method','id':'@id'}, {
        add: {method: 'post'},
        save:{method: 'post'},
        update:{method: 'put'}
    });
}])
.service('PostService', ['PostResource', '$q', function (PostResource, $q) {
    var that = this;

    /**
     * Get items
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @return {Void} 
     */
    this.getItems = function(data) {
        var defer = $q.defer(); 
        var temp  = new PostResource(data);
        temp.$save({method: 'posts'}, function success(data) {
            defer.resolve(data);
        }, function error(reponse) {
            defer.resolve(reponse.data);
        });
        return defer.promise;  
    }

    /**
     * Function create new post
     * @author Thanh Tuan <tuannt@acro.vn>
     * @param  {Array} data The data input
     * @return {Void}      
     */
	this.createPostProvider = function(data){

        // If isset id of post then call function edit post
        if(typeof data['id'] != 'undefined') {
            return that.editPostProvider(data);
        }

		var defer = $q.defer(); 
        var temp  = new PostResource(data);

        temp.$save({}, function success(data) {
            // Resolve result 
            defer.resolve(data);
        }, function error(reponse) {
            // Resolve result 
        	defer.resolve(reponse.data);
        });
        return defer.promise;  
	};

    /**
     * The function edit post
     * @author Thanh Tuan <tuannt@acro.vn>
     * @param  {Array} data The data input 
     * @return {Void}      
     */
    this.editPostProvider = function(data){

        var defer = $q.defer(); 
        var temp  = new PostResource(data);

        // Update post successfull 
        temp.$update({id: data['id']}, function success(data) {
            defer.resolve(data);
        }, function error(reponse) {
            // If create post is error 
            defer.resolve(reponse.data);
        });
        
        return defer.promise;  
    };

    /**
     * Delete the posts
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  {Integer} id The post ids
     * @return {Void}    
     */
    this.deletePosts = function (ids) {

        var defer = $q.defer(); 
        var temp  = new PostResource(ids);

        // Delete posts is successfull
        temp.$save({method: 'delete'}, function success(data) {
            defer.resolve(data);
        },
        
        // If delete post is error
        function error(reponse) {
            defer.resolve(reponse.data);
        });
        return defer.promise;  
    }

    /**
     * Clone the post
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  {Integer} id The post id
     * @return {Void}    
     */
    this.clonePost = function (id) {

        var defer = $q.defer(); 
        var temp  = new PostResource();

        // Delete posts is successfull
        temp.$add({method: 'clone', id: id}, function success(data) {
            defer.resolve(data);
        },
        
        // If delete post is error
        function error(reponse) {
            defer.resolve(reponse.data);
        });
        return defer.promise;  
    }

}]);
