postApp.controller('PostController', ['$scope', '$uibModal', '$filter', 'PostService', '$timeout', '$rootScope', function ($scope, $uibModal, $filter, PostService, $timeout, $rootScope) {

	// When js didn't  loaded then hide table post
	$('.container-fluid').removeClass('hidden');
	HoldOn.open(optionsHoldOn);

	/**
	 * Get list items
	 * @author Thanh Tuan <thanhtuancr2011@gmail.com>
	 * @return {Void} 
	 */
	$scope.getItems = function() {

		// Reset all checkbox
		$scope.toggleSelection('none');

		PostService.getItems({
			pageOptions: $scope.pageOptions,
			searchOptions: $scope.searchOptions,
		}).then(function (data) {
			if (data.status == 1) {

				// Set posts value
				$scope.posts = angular.copy(data.posts);
				$scope.authors = angular.copy(data.authors);
				$scope.lstStatus = angular.copy(data.lstStatus);
				$scope.pageOptions.totalPages = data.totalPages;
				$scope.pageOptions.totalItems = data.totalItems;
				$rootScope.$broadcast('pageOptions', $scope.pageOptions);

				// Show loading
				HoldOn.close();
			}
		});
	}

	$timeout(function() {
		// Set type search
		$scope.searchOptions.type = $scope.type;
		$scope.getItems();
	});

	/**
     * Onload when controller call to set page options
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  {Object} event The event 
     * @param  {Object} data) The page options
     * @return {Void}       
     */
    $scope.$on('setPageOptions',function(event, data) {
        HoldOn.open(optionsHoldOn);
        $scope.pageOptions = data;
        $scope.getItems();
    });

	/**
     * Search posts
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @return {Void}                 
	 */
    $scope.searchPosts = function(keyEvent) {
    	if (keyEvent == 'submit' || keyEvent == 'ng-change' || keyEvent.which === 13) {
    		HoldOn.open(optionsHoldOn);
	    	$scope.getItems();
    	}
    } 

	/**
	 * Delete the post
	 * @author Thanh Tuan <thanhtuancr2011@gmail.com>
	 * @param  {Integer} id   The post id
	 * @return {Void}     
	 */
	$scope.removePost = function(id){

		if (angular.isDefined(id)) {
			var postIds = [id];
		} else {
			var postIds = $scope.listIdsSelected;
		}
		
		var template = '/backend/app/components/post/views/delete.html?v=' + new Date().getTime();
		var modalInstance = $uibModal.open({
		    animation: $scope.animationsEnabled,
		    templateUrl: window.baseUrl + template,
		    controller: 'ModalDeletePostCtrl',
		    size: 'sm',
		    resolve: {
		    	postIds: function(){
		            return postIds;
		        }
		    }
		});

		modalInstance.result.then(function (data) {
			$scope.getItems();
		}, function () {});
	};

	/**
	 * Clone the post
	 * @author Thanh Tuan <thanhtuancr2011@gmail.com>
	 * @param  {Integer} id   The post id
	 * @return {Void}     
	 */
	$scope.clonePost = function(id) {
		
		var template = '/backend/app/components/post/views/clone.html?v=' + new Date().getTime();
		var modalInstance = $uibModal.open({
		    animation: $scope.animationsEnabled,
		    templateUrl: window.baseUrl + template,
		    controller: 'ModalClonePostCtrl',
		    size: 'sm',
		    resolve: {
		    	postId: function() {
		            return id;
		        },
		        type: function() {
		        	return $scope.type;
		        }
		    }
		});

		modalInstance.result.then(function (data) {
			$scope.getItems();
		}, function () {});
	};

}]).controller('ModalDeletePostCtrl', ['$scope', '$uibModalInstance', 'postIds', 'PostService', function ($scope, $uibModalInstance, postIds, PostService) {
	
	// When post click update the post for post
	$scope.submit = function () {

		// Loading
		HoldOn.open(optionsHoldOn);

		PostService.deletePosts(postIds).then(function (data) {
			
			// Close loading
			HoldOn.close();

			$uibModalInstance.close();
			Notify(data.msg);
		});
	};

	// When post click cancel then close modal popup
	$scope.cancel = function () {
	    $uibModalInstance.dismiss('cancel');
	};
}]).controller('CreatePostController', ['$scope', 'PostService', '$uibModal', '$http', '$timeout', 'SettingService', function ($scope, PostService, $uibModal, $http, $timeout, SettingService) {
	
	// When js didn't  loaded then hide table post
	$('.container-fluid').removeClass('hidden');

	$timeout(function() {
		$( "#sortable" ).sortable({
            items: '.img-slide',
            cancel: '.add-slide',
            start: function(event, ui) {
		        ui.item.startPos = ui.item.index();
		    },
            stop: function(event, ui) {
            	$scope.customFields.slide.images = [];
            	$('#sortable .img-slide').each(function() {
				  	$scope.customFields.slide.images.push($(this).data('image'));
				});
		    }
        });
	})

	$scope.getModalSlide = function(index) {

		$scope.item = {};

		if (angular.isDefined($scope.customFields.slide.images[index])) {
			$scope.item = $scope.customFields.slide.images[index];
		} else {
			if ($scope.customFields.slide.images.length >= 15) {
				Notify('Bạn chỉ có thể chọn tối đa 15 ảnh.');
				return;
			}
		}

		var template = '/admin/image-slider/detail?v=' + new Date().getTime();
		var modalInstance = $uibModal.open({
		    animation: true,
		    templateUrl: window.baseUrl + template,
		    controller: 'ModalEditSlide',
		    size: null,
		    resolve: {
		    	image: function() {
		            return $scope.item;
		        }
		    }
		});

		modalInstance.result.then(function (data) {
			if (data.type == 'video') {
				var strTmp = data.link.split('=');
				if (angular.isDefined(strTmp[1]))
					data.url = 'https://i1.ytimg.com/vi/' + strTmp[1] + '/mqdefault.jpg';
			}

			if (angular.isDefined(index)) {
				$scope.customFields.slide.images[index] = data;
			} else {
				$scope.customFields.slide.images.push(data);
			}
		}, function () {});
	}

	$scope.cloneSlide = function(image) {
		$scope.customFields.slide.images.push(image);
	}

	$scope.removeSlide = function(index) {
		
		var template = '/backend/app/components/slide/views/delete.html?v=' + new Date().getTime();
		var modalInstance = $uibModal.open({
		    animation: true,
		    templateUrl: window.baseUrl + template,
		    controller: 'ModalDeleteSlideCtrl',
		    size: 'sm',
		    resolve: {
		    }
		});

		modalInstance.result.then(function (data) {
			$scope.customFields.slide.images.splice(index, 1);
		}, function () {});
	}

	/**
	 * Save or update the post
	 * @param  {Boolean} validate The validate value
	 * @return {Void}          
	 */
	$scope.submit = function (validate) {

		// Validate
		$scope.submitted = true;
		if (!validate) return;

		// Contain custom fields
		$scope.postItem.customFields = [];

		// Basic custom fields
		angular.forEach($scope.customFields.basic, function(value, key) {
			var metaField = {};
			metaField.meta_key = 'basic_' + key;
			metaField.meta_value = value.value;
			$scope.postItem.customFields.push(metaField);
		});
		
		// Image custom fields
		var metaField = {};
		metaField.meta_key = 'slide_images';
		metaField.meta_value = $scope.customFields.slide.images;
		$scope.postItem.customFields.push(metaField);
		
		// Loading
		HoldOn.open(optionsHoldOn);

		// Set post type
		$scope.postItem.post_type = $scope.type;

		// Set post slug
		if (angular.isDefined($scope.postItem.slug) && $scope.postItem.slug != '' && $scope.postItem.slug != null) {
			$scope.postItem.slug = slugValue($scope.postItem.slug, '-');
		} else {
			$scope.postItem.slug = slugValue($scope.postItem.title, '-');
		}

		// Categories
		$scope.postItem.categories = [];

		$timeout(function() {
			PostService.createPostProvider($scope.postItem).then(function (data) {

				// Close loading
				HoldOn.close();

				if (data.status == -1) {
					$scope.errors = data.errors;
				} else {
					Notify(data.msg);

					$timeout(function() {
						if (!angular.isDefined($scope.postItem.id)) {
							window.location.href = '/admin/image-slider/' + data.post.id + '/edit'; 
						} else {
							window.location.reload();
						}
					})
				}
			});
		});
	};

	/* When post click cancel then close modal popup */
	$scope.cancel = function () {
	    $uibModalInstance.dismiss('cancel');
	};
}])
.controller('ModalDeleteSlideCtrl', ['$scope', '$uibModalInstance', function ($scope, $uibModalInstance) {

	// When category click update the post for category
	$scope.submit = function () {
		$uibModalInstance.close();
	};

	// When category click cancel then close modal popup
	$scope.cancel = function () {
	    $uibModalInstance.dismiss('cancel');
	};
}])
.controller('ModalEditSlide', ['$scope', '$uibModalInstance', 'PostService', '$timeout', '$filter', 'image', '$uibModal', function ($scope, $uibModalInstance, PostService, $timeout, $filter, image, $uibModal) {

	$scope.item = image;

	$scope.resetModel = function() {
        $('.modal-content').css('height', 'auto');
        $('.modal.fade .modal-dialog').css({'width': '45%', 'height': '100%', 'top': 0, 'padding': '20px'});
        $('.modal-body .tab-pane').css({'padding': '0'});
    }

	// When menu click add or edit menu
	$scope.submit = function (validate) {

		// Validate
		$scope.submitted = true;
		if (!validate) return;

		$uibModalInstance.close($scope.item);
	};

	$scope.initTinyMCE = function(id) {
		tinyMCE.remove();
		$timeout(function() {
			tinyMCE.init({
				selector: '#' + id, 
				height: 200,
				entity_encoding : "raw",
				relative_urls: false,
				theme: 'modern',
				remove_script_host : false,
				plugins : "link image code table",
				theme_advanced_buttons3_add : "tablecontrols",
				table_styles : "Header 1=header1;Header 2=header2;Header 3=header3",
				table_cell_styles : "Header 1=header1;Header 2=header2;Header 3=header3;Table Cell=tableCel1",
				table_row_styles : "Header 1=header1;Header 2=header2;Header 3=header3;Table Row=tableRow1",
				table_cell_limit : 100,
				table_row_limit : 5,
				table_col_limit : 5,
				toolbar: 'styleselect | table | undo redo | bold italic | image imagetools | alignleft aligncenter alignright | link unlink | fontselect fontsizeselect | code',
				init_instance_callback: function (editor) {
			    	editor.on('click', function (e) {
			    		$scope.item.content = editor.getContent();
			    	});
			    	editor.on('keyup', function(e) {
				    	$scope.item.content = editor.getContent();
			        });
				    editor.on('SetContent', function (e) {
				    	$scope.item.content = editor.getContent();
				    });
		  		}
			});
		})
	}

	/**
	 * Inser media to editor
	 * author Thanh Tuan <thanhtuancr2011@gmail.com>
	 * @param {Array} data List media
	 */
	$scope.setMediaEditor = function(data) {
		// Each file
		angular.forEach(data, function(value, key) {

			// Get cursor position
			var ed = tinyMCE.activeEditor;
			var range = ed.selection.getRng();

			// Media file type
			var typeMedia = $scope.mimeContentTypes[value.file_name.split('.').pop()]['group'];

			if (typeMedia == 'Image') {
				var newNode = ed.getDoc().createElement("img"); 
				newNode.src = value.url;  						
				newNode.alt = value.title; 
				newNode.width  = 560;			
			} else if (typeMedia == 'Video' || typeMedia == 'Audio') {
				var newNode = ed.getDoc().createElement("iframe"); 
				newNode.src = value.url;  						
				newNode.alt = value.excerpt; 
				newNode.width  = 560; 
				newNode.height = 315; 
				newNode.frameborder = 0; 
				newNode.allowfullscreen = 'true'; 
			} else {
				// Set link
				var newNode = ed.getDoc().createElement("a");
				newNode.innerText = value.title;
				newNode.href = value.url;  						
				newNode.alt = value.title;  						
				newNode.target  = '_self';  
			}
			// Insert content
			range.insertNode(newNode);  
		});
	}

	/**
	 * Show libraries model
	 * @param  {String} type      Type view file
	 * @param  {String} _function Function name use
	 * @return {Void}           
	 */
	$scope.getModalLibraries = function(type, _function) {

		// When update
		template = '/admin/post/library?v=' + new Date().getTime();

		var modalInstance = $uibModal.open({
		    animation: true,
		    templateUrl: window.baseUrl + template,
		    controller: 'ModalLibrariesCtrl',
		    size: null,
		    resolve: {
		    	type: function() {
		    		return type;
		    	}, 
		    	_function: function() {
		    		return false;
		    	}, 
		    	maxSizeUpload: function() {
		    		return $scope.maxSizeUpload;
		    	}
		    }
		});

		modalInstance.result.then(function (data) {
			$scope.item.url = data[0].url;
			$timeout(function() {
				$scope.resetModel();
			})
		}, function () {
			$timeout(function() {
				$scope.resetModel();
			})
		});
	};

	// When menu click cancel then close modal popup 
	$scope.cancel = function () {
	    $uibModalInstance.dismiss('cancel');
	};
}]).controller('ModalLibrariesCtrl', ['$scope', '$uibModalInstance', '$timeout', 'LibraryService', 'type', '_function', 'maxSizeUpload', '$rootScope', function ($scope, $uibModalInstance, $timeout, LibraryService, type, 
	_function, maxSizeUpload, $rootScope) {

	// Loading
	HoldOn.open(optionsHoldOn);
	
	$timeout(function() {

		$scope.maxSizeUpload = maxSizeUpload;

		$('.modal-content').css('height', ($(window).height() - 40) + 'px');
		$('.left-detail').css('height', ($(window).height() - 115) + 'px');
		$('.right-detail').css('height', ($(window).height() - 115) + 'px');
		$('.modal.fade .modal-dialog').css({'width': '100%', 'height': '100%', 'top': 0, 'padding': '20px'});
		$('.modal-body .tab-pane').css({'padding': '0'});

		/**
		 * Load file when user scroll
		 * @return {Void}   
		 */
		document.getElementById("myDIV").onscroll = function(e) {scrollDiv(e)};
		function scrollDiv(e) {
			var element = event.target;
			if (element.scrollHeight - element.scrollTop === element.clientHeight
				&& $scope.pageOptions.currentPage < $scope.pageOptions.totalPages) {
				$scope.pageOptions.currentPage++;
		        $scope.getItems();
		    }
		}

		$("#tree").fancytree({
	        source: $scope.folders,
	        activate: function(event, data) {

	            // Set scope node active
	            $scope.activeNode = data.node;

	            // Get directory path
	            $scope.directoryPath = data.node.getParentList(includeRoot = false, includeSelf = true);

	            // Search data
	            $scope.searchOptions.asset_id = data.node.key;

	            // Active and expand node
	            data.node.setActive();
	            data.node.setExpanded();

	            // Reset list files
	            $scope.librariesGridView = [];

	            if (!$scope.$$phase) {
	                $scope.$apply();
	            }

	            $scope.getItems();

	            // Set folder upload file
                $rootScope.$broadcast('userChooseFolder', data.node.key);
	        },
	        extensions: ["glyph"],
	        glyph: {
	            // preset: "awesome4",
	            map: {
	                _addClass: "glyphicon",
	                checkbox: "fa-square-o",
	                checkboxSelected: "fa-check-square-o",
	                checkboxUnknown: "fa-square",
	                dragHelper: "glyphicon-chevron-right",
	                dropMarker: "fa-long-arrow-right",
	                error: "fa-warning",
	                expanderClosed: "glyphicon-chevron-right",
	                expanderLazy: "glyphicon-chevron-right",
	                expanderOpen: "glyphicon-chevron-down",
	                loading: "fa-spinner fa-pulse",
	                nodata: "fa-meh-o",
	                noExpander: "",
	                radio: "fa-circle-thin",
	                radioSelected: "fa-circle",
	                doc: "fa-file",
	                docOpen: "fa-file",
	                folder: "glyphicon-folder-close",
	                folderOpen: "glyphicon-folder-open"
	            }
	        },
	    });

	    var tree = $('#tree').fancytree("getTree");
	    $timeout(function() {
	        var node = tree.getNodeByKey(tree.getFirstChild().key.toString());
	        node.setActive();
	        node.setExpanded();
	    });
	});

	// List colected
	$scope.listIdsSelected = [];

	/**
	 * When user click on file
	 * @param  {Void} e Element
	 * @return {Void}   
	 */
	$scope.chooseFile = function(file, e) {

		$scope.fileShow = file;

		// Set image feature
		if (_function == 'feature' || _function == 'facebook') {
			var active = false;

			// Active class check
			if ($('.want-active-' + file.id).hasClass('active')) {
				active = true;
			}

			// Remove all active
			$('.check-file-active').removeClass('active');
			
			if (active == true) return;

			$scope.currentSelected = [file];
			$('.want-active-' + file.id).addClass('active');

		} else {

			if ($scope.listIdsSelected.indexOf(file.id) == -1) {
				$scope.listIdsSelected.push(file.id);
				$('.want-active-' + file.id).addClass('active');
			} else {
				$scope.listIdsSelected.splice($scope.listIdsSelected.indexOf(file.id), 1);
				$('.want-active-' + file.id).removeClass('active');
			}

			$scope.currentSelected = angular.copy($scope.librariesGridView).filter(function(obj) {
			  	return ($scope.listIdsSelected.indexOf(obj.id) != -1);
			});
		}
	}

	$scope.pageOptions = {};
	$scope.pageOptions.totalPages   = 0;
	$scope.pageOptions.totalItems   = 0;
	$scope.pageOptions.currentPage  = 1;
	$scope.pageOptions.itemsPerPage = 30;

	$scope.searchOptions = {};
	$scope.searchOptions.type = type;

	/**
	 * Get list items
	 * @author Thanh Tuan <thanhtuancr2011@gmail.com>
	 * @return {Void} 
	 */
	$scope.getItems = function(event = null) {

		LibraryService.getItems({
			pageOptions: $scope.pageOptions,
			searchOptions: $scope.searchOptions,
		}).then(function (data) {
			
			if (data.status == 1) {
				
				// Folder path
				$scope.folderPath = data.folderPath;

				if (angular.isUndefined($scope.librariesGridView)) {
					$scope.librariesGridView = [];
				}

				$scope.folderPath = data.folderPath;

				// Set page options
				$scope.pageOptions.totalPages = data.totalPages;
				$scope.pageOptions.totalItems = data.totalItems;

				$scope.librariesGridView = $scope.librariesGridView.concat(angular.copy(data.libraries));

				// Set height
				$timeout(function() {
					$scope.gridMediaHeight = $('.grid-view-media').width();
				});

				// Show loading
				HoldOn.close();
			}
		});
	}

	/**
     * Onload when file upload finished
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  {Object} event The event 
     * @param  {Object} data) The page options
     * @return {Void}       
     */
    $scope.$on('uploadFinish',function(event, data) {
    	console.log(data, 'data');
		$scope.librariesGridView = [data.file].concat($scope.librariesGridView);
    });

	// When post click add or edit post
	$scope.submit = function (validate) {
		if (angular.isDefined($scope.currentSelected) && $scope.currentSelected.length > 0) {
			angular.forEach($scope.currentSelected, function(value, key) {
				value.url = $scope.folderPath + value.file_name;
			});
			$timeout(function() {
				$uibModalInstance.close($scope.currentSelected);
			});
		}
	};

	/* When post click cancel then close modal popup */
	$scope.cancel = function () {
	    $uibModalInstance.dismiss('cancel');
	};
}]).filter('spaceless',function() {
    return function(input) {
        if (input) {
            return input.replace(/\s+/g, '%');    
        }
    }
}).filter('listNameCategories',function() {
    return function(categories) {
    	var strCats = '';
    	angular.forEach(categories, function(value, key) {
    		if (strCats == '') 
    			strCats += value.name;
    		else
    			strCats += ', ' + value.name;
    	});

    	return strCats;
    }
});