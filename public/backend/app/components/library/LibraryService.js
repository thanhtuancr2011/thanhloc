var libraryApp = angular.module('appLibrary', []);
libraryApp.factory('LibraryResource',['$resource', function ($resource){
    return $resource('/api/library/:method/:id', {'method':'@method','id':'@id'}, {
        add: {method: 'post'},
        save:{method: 'post'},
        update:{method: 'put'}
    });
}])
.service('LibraryService', ['LibraryResource', '$q', function (LibraryResource, $q) {
    var that = this;

    /**
     * Get items
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @return {Void} 
     */
    this.getItems = function(data) {
        var defer = $q.defer(); 
        var temp  = new LibraryResource(data);
        temp.$save({method: 'libraries'}, function success(data) {
            defer.resolve(data);
        }, function error(reponse) {
            defer.resolve(reponse.data);
        });
        return defer.promise;  
    }

    /**
     * The function edit role
     * @author Thanh Tuan <tuannt@acro.vn>
     * @param  {Array} data The data input 
     * @return {Void}      
     */
    this.editLibraryProvider = function(data){

        var defer = $q.defer(); 
        var temp  = new LibraryResource(data);

        // Update role successfull 
        temp.$update({id: data['id']}, function success(data) {
            defer.resolve(data);
        }, function error(reponse) {
            // If create role is error 
            defer.resolve(reponse.data);
        });
        
        return defer.promise;  
    };

    /**
     * Delete the files
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  {Integer} id The role ids
     * @return {Void}    
     */
    this.deleteFiles = function (fileIds) {
        
        var defer = $q.defer(); 
        var temp  = new LibraryResource(fileIds);

        // Delete categories is successfull
        temp.$save({method: 'delete'}, function success(data) {
            defer.resolve(data);
        },
        
        // If delete category is error
        function error(reponse) {
            defer.resolve(reponse.data);
        });
        return defer.promise;  
    }

}]);
