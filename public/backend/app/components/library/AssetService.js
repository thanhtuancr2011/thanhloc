var assetApp = angular.module('appAsset', []);
assetApp.factory('AssetResource',['$resource', function ($resource){
    return $resource('/api/asset/:method/:id', {'method':'@method','id':'@id'}, {
        add: {method: 'post'},
        save:{method: 'post'},
        update:{method: 'put'}
    });
}])
.service('AssetService', ['AssetResource', '$q', function (AssetResource, $q) {
    var that = this;

    /**
     * Function create new folder
     * @author Thanh Tuan <tuannt@acro.vn>
     * @param  {Array} data The data input
     * @return {Void}      
     */
	this.createAssetProvider = function(data){

        // If isset id of folder then call function edit folder
        if(typeof data['id'] != 'undefined') {
            return that.editAssetProvider(data);
        }

		var defer = $q.defer(); 
        var temp  = new AssetResource(data);

        temp.$save({}, function success(data) {
            // Resolve result 
            defer.resolve(data);
        }, function error(reponse) {
            // Resolve result 
        	defer.resolve(reponse.data);
        });
        return defer.promise;  
	};

    /**
     * The function edit folder
     * @author Thanh Tuan <tuannt@acro.vn>
     * @param  {Array} data The data input 
     * @return {Void}      
     */
    this.editAssetProvider = function(data){

        var defer = $q.defer(); 
        var temp  = new AssetResource(data);

        // Update folder successfull 
        temp.$update({id: data['id']}, function success(data) {
            defer.resolve(data);
        }, function error(reponse) {
            // If create folder is error 
            defer.resolve(reponse.data);
        });
        
        return defer.promise;  
    };

    /**
     * The function edit folder
     * @author Thanh Tuan <tuannt@acro.vn>
     * @param  {Array} folderId The folder id input 
     * @return {Void}      
     */
    this.deleteFolder = function(folderId){

        var defer = $q.defer(); 
        var temp  = new AssetResource();

        // Update folder successfull 
        temp.$get({id: folderId}, function success(data) {
            defer.resolve(data);
        }, function error(reponse) {
            // If create folder is error 
            defer.resolve(reponse.data);
        });
        
        return defer.promise;  
    };

}]);
