libraryApp.controller('LibraryController', ['$scope', '$uibModal', '$filter', 'LibraryService', 'FileService', '$timeout', '$rootScope', function ($scope, $uibModal, $filter, LibraryService, FileService, $timeout, $rootScope) {

	// When js didn't  loaded then hide table term
	$('.container-fluid').removeClass('hidden');
	HoldOn.open(optionsHoldOn);

	// Set page options and search options of grid
	$scope.pageOptions = {};
	$scope.pageOptions.totalPages   = 0;
	$scope.pageOptions.totalItems   = 0;
	$scope.pageOptions.currentPage  = 1;
    $scope.pageOptions.itemsPerPage = 15;
	$scope.searchOptions = {};

    // Set page options and search options of grid
	$scope.pageGridOptions = {};
	$scope.pageGridOptions.totalPages   = 0;
	$scope.pageGridOptions.totalItems   = 0;
	$scope.pageGridOptions.currentPage  = 1;
    $scope.pageGridOptions.itemsPerPage = 18;
	$scope.searchGridOptions = {};

	/**
	 * Load file when user scroll
	 * author Thanh Tuan <thanhtuancr2011@gmail.com>
	 * @return {Void}   
	 */
	$(window).scroll(function() {
	   	if($(window).scrollTop() + $(window).height() == $(document).height() 
	   	   && $scope.pageGridOptions.currentPage < $scope.pageGridOptions.totalPages
	   	   && $scope.showStyle == 'grid') {
	   		$scope.pageGridOptions.currentPage++;
	   		$scope.getGridItems();
	   	}
	});

	/**
	 * Get list items
	 * @author Thanh Tuan <thanhtuancr2011@gmail.com>
	 * @return {Void} 
	 */
	$scope.getListItems = function() {

		// Reset all checkbox
		$scope.toggleSelection('none');

		LibraryService.getItems({
			pageOptions: $scope.pageOptions,
			searchOptions: $scope.searchOptions,
		}).then(function (data) {
			if (data.status == 1) {

				// Folder path
				$scope.folderPath = data.folderPath;

				// Set page options
				$scope.pageOptions.totalPages = data.totalPages;
				$scope.pageOptions.totalItems = data.totalItems;
				$rootScope.$broadcast('pageOptions', $scope.pageOptions);

				// Set libraries value
				$scope.authors = angular.copy(data.authors);
				$scope.libraries = angular.copy(data.libraries);

				// Show loading
				HoldOn.close();
			}
		});
	}

	/**
	 * Get grid items
	 * @author Thanh Tuan <thanhtuancr2011@gmail.com>
	 * @return {Void} 
	 */
	$scope.getGridItems = function(event = null) {

		LibraryService.getItems({
			pageOptions: $scope.pageGridOptions,
			searchOptions: $scope.searchGridOptions,
		}).then(function (data) {
			if (data.status == 1) {

				// Folder path
				$scope.folderPath = data.folderPath;

				// Set page options
				$scope.pageGridOptions.totalPages = data.totalPages;
				$scope.pageGridOptions.totalItems = data.totalItems;

				// Set default value
				if (angular.isUndefined($scope.librariesGridView)) {
					$scope.librariesGridView = [];
				}

				// Set libraries value
				$scope.librariesGridView = $scope.librariesGridView.concat(angular.copy(data.libraries));

				// Set height
				$timeout(function() {
					$scope.gridMediaHeight = $('.grid-view-media').width();
				});

				// Show loading
				HoldOn.close();
			}
		});
	}

	/**
	 * When user click on file
	 * @param  {Void} e Element
	 * @return {Void}   
	 */
	$scope.chooseFile = function(fileId, e) {

		if ($scope.multipleChooice) {

			// Active class check
			if ($('.want-active-' + fileId).hasClass('active')) {
				$('.want-active-' + fileId).removeClass('active');
			} else {
				$('.want-active-' + fileId).addClass('active');
			}

			// Check file id is exists in array
			if ($scope.choosenIds.indexOf(fileId) == -1) {
				$scope.choosenIds.push(fileId);
			} else {
				$scope.choosenIds.splice($scope.choosenIds.indexOf(fileId), 1);
			}

			if ($scope.choosenIds.length > 0) {
				$scope.showBtnDlt = true;
			}

		} else {
			$scope.editFile(fileId);
		}
	}

	/**
	 * Change style show media
	 * @param  {String} style The style show
	 * @return {Void}       
	 */
	$scope.changeStyle = function(style) {

		HoldOn.open(optionsHoldOn);
		$scope.showStyle = style;

		// Set page options and search options of grid
		$scope.pageOptions = {};
		$scope.pageOptions.totalPages   = 0;
		$scope.pageOptions.totalItems   = 0;
		$scope.pageOptions.currentPage  = 1;
	    $scope.pageOptions.itemsPerPage = 15;

	    // Set page options and search options of grid
		$scope.pageGridOptions = {};
		$scope.pageGridOptions.totalPages   = 0;
		$scope.pageGridOptions.totalItems   = 0;
		$scope.pageGridOptions.currentPage  = 1;
	    $scope.pageGridOptions.itemsPerPage = 18;

		// Reset all choose file
		$scope.choosenIds = [];
		$scope.showBtnDlt = false;
		$scope.librariesGridView = [];
		$scope.multipleChooice = false;
		$('.check-file-active').removeClass('active');

		switch(style) {
		    case 'list':
		        $scope.getListItems();
		        break;
		    case 'grid':
		        $scope.getGridItems();
		        break;
		}
	}

	$timeout(function() {
		$("#tree").fancytree({
	        source: $scope.folders,
	        activate: function(event, data) {

	            // Set scope node active
	            $scope.activeNode = data.node;

	            // Get directory path
	            $scope.directoryPath = data.node.getParentList(includeRoot = false, includeSelf = true);

	            // Search data
	            $scope.searchOptions.asset_id = data.node.key;
	            $scope.searchGridOptions.asset_id = data.node.key;

	            // Active and expand node
	            data.node.setActive();
	            data.node.setExpanded();

	            // Reset list files
	            $scope.librariesGridView = [];

	            if (!$scope.$$phase) {
	                $scope.$apply();
	            }

	            // Get files
	            if ($scope.showStyle == 'list') {
	                $scope.getListItems();
	            } else {
	            	$scope.getGridItems();
	            }

	            // Set folder upload file
                $rootScope.$broadcast('userChooseFolder', data.node.key);

	        },
	        extensions: ["glyph"],
	        glyph: {
	            // preset: "awesome4",
	            map: {
	                _addClass: "glyphicon",
	                checkbox: "fa-square-o",
	                checkboxSelected: "fa-check-square-o",
	                checkboxUnknown: "fa-square",
	                dragHelper: "glyphicon-chevron-right",
	                dropMarker: "fa-long-arrow-right",
	                error: "fa-warning",
	                expanderClosed: "glyphicon-chevron-right",
	                expanderLazy: "glyphicon-chevron-right",
	                expanderOpen: "glyphicon-chevron-down",
	                loading: "fa-spinner fa-pulse",
	                nodata: "fa-meh-o",
	                noExpander: "",
	                radio: "fa-circle-thin",
	                radioSelected: "fa-circle",
	                doc: "fa-file",
	                docOpen: "fa-file",
	                folder: "glyphicon-folder-close",
	                folderOpen: "glyphicon-folder-open"
	            }
	        },
	    });

	    var tree = $('#tree').fancytree("getTree");
	    $timeout(function() {
	        var node = tree.getNodeByKey(tree.getFirstChild().key.toString());
	        node.setActive();
	        node.setExpanded();
	    });
	});

	/**
	 * When user change style show
	 * @param  {String} newVal  Type style show
	 * @param  {String} oldVal  Type style show
	 * @return {Void}         
	 */
	$scope.$watch('multipleChooice', function(newVal, oldVal) {
		if (newVal == false) {
			$scope.choosenIds = [];
			$scope.showBtnDlt = false;
			$scope.multipleChooice = false;
			$('.check-file-active').removeClass('active');
		}
	});

	/**
     * Onload when controller call to set page options
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  {Object} event The event 
     * @param  {Object} data) The page options
     * @return {Void}       
     */
    $scope.$on('setPageOptions',function(event, data) {
        HoldOn.open(optionsHoldOn);
        $scope.pageOptions = data;
        $scope.getListItems();
    });

    /**
     * Onload when file upload finished
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  {Object} event The event 
     * @param  {Object} data) The page options
     * @return {Void}       
     */
    $scope.$on('allUploadFinish',function(event, data) {
    	if ($scope.showStyle == 'list') {
	     	HoldOn.open(optionsHoldOn);
	        $scope.getListItems();
    	}
    });

  
    /**
     * Onload when file upload finished
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  {Object} event The event 
     * @param  {Object} data) The page options
     * @return {Void}       
     */
    $scope.$on('uploadFinish',function(event, data) {
    	if ($scope.showStyle == 'grid') {
    		$scope.librariesGridView = [data.file].concat($scope.librariesGridView);
    	} 
    });

  
	/**
     * Search roles
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @return {Void}                 
	 */
    $scope.searchLibraries = function(keyEvent) {
    	if (keyEvent == 'submit' || keyEvent.which === 13) {
    		HoldOn.open(optionsHoldOn);
	    	$scope.getListItems();
    	}
    } 

    /**
	 * Edit file
	 * @author Thanh Tuan <thanhtuancr2011@gmail.com>
	 * @param  {Integer} id   The file id
	 * @return {Void}     
	 */
	$scope.editFile = function(id) {
		template = '/admin/library/'+ id + '/edit?v=' + new Date().getTime();
        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: window.baseUrl + template,
            controller: 'ModelEditFileCtrl',
            size: 'lg',
            resolve: {
            	fileId: function(){
                    return id;
                }
            }
        });

        /* After create or edit user then reset user and reload ng-table */
        modalInstance.result.then(function (data) {
        	angular.forEach($scope.libraries, function(key, value) {
        		if (id == value.id) {
        			value = data.library;
        		}
        	});
        }, function () {});
	};

	/**
	 * Create or edit new folder
	 * @author Thanh Tuan <thanhtuancr2011@gmail.com>
	 * @param  {Integer} type  Create or edit type
	 * @return {Void}     
	 */
	$scope.getModalFolder = function(type) {

		if (type == 'add') {
			var parentId = $scope.activeNode.key;
			var template = '/admin/asset/create?v=' + new Date().getTime();
		} else {
			var parentId = $scope.activeNode.data.parent_id;
			var template = '/admin/asset/' + $scope.activeNode.key + '/edit?v=' + new Date().getTime();
		}

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: window.baseUrl + template,
            controller: 'ModalCreateFolderCtrl',
            size: null,
            resolve: {
            	parentId: function() {
            		return parentId;
            	}
            }
        });

        /* After create or edit user then reset user and reload ng-table */
        modalInstance.result.then(function (data) {
        	var tree = $('#tree').fancytree("getTree");
        	if (type == 'add') {
        		// Set data for node added
        		data.asset.folder = true;
        		data.asset.key = data.asset.id;
        		data.asset.title = data.asset.name;

        		// Parent node 
        		parentNode = tree.getNodeByKey(data.asset.parent_id.toString());
        		parentNode.addNode(data.asset);
        		parentNode.setExpanded();

        		// Get node added
        		var node = tree.getNodeByKey(data.asset.key.toString());
    			node.setActive();
        	} else {
        		node = tree.getNodeByKey(data.asset.id.toString());
        		node.setTitle(data.asset.name);
        		node.data.name = data.asset.name;
        		console.log(node, 'node');
    		}
        }, function () {});
	};

	/**
	 * Delete folder
	 * @author Thanh Tuan <thanhtuancr2011@gmail.com>
	 * @return {Void}     
	 */
	$scope.removeFolder = function() {

		var template = '/backend/app/components/library/views/delete.html?v=' + new Date().getTime();
		var modalInstance = $uibModal.open({
		    animation: $scope.animationsEnabled,
		    templateUrl: window.baseUrl + template,
		    controller: 'ModalDeleteFolderCtrl',
		    size: 'sm',
		    resolve: {
		    	folderId: function(){
		            return $scope.activeNode.key;
		        }
		    }
		});

		modalInstance.result.then(function (data) {
			// Remove folder
			var tree = $('#tree').fancytree('getTree');
			tree.getNodeByKey($scope.activeNode.key.toString()).remove();
			tree.getNodeByKey($scope.activeNode.data.parent_id.toString()).setActive();
		}, function () {});
	};

    /**
	 * Delete file
	 * @author Thanh Tuan <thanhtuancr2011@gmail.com>
	 * @param  {Integer} id   The file id
	 * @return {Void}     
	 */
	$scope.removeFiles = function(id){

		if (angular.isDefined(id)) {
			var fileIds = [id];
		} else {
			var fileIds = $scope.listIdsSelected;
		}
		
		var template = '/backend/app/components/library/views/delete.html?v=' + new Date().getTime();
		var modalInstance = $uibModal.open({
		    animation: $scope.animationsEnabled,
		    templateUrl: window.baseUrl + template,
		    controller: 'ModalDeleteFileCtrl',
		    size: 'sm',
		    resolve: {
		    	fileIds: function(){
		            return fileIds;
		        }
		    }
		});

		modalInstance.result.then(function (data) {
			$scope.getListItems();
		}, function () {});
	};	
}]).controller('ModalCreateFolderCtrl', ['$scope', '$uibModalInstance', 'AssetService', 'parentId', function ($scope, $uibModalInstance, AssetService, parentId) {
	
	// When category click update the post for category
	$scope.submit = function () {

		// Loading
		HoldOn.open(optionsHoldOn);

		// Parent folder
		$scope.folderItem.parent_id = parentId;

		AssetService.createAssetProvider($scope.folderItem).then(function (data) {
			// Close loading
			HoldOn.close();

			if (data.status == -1) {
				$scope.errors = data.errors;
			} else {
				$uibModalInstance.close(data);
				Notify(data.msg);
			}
		});
	};

	// When category click cancel then close modal popup
	$scope.cancel = function () {
	    $uibModalInstance.dismiss('cancel');
	};
}]).controller('ModalDeleteFileCtrl', ['$scope', '$uibModalInstance', 'fileIds', 'LibraryService', function ($scope, $uibModalInstance, fileIds, LibraryService) {
	
	// When category click update the post for category
	$scope.submit = function () {

		// Loading
		HoldOn.open(optionsHoldOn);

		LibraryService.deleteFiles(fileIds).then(function (data) {
			
			// Close loading
			HoldOn.close();

			$uibModalInstance.close();
			Notify(data.msg);
		});
	};

	// When category click cancel then close modal popup
	$scope.cancel = function () {
	    $uibModalInstance.dismiss('cancel');
	};
}]).controller('ModalDeleteFolderCtrl', ['$scope', '$uibModalInstance', 'folderId', 'AssetService', function ($scope, $uibModalInstance, folderId, AssetService) {
	
	// When category click update the post for category
	$scope.submit = function () {

		// Loading
		HoldOn.open(optionsHoldOn);

		AssetService.deleteFolder(folderId).then(function (data) {
			
			// Close loading
			HoldOn.close();

			$uibModalInstance.close();
			Notify(data.msg);
		});
	};

	// When category click cancel then close modal popup
	$scope.cancel = function () {
	    $uibModalInstance.dismiss('cancel');
	};
}]).controller('ModelEditFileCtrl', ['$scope', '$uibModalInstance', 'fileId', 'LibraryService', '$timeout', function ($scope, $uibModalInstance, fileId, LibraryService, $timeout) {
    
	var windowWidth  = $(window).width();
	var windowHeight = $(window).height();

	$timeout(function() {
		$('.modal-dialog.modal-lg').css('top', 0);
		// $('.modal-dialog.modal-lg').css('width', windowWidth);
		// $('.modal-dialog.modal-lg').css('height', windowHeight);
		// $('.modal-dialog.modal-lg').css('padding', '50px');

		$('.left-detail').css('height', windowHeight - 185);
		// $('.left-detail').find('img').css('max-width', $('.left-detail').width());
		// $('.left-detail').find('img').css('max-height', $('.left-detail').height() - 200);

		$('.right-detail').css('height', windowHeight - 185);

		var cropMaxWidth = 400;
		var cropMaxHeight = 400;
		var canvas;
		var context;
		var jcropApi;

		$scope.prefSize;

		$timeout(function() {
			if ($scope.mimeContentTypes[$scope.fileItem.file_name.split('.').pop()]['group'] == 'Image') {
				var request = new XMLHttpRequest();
				request.open('GET', $scope.folderPath + $scope.fileItem.file_name, true);
				request.responseType = 'blob';
				request.onload = function() {
				    var reader = new FileReader();
				    reader.readAsDataURL(request.response);
				    reader.onload =  function(e){
				    	$scope.image = new Image();
				    	$scope.image.onload = validateImage;
		            	$scope.image.src = e.target.result;
		            	$timeout(function() {
							restartJcrop();
						})
				    };
				};
				request.send();
			}
		});

		function validateImage() {
		    if (canvas != null) {
		        $scope.image = new Image();
		        $scope.image.onload = restartJcrop;
		        $scope.image.src = canvas.toDataURL('image/png');
		    } else restartJcrop();
		}

		function dataURLtoBlob(dataURL) {
			var BASE64_MARKER = ";base64,";
		    if (dataURL.indexOf(BASE64_MARKER) == -1) {
		        var parts = dataURL.split(",");
		        var contentType = parts[0].split(":")[1];
		        var raw = decodeURIComponent(parts[1]);

		        return new Blob([raw], {type: contentType});
		    }

		    var parts = dataURL.split(BASE64_MARKER);
		    var contentType = parts[0].split(":")[1];
		    var raw = window.atob(parts[1]);
		    var rawLength = raw.length;

		    var uInt8Array = new Uint8Array(rawLength);

		    for (var i = 0; i < rawLength; ++i) {
		        uInt8Array[i] = raw.charCodeAt(i);
		    }

		    return new Blob([uInt8Array], {type: contentType});
		}

		function restartJcrop() {
		    if (jcropApi != null) {
		        jcropApi.destroy();
		    }
		    $("#views").empty();
		    $("#views").append("<canvas id=\"canvas\">");
		    canvas  = $("#canvas")[0];

		    context = canvas.getContext("2d");
		    canvas.width = $scope.image.width;
		    canvas.height = $scope.image.height;
		    context.drawImage($scope.image, 0, 0);

		    $("#canvas").Jcrop({
		        onSelect: selectCanvas,
		        onRelease: clearCanvas,
		        boxWidth: cropMaxWidth,
		        boxHeight: cropMaxHeight
		    }, function() {
		        jcropApi = this;
		    });
		    clearCanvas();
		}

		function clearCanvas() {
		    $scope.prefSize = {
		        x: 0,
		        y: 0,
		        w: canvas.width,
		        h: canvas.height,
		    };

		    if(!$scope.$$phase) {
			    $scope.$apply();
		    }
		}

		function selectCanvas(coords) {
		    $scope.prefSize = {
		        x: Math.round(coords.x),
		        y: Math.round(coords.y),
		        w: Math.round(coords.w),
		        h: Math.round(coords.h)
		    };

		    if(!$scope.$$phase) {
			    $scope.$apply();
		    }
		}

		$scope.applyCrop = function() { 
		    canvas.width = $scope.prefSize.w;
		    canvas.height = $scope.prefSize.h;
		    context.drawImage(
		    	$scope.image, 
		    	$scope.prefSize.x, 
		    	$scope.prefSize.y, $scope.prefSize.w, 
		    	$scope.prefSize.h, 
		    	0, 
		    	0, 
		    	canvas.width, 
		    	canvas.height
		    );
		    $scope.dsbSaveBtn = false;
		    validateImage();
		}

		// function applyScale(scale) {
		//     if (scale == 1) return;
		//     canvas.width = canvas.width * scale;
		//     canvas.height = canvas.height * scale;
		//     context.drawImage(image, 0, 0, canvas.width, canvas.height);
		//     validateImage();
		// }

		// function applyRotate() {
		//     canvas.width = image.height;
		//     canvas.height = image.width;
		//     context.clearRect(0, 0, canvas.width, canvas.height);
		//     context.translate(image.height / 2, image.width / 2);
		//     context.rotate(Math.PI / 2);
		//     context.drawImage(image, -image.width / 2, -image.height / 2);
		//     validateImage();
		// }

		// function applyHflip() {
		//     context.clearRect(0, 0, canvas.width, canvas.height);
		//     context.translate(image.width, 0);
		//     context.scale(-1, 1);
		//     context.drawImage(image, 0, 0);
		//     validateImage();
		// }

		// function applyVflip() {
		//     context.clearRect(0, 0, canvas.width, canvas.height);
		//     context.translate(0, image.height);
		//     context.scale(1, -1);
		//     context.drawImage(image, 0, 0);
		//     validateImage();
		// }

		/**
		 * Upload image cropped
		 * @param  {Object} e The object element
		 * @return {Void}           
		 */
		$scope.saveImage = function() {

		    // Add file blob to the form data
		    var blob = dataURLtoBlob(canvas.toDataURL('image/png'));
		    formData = new FormData($(this)[0]);	
		    formData.append("cropped_image", blob, $scope.fileItem.file_name);

		    // Loading
			HoldOn.open(optionsHoldOn);

		    $.ajax({
		        url: window.baseUrl + '/api/library/crop?fileId=' + $scope.fileItem.id + '&v=' + new Date().getTime(),
		        type: "POST",
		        data: formData,
		        contentType: false,
		        cache: false,
		        processData: false,
		        success: function(data) {

		        	// Close loading
					HoldOn.close();

		        	if (data.status == 1) {
		        		$scope.fileName = data.file;
		        		$uibModalInstance.close();
						Notify(data.msg);
		        	}
		        },
		        error: function(data) {
		            // Close loading
					HoldOn.close();
					Notify(data.responseJSON.msg);
		        },
		        complete: function(data) {}
		    });
		}
	});

    $scope.submit = function (validate) {

        // Validate
		$scope.submitted = true;
		if (!validate) return;

		// Loading
		HoldOn.open(optionsHoldOn);

		LibraryService.editLibraryProvider($scope.fileItem).then(function (data) {

			// Close loading
			HoldOn.close();

			if (data.status == -1) {
				$scope.errors = data.errors;
			} else {
				$uibModalInstance.close(data);
				Notify(data.msg);
			}
		})
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);