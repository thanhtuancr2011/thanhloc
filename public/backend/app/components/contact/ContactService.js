var contactApp = angular.module('appContact', []);
contactApp.factory('ContactResource',['$resource', function ($resource){
    return $resource('/api/form/:method/:id', {'method':'@method','id':'@id'}, {
        add: {method: 'post'},
        save:{method: 'post'},
        update:{method: 'put'}
    });
}])
.service('ContactService', ['ContactResource', '$q', function (ContactResource, $q) {
    var that = this;

    /**
     * Get items
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @return {Void} 
     */
    this.getItems = function(data) {
        var defer = $q.defer(); 
        var temp  = new ContactResource(data);
        temp.$save({method: 'contacts'}, function success(data) {
            defer.resolve(data);
        }, function error(reponse) {
            defer.resolve(reponse.data);
        });
        return defer.promise;  
    }

    /**
     * Delete the contacts
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  {Integer} id The contact ids
     * @return {Void}    
     */
    this.deleteContacts = function (contactIds) {

        var defer = $q.defer(); 
        var temp  = new ContactResource(contactIds);

        // Delete item is successfull
        temp.$save({method: 'delete'}, function success(data) {
            defer.resolve(data);
        },
        
        // If delete item is error
        function error(reponse) {
            defer.resolve(reponse.data);
        });
        return defer.promise;  
    }

}]);
