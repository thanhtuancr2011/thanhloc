contactApp.controller('ContactController', ['$scope', '$uibModal', '$filter', 'ContactService', '$timeout', '$rootScope', function ($scope, $uibModal, $filter, ContactService, $timeout, $rootScope) {

    // When js didn't  loaded then hide table term
    $('.container-fluid').removeClass('hidden');
    HoldOn.open(optionsHoldOn);

    /**
     * Get list items
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @return {Void}
     */
    $scope.getItems = function () {

        // Reset all checkbox
        $scope.toggleSelection('none');

        ContactService.getItems({
            pageOptions: $scope.pageOptions,
            searchOptions: $scope.searchOptions,
        }).then(function (data) {
            if (data.status == 1) {

                // Set terms value
                $scope.contacts = angular.copy(data.contacts);
                $scope.pageOptions.totalPages = data.totalPages;
                $scope.pageOptions.totalItems = data.totalItems;
                $rootScope.$broadcast('pageOptions', $scope.pageOptions);

                // Show loading
                HoldOn.close();
            }
        });
    }

    // Call function get items
    $timeout(function () {
        $scope.getItems();
    });

    /**
     * Search contacts
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @return {Void}
     */
    $scope.searchContacts = function (keyEvent) {

        if (keyEvent == 'submit' || keyEvent == 'ng-change' || keyEvent.which === 13) {
            HoldOn.open(optionsHoldOn);
            $scope.getItems();
        }
    }

    /**
     * export file to excel
     * @author AnhVN <anhvn@diziweb.com>
     * @return {Void}
     */
    $scope.getExport = function () {
        HoldOn.open(optionsHoldOn);
        // Reset all checkbox
        $scope.toggleSelection('none');

        ContactService.getItems({
            is_export: true,
            pageOptions: $scope.pageOptions,
            searchOptions: $scope.searchOptions,
        }).then(function (data) {
            console.log(data);
            if (data.status == 1) {
                alasql('SELECT * INTO XLSX("contact.xlsx",{headers:true, type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=base64"}) FROM ?', [data.contacts]);
                // window.location = data.link;
                // Show loading
                HoldOn.close();
            }
        });
    }
    /**
     * Delete the contacts
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  {Integer} id   The contact id
     * @param  {String}  size Type of modal
     * @return {Void}
     */
    $scope.removeContacts = function (id) {

        if (angular.isDefined(id)) {
            var contactIds = [id];
        } else {
            var contactIds = $scope.listIdsSelected;
        }

        var template = '/backend/app/components/contact/views/delete.html?v=' + new Date().getTime();
        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: window.baseUrl + template,
            controller: 'ModalDeleteContactCtrl',
            size: 'sm',
            resolve: {
                contactIds: function () {
                    return contactIds;
                }
            }
        });

        modalInstance.result.then(function (data) {
            $scope.getItems();
        }, function () {
        });
    };

    /**
     * Onload when controller call to set page options
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  {Object} event The event
     * @param  {Object} data) The page options
     * @return {Void}
     */
    $scope.$on('setPageOptions', function (event, data) {
        HoldOn.open(optionsHoldOn);
        $scope.pageOptions = data;
        $scope.getItems();
    });

}])
.controller('ModalDeleteContactCtrl', ['$scope', '$uibModalInstance', 'contactIds', 'ContactService', function ($scope, $uibModalInstance, contactIds, ContactService) {

    // When category click update the post for category
    $scope.submit = function () {

        // Loading
        HoldOn.open(optionsHoldOn);

        ContactService.deleteContacts(contactIds).then(function (data) {

            // Close loading
            HoldOn.close();

            $uibModalInstance.close();
            Notify(data.msg);
        });
    };

    // When category click cancel then close modal popup
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);