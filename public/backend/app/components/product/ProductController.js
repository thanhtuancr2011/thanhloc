productApp.controller('ProductController', ['$scope', '$uibModal', '$filter', 'ProductService', '$timeout', '$rootScope', function ($scope, $uibModal, $filter, ProductService, $timeout, $rootScope) {

	// When js didn't  loaded then hide table category
	$('.container-fluid').removeClass('hidden');
	HoldOn.open(optionsHoldOn);

	/**
	 * Get list items
	 * @author Thanh Tuan <thanhtuancr2011@gmail.com>
	 * @return {Void} 
	 */
	$scope.getItems = function() {

		// Reset all checkbox
		$scope.toggleSelection('none');
		
		ProductService.getItems({
			pageOptions: $scope.pageOptions,
			searchOptions: $scope.searchOptions,
		}).then(function (data) {
			if (data.status == 1) {

				// Set products value
				$scope.products = angular.copy(data.products);
				$scope.pageOptions.totalPages = data.totalPages;
				$scope.pageOptions.totalItems = data.totalItems;
				$rootScope.$broadcast('pageOptions', $scope.pageOptions);

				// Show loading
				HoldOn.close();
			}
		});
	}

	$timeout(function() {
		$scope.searchOptions.type = $scope.type;
		$scope.getItems();
	});

	/**
     * Onload when controller call to set page options
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  {Object} event The event 
     * @param  {Object} data) The page options
     * @return {Void}       
     */
    $scope.$on('setPageOptions',function(event, data) {
        HoldOn.open(optionsHoldOn);
        $scope.pageOptions = data;
        $scope.getItems();
    });

	/**
     * Search products
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @return {Void}                 
	 */
    $scope.searchProducts = function(keyEvent) {
    	if (keyEvent == 'submit' || keyEvent == 'ng-change' || keyEvent.which === 13) {
    		HoldOn.open(optionsHoldOn);
	    	$scope.getItems();
    	}
    } 

	/**
	 * Delete the category
	 * @author Thanh Tuan <thanhtuancr2011@gmail.com>
	 * @param  {Integer} id   The category id
	 * @return {Void}     
	 */
	$scope.removeProduct = function(id){

		if (angular.isDefined(id)) {
			var productIds = [id];
		} else {
			var productIds = $scope.listIdsSelected;
		}
		
		var template = '/backend/app/components/category/views/delete.html?v=' + new Date().getTime();
		var modalInstance = $uibModal.open({
		    animation: true,
		    templateUrl: window.baseUrl + template,
		    controller: 'ModalDeleteProductCtrl',
		    size: 'sm',
		    resolve: {
		    	productIds: function(){
		            return productIds;
		        }
		    }
		});

		modalInstance.result.then(function (data) {
			$scope.getItems();
		}, function () {});
	};

}]).controller('ModalDeleteProductCtrl', ['$scope', '$uibModalInstance', 'productIds', 'ProductService', function ($scope, $uibModalInstance, productIds, ProductService) {
	
	// When category click update the category for category
	$scope.submit = function () {

		// Loading
		HoldOn.open(optionsHoldOn);

		ProductService.deleteProducts(productIds).then(function (data) {
			
			// Close loading
			HoldOn.close();

			$uibModalInstance.close();
			Notify(data.msg);
		});
	};

	// When category click cancel then close modal popup
	$scope.cancel = function () {
	    $uibModalInstance.dismiss('cancel');
	};
}]).controller('CreateProductController', ['$scope', 'ProductService', '$uibModal', '$http', '$timeout', 'SettingService', '$filter', function ($scope, ProductService, $uibModal, $http, $timeout, SettingService, $filter) {
	
	// When js didn't  loaded then hide table category
	$('.container-fluid').removeClass('hidden');

	// Get domain name
	$scope.domainName = $('<a id="domain-name">').prop('href', window.baseUrl).prop('hostname');

	/**
	 * Init slug 
	 * @return Void
	 */
	$scope.$watch('productItem.title', function(newVal, oldVal) {
		if (angular.isDefined(newVal) && !angular.isDefined($scope.productItem.id)) {
			$scope.productItem.slug = slugValue(newVal, '-');
		}
	}, true);

	// setup variable for date picker
	$scope.format = 'dd-MM-yyyy';

	// Date picker popup
	$scope.dateOptions = {
	    formatYear: 'yy',
	    // minDate: new Date(),
	    startingDay: 1
  	};

	/* Open calendar when create page*/
	$scope.open = function($event, name) {
    	$event.preventDefault();
    	$event.stopPropagation();
    	$scope.opened = {};
    	$scope.opened[name] = true;
  	};
	
	/**
	 * Calculating all score for SEO
	 * @param  {Object} allScores All score
	 * @return {Void}           
	 */
	$scope.calcScore = function(allScores) {
		$scope.currentScore = 26;
		angular.forEach(allScores, function(value, key) {
			$scope.currentScore += value;
		});
	}

	// Set category checkbox when edit
	$timeout(function() {

		// When edit
		if (angular.isDefined($scope.productItem.id)) {
			$.map($scope.productItem.categories, function(category) {
				$('#checkbox' + category.id).prop('checked', true);
			});

			$('#no-index').prop('checked', $scope.productItem.no_index);
			$('#no-follow').prop('checked', $scope.productItem.no_follow);
			$('#price-fluctuations').prop('checked', $scope.productItem.price_fluctuations);
		}

		// Price 
		$('input[name="price"]').mask("#,##0", {reverse: true});
		$('input[name="sale_price"]').mask("#,##0", {reverse: true});
		$('input[name="sale_price_10"]').mask("#,##0", {reverse: true});
		$('input[name="sale_price_50"]').mask("#,##0", {reverse: true});
		$('input[name="sale_price_100"]').mask("#,##0", {reverse: true});

		// Published date
		if (!angular.isDefined($scope.productItem.published_at) || $scope.productItem.published_at == null) {
            if (angular.isDefined($scope.productItem.created_at)) {
                $scope.productItem.published_at = angular.copy($scope.productItem.created_at);
            } else {
                $scope.productItem.published_at = new Date();
            }
        }
        $scope.productItem.published_at = new Date($scope.productItem.published_at);

		// TinyMCE options
		$scope.tinymceOptions = {
			height: 500,
			selector: 'textarea#tinymceEditor',
			entity_encoding : "raw",
			relative_urls: false,
			theme: 'modern',
			relative_urls : false,
			remove_script_host : false,
			plugins : "link image code table",
			theme_advanced_buttons3_add : "tablecontrols",
			table_styles : "Header 1=header1;Header 2=header2;Header 3=header3",
			table_cell_styles : "Header 1=header1;Header 2=header2;Header 3=header3;Table Cell=tableCel1",
			table_row_styles : "Header 1=header1;Header 2=header2;Header 3=header3;Table Row=tableRow1",
			table_cell_limit : 100,
			table_row_limit : 5,
			table_col_limit : 5,
			toolbar: 'styleselect | table | undo redo | bold italic | image imagetools | alignleft aligncenter alignright | link unlink | fontselect fontsizeselect | code',
			init_instance_callback: function (editor) {
		    	editor.on('click', function (e) {
		    		$scope.productItem.content = tinyMCE.activeEditor.getContent();
					$scope.$apply();
		    	});
		    	editor.on('keyup', function(e) {
					$scope.productItem.content = tinyMCE.activeEditor.getContent();
					$scope.$apply();
		        });
			    editor.on('SetContent', function (e) {
			      	$scope.productItem.content = tinyMCE.activeEditor.getContent();
					$scope.$apply();
			    });
	  		}
		}

		var firstLoadJS = true;
		var editor = tinyMCE.init($scope.tinymceOptions);

		// Set variable for check seo score
		$scope.currentScore = 26;
		$scope.totalScore = 100;

		$scope.allScores = {};

		// When content change
		$scope.$watch('productItem.content', function(newVal, oldVal) {
			// console.log('productItem.content');
			$scope.percentAppear = 0;
			$scope.keyWordInAltImg = false;
			$scope.keyWordInContent = false;
			$scope.hasBUITagInContent = false;
			$scope.appearKeyWordInContent = false;
			$scope.keyWordInLast150Chracters = false;
			$scope.keyWordInFirst150Chracters = false;
			$scope.existsInternalLinkInContent = false;
			$scope.notExistsInternalLinkInContent = false;

			$scope.allScores.scoreKeyWordInAltImg = 0;
			$scope.allScores.scoreKeyWordInContent = 0;
			$scope.allScores.scoreHasBUITagInContent = 0;
			$scope.allScores.scoreAppearKeyWordInContent = 0;
			$scope.allScores.scoreKeyWordInLast150Chracters = 0;
			$scope.allScores.scoreKeyWordInFirst150Chracters = 0;
			$scope.allScores.scoreNotExistsInternalLinkInContent = 0;
			if (angular.isDefined(newVal) && newVal != '' && newVal != null) {

				// If the key word in alt tag
				var divTmp = document.createElement('div');
				divTmp.innerHTML = newVal;

				$scope.productItem.in_link = 0;
				$scope.productItem.out_link = 0;

				// Has exists internal link and external link
				angular.forEach(divTmp.getElementsByTagName('a'), function(value1, key1) {
					if (value1.href.indexOf($scope.domainName) != -1) {
						$scope.existsInternalLinkInContent = true;
						$scope.allScores.scoreExistsInternalLinkInContent = 5;
						$scope.productItem.in_link += 1; 
					} else {
						$scope.notExistsInternalLinkInContent = true;
						$scope.allScores.scoreNotExistsInternalLinkInContent = 5;
						$scope.productItem.out_link += 1; 
					}
				});

				// Check primary keyword in content
				if (angular.isDefined($scope.productItem.primary_keyword) && $scope.productItem.primary_keyword != null) {

					// Isset in the content
					if (newVal.toLowerCase().replace(/(<([^>]+)>)/ig,"").indexOf($scope.productItem.primary_keyword.toLowerCase()) != -1) {
						$scope.keyWordInContent = true;
						$scope.allScores.scoreKeyWordInContent = 5;
					}
					// Isset in the first 150 characters of content
					if (newVal.toLowerCase().replace(/(<([^>]+)>)/ig,"").substring(0, 150).indexOf($scope.productItem.primary_keyword.toLowerCase()) != -1) {
						$scope.keyWordInFirst150Chracters = true;
						$scope.allScores.scoreKeyWordInFirst150Chracters = 2;
					}

					// Isset in the last 150 characters of content
					if (newVal.toLowerCase().replace(/(<([^>]+)>)/ig,"").length > 150 && 
						newVal.toLowerCase().replace(/(<([^>]+)>)/ig,"").substring(newVal.toLowerCase().replace(/(<([^>]+)>)/ig,"").length - 150).indexOf($scope.productItem.primary_keyword.toLowerCase()) != -1) {
						$scope.keyWordInLast150Chracters = true;
						$scope.allScores.scoreKeyWordInLast150Chracters = 2;
					}

					// The primary keywords exists in content with percent appear is 2 - 5%
					if ((newVal.replace(/(<([^>]+)>)/ig,"").toLowerCase().match(new RegExp($scope.productItem.primary_keyword.toLowerCase(), 'g')) || []).length > 0) {
						var numberAppear  = ((newVal.replace(/(<([^>]+)>)/ig,"").toLowerCase().match(new RegExp($scope.productItem.primary_keyword.toLowerCase(), 'g')) || []).length);
						var numberTotal   = newVal.replace(/(<([^>]+)>)/ig,"").toLowerCase().length;
						var keyWordLength = angular.copy($scope.productItem.primary_keyword).length;
						$scope.percentAppear = parseFloat(numberAppear*keyWordLength/numberTotal*100).toFixed(2);
						if ($scope.percentAppear >= 2 && $scope.percentAppear <= 5) {
							$scope.appearKeyWordInContent = true;
							$scope.allScores.scoreAppearKeyWordInContent = 5;
						}
					}

					// Has exists key word in img alt
					angular.forEach(divTmp.getElementsByTagName('img'), function(value, key) {
						if (value.alt.toLowerCase().indexOf($scope.productItem.primary_keyword.toLowerCase()) != -1) {
							$scope.keyWordInAltImg = true;
							$scope.allScores.scoreKeyWordInAltImg = 5;
						}
					});

					// Has exists keywords is bold
					angular.forEach(divTmp.getElementsByTagName('strong'), function(value2, key2) {
						if (value2.innerHTML.toLowerCase().indexOf($scope.productItem.primary_keyword.toLowerCase()) != -1) {
							$scope.hasBUITagInContent = true;
							$scope.allScores.scoreHasBUITagInContent = 5;
						}
					});

					// Has exists keywords is bold
					angular.forEach(divTmp.getElementsByTagName('b'), function(value3, key3) {
						if (value3.innerHTML.toLowerCase().indexOf($scope.productItem.primary_keyword.toLowerCase()) != -1) {
							$scope.hasBUITagInContent = true;
							$scope.allScores.scoreHasBUITagInContent = 5;
						}
					});

					// Has exists keywords is italic
					angular.forEach(divTmp.getElementsByTagName('em'), function(value4, key4) {
						if (value4.innerHTML.toLowerCase().indexOf($scope.productItem.primary_keyword.toLowerCase()) != -1) {
							$scope.hasBUITagInContent = true;
							$scope.allScores.scoreHasBUITagInContent = 5;
						}
					});

					// Has exists keywords is italic
					angular.forEach(divTmp.getElementsByTagName('i'), function(value5, key5) {
						if (value5.innerHTML.toLowerCase().indexOf($scope.productItem.primary_keyword.toLowerCase()) != -1) {
							$scope.hasBUITagInContent = true;
							$scope.allScores.scoreHasBUITagInContent = 5;
						}
					});

					// Has exists keywords is underline
					angular.forEach(divTmp.getElementsByTagName('u'), function(value6, key6) {
						if (value6.innerHTML.toLowerCase().indexOf($scope.productItem.primary_keyword.toLowerCase()) != -1) {
							$scope.hasBUITagInContent = true;
							$scope.allScores.scoreHasBUITagInContent = 5;
						}
					});
				}
			}

			$timeout(function() {
				$scope.calcScore($scope.allScores);
			})
		}, true);

		// When primary keyword change
		$scope.$watch('productItem.primary_keyword', function(newVal, oldVal) {
			// console.log('productItem.primary_keyword');
			
			$scope.percentAppear = 0;
			$scope.keyWordInUrl = false;
			$scope.keyWordInTitle = false;
			$scope.keyWordInAltImg = false;
			$scope.keyWordInContent = false;
			$scope.hasBUITagInContent = false;
			$scope.keyWordInDescription = false;
			$scope.appearKeyWordInContent = false;
			$scope.keyWordInLast150Chracters = false;
			$scope.keyWordInFirst150Chracters = false;

			$scope.allScores.scoreKeyWordInUrl = 0;
			$scope.allScores.scoreKeyWordInTitle = 0;
			$scope.allScores.scoreKeyWordInAltImg = 0;
			$scope.allScores.scoreKeyWordInContent = 0;
			$scope.allScores.scoreHasBUITagInContent = 0;
			$scope.allScores.scoreKeyWordInDescription = 0;
			$scope.allScores.scoreAppearKeyWordInContent = 0;
			$scope.allScores.scoreKeyWordInLast150Chracters = 0;
			$scope.allScores.scoreKeyWordInFirst150Chracters = 0;

			if (angular.isDefined(newVal) && newVal != '' && newVal != null) {

				// Check primary keyword isset in title
				if (angular.isDefined($scope.productItem.seo_title) && $scope.productItem.seo_title != '' && $scope.productItem.seo_title != null) {
					if ($scope.productItem.seo_title.toLowerCase().indexOf(newVal.toLowerCase()) != -1) {
						$scope.keyWordInTitle = true;
						$scope.allScores.scoreKeyWordInTitle = 5;
					}
				}

				// Check primary keyword isset in url
				if (angular.isDefined($scope.productItem.slug) && $scope.productItem.slug != '' && $scope.productItem.slug != null) {
					if (slugValue($scope.productItem.slug, '-').indexOf(slugValue(newVal, '-')) != -1) {
						$scope.keyWordInUrl = true;
						$scope.allScores.scoreKeyWordInUrl = 10;
					}
				}

				// Check primary keyword isset in description
				if (angular.isDefined($scope.productItem.seo_description) && $scope.productItem.seo_description != '' && $scope.productItem.seo_description != null) {
					if ($scope.productItem.seo_description.toLowerCase().indexOf(newVal.toLowerCase()) != -1) {
						$scope.keyWordInDescription = true;
						$scope.allScores.scoreKeyWordInDescription = 5;
					}
				}

				if (!firstLoadJS) {
					// Set product content
					$scope.productItem.content = tinyMCE.activeEditor.getContent();
				}

				// Check primary keyword in content
				if (angular.isDefined($scope.productItem.content) && $scope.productItem.content != '' && $scope.productItem.content != null) {

					// If the key word in alt tag
					var divTmp = document.createElement('div');
					divTmp.innerHTML = $scope.productItem.content;

					// Has exists key word in img alt
					angular.forEach(divTmp.getElementsByTagName('img'), function(value, key) {
						if (value.alt.toLowerCase().indexOf(newVal.toLowerCase()) != -1) {
							$scope.keyWordInAltImg = true;
							$scope.allScores.scoreKeyWordInAltImg = 5;
						}
					});

					// Has exists keywords is bold
					angular.forEach(divTmp.getElementsByTagName('strong'), function(value2, key2) {
						if (value2.innerHTML.toLowerCase().indexOf(newVal.toLowerCase()) != -1) {
							$scope.hasBUITagInContent = true;
							$scope.allScores.scoreHasBUITagInContent = 5;
						}
					});

					// Has exists keywords is bold
					angular.forEach(divTmp.getElementsByTagName('b'), function(value3, key3) {
						if (value3.innerHTML.toLowerCase().indexOf(newVal.toLowerCase()) != -1) {
							$scope.hasBUITagInContent = true;
							$scope.allScores.scoreHasBUITagInContent = 5;
						}
					});

					// Has exists keywords is italic
					angular.forEach(divTmp.getElementsByTagName('em'), function(value4, key4) {
						if (value4.innerHTML.toLowerCase().indexOf(newVal.toLowerCase()) != -1) {
							$scope.hasBUITagInContent = true;
							$scope.allScores.scoreHasBUITagInContent = 5;
						}
					});

					// Has exists keywords is italic
					angular.forEach(divTmp.getElementsByTagName('i'), function(value5, key5) {
						if (value5.innerHTML.toLowerCase().indexOf(newVal.toLowerCase()) != -1) {
							$scope.hasBUITagInContent = true;
							$scope.allScores.scoreHasBUITagInContent = 5;
						}
					});

					// Has exists keywords is underline
					angular.forEach(divTmp.getElementsByTagName('u'), function(value6, key6) {
						if (value6.innerHTML.toLowerCase().indexOf(newVal.toLowerCase()) != -1) {
							$scope.hasBUITagInContent = true;
							$scope.allScores.scoreHasBUITagInContent = 5;
						}
					});

					// Isset in the content
					if ($scope.productItem.content.replace(/(<([^>]+)>)/ig,"").toLowerCase().indexOf(newVal.toLowerCase()) != -1) {
						$scope.keyWordInContent = true;
						$scope.allScores.scoreKeyWordInContent = 5;
					}
					// Isset in the first 150 characters of content
					if ($scope.productItem.content.replace(/(<([^>]+)>)/ig,"").toLowerCase().substring(0, 150).indexOf(newVal.toLowerCase()) != -1) {
						$scope.keyWordInFirst150Chracters = true;
						$scope.allScores.scoreKeyWordInFirst150Chracters = 2;
					}
					// Isset in the last 150 characters of content
					if ($scope.productItem.content.replace(/(<([^>]+)>)/ig,"").length >= 150 && $scope.productItem.content.replace(/(<([^>]+)>)/ig,"").toLowerCase().substring($scope.productItem.content.replace(/(<([^>]+)>)/ig,"").toLowerCase().length - 150).indexOf(newVal.toLowerCase()) != -1) {
						$scope.keyWordInLast150Chracters = true;
						$scope.allScores.scoreKeyWordInLast150Chracters = 2;
					}

					// The primary keywords exists in content with percent appear is 2 - 5%
					if (($scope.productItem.content.replace(/(<([^>]+)>)/ig,"").toLowerCase().match(new RegExp(newVal.toLowerCase(), 'g')) || []).length > 0) {
						var numberAppear = ((angular.copy($scope.productItem.content).replace(/(<([^>]+)>)/ig,"").toLowerCase().match(new RegExp(newVal.toLowerCase(), 'g')) || []).length);
						var numberTotal  = angular.copy($scope.productItem.content).replace(/(<([^>]+)>)/ig,"").toLowerCase().length;
						var keyWordLength = angular.copy(newVal).length;
						$scope.percentAppear = parseFloat(numberAppear*keyWordLength/numberTotal*100).toFixed(2);
						if ($scope.percentAppear >= 2 && $scope.percentAppear <= 5) {
							$scope.appearKeyWordInContent = true;
							$scope.allScores.scoreAppearKeyWordInContent = 5;
						}
					}
				}

				$timeout(function() {
					firstLoadJS = false;
				})
			}
			$timeout(function() {
				$scope.calcScore($scope.allScores);
			});
		}, true);

		// When sub keyword change
		$scope.$watch('productItem.sub_keyword', function(newVal, oldVal) {
			// console.log('productItem.sub_keyword');
			
			$scope.subKeyWordInTitle = false;
			$scope.subKeyWordInDescription = false;
			$scope.allScores.scoreSubKeyWordInTitle = 0;
			$scope.allScores.scoreSubKeyWordInDescription = 0;

			if (angular.isDefined(newVal) && newVal != '' && newVal !== null) {
				// Check primary keyword isset in description
				if (angular.isDefined($scope.productItem.seo_title) && $scope.productItem.seo_title != '' && $scope.productItem.seo_title != null) {
					if ($scope.productItem.seo_title.toLowerCase().indexOf(newVal.toLowerCase()) != -1) {
						$scope.subKeyWordInTitle = true;
						$scope.allScores.scoreSubKeyWordInTitle = 5;
					}
				}
				// Check sub keyword isset in seo description
				if (angular.isDefined($scope.productItem.seo_description) && $scope.productItem.seo_description != '' && $scope.productItem.seo_description != null) {
					if ($scope.productItem.seo_description.toLowerCase().indexOf(newVal.toLowerCase()) != -1) {
						$scope.subKeyWordInDescription = true;
						$scope.allScores.scoreSubKeyWordInDescription = 5;
					}
				}
			}
			$scope.calcScore($scope.allScores);
		}, true);

		// When seo description change
		$scope.$watch('productItem.seo_description', function(newVal, oldVal) {
			// console.log('productItem.seo_description');
			
			$scope.keyWordInDescription = false;
			$scope.subKeyWordInDescription = false;
			$scope.allScores.scoreKeyWordInDescription = 0;
			$scope.allScores.scoreSubKeyWordInDescription = 0;

			if (angular.isDefined(newVal) && newVal != '' && newVal !== null) {
				// Check primary keyword isset in description
				if (angular.isDefined($scope.productItem.primary_keyword) && $scope.productItem.primary_keyword != '' && $scope.productItem.primary_keyword != null) {
					if (newVal.toLowerCase().indexOf($scope.productItem.primary_keyword.toLowerCase()) != -1) {
						$scope.keyWordInDescription = true;
						$scope.allScores.scoreKeyWordInDescription = 5;
					}
				}
				// Check sub keyword isset in seo description
				if (angular.isDefined($scope.productItem.sub_keyword) && $scope.productItem.sub_keyword != '' && $scope.productItem.sub_keyword != null) {
					if (newVal.toLowerCase().indexOf($scope.productItem.sub_keyword.toLowerCase()) != -1) {
						$scope.subKeyWordInDescription = true;
						$scope.allScores.scoreSubKeyWordInDescription = 5;
					}
				}
			}
			$scope.calcScore($scope.allScores);
		}, true);

		// Get uncategory
		angular.forEach($scope.categoriesTree, function(value, key) {
			if (value.slug == 'uncategory')
				$scope.uncategory = value;
		});

		// When product slug change
		$scope.$watch('productItem.slug', function(newVal, oldVal) {
			// console.log('productItem.slug');
			
			$scope.keyWordInUrl = false;
			$scope.allScores.scoreKeyWordInUrl = 0;

			if (angular.isDefined(newVal) && newVal != '' && newVal !== null) {

				// Set slug for link
				$scope.slugView = slugValue(newVal, '-');

				// Get category slug for link
				if (angular.isDefined($scope.productItem.categories[0])) {
					$scope.categorySlug = $scope.productItem.categories[0].slug;
				} else {
					$scope.categorySlug = $scope.uncategory.slug;
				}

				// Check primary keyword isset in url
				if (angular.isDefined($scope.productItem.primary_keyword) && $scope.productItem.primary_keyword != '' && $scope.productItem.primary_keyword != null) {
					if ($scope.slugView.indexOf(slugValue($scope.productItem.primary_keyword, '-')) != -1) {
						$scope.keyWordInUrl = true;
						$scope.allScores.scoreKeyWordInUrl = 10;
					}
				}
			}
			$scope.calcScore($scope.allScores);
		}, true);

		// When seo title change
		$scope.$watch('productItem.seo_title', function(newVal, oldVal) {
			// console.log('productItem.seo_title');
			
			$scope.keyWordInTitle = false;
			$scope.subKeyWordInTitle = false;
			$scope.allScores.scoreKeyWordInTitle = 0;
			$scope.allScores.scoreSubKeyWordInTitle = 0;

			if (angular.isDefined(newVal) && newVal != '' && newVal !== null) {
				// Check primary keyword isset in description
				if (angular.isDefined($scope.productItem.primary_keyword) && $scope.productItem.primary_keyword != '' && $scope.productItem.primary_keyword != null) {
					if (newVal.toLowerCase().indexOf($scope.productItem.primary_keyword.toLowerCase()) != -1) {
						$scope.keyWordInTitle = true;
						$scope.allScores.scoreKeyWordInTitle = 5;
					}
				}
				// Check sub keyword isset in seo description
				if (angular.isDefined($scope.productItem.sub_keyword) && $scope.productItem.sub_keyword != '' && $scope.productItem.sub_keyword != null) {
					if (newVal.toLowerCase().indexOf($scope.productItem.sub_keyword.toLowerCase()) != -1) {
						$scope.subKeyWordInTitle = true;
						$scope.allScores.scoreSubKeyWordInTitle = 5;
					}
				}
			}
			$scope.calcScore($scope.allScores);
		}, true);
	});

	/**
	 * Show libraries model
	 * @param  {String} type      Type view file
	 * @param  {String} _function Function name use
	 * @return {Void}           
	 */
	$scope.getModalLibraries = function(type, _function) {

		// When update
		template = '/admin/product/library?v=' + new Date().getTime();

		var modalInstance = $uibModal.open({
		    animation: true,
		    templateUrl: window.baseUrl + template,
		    controller: 'ModalLibrariesCtrl',
		    size: 'lg',
		    resolve: {
		    	type: function() {
		    		return type;
		    	},
		    	_function: function() {
		    		return _function;
		    	},
		    	maxSizeUpload: function() {
		    		return $scope.maxSizeUpload;
		    	}
		    }
		});

		modalInstance.result.then(function (data) {

			// Set image feature
			if (type == 'image') {
				if (_function == 'feature') {
					$scope.productItem.image_feature = {
						'src': data[0].url,
						'alt': data[0].title
					};
				} else {
					$scope.productItem.seo_facebook_image = window.baseUrl + data[0].url;
				}
			} else {
				// Each file
				angular.forEach(data, function(value, key) {

					// Get cursor position
					var ed = tinyMCE.activeEditor;
					var range = ed.selection.getRng();

					// Media file type
					var typeMedia = $scope.mimeContentTypes[value.file_name.split('.').pop()]['group'];

					if (typeMedia == 'Image') {
						var newNode = ed.getDoc().createElement("img"); 
						newNode.src = value.url;  						
						newNode.alt = value.title; 
						newNode.width  = 560;			
					} else if (typeMedia == 'Video' || typeMedia == 'Audio') {
						var newNode = ed.getDoc().createElement("iframe"); 
						newNode.src = value.url;  						
						newNode.alt = value.excerpt; 
						newNode.width  = 560; 
						newNode.height = 315; 
						newNode.frameborder = 0; 
						newNode.allowfullscreen = 'true'; 
					} else {
						// Set link
						var newNode = ed.getDoc().createElement("a");
						newNode.innerText = value.title;
						newNode.href = value.url;  						
						newNode.alt = value.title;  						
						newNode.target  = '_self';  
					}
					// Insert content
					range.insertNode(newNode);  
				});
			}
		}, function () {});
	};

	// Category invalid
	$scope.invalidCategory = true;

	$scope.chooseCategory = function(id) {
		if ($scope.productItem.categories.indexOf(id) == -1) {
			$scope.productItem.categories.push(id);
		} else {
			$scope.productItem.categories.splice($scope.productItem.categories.indexOf(id)	, 1);
		}

		$timeout(function() {
			$scope.invalidCategory = true;
			if ($scope.productItem.categories.length > 0) {
				$scope.invalidCategory = false;
			}
		})
	}

	/**
	 * Save or update the product
	 * @param  {Boolean} validate The validate value
	 * @return {Void}          
	 */
	$scope.submit = function (validate) {

		// Category ids
		$scope.productItem.categories = $('.ckb-cate:checked').map(function() {
		    return parseInt($(this).val());
		}).get();

		// Validate category
		if ($scope.productItem.categories.length > 0) {
			$scope.invalidCategory = false;
		} else {
			$scope.invalidCategory = true;
		}
		
		// Validate
		$scope.submitted = true;
		if (!validate || $scope.invalidCategory) return;
		
		$scope.productItem.no_index = 0;
		$scope.productItem.no_follow = 0;
		$scope.productItem.price_fluctuations = 0;

		// No index and no follow
		if ($('#no-index').is(':checked')) {
			$scope.productItem.no_index = 1;
		}
		if ($('#no-follow').is(':checked')) {
			$scope.productItem.no_follow = 1;
		}

		// Price fluctuations
		if ($('#price-fluctuations').is(':checked')) {
			$scope.productItem.price_fluctuations = 1;
		}

		// Format price
		$scope.productItem.price = angular.isDefined($scope.productItem.price) ? slugValue($scope.productItem.price, '') : '';
		$scope.productItem.sale_price = angular.isDefined($scope.productItem.sale_price) ? slugValue($scope.productItem.sale_price, '') : '';
		
		// Product meta fields
		$scope.productItem.product_metas = [];
		if (angular.isDefined($scope.productFields)) {
			angular.forEach($scope.productFields, function(field, field_name) {
				$scope.productItem.product_metas.push({
					'meta_key': field_name,
					'meta_value': angular.isDefined(field.value) ? slugValue(field.value, '') : ''
				});
			});
		}

		// Set product content
		$scope.productItem.content = tinyMCE.activeEditor.getContent();

		// Set score
		$scope.productItem.score = $scope.currentScore;

		// Set product slug
		if (angular.isDefined($scope.productItem.slug) && $scope.productItem.slug != '' && $scope.productItem.slug != null) {
			$scope.productItem.slug = slugValue($scope.productItem.slug, '-');
		} else {
			$scope.productItem.slug = slugValue($scope.productItem.title, '-');
		}

		// Publish date
        $scope.productItem.published_at = $filter('date')(new Date($scope.productItem.published_at), 'yyyy-MM-dd HH:mm:ss');
  
        // Save product
		ProductService.createProductProvider($scope.productItem).then(function (data) {

			// Close loading
			HoldOn.close();

			if (data.status == -1) {
				$scope.errors = data.errors;
			} else {
				Notify(data.msg);

				$timeout(function() {
					if (!angular.isDefined($scope.productItem.id)) {
						window.location.href = '/admin/product/' + data.product.id + '/edit'; 
					} else {
						window.location.reload();
					}
				});
			}
		})
	};

	/* When product click cancel then close modal popup */
	$scope.cancel = function () {
	    $uibModalInstance.dismiss('cancel');
	};
}]).controller('ModalLibrariesCtrl', ['$scope', '$uibModalInstance', '$timeout', 'LibraryService', 'type', '_function', 'maxSizeUpload', '$rootScope', function ($scope, $uibModalInstance, $timeout, LibraryService, type, 
	_function, maxSizeUpload, $rootScope) {

	// Loading
	HoldOn.open(optionsHoldOn);
	
	$timeout(function() {

		$scope.maxSizeUpload = maxSizeUpload;

		$('.modal-content').css('height', ($(window).height() - 40) + 'px');
		$('.left-detail').css('height', ($(window).height() - 115) + 'px');
		$('.right-detail').css('height', ($(window).height() - 115) + 'px');
		$('.modal.fade .modal-dialog').css({'width': '100%', 'height': '100%', 'top': 0, 'padding': '20px'});
		$('.modal-body .tab-pane').css({'padding': '0'});

		/**
		 * Load file when user scroll
		 * @return {Void}   
		 */
		document.getElementById("myDIV").onscroll = function(e) {scrollDiv(e)};
		function scrollDiv(e) {
			var element = event.target;
			if (element.scrollHeight - element.scrollTop === element.clientHeight
				&& $scope.pageOptions.currentPage < $scope.pageOptions.totalPages) {
				$scope.pageOptions.currentPage++;
		        $scope.getItems();
		    }
		}

		$("#tree").fancytree({
	        source: $scope.folders,
	        activate: function(event, data) {

	            // Set scope node active
	            $scope.activeNode = data.node;

	            // Get directory path
	            $scope.directoryPath = data.node.getParentList(includeRoot = false, includeSelf = true);

	            // Search data
	            $scope.searchOptions.asset_id = data.node.key;

	            // Active and expand node
	            data.node.setActive();
	            data.node.setExpanded();

	            // Reset list files
	            $scope.librariesGridView = [];

	            if (!$scope.$$phase) {
	                $scope.$apply();
	            }

	            $scope.getItems();

	            // Set folder upload file
                $rootScope.$broadcast('userChooseFolder', data.node.key);
	        },
	        extensions: ["glyph"],
	        glyph: {
	            // preset: "awesome4",
	            map: {
	                _addClass: "glyphicon",
	                checkbox: "fa-square-o",
	                checkboxSelected: "fa-check-square-o",
	                checkboxUnknown: "fa-square",
	                dragHelper: "glyphicon-chevron-right",
	                dropMarker: "fa-long-arrow-right",
	                error: "fa-warning",
	                expanderClosed: "glyphicon-chevron-right",
	                expanderLazy: "glyphicon-chevron-right",
	                expanderOpen: "glyphicon-chevron-down",
	                loading: "fa-spinner fa-pulse",
	                nodata: "fa-meh-o",
	                noExpander: "",
	                radio: "fa-circle-thin",
	                radioSelected: "fa-circle",
	                doc: "fa-file",
	                docOpen: "fa-file",
	                folder: "glyphicon-folder-close",
	                folderOpen: "glyphicon-folder-open"
	            }
	        },
	    });

	    var tree = $('#tree').fancytree("getTree");
	    $timeout(function() {
	        var node = tree.getNodeByKey(tree.getFirstChild().key.toString());
	        node.setActive();
	        node.setExpanded();
	    });
	});

	// List colected
	$scope.listIdsSelected = [];

	/**
	 * When user click on file
	 * @param  {Void} e Element
	 * @return {Void}   
	 */
	$scope.chooseFile = function(file, e) {

		$scope.fileShow = file;

		// Set image feature
		if (_function == 'feature' || _function == 'facebook') {
			var active = false;

			// Active class check
			if ($('.want-active-' + file.id).hasClass('active')) {
				active = true;
			}

			// Remove all active
			$('.check-file-active').removeClass('active');
			
			if (active == true) return;

			$scope.currentSelected = [file];
			$('.want-active-' + file.id).addClass('active');

		} else {

			if ($scope.listIdsSelected.indexOf(file.id) == -1) {
				$scope.listIdsSelected.push(file.id);
				$('.want-active-' + file.id).addClass('active');
			} else {
				$scope.listIdsSelected.splice($scope.listIdsSelected.indexOf(file.id), 1);
				$('.want-active-' + file.id).removeClass('active');
			}

			$scope.currentSelected = angular.copy($scope.librariesGridView).filter(function(obj) {
			  	return ($scope.listIdsSelected.indexOf(obj.id) != -1);
			});
		}
	}

	$scope.pageOptions = {};
	$scope.pageOptions.totalPages   = 0;
	$scope.pageOptions.totalItems   = 0;
	$scope.pageOptions.currentPage  = 1;
	$scope.pageOptions.itemsPerPage = 30;

	$scope.searchOptions = {};
	$scope.searchOptions.type = type;

	/**
	 * Get list items
	 * @author Thanh Tuan <thanhtuancr2011@gmail.com>
	 * @return {Void} 
	 */
	$scope.getItems = function(event = null) {

		LibraryService.getItems({
			pageOptions: $scope.pageOptions,
			searchOptions: $scope.searchOptions,
		}).then(function (data) {
			
			if (data.status == 1) {
				
				// Folder path
				$scope.folderPath = data.folderPath;

				if (angular.isUndefined($scope.librariesGridView)) {
					$scope.librariesGridView = [];
				}

				$scope.folderPath = data.folderPath;

				// Set page options
				$scope.pageOptions.totalPages = data.totalPages;
				$scope.pageOptions.totalItems = data.totalItems;

				$scope.librariesGridView = $scope.librariesGridView.concat(angular.copy(data.libraries));

				// Set height
				$timeout(function() {
					$scope.gridMediaHeight = $('.grid-view-media').width();
				});

				// Show loading
				HoldOn.close();
			}
		});
	}

	/**
     * Onload when file upload finished
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  {Object} event The event 
     * @param  {Object} data) The page options
     * @return {Void}       
     */
    $scope.$on('uploadFinish',function(event, data) {
    	console.log(data, 'datauploaded');
		$scope.librariesGridView = [data.file].concat($scope.librariesGridView);
    });

	// When product click add or edit product
	$scope.submit = function (validate) {
		if (angular.isDefined($scope.currentSelected) && $scope.currentSelected.length > 0) {
			angular.forEach($scope.currentSelected, function(value, key) {
				value.url = $scope.folderPath + value.file_name;
			});
			$timeout(function() {
				$uibModalInstance.close($scope.currentSelected);
			});
		}
	};

	/* When product click cancel then close modal popup */
	$scope.cancel = function () {
	    $uibModalInstance.dismiss('cancel');
	};
}]).filter('spaceless',function() {
    return function(input) {
        if (input) {
            return input.replace(/\s+/g, '%');    
        }
    }
}).filter('listNameCategories',function() {
    return function(categories) {
    	var strCats = '';
    	angular.forEach(categories, function(value, key) {
    		if (strCats == '') 
    			strCats += value.name;
    		else
    			strCats += ', ' + value.name;
    	});

    	return strCats;
    }
});