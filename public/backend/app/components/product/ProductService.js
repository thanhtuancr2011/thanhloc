var productApp = angular.module('productApp', []);
productApp.factory('ProductResource',['$resource', function ($resource){
    return $resource('/api/product/:method/:id', {'method':'@method','id':'@id'}, {
        add: {method: 'post'},
        save:{method: 'post'},
        update:{method: 'put'}
    });
}])
.service('ProductService', ['ProductResource', '$q', function (ProductResource, $q) {
    var that = this;

    /**
     * Get items
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @return {Void} 
     */
    this.getItems = function(data) {
        var defer = $q.defer(); 
        var temp  = new ProductResource(data);
        temp.$save({method: 'items'}, function success(data) {
            defer.resolve(data);
        }, function error(reponse) {
            defer.resolve(reponse.data);
        });
        return defer.promise;  
    }

    /**
     * Function create new product
     * @author Thanh Tuan <tuannt@acro.vn>
     * @param  {Array} data The data input
     * @return {Void}      
     */
	this.createProductProvider = function(data){

        // If isset id of product then call function edit product
        if(typeof data['id'] != 'undefined') {
            return that.editProductProvider(data);
        }

		var defer = $q.defer(); 
        var temp  = new ProductResource(data);

        temp.$save({}, function success(data) {
            // Resolve result 
            defer.resolve(data);
        }, function error(reponse) {
            // Resolve result 
        	defer.resolve(reponse.data);
        });
        return defer.promise;  
	};

    /**
     * The function edit product
     * @author Thanh Tuan <tuannt@acro.vn>
     * @param  {Array} data The data input 
     * @return {Void}      
     */
    this.editProductProvider = function(data){

        var defer = $q.defer(); 
        var temp  = new ProductResource(data);

        // Update product successfull 
        temp.$update({id: data['id']}, function success(data) {
            defer.resolve(data);
        }, function error(reponse) {
            // If create product is error 
            defer.resolve(reponse.data);
        });
        
        return defer.promise;  
    };

    /**
     * Delete the products
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  {Integer} id The product ids
     * @return {Void}    
     */
    this.deleteProducts = function (ids) {

        var defer = $q.defer(); 
        var temp  = new ProductResource(ids);

        // Delete products is successfull
        temp.$save({method: 'delete'}, function success(data) {
            defer.resolve(data);
        },
        
        // If delete product is error
        function error(reponse) {
            defer.resolve(reponse.data);
        });
        return defer.promise;  
    }

    /**
     * Clone the product
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  {Integer} id The product id
     * @return {Void}    
     */
    this.cloneProduct = function (id) {

        var defer = $q.defer(); 
        var temp  = new ProductResource();

        // Delete products is successfull
        temp.$add({method: 'clone', id: id}, function success(data) {
            defer.resolve(data);
        },
        
        // If delete product is error
        function error(reponse) {
            defer.resolve(reponse.data);
        });
        return defer.promise;  
    }

}]);
