var categoryApp = angular.module('appCategory', []);
categoryApp.factory('CategoryResource',['$resource', function ($resource){
    return $resource('/api/category/:method/:id', {'method':'@method','id':'@id'}, {
        add: {method: 'post'},
        save:{method: 'post'},
        update:{method: 'put'}
    });
}])
.service('CategoryService', ['CategoryResource', '$q', function (CategoryResource, $q) {
    var that = this;

    /**
     * Get items
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @return {Void} 
     */
    this.getItems = function(data) {
        var defer = $q.defer(); 
        var temp  = new CategoryResource(data);
        
        temp.$save({method: 'items'}, function success(data) {
            defer.resolve(data);
        }, function error(reponse) {
            defer.resolve(reponse.data);
        });
        return defer.promise;  
    }

    /**
     * Function create new category
     * @author Thanh Tuan <tuannt@acro.vn>
     * @param  {Array} data The data input
     * @return {Void}      
     */
    this.createCategoryProvider = function(data){

        // If isset id of category then call function edit category
        if(typeof data['id'] != 'undefined') {
            return that.editCategoryProvider(data);
        }

        var defer = $q.defer(); 
        var temp  = new CategoryResource(data);

        temp.$save({}, function success(data) {
            // Resolve result 
            defer.resolve(data);
        }, function error(reponse) {
            // Resolve result 
            defer.resolve(reponse.data);
        });
        return defer.promise;  
    };

    /**
     * The function edit category
     * @author Thanh Tuan <tuannt@acro.vn>
     * @param  {Array} data The data input 
     * @return {Void}      
     */
    this.editCategoryProvider = function(data){

        var defer = $q.defer(); 
        var temp  = new CategoryResource(data);

        // Update category successfull 
        temp.$update({id: data['id']}, function success(data) {
            defer.resolve(data);
        }, function error(reponse) {
            // If create category is error 
            defer.resolve(reponse.data);
        });
        
        return defer.promise;  
    };

    /**
     * Delete the categorys
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  {Integer} id The category ids
     * @return {Void}    
     */
    this.deleteCategories = function (ids) {

        var defer = $q.defer(); 
        var temp  = new CategoryResource(ids);

        // Delete categorys is successfull
        temp.$save({method: 'delete'}, function success(data) {
            defer.resolve(data);
        },
        
        // If delete category is error
        function error(reponse) {
            defer.resolve(reponse.data);
        });
        return defer.promise;  
    }

}]);
