termApp.controller('TermController', ['$scope', '$uibModal', '$filter', 'TermService', '$timeout', '$rootScope', function ($scope, $uibModal, $filter, TermService, $timeout, $rootScope) {

	// When js didn't  loaded then hide table term
	$('.container-fluid').removeClass('hidden');
	HoldOn.open(optionsHoldOn);
	
	/**
	 * Get list items
	 * @author Thanh Tuan <thanhtuancr2011@gmail.com>
	 * @return {Void} 
	 */
	$scope.getItems = function() {

		// Reset all checkbox
		$scope.toggleSelection('none');

		TermService.getItems({
			pageOptions: $scope.pageOptions,
			searchOptions: $scope.searchOptions,
		}).then(function (data) {
			if (data.status == 1) {

				// Set terms value
				$scope.terms = angular.copy(data.terms);
				$scope.pageOptions.totalPages = data.totalPages;
				$scope.pageOptions.totalItems = data.totalItems;
				$rootScope.$broadcast('pageOptions', $scope.pageOptions);

				// Show loading
				HoldOn.close();
			}
		});
	}

	$timeout(function() {
		// Call function get items
		$scope.searchOptions.taxonomy = $scope.taxonomy;
		$scope.getItems();
	});

	/**
     * Search terms
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @return {Void}                 */
    $scope.searchTerms = function(keyEvent) {

    	if (keyEvent == 'submit' || keyEvent.which === 13) {
    		HoldOn.open(optionsHoldOn);
	    	$scope.getItems();
    	}
    } 
	
	$scope.getModalTerm = function(id) {
		
		// When create
		var template = '/admin/term/create?taxonomy=' + $scope.taxonomy + '&v='+ new Date().getTime();

		// When update
		if(typeof id != 'undefined'){
			template = '/admin/term/'+ id + '/edit?taxonomy=' + $scope.taxonomy + '&v=' + new Date().getTime();
		}

		var modalInstance = $uibModal.open({
		    animation: true,
		    templateUrl: window.baseUrl + template,
		    controller: 'ModalCreateTermCtrl',
		    size: null,
		    resolve: {
		    }
		});

		/* After create or edit term then reset term and reload ng-table */
		modalInstance.result.then(function (data) {
			$scope.getItems();
		}, function () {

		   });
	};

	/**
	 * Delete the term
	 * @author Thanh Tuan <thanhtuancr2011@gmail.com>
	 * @param  {Integer} id   The term id
	 * @param  {String}  size Type of modal
	 * @return {Void}     
	 */
	$scope.removeTerm = function(id){
		
		if (angular.isDefined(id)) {
			var termIds = [id];
		} else {
			var termIds = $scope.listIdsSelected;
		}
		
		var template = '/backend/app/components/term/views/delete.html?v=' + new Date().getTime();
		var modalInstance = $uibModal.open({
		    animation: $scope.animationsEnabled,
		    templateUrl: window.baseUrl + template,
		    controller: 'ModalDeleteTermCtrl',
		    size: 'sm',
		    resolve: {
		    	termIds: function(){
		            return termIds;
		        }
		    }
		});

		modalInstance.result.then(function (data) {
			$scope.getItems();
		}, function () {});
	};

	/**
     * Onload when controller call to set page options
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  {Object} event The event 
     * @param  {Object} data) The page options
     * @return {Void}       
     */
    $scope.$on('setPageOptions',function(event, data) {
        HoldOn.open(optionsHoldOn);
        $scope.pageOptions = data;
        $scope.getItems();
    });

}])
.controller('ModalCreateTermCtrl', ['$scope', '$uibModalInstance', 'TermService', '$timeout', function ($scope, $uibModalInstance, TermService, $timeout) {

	$scope.$watch('termItem.name', function(newVal, oldVal) {
		if (angular.isDefined(newVal) && !angular.isDefined($scope.termItem.id)) {
			$scope.termItem.slug = slugValue(newVal, '-');
		}
	}, true);

	$timeout(function() {
		if (angular.isDefined($scope.termItem.id)) {
			$("#ckb-status-" + $scope.termItem.parent_id).prop("checked", true);
		}
	});

	// When term click add or edit term
	$scope.submit = function (validate) {

		// Validate
		$scope.submitted = true;
		if (!validate) return;

		// Set term slug
		if (!angular.isDefined($scope.termItem.slug) || $scope.termItem.slug == '') {	
			$scope.termItem.slug = slugValue($scope.termItem.name, '-');
		} else {
			$scope.termItem.slug = slugValue($scope.termItem.slug, '-');
		}
		
		// Loading
		HoldOn.open(optionsHoldOn);
		
		TermService.createTermProvider($scope.termItem).then(function (data) {

			// Close loading
			HoldOn.close();

			if (data.status == -1) {
				$scope.errors = data.errors;
			} else {
				$uibModalInstance.close();
				Notify(data.msg);
			}
		})
	};

	/* When term click cancel then close modal popup */
	$scope.cancel = function () {
	    $uibModalInstance.dismiss('cancel');
	};
}])
.controller('ModalDeleteTermCtrl', ['$scope', '$uibModalInstance', 'termIds', 'TermService', function ($scope, $uibModalInstance, termIds, TermService) {
	
	// When term click update the post for term
	$scope.submit = function () {

		// Loading
		HoldOn.open(optionsHoldOn);

		TermService.deleteTerms(termIds).then(function (data) {
			
			// Close loading
			HoldOn.close();

			$uibModalInstance.close();
			Notify(data.msg);
		});
	};

	// When term click cancel then close modal popup
	$scope.cancel = function () {
	    $uibModalInstance.dismiss('cancel');
	};
}]);