fileModule.directive("fileUpload", ['FileService', 'Upload', '$timeout', '$rootScope',
    function(FileService, Upload, $timeout, $rootScope) {
        return {
            restrict: 'EA',
            require : '^ngModel',
            scope: {
                filesUpload: '=',
                multipleFile: '=',
                maxSizeUpload: '=',
                mimeContentTypes: '=',
            },
            replace: true,
            transclude: true,
            templateUrl: baseUrl + '/backend/app/shared/file-upload/views/view.html?v=' + new Date().getTime(),
            link: function(scope, element, attrs, ngModel) {

                scope.baseUrl = baseUrl;
                scope.fileError  = {};

                // Event listen when user choose folder
                scope.$on('userChooseFolder',function(event, data) {
                    scope.folderId = data;
                });
                
                /**
                 * Choose file upload
                 * @author Thanh Tuan <tuan@httsolution.com>
                 * @param  {File} files File
                 * @return {Void}       
                 */
                scope.chooseFile = function(files) {

                    // Reset file upload
                    scope.fileUpload = {};
                    scope.totalFiles = 0;
                    scope.validFile  = false;

                    // Each file
                    if (files && files.length) {
                        for (var i = 0; i < files.length; i++) {

                            (function(i) {
                                scope.totalFiles++;
                                var file = files[i];
                                // Error max file size
                                if (angular.isDefined(scope.maxSizeUpload) && (file['size']/(Math.pow(1024, 2))) > scope.maxSizeUpload) {
                                    file['uniId'] = getId();
                                    file['proccess'] = 100;
                                    file['error'] = 1;
                                    file['status'] = 0;
                                    scope.fileUpload[file['uniId']] = file;
                                    scope.fileUpload[file['uniId']]['error'] = 'Dung lượng tối đa ' + scope.maxSizeUpload + ' MB';
                                    scope.fileError[file['uniId']] = file;
                                    return;

                                // Error invalid file upload
                                } else if (!angular.isDefined(scope.mimeContentTypes[file.name.split('.').pop()])) {
                                    file['uniId'] = getId();
                                    file['proccess'] = 100;
                                    file['error'] = 1;
                                    file['status'] = 0;
                                    scope.fileUpload[file['uniId']] = file;
                                    scope.fileUpload[file['uniId']]['error'] = 'Tập tin không được hỗ trợ';
                                    scope.fileError[file['uniId']] = file;
                                    return;
                                } else {
                                    scope.validFile = true;
                                    file['error'] = '';
                                    file['proccess'] = 0;
                                    file['uniId'] = getId();
                                    scope.fileUpload[file['uniId']] = file;
                                    scope.fileUpload[file['uniId']].name = '';
                                }

                            })(i);
                        }

                        $timeout(function() {
                            if (!scope.validFile) return;
                            scope.upload(scope.fileUpload);
                        });
                    }
                }

                /**
                 * Upload file
                 * @author Thanh Tuan <thanhtuancr2011@gmail.com>
                 * @param  {File} files File
                 * @return {Void}       
                 */
                scope.upload = function (files) {
                    var countFile = 0;
                    angular.forEach(scope.fileUpload, function(file, key) {
                        Upload.upload({
                            url: baseUrl + '/api/library?folder=' + scope.folderId,
                            file: file, 
                        }).progress(function(evt) {
                            if (angular.isDefined(scope.fileUpload[file['uniId']])) {
                                var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                            } else {
                                var progressPercentage = 100;
                            }
                            if (angular.isDefined(scope.fileUpload[file['uniId']])) {
                                scope.fileUpload[file['uniId']]['proccess'] = progressPercentage;
                            }
                        }).error(function(data, status, headers, config) {
                            if (angular.isDefined(scope.fileUpload[file['uniId']])) {
                                if (angular.isDefined(data.message)) {
                                    scope.fileUpload[file['uniId']]['error'] = data.message;
                                }
                                scope.fileError[config.file['uniId']] = data;
                            }
                            delete scope.fileUpload[key]; 
                        }).success(function(data, status, headers, config) {
                            $rootScope.$broadcast('uploadFinish', {file: data.file});
                        }).finally(function(data) {
                            countFile++;
                            $timeout(function() {
                                if (countFile == scope.totalFiles) {
                                    Notify('Tải lên tập tin thành công');
                                    $rootScope.$broadcast('allUploadFinish', true);
                                    scope.fileUpload = {};
                                }
                            });
                        });
                    });
                }

                ngModel.$render = function() {
                    $timeout(function(){
                        scope.filesUpload = ngModel.$viewValue; 
                    })   
                }

                /**
                  * [checkFile description]
                  * @param  {[type]} type [description]
                  * @return {[type]}      [description]
                  */
                scope.checkFile = function(type){
                    return FileService.checkFile(type);
                }

                /**
                 * Get id of file
                 * @return {Void} 
                 */
                function getId() {
                    return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
                }

                /**
                 * Delete file
                 * @param  {Double} uniId 
                 * @return {Void}       
                 */
                scope.deleteFile = function(uniId) {
                    delete scope.fileUpload[uniId];
                }
                
                scope.$on("emptyFiles", function (event, args) {
                    scope.fileUpload = {};
                    scope.fileUploaded = [];
                });
                
            }
        }
    }
]).filter('bytes', function() {
    return function(bytes, precision) {
        if (isNaN(parseFloat(bytes)) || !isFinite(bytes)) return '-';
        if (typeof precision === 'undefined') precision = 1;
        var units = ['bytes', 'kB', 'MB', 'GB', 'TB', 'PB'],
            number = Math.floor(Math.log(bytes) / Math.log(1024));
        return (bytes / Math.pow(1024, Math.floor(number))).toFixed(precision) + ' ' + units[number];
    }
}).filter('mathPow', function(){
    return function(base, exponent){
        return Math.pow(base, exponent);
    }
});