angular.module('appBase').controller('BaseController', ['$scope', '$parse', '$filter', '$timeout', '$uibModal', '$rootScope', function($scope, $parse, $filter, $timeout, $uibModal, $rootScope) {
	
	// Translate
	$rootScope.transClone = window.transClone;
	$rootScope.transCancel = window.transCancel;
	$rootScope.transDelete = window.transDelete;
	$rootScope.transConfirmClone = window.transConfirmClone;
	$rootScope.transConfirmDelete = window.transConfirmDelete;

	/**
	 * Format date
	 * @author Thanh Tuan <tuannt@acro.vn>
	 * @param  {String} date The date
	 * @return {Void}      
	 */
	$scope.formatDate = function(date){
		var dateOut = new Date(date);
		return dateOut;
    };

    // setup variable for date picker
    $scope.format = 'dd-MM-yyyy';

    // Date picker popup
    $scope.dateOptions = {
        formatYear: 'yy',
        // minDate: new Date(),
        startingDay: 1
    };

    /* Open calendar when create page*/
    $scope.open = function($event, name) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.opened = {};
        $scope.opened[name] = true;
    };

    // Set page options and search options
	$scope.pageOptions = {};
	$scope.pageOptions.totalPages   = 0;
	$scope.pageOptions.totalItems   = 0;
	$scope.pageOptions.currentPage  = 1;
	$scope.pageOptions.itemsPerPage = 15;
	$scope.searchOptions = {};

	// Init list ids selected
	$scope.listIdsSelected = [];

	// Init show btn
	$scope.showBtn = false;

	/**
	 * Toggle selected checkbox
	 * @author Thanh Tuan <thanhtuancr2011@gmail.com>
	 * @param  {String} type The type checked
	 * @return {Void}      
	 */
	$scope.toggleSelection = function(type) {
		
		// Checkbox checked
		switch(type) {
		    case 'all':
		        $('input[type=checkbox]').prop('checked', false);
	    		if ($scope.listIdsSelected.length == 0) {
		    		$('input[type=checkbox]').prop('checked', true);
	    		}
		        break;
		    case 'none':
		        $('input[type=checkbox]').prop('checked', false);
		        break;
		}

    	// Get list ids selected
    	$scope.listIdsSelected = [];
    	$scope.listIdsSelected = $('.ckb:checked').map(function() {
		    return parseInt($(this).val());
		}).get();

		$timeout(function() {
			$scope.showBtn = false;
			if ($scope.listIdsSelected.length > 0) {
				$scope.showBtn = true;
			}
		});
	}

}]);