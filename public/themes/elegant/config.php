<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Inherit from another theme
    |--------------------------------------------------------------------------
    |
    | Set up inherit from another if the file is not exists,
    | this is work with "layouts", "partials" and "views"
    |
    | [Notice] assets cannot inherit.
    |
    */

    'inherit' => null, //default

    /*
    |--------------------------------------------------------------------------
    | Listener from events
    |--------------------------------------------------------------------------
    |
    | You can hook a theme when event fired on activities
    | this is cool feature to set up a title, meta, default styles and scripts.
    |
    | [Notice] these event can be override by package config.
    |
    */

    'events' => [

        // Before event inherit from package config and the theme that call before,
        // you can use this event to set meta, breadcrumb template or anything
        // you want inheriting.
        'before' => function($theme)
        {
            // You can remove this line anytime.
            $theme->setTitle('Copyright ©  2018 - Theme');
        },

        // Listen on event before render a theme,
        // this event should call to assign some assets,
        // breadcrumb template.
        'beforeRenderTheme' => function($theme)
        {
            // You may use this event to set up your assets.
            $theme->asset()->usePath()->add('jquery-js', 'lib/jquery/jquery-1.11.2.min.js');
            $theme->asset()->usePath()->add('bootstrap-js', 'lib/bootstrap/js/bootstrap.min.js');
            $theme->asset()->usePath()->add('select2-js', 'lib/select2/js/select2.min.js');
            $theme->asset()->usePath()->add('bxslider-js', 'lib/jquery.bxslider/jquery.bxslider.min.js');
            $theme->asset()->usePath()->add('owl.carousel-js', 'lib/owl.carousel/owl.carousel.min.js');
            $theme->asset()->usePath()->add('actual-js', 'js/jquery.actual.min.js');
            $theme->asset()->usePath()->add('theme-js', 'js/theme-script.js');
            $theme->asset()->usePath()->add('HoldOn-js', '../../../backend/js/HoldOn.min.js');
            $theme->asset()->usePath()->add('HoldOn-js', '../../../backend/js/HoldOn.min.js');
            
            $theme->asset()->usePath()->add('angular-js', '../../../bower_components/angular/angular.min.js');
            $theme->asset()->usePath()->add('angular-resource-js', '../../../bower_components/angular-resource/angular-resource.js');
            $theme->asset()->usePath()->add('angular-sanitize-js', '../../../bower_components/angular-sanitize/angular-sanitize.min.js');
            $theme->asset()->usePath()->add('bootstrap-notify-js', '../../../bower_components/remarkable-bootstrap-notify/bootstrap-notify.min.js');
            
            $theme->asset()->usePath()->add('main-service-js', 'js/angular/MainService.js');
            $theme->asset()->usePath()->add('main-controller-js', 'js/angular/MainController.js');

            $theme->asset()->usePath()->add('jquery-ui-css', 'lib/jquery-ui/jquery-ui.css');
            $theme->asset()->usePath()->add('bootstrap-min-css', 'lib/bootstrap/css/bootstrap.min.css');
            $theme->asset()->usePath()->add('font-awesome-css', 'lib/font-awesome/css/font-awesome.min.css');
            $theme->asset()->usePath()->add('carousel-css', 'lib/owl.carousel/owl.carousel.css');
            $theme->asset()->usePath()->add('bxslider-css', 'lib/jquery.bxslider/jquery.bxslider.css');
            $theme->asset()->usePath()->add('animate-css', 'css/animate.css');
            $theme->asset()->usePath()->add('reset-css', 'css/reset.css');
            $theme->asset()->usePath()->add('style-css', 'css/style.css');
            $theme->asset()->usePath()->add('responsive-css', 'css/responsive.css');
            $theme->asset()->usePath()->add('option5-css', 'css/option5.css');
            $theme->asset()->usePath()->add('mystyle-css', 'css/mystyle.css');
            $theme->asset()->usePath()->add('HoldOn-css', '../../../backend/css/HoldOn.min.css');
        },

        // Listen on event before render a layout,
        // this should call to assign style, script for a layout.
        'beforeRenderLayout' => [

            'default' => function($theme) {
                // $theme->asset()->usePath()->add('ipad', 'css/layouts/ipad.css');
            }
        ]
    ]
];
