<?php

namespace Theme\Elegant\Http\Controllers;

use Theme;
use SeoHelper;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;

use Core\Base\Services\FileService;
use Core\Base\Repositories\Contracts\SettingRepositoryInterface;
use Core\Modules\Post\Repositories\Contracts\PostRepositoryInterface;
use Core\Modules\User\Repositories\Contracts\UserRepositoryInterface;
use Core\Modules\Term\Repositories\Contracts\TermRepositoryInterface;
use Core\Modules\Product\Repositories\Contracts\ProductRepositoryInterface;
use Core\Modules\Product\Repositories\Contracts\CategoryRepositoryInterface;

class CustomerController extends Controller
{
    protected $userRepository;
    protected $postRepository;
    protected $termRepository;
    protected $settingRepository;
    protected $productRepository;
    protected $categoryRepository;

    public function __construct(CategoryRepositoryInterface $categoryRepository, ProductRepositoryInterface $productRepository, SettingRepositoryInterface $settingRepository, PostRepositoryInterface $postRepository, TermRepositoryInterface $termRepository, UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
        $this->postRepository = $postRepository;
        $this->termRepository = $termRepository;
        $this->productRepository  = $productRepository;
        $this->settingRepository  = $settingRepository;
        $this->categoryRepository = $categoryRepository;
    }

    public function login() 
    {
        SeoHelper::meta()->setTitle('Đăng nhập')->setDescription('Đăng nhập');
        return Theme::scope('auth.login')->render();
    }

    public function account() 
    {
        SeoHelper::meta()->setTitle('Thông tin tài khoản')->setDescription('Thông tin tài khoản');
        return Theme::scope('auth.account')->render();
    }

    public function register() 
    {
        SeoHelper::meta()->setTitle('Đăng ký tài khoản')->setDescription('Đăng ký tài khoản');
        return Theme::scope('auth.register')->render();
    }

    /**
     * Set data validate
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Array $data The data input
     * @return Void       
     */
    public function dataValidate($data)
    {
        \Validator::extend('old_password', function ($attribute, $value, $parameters, $validator) {
            return \Hash::check($value, current($parameters));
        });

        $validate = [
            'rules' => [
                'full_name' => 'required',
                'email'      => 'required|unique:users,email',
                'old_password' => 'old_password:' . \Auth::user()->password,
                'password_confirmation' => 'same:password'
            ],
            'messages' => [
                'full_name.required'  => 'Mời nhập tên',
                'email.required'      => 'Mời nhập email',
                'email.unique'        => 'Email đã tồn tại trong hệ thống',
                'old_password.old_password' => 'Mật khẩu cũ không chính xác',
            ]
        ];

        // If update
        if (!empty($data['id'])) {

            // Not required password
            unset($validate['rules']['password']);

            // Find user
            $user = $this->userRepository->edit($data['id']);

            // If email input = email of category edit
            if($user->email == $data['email']){
                $validate['rules']['email'] = 'unique:users,email,NULL,id,email,$user->email';
            }
        }

        return $validate;
    }

    public function update($id, Request $request)
    {
        // All data input
        $data = $request->all();

        // Call function set data validate and validate the data input
        $vali = $this->dataValidate($data);
        $data = $this->userRepository->validate($data, $vali['rules'], $vali['messages']);

        // Validate fail
        if (isset($data['errors'])) {
            return new JsonResponse($data);
        }

        // Call function save user
        $user = $this->userRepository->update($id, $data);

        if ($user) {
            $result = ['status' => 1, 'msg' => 'Cập nhật thành công'];
        } else {
            $result = ['status' => 0, 'msg' => 'Đã xảy ra lỗi. Mời thử lại'];
        }

        return new JsonResponse($result);
    }

    /**
     * Save user
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  Request $request Data input
     * @return Void           
     */
    public function store(Request $request)
    {
        // All data input
        $data = $request->all();

        $data['type'] = 'customer';

        $validate = [
            'rules' => [
                'full_name' => 'required',
                'email'      => 'required|unique:users,email'
            ],
            'messages' => [
                'full_name.required'  => 'Mời nhập tên',
                'email.required'      => 'Mời nhập email',
                'email.unique'        => 'Email đã tồn tại trong hệ thống'
            ]
        ];

        // If update
        if (!empty($data['id'])) {

            // Not required password
            unset($validate['rules']['password']);

            // Find user
            $user = $this->userRepository->edit($data['id']);

            // If email input = email of category edit
            if($user->email == $data['email']){
                $validate['rules']['email'] = 'unique:users,email,NULL,id,email,$user->email';
            }
        }

        $data = $this->userRepository->validate($data, $validate['rules'], $validate['messages']);

        // Validate fail
        if (isset($data['errors'])) {
            return new JsonResponse($data);
        }

        // Call function save user
        $user = $this->userRepository->store($data);

        if ($user) {
            $result = ['status' => 1, 'msg' => 'Bạn đã đăng kí tài khoản thành công, <br/>chúng tôi sẽ liên hệ trong thời gian sớm nhất.'];
        } else {
            $result = ['status' => 0, 'msg' => 'Đã xảy ra lỗi. Mời thử lại'];
        }
        
        return new JsonResponse($result);
    }
}