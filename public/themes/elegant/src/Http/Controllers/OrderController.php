<?php

namespace Theme\Elegant\Http\Controllers;

use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Auth;
use Theme;
use SeoHelper;
use Illuminate\Http\Request;
use Core\Base\Services\FileService;
use Core\Base\Repositories\Contracts\SettingRepositoryInterface;
use Core\Modules\Product\Repositories\Contracts\ProductRepositoryInterface;
use Core\Modules\Product\Repositories\Contracts\CategoryRepositoryInterface;

class OrderController extends Controller
{
    protected $productRepository;
    protected $categoryRepository;

    public function __construct(CategoryRepositoryInterface $categoryRepository, ProductRepositoryInterface $productRepository, SettingRepositoryInterface $settingRepository)
    {
        $this->productRepository = $productRepository;
        $this->settingRepository = $settingRepository;
        $this->categoryRepository = $categoryRepository;
    }

    public function checkout()
    {
        SeoHelper::meta()->setTitle('Thanh toán')->setDescription('Thanh toán');
        return Theme::scope('order.checkout')->render();   
    }
}