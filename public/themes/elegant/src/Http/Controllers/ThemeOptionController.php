<?php

namespace Theme\Elegant\Http\Controllers;

use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;

use Theme;
use Core\Base\Services\FileService;
use Core\Base\Repositories\Contracts\SettingRepositoryInterface;
use Core\Modules\Product\Repositories\Contracts\ProductRepositoryInterface;
use Core\Modules\Product\Repositories\Contracts\CategoryRepositoryInterface;

class ThemeOptionController extends Controller
{
    protected $productRepository;
    protected $categoryRepository;

    public function __construct(CategoryRepositoryInterface $categoryRepository, ProductRepositoryInterface $productRepository, SettingRepositoryInterface $settingRepository)
    {
        $this->middleware('auth');
        $this->productRepository = $productRepository;
        $this->settingRepository = $settingRepository;
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * Check string is json
     * author  Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  String $string The string data
     * @return Void
     */
    function isJson($string)
    {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }

    public function home()
    {   
        // Setting
        $item = $this->settingRepository->getItem();
        $maxSizeUpload = 2097152;                         // Max file size
        $mimeContentTypes = FileService::listMimeTypes(); // List mime type

        $listHome = [];
        $fieldsHome = getHomeInfo();

        foreach ($fieldsHome as $key => &$value) {
            $value['value'] = [];
            if (isset($item[$key]) && $this->isJson($item[$key]) && (strpos($item[$key], '{') !== false || strpos($item[$key], '[') !== false)) {
                $value['value'] = json_decode($item[$key], TRUE);
            }
        }

        // Return
        return view(Theme::getThemeNamespace() . '::views.admin.theme-options.home', compact('title', 'maxSizeUpload', 'mimeContentTypes', 'fieldsHome', 'listHome'));
    }
}