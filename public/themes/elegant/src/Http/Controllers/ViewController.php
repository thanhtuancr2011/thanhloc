<?php

namespace Theme\Elegant\Http\Controllers;

use Theme;
use SeoHelper;

use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;

use Core\Base\Services\FileService;
use Core\Base\Repositories\Contracts\SettingRepositoryInterface;
use Core\Modules\Post\Repositories\Contracts\PostRepositoryInterface;
use Core\Modules\Term\Repositories\Contracts\TermRepositoryInterface;
use Core\Modules\Product\Repositories\Contracts\ProductRepositoryInterface;
use Core\Modules\Product\Repositories\Contracts\CategoryRepositoryInterface;

class ViewController extends Controller
{
    protected $postRepository;
    protected $termRepository;
    protected $settingRepository;
    protected $productRepository;
    protected $categoryRepository;

    public function __construct(CategoryRepositoryInterface $categoryRepository, ProductRepositoryInterface $productRepository, SettingRepositoryInterface $settingRepository, PostRepositoryInterface $postRepository, TermRepositoryInterface $termRepository)
    {
        $this->postRepository = $postRepository;
        $this->termRepository = $termRepository;
        $this->productRepository = $productRepository;
        $this->settingRepository = $settingRepository;
        $this->categoryRepository = $categoryRepository;
    }

    public function thank()
    {
        SeoHelper::meta()->setTitle('Trang cảm ơn')->setDescription('Trang cảm ơn');
        return Theme::scope('page.thank')->render();   
    }
}