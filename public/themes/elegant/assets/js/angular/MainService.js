var mainApp = angular.module('mainApp', ['ngResource']);
mainApp.factory('MainResource',['$resource', function ($resource){
    return $resource('/api/:route/:method/:id', {'method':'@method','id':'@id'}, {
        add: {method: 'post'},
        save:{method: 'post'},
        update:{method: 'put'},
        delete:{method: 'delete'}
    });
}])
.service('MainService', ['MainResource', '$q', function (MainResource, $q) {
    var that = this;

    /**
     * Get items
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @return {Void} 
     */
    this.getItems = function(data) {
        var defer = $q.defer(); 
        var temp  = new MainResource(data);
        
        temp.$save({route: 'cart', method: 'items'}, function success(data) {
            defer.resolve(data);
        }, function error(reponse) {
            defer.resolve(reponse.data);
        });
        return defer.promise;  
    }

    /**
     * Add item to cart
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @return {Void} 
     */
    this.addToCart = function(data) {
        var defer = $q.defer(); 
        var temp  = new MainResource(data);
        
        temp.$save({route: 'cart'}, function success(data) {
            defer.resolve(data);
        }, function error(reponse) {
            defer.resolve(reponse.data);
        });
        return defer.promise;  
    }

    /**
     * Delete the cart
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  {Integer} id The cart id
     * @return {Void}    
     */
    this.deleteCart = function (rowId) {

        var defer = $q.defer(); 
        var temp  = new MainResource();

        // Delete categorys is successfull
        temp.$delete({route: 'cart', id: rowId}, function success(data) {
            defer.resolve(data);
        },
        
        // If delete category is error
        function error(reponse) {
            defer.resolve(reponse.data);
        });
        return defer.promise;  
    }

    /**
     * Checkout order
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  {Object} data The data input
     * @return {Void}      
     */
    this.checkout = function(data) {
        var defer = $q.defer(); 
        var temp  = new MainResource(data);
        temp.$save({route: 'cart', method: 'checkout'}, function success(data) {
            defer.resolve(data);
        }, function error(reponse) {
            defer.resolve(reponse.data);
        });
        return defer.promise;  
    }

    /**
     * Update customer
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  {Object} data The data input
     * @return {Void}      
     */
    this.updateCustomer = function(data) {
        var defer = $q.defer(); 
        var temp  = new MainResource(data);
        temp.$update({route: 'customer'}, function success(data) {
            defer.resolve(data);
        }, function error(reponse) {
            defer.resolve(reponse.data);
        });
        return defer.promise;  
    }

    /**
     * Function create new user
     * @author Thanh Tuan <tuannt@acro.vn>
     * @param  {Array} data The data input
     * @return {Void}      
     */
    this.createUserProvider = function(data){

        var defer = $q.defer(); 
        var temp  = new MainResource(data);

        temp.$save({route: 'customer'}, function success(data) {
            // Resolve result 
            defer.resolve(data);
        }, function error(reponse) {
            // Resolve result 
            defer.resolve(reponse.data);
        });
        return defer.promise;  
    };
}]);
