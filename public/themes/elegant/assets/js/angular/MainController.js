mainApp.controller('MainController', ['$scope', '$filter', 'MainService', '$timeout', function ($scope, $filter, MainService, $timeout) {

	var Notify = function(message, delay) {
        setTimeout(function() {
            $.notify({
                // options
                icon: '',
                title: '<span style="color: #08aba7">Thông báo: <br /></span>',
                message: message,
                url: 'javascript:void(0)',
                target: '_blank'
            },{
                // settings
                element: 'body',
                position: null,
                type: "info custom-notify",
                allow_dismiss: true,
                newest_on_top: false,
                showProgressbar: false,
                placement: {
                    from: "top",
                    align: "right"
                },
                offset: {
                    x: 30,
                    y: 10
                },
                spacing: 10,
                z_index: 1060,
                delay: angular.isDefined(delay) ? delay : 5000,
                timer: 1000,
                url_target: '_blank',
                mouse_over: null,
                animate: {
                    enter: 'animated fadeInRight',
                    exit: 'animated fadeOutRight'
                },
                onShow: null,
                onShown: null,
                onClose: null,
                onClosed: null,
                icon_type: 'class',
            });
        }, 500);
    }

    $scope.screenWidth = $(window).width();

	// Show loading
	var optionsHoldOn = {
	    theme: "sk-circle",
	    message: 'Loading...',
	    backgroundColor: "#000",
	    textColor: "#fff"
	};

	HoldOn.open(optionsHoldOn);

	$scope.getItems = function() {
		MainService.getItems({}).then(function (data) {
			if (data) {
				$scope.cartItem = angular.copy(data);
				$scope.checkoutItem.carts = angular.copy(data.carts);
			}

			HoldOn.close();
		});
	}

	$scope.changeValueQty = function(productId, quantity) {
		// If number of item is undefined
		if (typeof quantity == 'undefined') {
			$scope.quantity = 1;
			angular.forEach($scope.checkoutItem.carts, function(value, key) {
				if (value.id == productId) {
					value.qty = 1;
				}
			});
		}
	}

	$scope.addToCart = function(productId, quantity, isCheckOut) {

		$timeout(function() {
			// Init item
			$scope.item = {};
			$scope.item.productId = productId;
			$scope.item.isCheckOut = isCheckOut;
			$scope.item.quantity = angular.isDefined(quantity) ? quantity : 1;

			HoldOn.open(optionsHoldOn);

			// Call function add to cart
			MainService.addToCart($scope.item).then(function (data) {
				if (data.status == 1) 
					$scope.getItems();

				Notify(data.msg);
				HoldOn.close();
			});	
		});
	}

	$scope.removeCart = function(rowId) {

		// Call function add to cart
		MainService.deleteCart(rowId).then(function (data) {
			if (data.status == 1)
				$scope.getItems();
			Notify(data.msg);
		});
	}

	$scope.updateCustomer = function(validate) {
		
		$scope.submitted1 = true;
		if (!validate) return;
		$scope.errors = [];

		HoldOn.open(optionsHoldOn);

		// Call function update customer
		MainService.updateCustomer($scope.customer).then(function (data) {

			// Close loading
			HoldOn.close();

			if (data.status == -1) {
				$scope.errors = data.errors;
			} else {
				Notify(data.msg);
			}
		});
	}

	$scope.submit = function(validate) {

		$scope.submitted = true;
		if (!validate) return;

		HoldOn.open(optionsHoldOn);

		// Call function add to cart
		MainService.checkout($scope.checkoutItem).then(function (data) {
			if (data.status == 1)
				window.location.href = '/cam-on';
			HoldOn.close();
		});
	}

	$timeout(function() {

		$('.option5').toggleClass('hideClass');
		
		// Set payment method
		$scope.checkoutItem = {};

		// Customer
		if (angular.isDefined($scope.customer) && $scope.customer !== null) {
			$scope.checkoutItem.billing_email = $scope.customer.email;
			$scope.checkoutItem.billing_full_name = $scope.customer.full_name;
			$scope.checkoutItem.shipping_full_name = $scope.customer.full_name;
			$scope.checkoutItem.payment_method = 0;
		}

		// Get cart items
		$scope.getItems();
	});

	$timeout(function() {
		$('#popup-newsletter').modal('show');
	}, 2000);

	// When user click add or edit user
	$scope.submit = function (validate) {

		// Validate
		$scope.submitted = true;
		if (!validate) return;

		// Loading
		HoldOn.open(optionsHoldOn);

		MainService.createUserProvider($scope.customer).then(function (data) {

			// Close loading
			HoldOn.close();

			if (data.status == -1) {
				$scope.errors = data.errors;
			} else {
				$scope.submitted = false;
				Notify(data.msg, 10000);
				$scope.customer = {};
			}
		});
	};
}]);