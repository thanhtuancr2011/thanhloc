@foreach ($menu_nodes as $key => $row)
    <li><a href="{{ getUrl($row['route'], $row['url']) }}">{{ $row['title'] }}</a></li>
@endforeach
