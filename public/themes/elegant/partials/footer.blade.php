<!-- Footer -->
<footer id="footer">
    <div class="container">
        <!-- introduce-box -->
        <div id="introduce-box" class="row">
            <div class="col-md-3">
                <div id="address-box">
                    <a href="{{ URL::to('/') }}"><img src="{{ setting('logo_src') }}" alt="logo" /></a>
                    <div id="address-list">
                        <div class="tit-name">Địa chỉ:</div>
                        <div class="tit-contain">{{ setting('address') }}</div>
                        <div class="tit-name">SĐT:</div>
                        <div class="tit-contain">{{ setting('phone') }}</div>
                        <div class="tit-name">Email:</div>
                        <div class="tit-contain">{{ setting('email') }}</div>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="row">
                    <div class="col-sm-3 col-xs-6">
                        <div class="introduce-title">Công ty</div>
                        <ul id = "introduce-Account" class="introduce-list">
                            {!!
                                Menu::generateMenu([
                                    'slug' => 'cong-ty',
                                    'options' => ['class' => 'nav navbar-nav '],
                                    'view' => 'footer-menu'
                                ])
                            !!}
                        </ul>
                    </div>
                    <div class="col-sm-3 col-xs-6">
                        <div class="introduce-title">Tài khoản</div>
                        <ul id = "introduce-Account" class="introduce-list">
                            {!!
                                Menu::generateMenu([
                                    'slug' => 'tai-khoan',
                                    'options' => ['class' => 'nav navbar-nav '],
                                    'view' => 'footer-menu'
                                ])
                            !!}
                        </ul>
                    </div>
                    <div class="col-sm-3 col-xs-6">
                        <div class="introduce-title">Hỗ trợ</div>
                        <ul id = "introduce-Account" class="introduce-list">
                            {!!
                                Menu::generateMenu([
                                    'slug' => 'ho-tro',
                                    'options' => ['class' => 'nav navbar-nav '],
                                    'view' => 'footer-menu'
                                ])
                            !!}
                        </ul>
                    </div>
                    <div class="col-sm-3 col-xs-6">
                        <!-- /input-group -->
                        <div class="introduce-title">Cộng đồng</div>
                        <div class="social-link">
                            <a href="{{ setting('facebook_fanpage') }}"><i class="fa fa-facebook"></i></a>
                            <a href="{{ setting('twitter') }}"><i class="fa fa-twitter"></i></a>
                            <a href="{{ setting('google_plus') }}"><i class="fa fa-google-plus"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /#introduce-box -->
        <div id="footer-menu-box">
            <p class="text-center">Copyrights &#169; 2018</p>
        </div>
        <!-- /#footer-menu-box -->
    </div>

    {!! Theme::asset()->container('footer')->scripts() !!}
</footer>