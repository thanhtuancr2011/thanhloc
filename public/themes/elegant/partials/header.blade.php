<div id="header" class="header">
    <div class="top-header">
        <div class="container">
            <div class="top-bar-social">
                <a href="{{setting('facebook_fanpage')}}"><i class="fa fa-facebook"></i></a>
                <a href="{{setting('twitter')}}"><i class="fa fa-twitter"></i></a>
                <a href="{{setting('google_plus')}}"><i class="fa fa-google-plus"></i></a>
            </div>
            <div class="support-link">
                <a href="#">Giới thiệu</a>
            </div>
        </div>
    </div>
    <!--/.top-header -->
    <!-- MAIN HEADER -->
    <div class="container main-header">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-4 logo">
                <a href="/">
                    <img alt="Nhà thuốc Thanh Hà" src="{{setting('logo_src')}}" />
                    <div class="clearfix"></div>
                </a>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 header-search-box">
                <form class="form-inline search" method="GET" action="/tim-kiem" novalidate="">
                    <div class="form-group input-serach">
                        <input type="search" placeholder="Nhập tên thuốc bạn muốn tìm kiếm ..." name="q" class="searchbox-input" required="" autocomplete="off">
                    </div>
                </form>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-5 col-lg-4 group-button-header">
                <div class="col-md-10 col-sm-10 col-xs-12">
                    @if(\Auth::user()) 
                        <div class="login-info">
                            <h4 class="user-name">Xin chào, {{ \Auth::user()->full_name }}</h4>
                            <a href="{{ URL::to('auth/logout') }}" class="log-out">Đăng xuất</a>
                            <a href="{{ URL::to('tai-khoan') }}" class="btn btn-success">Thông tin tài khoản</a>
                        </div>
                    @else
                        <a href="{{ URL::to('dang-nhap') }}" class="btn btn-success">Đăng nhập</a>
                        <a href="/dang-ky" class="log-in">Đăng kí tài khoản</a>
                    @endif
                </div>
                <div class="btn-cart col-md-2" id="cart-block">
                    <a title="Giỏ hàng" href="{{ URL::to('thanh-toan') }}"></a>
                    <span class="notify notify-right">@{{cartItem.numberItems ? cartItem.numberItems : 0}}</span>
                    <div class="cart-block">
                        <div class="cart-block-content">
                            <h5 class="cart-title">Có @{{cartItem.numberItems}} sản phẩm</h5>
                            <div class="cart-block-list">
                                <ul>
                                    <li class="product-info" ng-repeat="(key, cart) in cartItem.carts">
                                        <div class="p-left">
                                            <a href="javascript:void(0)" ng-click="removeCart(key)" class="remove_link">
                                                <i class="glyphicon glyphicon-remove"></i>
                                            </a>
                                            <a href="@{{cart.options.url}}">
                                                <img class="img-responsive" ng-src="@{{cart.options.image_feature}}" alt="">
                                            </a>
                                        </div>
                                        <div class="p-right">
                                            <p class="p-name">@{{cart.name}}</p>
                                            <p class="p-rice">@{{cart.price|currency:"":0}} ₫</p>
                                            <p>SL: @{{cart.qty}}</p>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="toal-cart">
                                <span>Tổng</span>
                                <span class="toal-price pull-right">@{{cartItem.subtotal}} ₫</span>
                            </div>
                            <div class="cart-buttons">
                                <a href="{{ URL::to('thanh-toan') }}" class="btn-check-out">Thanh toán</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    {!!
        Menu::generateMenu([
            'slug' => 'menu-chinh',
            'options' => ['class' => 'nav navbar-nav '],
            'view' => 'main-menu'
        ])
    !!}
</div>