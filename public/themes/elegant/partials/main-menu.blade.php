<!-- END MANIN HEADER -->
<div id="nav-top-menu" class="nav-top-menu">
    <div class="container">
        <div class="row">
            <div id="main-menu" class="col-sm-9 main-menu">
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                        <div id="navbar" class="navbar-collapse collapse">
                            <ul class="nav navbar-nav">
                                @foreach ($menu_nodes as $key => $row)
                                    <li @if ($row['nodes']) class="dropdown" @endif>
                                        <a href="{{ getUrl($row['route'], $row['url']) }}" @if ($row['nodes']) class="dropdown-toggle" data-toggle="dropdown" @endif>{{ $row['title'] }}</a>
                                        @if ($row['nodes'])
                                            <ul class="dropdown-menu container-fluid">
                                                <li class="block-container">
                                                    <ul class="block">
                                                        @foreach ($row['nodes'] as $node)
                                                            <li class="link_container"><a href="{{ getUrl($node['route'], $node['url']) }}">{{ $node['title'] }}</a></li>
                                                        @endforeach
                                                    </ul>
                                                </li>
                                            </ul>
                                        @endif
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                        <!--/.nav-collapse -->
                    </div>
                </nav>
            </div>
        </div>
        <!-- userinfo on top-->
        <div id="form-search-opntop">
        </div>
        <!-- userinfo on top-->
        <div id="user-info-opntop">
        </div>
        <!-- CART ICON ON MMENU -->
        <div id="shopping-cart-box-ontop">
            <i class="fa fa-shopping-cart"></i>
            <div class="shopping-cart-box-ontop-content"></div>
        </div>
    </div>
</div>