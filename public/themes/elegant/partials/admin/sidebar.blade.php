<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="javascript:void(0)">
        <i class="icon-frame" aria-hidden="true"></i> Hiển thị
    </a>
    <ul class="nav-dropdown-items">
        <li class="nav-item">
            <a class="nav-link" href="{{ URL::to('admin/theme-option/home') }}">
                <i class="fa fa-certificate"></i> Trang chủ
            </a>
        </li>
    </ul>
</li>