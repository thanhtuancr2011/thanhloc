<!DOCTYPE html>
<html ng-app="mainApp">
    <head>
        <meta charset="UTF-8">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="robots" content="noindex,follow,noodp"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">

        {!! SeoHelper::render() !!}

        {!! Theme::asset()->styles() !!}
        {!! Theme::asset()->scripts() !!}

        <link rel="alternate" href="{{url('/')}}" hreflang="vi" />
        <link rel="shortcut icon" type="image/x-icon" href="{{setting('favicon_src')}}" />

        <style type="text/css">
            .hideClass {
                visibility:hidden
            }
        </style>
    </head>
    <body class="option5 hideClass" ng-controller="MainController" ng-init="customer={{json_encode(\Auth::user())}}">
        
        <!-- HEADER -->
        {!! Theme::partial('header') !!}
        <!-- end header -->

        <!-- Theme content -->
        {!! Theme::content() !!}
        <!-- End theme content -->

        <!-- Theme footer -->
        {!! Theme::partial('footer') !!}
        <!-- End theme footer -->

        {{-- <a href="#" class="scroll_top" title="Scroll to Top" style="display: inline;">Scroll</a> --}}

        <div id="cart-header" class="cart-header-sticky">
            <a href="{{ URL::to('') }}" class="button button--alt">
                Thêm SP
            </a>
            <a href="{{ URL::to('thanh-toan') }}" class="button button--alt">
                <span class="text">Đơn hàng</span>
                <span class="mini-cart-counter">@{{cartItem.numberItems ? cartItem.numberItems : 0}}</span>
            </a>
            <a href="tel:{{ setting('phone') }}" class="button button--alt button-call-phone">
                Hotline - {{ setting('phone') }}
            </a>
        </div>

        <!-- Subiz -->
        <script>
          (function(s, u, b, i, z){
            u[i]=u[i]||function(){
              u[i].t=+new Date();
              (u[i].q=u[i].q||[]).push(arguments);
            };
            z=s.createElement('script');
            var zz=s.getElementsByTagName('script')[0];
            z.async=1; z.src=b; z.id='subiz-script';
            zz.parentNode.insertBefore(z,zz);
          })(document, window, 'https://widgetv4.subiz.com/static/js/app.js', 'subiz');
          subiz('setAccount', 'acqcerevcxqyrjcqxznb');
        </script>
        <!-- End Subiz -->
    </body>
</html>