<?php

require_once __DIR__ . '/../vendor/autoload.php';

Route::group(['middleware' => ['web']], function () {
    // Login route
    Route::get('dang-nhap', 'Theme\Elegant\Http\Controllers\CustomerController@login');

    // Login route
    Route::get('dang-ky', 'Theme\Elegant\Http\Controllers\CustomerController@register');

    // Thank route
    Route::get('cam-on', 'Theme\Elegant\Http\Controllers\ViewController@thank');

    Route::group(['prefix' => 'api'], function () {
        // Customer route
        Route::resource('customer', 'Theme\Elegant\Http\Controllers\CustomerController');
    });
});

Route::group(['middleware' => ['web', 'customer']], function () {

	// Checkout toure
    Route::get('thanh-toan', 'Theme\Elegant\Http\Controllers\OrderController@checkout');

    // Account route
    Route::get('tai-khoan', 'Theme\Elegant\Http\Controllers\CustomerController@account');
});

Route::group(['middleware' => ['web']], function () {
	
    // Admin route 
    Route::group(['prefix' => 'admin'], function () {

        // Theme options
        Route::get('theme-option/home', 'Theme\Elegant\Http\Controllers\ThemeOptionController@home');
    });
});
