<?php

function getHomeInfo()
{
    // Get categories
    $categories = app(Core\Modules\Product\Repositories\Contracts\CategoryRepositoryInterface::class)
                    ->create()->orderBy('name', 'asc')->pluck('name', 'id');

    // Get products
    $products = app(Core\Modules\Product\Repositories\Contracts\ProductRepositoryInterface::class)
                    ->create()->orderBy('title', 'asc')->pluck('title', 'id');

    return [
        'home_categories' => [
            'type' => 'select',
            'options' => $categories,
            'title' => 'Chọn danh mục',
            'caption' => 'Danh mục đã chọn'
        ],
        'home_hot_product' => [
            'type' => 'select',
            'options' => $products,
            'title' => 'Chọn sản phẩm hot',
            'caption' => 'Sản phẩm hot đã chọn'
        ],
        'home_hot_price_product' => [
            'type' => 'select',
            'options' => $products,
            'title' => 'Chọn sản phẩm giá đẹp',
            'caption' => 'Sản phẩm giá đẹp đã chọn'
        ]
    ];
}

function productFields()
{
    return [
        'sale_price_10' => [
            'type' => 'input',
            'required' => false,
            'title' => 'Giá khi mua 10 sản phẩm'
        ],
        'sale_price_50' => [
            'type' => 'input',
            'required' => false,
            'title' => 'Giá khi mua 50 sản phẩm'
        ],
        'sale_price_100' => [
            'type' => 'input',
            'required' => false,
            'title' => 'Giá khi mua 100 sản phẩm'
        ]
    ];
}
