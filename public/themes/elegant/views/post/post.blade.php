@php
    $post = $item->first();
    $category = $post->categories()->first();
@endphp

<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="{{ URL::to('/') }}" >Trang chủ</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span><a class="home" href="{{ URL::to($category->slug) }}" >{{ $category->name }}</a></span>
            <span class="navigation-pipe">&nbsp;</span>
            <span>{{ $post->title }}</span>
        </div>
        <!-- ./breadcrumb -->
        <!-- row -->
        <div class="row">
            <!-- Left colunm -->
            <div class="column col-xs-12 col-sm-3" id="left_column">
                <!-- Blog category -->
                <div class="block left-module">
                    <p class="title_block">Danh mục</p>
                    <div class="block_content">
                        <!-- layered -->
                        <div class="layered layered-category">
                            <div class="layered-content">
                                <ul class="sub-category-list tree-menu">
                                    @php
                                        $categories = app(Core\Modules\Term\Repositories\Contracts\TermRepositoryInterface::class)->getAllCategoriesTree('');
                                    @endphp
                                    @if (!empty($categories))
                                        @foreach ($categories as $key => $cat)
                                            @if ($cat['name'] != 'Uncategory' && $cat['name'] != 'Trống')
                                                <li>
                                                    <span></span><a href="{{ URL::to($cat['slug']) }}">{{ $cat['name'] }}</a>
                                                    @if (!empty($cat['subFolder']))
                                                        <ul style="display: none;">
                                                            @foreach ($cat['subFolder'] as $cat)
                                                            <li><span></span><a href="{{ URL::to($cat['slug']) }}">{{ $cat['name'] }}</a></li>
                                                            @endforeach
                                                        </ul>
                                                    @endif
                                                </li>
                                            @endif
                                        @endforeach
                                    @endif
                                </ul>
                            </div>
                        </div>
                        <!-- ./layered -->
                    </div>
                </div>
                <!-- ./blog category  -->
            </div>
            <!-- ./left colunm -->
            <!-- Center colunm-->
            <div class="center_column col-xs-12 col-sm-9" id="center_column">
                <h1 class="page-heading">
                    <span class="page-heading-title2">{{ $post->title }}</span>
                </h1>
                <article class="entry-detail">
                    <div class="entry-meta-data">
                        <span class="author"> <i class="fa fa-user"></i> Tác giả: Admin</span>
                        <span class="cat"><i class="fa fa-folder-o"></i> Chuyên mục: Tin tức</span>
                        <span class="date">
                            <i class="fa fa-calendar">
                            </i> {{ formatDateStr($post->published_at, 'd F, Y') }}
                        </span>
                    </div>
                    <div class="entry-photo">
                        @php
                            $imageFeature = json_decode($post->image_feature, TRUE);
                        @endphp
                        <img src="{{ $imageFeature['src'] }}" alt="{{ $imageFeature['alt'] }}">
                    </div>
                    <div class="content-text clearfix">{!! $post->content !!}</div>
                    @php
                        $tags = $post->tags;
                    @endphp
                    @if (count($tags) > 0)
                    <i class="fa fa-tags" style="float:left; transform:rotate(90deg)"></i>
                    <div class="entry-tags" style="float:left; line-height: 15px; margin-left: 5px;">
                        <span style="font-weight: bold;">Từ khóa:</span>
                        @foreach ($tags as $key => $tag)
                            <a href="{{ URL::to('tu-khoa/' . $tag->slug) }}">{{ $tag->name }}{{ $key < (count($tags) - 1) ? ', ' : '' }}</a>
                        @endforeach
                    </div>
                    @endif
                </article>
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>