<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="{{ URL::to('/') }}" >Trang chủ</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="">Tìm kiếm</span>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="">{{$search}}</span>
        </div>
        <!-- ./breadcrumb -->
        <!-- row -->
        <div class="row">
            <!-- Left colunm -->
            <div class="column col-xs-12 col-sm-3" id="left_column">
                <!-- block category -->
                <div class="block left-module">
                    <p class="title_block">Danh mục</p>
                    <div class="block_content">
                        <!-- layered -->
                        <div class="layered layered-category">
                            <div class="layered-content">
                                <ul class="sub-category-list tree-menu">
                                    @php
                                        $categories = app(Core\Modules\Product\Repositories\Contracts\CategoryRepositoryInterface::class)->getAllCategoriesTree('');
                                    @endphp
                                    @if (!empty($categories))
                                        @foreach ($categories as $key => $category)
                                            @if ($category['name'] != 'Uncategory' && $category['name'] != 'Trống')
                                                <li>
                                                    <span></span><a href="{{ URL::to($category['slug']) }}">{{ $category['name'] }}</a>
                                                    @if (!empty($category['subFolder']))
                                                        <ul style="display: none;">
                                                            @foreach ($category['subFolder'] as $cat)
                                                            <li><span></span><a href="{{ URL::to($cat['slug']) }}">{{ $cat['name'] }}</a></li>
                                                            @endforeach
                                                        </ul>
                                                    @endif
                                                </li>
                                            @endif
                                        @endforeach
                                    @endif
                                </ul>
                            </div>
                        </div>
                        <!-- ./layered -->
                    </div>
                </div>
                <!-- ./block category  -->
            </div>
            <!-- ./left colunm -->
            <!-- Center colunm-->
            <div class="center_column col-xs-12 col-sm-9" id="center_column">
                <!-- view-product-list-->
                <div id="view-product-list" class="view-product-list" style="margin: 0">
                    <h2 class="page-heading">
                        <span class="page-heading-title" style="padding: 0">Kết quả tìm kiếm: {{ $search }}</span>
                    </h2>
                    <!-- PRODUCT LIST -->
                    <ul class="row product-list grid">
                        @foreach ($item as $product)
                            @php
                                $imageProduct = json_decode($product->image_feature, TRUE);
                            @endphp
                            <li class="col-xs-6 col-sm-3">
                                <div class="product-container">
                                    <div class="left-block">
                                        <a href="{{ URL::to($product->categories()->first()->slug . '/' . $product->slug . '.html') }}">
                                        <img class="img-responsive" alt="{{ $imageProduct['alt'] }}" src="{{ $imageProduct['src'] }}">
                                        </a>
                                        @if ($product->price_fluctuations)
                                            <div class="quick-view">
                                                <a title="Biến động giá" class="search" href="javascript:void(0)">
                                                    <i class="fa fa-line-chart" aria-hidden="true"></i>
                                                </a>
                                            </div>
                                        @endif
                                        <div class="add-to-cart">
                                            <a title="Add to Cart" href="javascript:void(0)" ng-click="addToCart({{$product->id}})">Thêm vào giỏ hàng</a>
                                        </div>
                                    </div>
                                    <div class="right-block">
                                        <h5 class="product-name">
                                            <a href="{{ URL::to($product->categories()->first()->slug . '/' . $product->slug . '.html') }}">{{ $product->title }}</a>
                                        </h5>
                                        <div class="content_price">
                                            @if (!empty($product->sale_price) && $product->sale_price < $product->price)
                                                <span class="price product-price">{{ number_format($product->sale_price , 0, ',', ',') }} ₫</span>
                                                <span class="price old-price">{{ number_format($product->price , 0, ',', ',') }} ₫</span>
                                            @else
                                                <span class="price product-price">{{ number_format($product->price , 0, ',', ',') }} ₫</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                    <!-- ./PRODUCT LIST -->
                </div>
                <!-- ./view-product-list-->
                <div class="sortPagiBar">
                    <div class="bottom-pagination">
                        <nav>
                            {!! $item->links(Theme::getPartialsName('pagination')) !!}
                        </nav>
                    </div>
                </div>
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>