@php
	$product = $item->first();
	$categoryProduct = $product->categories()->first();
@endphp
<div class="container" id="columns">
	<!-- breadcrumb -->
	<div class="breadcrumb clearfix">
		<a class="home" href="{{ URL::to('/') }}" >Trang chủ</a>
        <span class="navigation-pipe">&nbsp;</span>
		<a href="{{ URL::to($categoryProduct->slug) }}">{{ $categoryProduct->name }}</a>
		<span class="navigation-pipe">&nbsp;</span>
		<span class="">{{ $product->title }}</span>
		</a>
	</div>
	<!-- ./breadcrumb -->
	<!-- row -->
	<div class="row">
		<!-- Left colunm -->
        <div class="column col-xs-12 col-sm-3" id="left_column">
            <!-- block category -->
            <div class="block left-module">
                <p class="title_block">Danh mục</p>
                <div class="block_content">
                    <!-- layered -->
                    <div class="layered layered-category">
                        <div class="layered-content">
                            <ul class="sub-category-list tree-menu">
                                @php
                                $categories = app(Core\Modules\Product\Repositories\Contracts\CategoryRepositoryInterface::class)
                                ->getAllCategoriesTree('');
                                @endphp
                                @if (!empty($categories))
	                                @foreach ($categories as $key => $category)
		                                @if ($category['name'] != 'Uncategory' && $category['name'] != 'Trống')
			                                <li>
			                                    <span></span><a href="{{ URL::to($category['slug']) }}">{{ $category['name'] }}</a>
			                                    @if (!empty($category['subFolder']))
			                                    <ul style="display: none;">
			                                        @foreach ($category['subFolder'] as $cat)
			                                        	<li><span></span><a href="{{ URL::to($cat['slug']) }}">{{ $cat['name'] }}</a></li>
			                                        @endforeach
			                                    </ul>
			                                    @endif
			                                </li>
		                                @endif
	                                @endforeach
                                @endif
                            </ul>
                        </div>
                    </div>
                    <!-- ./layered -->
                </div>
            </div>
            <!-- ./block category  -->
        </div>
        <!-- ./left colunm -->
		<!-- Center colunm-->
		<div class="center_column col-xs-12 col-sm-9" id="center_column">
			<!-- Product -->
			<div id="product">
				<div class="primary-box row">
					<div class="pb-left-column col-xs-12 col-sm-6">
						<!-- product-imge-->
						<div class="product-image">
							<div class="product-full">
								@php
                                    $imageProduct = json_decode($product->image_feature, TRUE);
                                @endphp
								<img id="{{-- product-zoom --}}" src="{{ $imageProduct['src'] }}" data-zoom-image="{{ $imageProduct['src'] }}">
							</div>
						</div>
						<!-- product-imge-->
					</div>
					<div class="pb-right-column col-xs-12 col-sm-6">
						<h1 class="product-name">{{ $product->title }}</h1>
						<div class="product-price-group">
							@if (!empty($product->sale_price) && $product->sale_price < $product->price)
                                <span class="price">{{ number_format($product->sale_price , 0, ',', ',') }} ₫</span>
                                <span class="old-price">{{ number_format($product->price , 0, ',', ',') }} ₫</span>
                            @else
                                <span class="price">{{ number_format($product->price , 0, ',', ',') }} ₫</span>
                            @endif
                            @if (!empty($product->sale_price) && $product->sale_price < $product->price)
								<span class="discount">-{{ number_format(100 - $product->sale_price/$product->price*100 , 0, ',', ',') }}%</span>
							@endif
							@if (!empty($product->price_fluctuations))
								<i class="fluctuation fa fa-line-chart" aria-hidden="true"></i>
							@endif
						</div>
						<div class="info-orther">
							<p>Xuất sứ: <span class="in-stock">{{ $product->origin }}</span></p>
						</div>
						<div class="product-desc">{{ $product->excerpt }}</div>
						<div class="form-option">
							<p class="form-option-title">Lựa chọn:</p>
							<div class="attributes" ng-init="quantity = 1">
								<div class="attribute-label">Số lượng:</div>
								<button class="btn btn-default" ng-show="quantity > 1" style="float: left; border-radius: inherit" ng-click="quantity = quantity - 1">
									<i class="fa fa-minus fa-4"></i>
								</button>
								<button class="btn btn-default" ng-show="quantity <= 1" disabled="" style="float: left; border-radius: inherit" ng-click="quantity = quantity - 1">
									<i class="fa fa-minus fa-4"></i>
								</button>
								<input id="option-product-qty" type="number" min="1" ng-model="quantity" ng-change="changeValueQty({{$product->id}}, quantity)" style=" border: 1px solid #ccc;">
								<button class="btn btn-default" ng-show="quantity < 100" style="float: left; border-radius: inherit" ng-click="quantity = quantity + 1">
									<i class="fa fa-plus fa-1" ></i>
								</button>
								<button class="btn btn-default" ng-show="quantity >= 100" style="float: left; border-radius: inherit" disabled="">
									<i class="fa fa-plus fa-1" ></i>
								</button>
							</div>
						</div>
						<div class="form-action">
							<div class="button-group">
								<a class="btn-add-cart" href="javascript:void(0)" ng-click="addToCart({{$product->id}}, quantity)">Thêm vào giỏ hàng</a>
							</div>
						</div>
					</div>
				</div>
				<!-- tab product -->
				<div class="product-tab">
					<ul class="nav-tab">
						<li class="active">
							<a aria-expanded="false" data-toggle="tab" href="#product-detail">Chi tiết sản phẩm</a>
						</li>
					</ul>
					<div class="tab-container">
						<div id="product-detail" class="tab-panel active">{!! $product->content !!}</div>
					</div>
				</div>
				<!-- ./tab product -->
				<!-- box product -->
				<div class="page-product-box">
                    @php
						$relatedProducts = $categoryProduct->products()->where('status', '1')
													->where('products.id', '!=', $product->id)
                                                    ->whereDate('published_at', '<=', date('Y-m-d H:i:s'))
                                                    ->get();
					@endphp
					
					@if (count($relatedProducts) > 0)
						<h3 class="heading">Sản phẩm liên quan</h3>
						<ul class="product-list owl-carousel" data-dots="false" data-loop="true" data-nav = "true" data-margin = "10" data-autoplayTimeout="1000" data-autoplayHoverPause = "true" data-responsive='{"0":{"items":2},"600":{"items":3},"1000":{"items":4}}'>
							@foreach ($relatedProducts as $relatedProduct)
								@php
	                                $imageRelatedProduct = json_decode($relatedProduct->image_feature, TRUE);
	                            @endphp
		                        <li>
		                            <div class="product-container">
		                                <div class="left-block">
		                                    <a href="{{ URL::to($categoryProduct->slug . '/' . $relatedProduct->slug . '.html') }}">
		                                        <img class="img-responsive" alt="{{ $imageRelatedProduct['alt'] }}" src="{{ $imageRelatedProduct['src'] }}" />
		                                    </a>
		                                    @if ($relatedProduct->price_fluctuations)
	                                            <div class="quick-view">
	                                                <a title="Biến động giá" class="search" href="javascript:void(0)">
	                                                    <i class="fa fa-line-chart" aria-hidden="true"></i>
	                                                </a>
	                                            </div>
	                                        @endif
		                                    <div class="add-to-cart">
		                                        <a title="Add to Cart" href="javascript:void(0)" ng-click="addToCart({{$relatedProduct->id}})">Thêm vào giỏ hàng</a>
		                                    </div>
		                                </div>
		                                <div class="right-block">
		                                    <h5 class="product-name"><a href="{{ URL::to($categoryProduct->slug . '/' . $relatedProduct->slug . '.html') }}">{{ $relatedProduct->title }}</a></h5>
		                                    <div class="content_price">
		                                        @if (!empty($relatedProduct->sale_price) && $relatedProduct->sale_price < $relatedProduct->price)
	                                                <span class="price product-price">{{ number_format($relatedProduct->sale_price , 0, ',', ',') }} ₫</span>
	                                                <span class="price old-price">{{ number_format($relatedProduct->price , 0, ',', ',') }} ₫</span>
	                                            @else
	                                                <span class="price product-price">{{ number_format($relatedProduct->price , 0, ',', ',') }} ₫</span>
	                                            @endif
		                                    </div>
		                                </div>
		                            </div>
		                        </li>
	                        @endforeach
	                    </ul>
                    @endif
				</div>
				<!-- ./box product -->
			</div>
			<!-- Product -->
		</div>
		<!-- ./ Center colunm -->
	</div>
	<!-- ./row-->
</div>