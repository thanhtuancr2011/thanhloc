<div class="container text-center" style="padding: 20px 0">
	<div class="mR-60" style="padding: 30px 0;">
		<img alt="#" src="{{setting('logo_src')}}">
	</div>
	<div class="d-f jc-c fxd-c" style="padding: 20px; line-height: 40px;">
		<h3 class="mB-10 fsz-lg c-grey-900 tt-c">Cảm ơn bạn đã sử dụng dịch vụ mua hàng của <strong>dược phẩm Hà Thanh</strong></h3>
		<p class="mB-30 fsz-def c-grey-700">Chúng tôi sẽ sớm liên hệ đến bạn để xác nhận đơn hàng trong thời gian sớm nhất.</p>
	</div>
	<div><a href="/" type="primary" class="btn btn-success">Tiếp tục mua hàng</a></div>
</div>