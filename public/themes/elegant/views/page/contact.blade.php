<script src='https://www.google.com/recaptcha/api.js'></script>
<div class="wraper">
    <div class="container">
    	<div class="row">
    		<style type="text/css">
	    	</style>
	    	<div class="post-content col-md-12">
	            <h1 class="content-title category"><a href="#">Liên hệ</a></h1>
	            <div class="goc-am-thuc-post">
	                <iframe src="https://maps.google.com/maps?q=21.059872,105.729459&hl=es;z=14&amp;output=embed" width="100%" height="370" frameborder="0" style="border:0" allowfullscreen></iframe> 
	            
	                <div class="row">
		                <style type="text/css">
	                        .content-sp{
	                            text-align: left;
	                            font-size: 14px;
	                            font-weight: 400;
	                            line-height: 20px;
	                        }
	                        .content-sp p{
	                            margin: 0;
	                            padding: 3px 0;
	                        }
	                        .content-sp .title-sp{
	                            font-size: 18px;
	                            margin-bottom: 10px;
	                            text-transform: uppercase;
	                            font-weight: bold;
	                            margin-top: 0
	                        }
	                        .content-sp .exp-sp{
	                            margin-bottom: 20px
	                        }
	                        .info-sp h3{
	                            font-size: 15px;
	                            color: green;
	                            font-weight: 600;
	                            text-transform: uppercase;
	                            margin-bottom: 21px;
	                        }
	                        .sp-detail img{
	                            margin: 0 auto;
	                            height: 180px
	                        }
	                        .table>tbody>tr>td {
	                        	border: none;
	                        }
	                        .table>tbody>tr>td.tbl-title {
	                        	width: 15%;
	                        	font-size: inherit;
	                        	line-height: 30px;
	                        }
	                        .table>tbody>tr>td.tbl-content {
	                        	width: 85%;
	                        }
	                        .no-padding {
	                        	padding: 0;
	                        }
	                        .contact-form label {
	                        	font-size: 13px;
	                        	font-weight: 400;
	                        	display: inherit;
	                        	color: #ff0001;
	                        }
	                    </style>
	                    <div class="col-md-12 content-sp">
	                        <div class="info-sp">
	                            <h3>{{get_config('company_name')}}</h3>
	                            <p><strong>Địa chỉ:</strong> {{get_config('company_address')}}</p>
	                            <p><strong>Tel:</strong> {{get_config('company_phone')}}</p>
	                            <p><strong>Fax:</strong> {{get_config('company_telephone')}}</p>
	                            <p><strong>Email:</strong> {{get_config('company_email')}}</p>
	                            <p><strong>Website:</strong> {{get_config('company_website')}}</p>
	                        </div>
	                    </div> 
		            </div>
	            </div>
	            
	            <div class="row">
	            	<div class="col-md-8 content-sp">
                        <div class="info-sp">
                            <h3>Liên hệ với chúng tôi để được tư vấn tốt nhất</h3>
                            <form action="{{ URL::to('api/form') }}" name="contact" class="contact-form" method="POST" novalidate="">
                            <input type="hidden" name="type" value="contact">
                            <table class="table">
                            	<tbody>
                            		<tr>
                            			<td class="tbl-title">Họ tên</td>
                            			<td class="tbl-content"><input type="text" id="full_name" name="full_name" placeholder="Nguyễn Văn Anh" class="form-control"></td>
                            		</tr>
                            		<tr>
                            			<td class="tbl-title">Email</td>
                            			<td class="tbl-content"><input type="text" placeholder="examples@gmail.com" id="email" name="email" class="form-control"></td>
                            		</tr>
                            		<tr>
                            			<td class="tbl-title">Điện thoại</td>
                            			<td class="tbl-content"><input type="text" id="phone_number" name="phone_number" placeholder="(+84) 12345689" class="form-control"></td>
                            		</tr>
                            		<tr>
                            			<td class="tbl-title">Tin nhắn</td>
                            			<td class="tbl-content">
                            				<textarea id="messages" name="content" rows="3" placeholder="Nhập nội dung..." class="form-control"></textarea>
                            			</td>
                            		</tr>
                            		<tr>
                            			<td class=""></td>
                            			<td class="tbl-content">
                            				<div class="col-md-12 no-padding">
                        						<div class="col-md-6 no-padding">
                        							<div class="g-recaptcha" data-sitekey="6Leof04UAAAAABY0FdWFKalMPSzy4xq7la9FYksR"></div>
                                					<div class="clearfix"></div>
                        						</div>
                            					<div class="col-md-6 no-padding" style="height: 75px;">
                            						<button class="btn btn-success pull-right" type="submit" name="submit" id="submit" class="filterData" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Đang lưu">Gửi</button>
                            					</div>
                            				</div>
                            			</td>
                            		</tr>
                            	</tbody>
                            </table>
                        </form>
                        </div>
                    </div>
	            </div> 
	        </div>
    	</div>
    </div>
</div>