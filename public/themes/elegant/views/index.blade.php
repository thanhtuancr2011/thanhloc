<!-- Home slideder-->
<div id="home-slider">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 header-top-right">
                <div class="homeslider">
                    @php
                        $homeSlide = get_post([
                            'post_status' => 'publish',
                            'post_type' => 'image_slider',
                            'post' => 'home-slider',
                        ])->first();
                        if ($homeSlide) {
                            $slides = $homeSlide->postMetas()->where('meta_key', 'slide_images')->first();
                            if ($slides)
                                $slides = json_decode($slides->meta_value);
                        }
                    @endphp

                    @if (isset($slides) && !empty($slides))
                        <ul id="contenhomeslider">
                            @foreach ($slides as $slide)
                                <li>
                                    <img alt="{{ $slide->title }}" src="{{ $slide->url }}" title="{{ $slide->title }}" />
                                </li>
                            @endforeach
                        </ul>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END Home slideder-->
<div class="content-page">
    <div class="container">
        <!-- featured category fashion -->
        <div class="category-featured fashion">
            <div class="product-featured clearfix">
                <div class="row">
                    <div class="container text-center" style="margin: 10px 0 30px 0">
                        <a href="/home" class="btn btn-success">Xem tất cả sản phẩm</a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-right-tab">
                        <div class="product-featured-tab-content">
                            <div class="tab-container">
                                @php
                                    $hotProductsList = app(Core\Base\Repositories\Contracts\SettingRepositoryInterface::class)->create()
                                                    ->where('key', 'home_hot_product')->pluck('value', 'key')->all();

                                    $hotProductsList = json_decode(isset($hotProductsList['home_hot_product']) ? $hotProductsList['home_hot_product'] : "{}", TRUE);
                                    
                                    $hotProducts = app(Core\Modules\Product\Repositories\Contracts\ProductRepositoryInterface::class)->create()
                                                        ->whereIn('id', $hotProductsList)
                                                        ->where('status', '1')
                                                        ->whereDate('published_at', '<=', date('Y-m-d H:i:s'))
                                                        ->get();
                                @endphp
                                @if (count($hotProducts) > 0)
                                    <div class="tab-panel active">
                                        <ul class="nav navbar-nav">
                                            <li class="active">Sản phẩm giá hot</li>
                                        </ul>
                                        <div class="clearfix"></div>
                                        <div class="box-right" style="width: 100%">
                                            <ul class="product-list row">
                                                @foreach ($hotProducts as $key => $product)
                                                    @php
                                                        $key += 1;
                                                        $categoryProduct = $product->categories()->first();
                                                    @endphp
                                                    <li class="col-sm-2 col-xs-6">
                                                        <div class="container-product">
                                                            <div class="left-block">
                                                                @php
                                                                    $imageProduct = json_decode($product->image_feature, TRUE);
                                                                @endphp
                                                                <a href="{{ URL::to($categoryProduct->slug . '/' . $product->slug . '.html') }}"><img class="img-responsive" alt="product" src="{{ $imageProduct['src'] }}" /></a>
                                                                @if ($product->price_fluctuations)
                                                                    <div class="quick-view">
                                                                        <a title="Biến động giá" class="search" href="javascript:void(0)">
                                                                            <i class="fa fa-line-chart" aria-hidden="true"></i>
                                                                        </a>
                                                                    </div>
                                                                @endif
                                                                <div class="add-to-cart">
                                                                    <a title="Add to Cart" href="javascript:void(0)" ng-click="addToCart({{$product->id}})">Thêm vào giỏ hàng</a>
                                                                </div>
                                                            </div>
                                                            <div class="right-block">
                                                                <h5 class="product-name"><a href="{{ URL::to($categoryProduct->slug . '/' . $product->slug . '.html') }}">{{ $product->title }}</a></h5>
                                                                <div class="content_price">
                                                                    @if (!empty($product->sale_price) && $product->sale_price < $product->price)
                                                                        <span class="price product-price">{{ number_format($product->sale_price , 0, ',', ',') }} ₫</span>
                                                                        <span class="price old-price">{{ number_format($product->price , 0, ',', ',') }} ₫</span>
                                                                    @else
                                                                        <span class="price product-price">{{ number_format($product->price , 0, ',', ',') }} ₫</span>
                                                                    @endif
                                                                </div>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                        </div>
                                                    </li>

                                                    @if ($key % 6 == 0 && $key != 0)
                                                        <div class="clearfix"></div>
                                                    @endif
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="content-wrap">
    @php
        $advertisementSlide = get_post([
            'post_status' => 'publish',
            'post_type' => 'image_slider',
            'post' => 'quang-cao',
        ])->first();
        if (!empty($advertisementSlide)) {
            $advertisementSlides = $advertisementSlide->postMetas()->where('meta_key', 'slide_images')->first();
            if ($advertisementSlides)
                $advertisementSlides = json_decode($advertisementSlides->meta_value);
        }
    @endphp

    @if (isset($advertisementSlides) && !empty($advertisementSlides))
        <!-- /.container -->
        <div id="popup-newsletter" class="modal fade" role="dialog" style="">
            <div class="modal-dialog" style="top: 50%; transform: translateY(-50%);">
                <!-- Modal content-->
                <div class="modal-content">
                    <button type="button" class="close" data-dismiss="modal" style="position: absolute; top: 5px; right: 5px; font-size: 20px;">&times;</button>
                    <img src="{{ $advertisementSlides[0]->url }}">
                </div>
            </div>
        </div>
    @endif
</div>