<!-- Home slideder-->
<div id="home-slider">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 header-top-right">
                <div class="homeslider">
                    @php
                        $homeSlide = get_post([
                            'post_status' => 'publish',
                            'post_type' => 'image_slider',
                            'post' => 'home-slider',
                        ])->first();
                        if ($homeSlide) {
                            $slides = $homeSlide->postMetas()->where('meta_key', 'slide_images')->first();
                            if ($slides)
                                $slides = json_decode($slides->meta_value);
                        }
                    @endphp

                    @if (isset($slides) && !empty($slides))
                        <ul id="contenhomeslider">
                            @foreach ($slides as $slide)
                                <li>
                                    <img alt="{{ $slide->title }}" src="{{ $slide->url }}" title="{{ $slide->title }}" />
                                </li>
                            @endforeach
                        </ul>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END Home slideder-->
<div class="page-top">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12">
                <h2 class="page-heading">
                    <span class="page-heading-title">Sản phẩm giá đẹp</span>
                </h2>
                <div class="latest-deals-product">
                    <ul class="product-list owl-carousel" data-dots="false" data-loop="true" data-nav = "true" data-margin = "10" data-autoplayTimeout="1000" data-autoplayHoverPause = "true" data-responsive='{"0":{"items":2},"600":{"items":4},"1000":{"items":6}}'>
                        @php
                            $hotPriceProductsList = app(Core\Base\Repositories\Contracts\SettingRepositoryInterface::class)->create()
                                                ->where('key', 'home_hot_price_product')->pluck('value', 'key')->all();

                            $hotPriceProductsList = json_decode(isset($hotPriceProductsList['home_hot_product']) ? $hotPriceProductsList['home_hot_product'] : "{}", TRUE);

                            $hotPriceProducts = app(Core\Modules\Product\Repositories\Contracts\ProductRepositoryInterface::class)->create()
                                                ->whereIn('id', $hotPriceProductsList)
                                                ->where('status', '1')
                                                ->whereDate('published_at', '<=', date('Y-m-d H:i:s'))
                                                ->get();
                        @endphp
                        @if (!empty($hotPriceProducts))
                            @foreach ($hotPriceProducts as $product)
                                @php
                                    $imageProduct = json_decode($product->image_feature, TRUE);
                                    $categorySaleProduct = $product->categories()->first();
                                @endphp
                                <li>
                                    <div class="left-block">
                                        <a href="{{ URL::to($categorySaleProduct->slug . '/' . $product->slug . '.html') }}">
                                            <img class="img-responsive" alt="{{ $product->title }}" src="{{ $imageProduct['src'] }}" />
                                        </a>
                                        @if ($product->price_fluctuations)
                                            <div class="quick-view">
                                                <a title="Biến động giá" class="search" href="javascript:void(0)">
                                                    <i class="fa fa-line-chart" aria-hidden="true"></i>
                                                </a>
                                            </div>
                                        @endif
                                        <div class="add-to-cart">
                                            <a title="Add to Cart" href="javascript:void(0)" ng-click="addToCart({{$product->id}})">Thêm vào giỏ hàng</a>
                                        </div>
                                    </div>
                                    <div class="right-block">
                                        <h5 class="product-name"><a href="{{ URL::to($categorySaleProduct->slug . '/' . $product->slug . '.html') }}">{{ $product->title }}</a></h5>
                                        <div class="content_price">
                                            @if (!empty($product->sale_price) && $product->sale_price < $product->price)
                                                <span class="price product-price">{{ number_format($product->sale_price , 0, ',', ',') }} ₫</span>
                                                <span class="price old-price">{{ number_format($product->price , 0, ',', ',') }} ₫</span>
                                            @else
                                                <span class="price product-price">{{ number_format($product->price , 0, ',', ',') }} ₫</span>
                                            @endif
                                        </div>
                                    </div>
                                </li>
                            @endforeach
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!---->
<div class="content-page">
    <div class="container">
        <!-- featured category fashion -->
        <div class="category-featured fashion">
            <div class="product-featured clearfix">
                <div class="row">
                    <div class="col-sm-3 sub-category-wapper">
                        <div class="title-left"><span>Danh mục</span></div>

                        <ul class="sub-category-list tree-menu">
                            @php
                                $categories = app(Core\Modules\Product\Repositories\Contracts\CategoryRepositoryInterface::class)
                                                ->getAllCategoriesTree('');
                            @endphp
                            @if (!empty($categories))
                                @foreach ($categories as $key => $category)
                                    @if ($category['name'] != 'Uncategory' && $category['name'] != 'Trống')
                                        <li>
                                            <span></span><a href="{{ URL::to($category['slug']) }}">{{ $category['name'] }}</a>
                                            @if (!empty($category['subFolder']))
                                                <ul style="display: none;">
                                                    @foreach ($category['subFolder'] as $cat)
                                                    <li><span></span><a href="{{ URL::to($cat['slug']) }}">{{ $cat['name'] }}</a></li>
                                                    @endforeach
                                                </ul>
                                            @endif
                                        </li>
                                    @endif
                                @endforeach
                            @endif
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-sm-9 col-right-tab">
                        <div class="product-featured-tab-content">
                            <div class="tab-container">
                                @php
                                    $homeCategories = app(Core\Base\Repositories\Contracts\SettingRepositoryInterface::class)->create()
                                                    ->where('key', 'home_categories')->pluck('value', 'key')->all();
                                    if (!empty($homeCategories)) {
                                        $homeCategories = json_decode($homeCategories['home_categories'], TRUE);

                                        $homeCategories = app(Core\Modules\Product\Repositories\Contracts\CategoryRepositoryInterface::class)
                                                            ->create()->whereIn('id', $homeCategories)->get();
                                    }
                                @endphp
                                @if (!empty($homeCategories))
                                    @foreach ($homeCategories as $key => $cat)
                                        @php
                                            $products = $cat->products()->where('status', '1')
                                                                        ->whereDate('published_at', '<=', date('Y-m-d H:i:s'))
                                                                        ->limit(8)->get();
                                        @endphp
                                        <div class="tab-panel active">
                                            <ul class="nav navbar-nav">
                                                <li class="active"><a href="{{ $cat->slug }}">{{ $cat->name }}</a></li>
                                            </ul>
                                            <div class="clearfix"></div>
                                            <div class="box-right" style="width: 100%">
                                                @if (!empty($products))
                                                    <ul class="product-list row">
                                                        @foreach ($products as $product)
                                                            @php
                                                                $categoryProduct = $product->categories()->first();
                                                            @endphp
                                                            <li class="col-sm-3 col-xs-6">
                                                                <div class="container-product">
                                                                    <div class="left-block">
                                                                        @php
                                                                            $imageProduct = json_decode($product->image_feature, TRUE);
                                                                        @endphp
                                                                        <a href="{{ URL::to($categoryProduct->slug . '/' . $product->slug . '.html') }}"><img class="img-responsive" alt="product" src="{{ $imageProduct['src'] }}" /></a>
                                                                        @if ($product->price_fluctuations)
                                                                            <div class="quick-view">
                                                                                <a title="Biến động giá" class="search" href="javascript:void(0)">
                                                                                    <i class="fa fa-line-chart" aria-hidden="true"></i>
                                                                                </a>
                                                                            </div>
                                                                        @endif
                                                                        <div class="add-to-cart">
                                                                            <a title="Add to Cart" href="javascript:void(0)" ng-click="addToCart({{$product->id}})">Thêm vào giỏ hàng</a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="right-block">
                                                                        <h5 class="product-name"><a href="{{ URL::to($categoryProduct->slug . '/' . $product->slug . '.html') }}">{{ $product->title }}</a></h5>
                                                                        <div class="content_price">
                                                                            @if (!empty($product->sale_price) && $product->sale_price < $product->price)
                                                                                <span class="price product-price">{{ number_format($product->sale_price , 0, ',', ',') }} ₫</span>
                                                                                <span class="price old-price">{{ number_format($product->price , 0, ',', ',') }} ₫</span>
                                                                            @else
                                                                                <span class="price product-price">{{ number_format($product->price , 0, ',', ',') }} ₫</span>
                                                                            @endif
                                                                        </div>
                                                                        <div class="clearfix"></div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                @endif
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="content-wrap">
    @php
        $advertisementSlide = get_post([
            'post_status' => 'publish',
            'post_type' => 'image_slider',
            'post' => 'quang-cao',
        ])->first();
        if (!empty($advertisementSlide)) {
            $advertisementSlides = $advertisementSlide->postMetas()->where('meta_key', 'slide_images')->first();
            if ($advertisementSlides)
                $advertisementSlides = json_decode($advertisementSlides->meta_value);
        }
    @endphp

    @if (isset($advertisementSlides) && !empty($advertisementSlides))
        <!-- /.container -->
        <div id="popup-newsletter" class="modal fade" role="dialog" style="">
            <div class="modal-dialog" style="top: 50%; transform: translateY(-50%);">
                <!-- Modal content-->
                <div class="modal-content">
                    <button type="button" class="close" data-dismiss="modal" style="position: absolute; top: 5px; right: 5px; font-size: 20px;">&times;</button>
                    <img src="{{ $advertisementSlides[0]->url }}">
                </div>
            </div>
        </div>
    @endif
</div>