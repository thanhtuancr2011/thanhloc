@php
    $posts = $item;
    $category = $option['term']->first();
@endphp
<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="{{ URL::to('/') }}" >Trang chủ</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="">{{ $category->name }}</span>
        </div>
        <!-- ./breadcrumb -->
        <!-- row -->
        <div class="row">
            <!-- Left colunm -->
            <div class="column col-xs-12 col-sm-3" id="left_column">
                <!-- Blog category -->
                <div class="block left-module">
                    <p class="title_block">Danh mục</p>
                    <div class="block_content">
                        <!-- layered -->
                        <div class="layered layered-category">
                            <div class="layered-content">
                                <ul class="sub-category-list tree-menu">
                                    @php
                                        $categories = app(Core\Modules\Term\Repositories\Contracts\TermRepositoryInterface::class)->getAllCategoriesTree('');
                                    @endphp
                                    @if (!empty($categories))
                                        @foreach ($categories as $key => $cat)
                                            @if ($cat['name'] != 'Uncategory' && $cat['name'] != 'Trống')
                                                <li>
                                                    <span></span><a href="{{ URL::to($cat['slug']) }}">{{ $cat['name'] }}</a>
                                                    @if (!empty($cat['subFolder']))
                                                        <ul style="display: none;">
                                                            @foreach ($cat['subFolder'] as $cat)
                                                            <li><span></span><a href="{{ URL::to($cat['slug']) }}">{{ $cat['name'] }}</a></li>
                                                            @endforeach
                                                        </ul>
                                                    @endif
                                                </li>
                                            @endif
                                        @endforeach
                                    @endif
                                </ul>
                            </div>
                        </div>
                        <!-- ./layered -->
                    </div>
                </div>
                <!-- ./blog category  -->
            </div>
            <!-- ./left colunm -->
            <!-- Center colunm-->
            <div class="center_column col-xs-12 col-sm-9" id="center_column">
                <h2 class="page-heading">
                    <span class="page-heading-title2">{{ $category->name }}</span>
                </h2>
                <ul class="blog-posts">
                    @foreach ($posts as $post)
                        @php
                            $imageFeature = json_decode($post->image_feature, TRUE);
                        @endphp
                        <li class="post-item">
                            <article class="entry">
                                <div class="row">
                                    <div class="col-sm-5">
                                        <div class="entry-thumb image-hover2">
                                            <a href="{{ URL::to($post->categories()->first()->slug . '/' . $post->slug . '.html') }}">
                                                <img src="{{ $imageFeature['src'] }}" alt="{{ $imageFeature['alt'] }}">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-sm-7">
                                        <div class="entry-ci">
                                            <h3 class="entry-title"><a href="{{ URL::to($post->categories()->first()->slug . '/' . $post->slug . '.html') }}">{{ $post->title }}</a></h3>
                                            <div class="entry-meta-data">
                                                <span class="author"> <i class="fa fa-user"></i> Tác giả: Admin</span>
                                                <span class="cat"><i class="fa fa-folder-o"></i> Chuyên mục: Tin tức</span>
                                                <span class="date">
                                                    <i class="fa fa-calendar">
                                                    </i> {{ formatDateStr($post->published_at, 'd F, Y') }}
                                                </span>
                                            </div>
                                            <div class="entry-excerpt">{{ $post->excerpt }}</div>
                                            <div class="entry-more">
                                                <a href="{{ URL::to($post->categories()->first()->slug . '/' . $post->slug . '.html') }}">Xem thêm</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </article>
                        </li>
                    @endforeach
                </ul>
                <div class="sortPagiBar clearfix">
                    <div class="bottom-pagination">
                        <nav>
                            {!! $item->links(Theme::getPartialsName('pagination')) !!}
                        </nav>
                    </div>
                </div>
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>