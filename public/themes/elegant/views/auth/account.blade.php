<!-- page wapper-->
<style type="text/css">
	h3 {
		margin-bottom: 20px;
		text-transform: uppercase;
	}

	input.form-control {
		border-radius: 0;
		/*background: #f2f2f2;*/
	}

	label small {
		color: red;
	}

	.account-info {
		/*max-width: 85%;*/
		margin: 0 auto;
	}
</style>
<div class="columns-container">
    <div class="container" id="columns">
    	<!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="{{ URL::to('/') }}" >Trang chủ</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="">Tài khoản</span>
        </div>
        <!-- ./breadcrumb -->
        <!-- ../page heading-->
        <div class="page-content">
            <div class="row">
                <div class="container account-info">
                	<h3 class="checkout-sep">Thông tin tài khoản</h3>
                	<form action="" id="form-account" name="formAccount" novalidate="">
                		<div class="form-group" ng-class="{true: 'has-error'}[submitted1 && formAccount.full_name.$invalid]">
	                		<label>Họ tên <small>(*)</small></label>
	                		<input type="text" name="full_name" class="form-control" ng-model="customer.full_name" required="">
	                		<label class="control-label" ng-show="submitted1 && formAccount.full_name.$error.required">
                                Mời nhập họ tên
                            </label>
	                	</div>
	                	<div class="form-group" ng-class="{true: 'has-error'}[submitted1 && formAccount.name_of_drugstore.$invalid]">
                            <label>Tên nhà thuốc <small>(*)</small></label>
                            <input type="text" name="name_of_drugstore" class="form-control" ng-model="customer.name_of_drugstore" required="">
                            <label class="control-label" ng-show="submitted1 && formAccount.name_of_drugstore.$error.required">
                                Mời nhập tên nhà thuốc
                            </label>
                        </div>
	                	<div class="form-group">
	                		<label>Email <small>(*)</small></label>
	                		<input type="text" name="email" class="form-control" ng-model="customer.email" disabled="">
	                	</div>

	                	<div class="form-group">
	                		<label>Số điện thoại</label>
	                		<input type="text" name="phone" class="form-control" ng-model="customer.phone">
	                	</div>

	                	<div class="form-group">
	                		<label>Địa chỉ</label>
	                		<input type="text" name="address" class="form-control" ng-model="customer.address">
	                	</div>

	                	<div class="form-group" ng-class="{true: 'has-error'}[submitted1 && formAccount.old_password.$invalid]">
	                		<label>Mật khẩu cũ (bỏ trống nếu không thay đổi)</label>
	                		<input type="password" name="old_password" class="form-control" ng-model="customer.old_password" ng-required="customer.password">
	                		<label class="control-label" ng-show="submitted1 && formAccount.old_password.$error.required">
                                Mời nhập mật khẩu cũ
                            </label>
	                	</div>

	                	<div class="form-group">
	                		<label>Mật khẩu mới (bỏ trống nếu không thay đổi)</label>
	                		<input type="password" name="password" class="form-control" ng-model="customer.password">
	                	</div>

	                	<div class="form-group" ng-class="{true: 'has-error'}[submitted1 && (formAccount.password_confirmation.$invalid || customer.password_confirmation != customer.password)]">
	                		<label>Nhập lại mật khẩu mới (bỏ trống nếu không thay đổi)</label>
	                		<input type="password" name="password_confirmation" class="form-control" ng-model="customer.password_confirmation" ng-required="customer.password">
	                		<label class="control-label" ng-show="submitted1 && formAccount.password_confirmation.$error.required">
                                Mời xác nhận mật khẩu
                            </label>
	                		<label class="control-label" ng-show="submitted1 && customer.password_confirmation != customer.password && customer.password_confirmation" >
                                Mật khẩu xác nhận chưa chính xác
                            </label>
	                	</div>

	                	<div class="alert alert-error alert-danger" ng-show="errors" ng-repeat="error in errors">
	                        @{{error}}
	                    </div>
					    <div class="clearfix"></div>
                	</form>
                	<div class="form-group">
                		<button class="btn btn-success pull-right" ng-click="updateCustomer(formAccount.$valid && (customer.password == customer.password_confirmation))">
					        <span><i class="fa fa-pencil-square-o"></i> Cập nhật </span>
					    </button>
					    <div class="clearfix"></div>
                	</div>

                	@php
                		$orders = app(Core\Modules\Product\Repositories\Contracts\OrderRepositoryInterface::class)->create()->where('user_id', Auth::user()->id)->get();
                	@endphp

                	@if (count($orders) > 0)
                	<h3 class="checkout-sep" style="margin-top: 20px;">Danh sách đơn hàng</h3>
	                <div class="box-border woocommerce">
	                    <table class="table table-bodered shop_table shop_table_responsive my_account_orders">
							<thead>
								<tr style="line-height: 35px; padding: 5px 10px;">
									<th class="order-number"><span class="nobr">Đơn hàng</span></th>
									<th class="order-date"><span class="nobr">Ngày</span></th>
									<th class="order-status"><span class="nobr">Tình trạng</span></th>
									<th class="order-total"><span class="nobr">Tổng cộng</span></th>
									{{-- <th class="order-actions"><span class="nobr">&nbsp;</span></th> --}}
								</tr>
							</thead>
							<tbody>
								@foreach ($orders as $order)
								<tr class="order" style="line-height: 35px;">
									<td class="order-number" data-title="Đơn hàng">
										<a href="https://giathuochapu.com/tai-khoan/xem-don-hang/86917">
											#{{$order->id}}							
										</a>
									</td>
									<td class="order-date" data-title="Ngày">
										<time datetime="" title="1533853309">{{ formatDateStr($order->created_at, 'd F, Y') }}</time>
									</td>
									<td class="order-status" data-title="Tình trạng">
										{{$order->status ? 'Đã hoàn thành' : 'Đang xử lý'}}
									</td>
									<td class="order-total" data-title="Tổng số tiền">
										<span class="amount">{{ number_format($order->subtotal , 0, ',', ',') }} ₫</span>
									</td>
									{{-- <td class="order-actions" data-title="&nbsp;">
										<a href="https://giathuochapu.com/tai-khoan/xem-don-hang/86917" class="btn btn-success">Xem</a>													
									</td> --}}
								</tr>
								@endforeach
							</tbody>
						</table>
	                </div>
	                @endif
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ./page wapper-->