<!-- page wapper-->
<style type="text/css">
	.social-btn .btn {
	    margin: 10px 0;
	    font-size: 15px;
	    text-align: left;
	    line-height: 24px;
	}
	.btn-block {
	    display: block;
	    width: 100%;
	}
	.social-btn .btn i {
	    float: left;
	    margin: 4px 15px 0 5px;
	    min-width: 15px;
	}
	h3 {
	    font-size: 15px;
	    padding: 15px 0;
	}
    .span-or {
        line-height: 40px;
        margin: 10px 0;
        display: block;
        width: 10%;
        float: right;
    }

    a.span-or {
        color: #5cb75b;
    }

    a.span-or:hover {
        color: #f96c0f;
    }
</style>
<div class="columns-container">
    <div class="container" id="columns">
    	<!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="{{ URL::to('/') }}" >Trang chủ</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="">Đăng nhập</span>
        </div>
        <!-- ./breadcrumb -->
        <!-- ../page heading-->
        <div class="page-content">
            <div class="row">
                <div class="box-authentication">
                    <h2>Đăng nhập</h2>
                    <h3><i>Đăng nhập trước khi xem thêm sản phẩm hoặc tiến hành thanh toán</i></h3>
                    <div class="text-center social-btn">
                    	<form action="{{ asset('auth/login') }}" method="post" id="loginForm">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <input class="form-control" type="text" name="email" placeholder="Email" value="{{ old('email') }}">
                            </div>
                            <div class="form-group">
                                <input class="form-control" type="password" name="password" placeholder="Mật khẩu" >
                            </div>
                            @if (count($errors))
                                <ul class="alert alert-warning" style="text-align: left;">
                                    @foreach($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            @endif
                            <div class="form-group text-center">
                                <a href="/dang-ky" class="span-or">Đăng ký</a>
                                <span class="span-or">hoặc</span>
                                <input type="submit" class="btn btn-primary pull-right" value="Đăng nhập" style="width: 30%; text-align: center;">
                            </div>
                            <div class="clearfix"></div>
                        </form>
			        </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ./page wapper-->