<!-- page wapper-->
<style type="text/css">
	.social-btn .btn {
	    margin: 10px 0;
	    font-size: 15px;
	    text-align: left;
	    line-height: 24px;
	}
	.btn-block {
	    display: block;
	    width: 100%;
	}
	.social-btn .btn i {
	    float: left;
	    margin: 4px 15px 0 5px;
	    min-width: 15px;
	}
	h3 {
	    font-size: 15px;
	    padding: 15px 0;
	}
    .span-or {
        line-height: 40px;
        margin: 10px 0;
        display: block;
        width: 10%;
        float: right;
    }

    a.span-or {
        color: #5cb75b;
    }

    a.span-or:hover {
        color: #f96c0f;
    }

    .form-group {
        margin-bottom: 5px;
    }
</style>
<div class="columns-container">
    <div class="container" id="columns">
    	<!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="{{ URL::to('/') }}" >Trang chủ</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="">Đăng ký</span>
        </div>
        <!-- ./breadcrumb -->
        <!-- ../page heading-->
        <div class="page-content">
            <div class="row">
                <div class="box-authentication">
                    <h2>Đăng ký</h2>
                    <h3><i>Đăng ký trước khi sử dụng các dịch vụ của chúng tôi</i></h3>
                    <div class="text-center social-btn">
                        <form action="" id="form-account" name="formAddUser" novalidate="" style="text-align: left;">
                            <div class="form-group" ng-class="{true: 'has-error'}[submitted && formAddUser.full_name.$invalid]">
                                <label>Họ tên <small>(*)</small></label>
                                <input type="text" name="full_name" class="form-control" ng-model="customer.full_name" required="">
                                <label class="control-label" ng-show="submitted && formAddUser.full_name.$error.required">
                                    Mời nhập họ tên
                                </label>
                            </div>
                            <div class="form-group" ng-class="{true: 'has-error'}[submitted && formAddUser.name_of_drugstore.$invalid]">
                                <label>Tên nhà thuốc <small>(*)</small></label>
                                <input type="text" name="name_of_drugstore" class="form-control" ng-model="customer.name_of_drugstore" required="">
                                <label class="control-label" ng-show="submitted && formAddUser.name_of_drugstore.$error.required">
                                    Mời nhập tên nhà thuốc
                                </label>
                            </div>
                            <div class="form-group" ng-class="{true: 'has-error'}[submitted && formAddUser.email.$invalid]">
                                <label>Email <small>(*)</small></label>
                                <input class="form-control" type="text" placeholder="Email" 
                                       ng-pattern="/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/" 
                                       name="email" ng-model="customer.email" required="true">
                                <label class="control-label" ng-show="submitted && formAddUser.email.$error.pattern">
                                    Không đúng định dạng email
                                </label>
                                <label class="control-label" ng-show="submitted && formAddUser.email.$error.required">
                                    Mời nhập email
                                </label>
                            </div>

                            <div class="form-group" ng-class="{true: 'has-error'}[submitted && formAddUser.phone.$invalid]">
                                <label>Số điện thoại <small>(*)</small></label>
                                <input type="text" name="phone" class="form-control" ng-model="customer.phone" required="">
                                <label class="control-label" ng-show="submitted && formAddUser.phone.$error.required">
                                    Mời nhập số điện thoại
                                </label>
                            </div>

                            <div class="form-group">
                                <label>Địa chỉ</label>
                                <input type="text" name="address" class="form-control" ng-model="customer.address">
                            </div>

                            <div class="form-group" ng-class="{true: 'has-error'}[submitted && formAddUser.password.$invalid]">
		                        <label for="inputPassword">Mật khẩu (*)</label>
		                        <div class="">
		                            <input class="form-control" type="password" placeholder="Mật khẩu" name="password" ng-model="customer.password" required="true">
		                            <label class="control-label" ng-show="submitted && formAddUser.password.$error.required" >
		                                Mời nhập mật khẩu
		                            </label>
		                        </div>
		                    </div>
		                    
		                    <div class="form-group" ng-class="{true: 'has-error'}[submitted && (formAddUser.rePassword.$invalid || customer.rePassword != customer.password)]">
		                        <label for="inputRePassword">Nhập lại mật khẩu (*)</label>
		                        <div class="">
		                            <input class="form-control" type="password" placeholder="Nhập lại mật khẩu" name="rePassword" ng-model="customer.rePassword" required="true">
		                            <label class="control-label" ng-show="submitted && formAddUser.rePassword.$error.required" >
		                                Xác nhận mật khẩu
		                            </label>
		                            <label class="control-label" ng-show="submitted && (customer.rePassword != customer.password) && !formAddUser.rePassword.$error.required" >
		                                Mật khẩu xác nhận chưa chính xác
		                            </label>
		                        </div>
		                    </div>

                            <div class="alert alert-error alert-danger" ng-show="errors" ng-repeat="error in errors">
                                @{{error}}
                            </div>
                        </form>
                        <div class="form-group text-center">
                            <input type="submit" ng-click="submit(formAddUser.$valid && (customer.rePassword == customer.password))" class="btn btn-primary pull-right" value="Đăng ký" style="width: 100%; text-align: center;">
                            <div class="clearfix"></div>
                        </div>
			        </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ./page wapper-->