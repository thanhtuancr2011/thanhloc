<!-- ./page wapper -->
<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="/" title="Return to Home">Trang chủ</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">Thanh toán</span>
        </div>
        <!-- ./breadcrumb -->
        <!-- ../page heading-->
        <div class="page-content checkout-page">
            <form method="POST" accept-charset="UTF-8" name="formCheckout">
                <h3 class="checkout-sep">1. Thông tin hóa đơn</h3>
                <div class="box-border">
                    <ul>
                        <li class="row">
                            <div class="col-md-6">
                                <label for="billing_full_name" class="required">Họ và tên</label>
                                <input type="text" class="input form-control" name="billing_full_name" ng-model="checkoutItem.billing_full_name" id="billing_full_name">
                            </div>
                            <div class="col-md-6">
                                <label for="billing_email" class="required">Email</label>
                                <input type="text" class="input form-control" name="billing_email" ng-model="checkoutItem.billing_email" id="billing_email">
                            </div>
                        </li>
                        <li class="row">
                            <div class="col-md-6">
                                <label for="billing_company" class="required">Công ty</label>
                                <input type="text" class="input form-control" name="billing_company" ng-model="checkoutItem.billing_company" id="billing_company">
                            </div>
                            <div class="col-md-6">
                                <label for="billing_address" class="required">Địa chỉ</label>
                                <input type="text" class="input form-control" name="billing_address" ng-model="checkoutItem.billing_address" id="billing_address">
                            </div>
                        </li>
                        <li class="row">
                            <div class="col-md-6">
                                <label for="billing_phone" class="required">Số điện thoại</label>
                                <input type="text" class="input form-control" name="billing_phone" ng-model="checkoutItem.billing_phone" id="billing_phone">
                            </div>
                            <div class="col-md-6">
                                <label for="billing_fax" class="required">Số fax</label>
                                <input type="text" class="input form-control" name="billing_fax" ng-model="checkoutItem.billing_fax" id="billing_fax">
                            </div>
                        </li>
                    </ul>
                </div>
                <h3 class="checkout-sep">2. Thông tin giao hàng</h3>
                <div class="box-border">
                    <ul>       
                        <li class="row">
                            <div class="col-md-6" ng-class="{true: 'has-error'}[submitted && formCheckout.shipping_full_name.$invalid]">
                                <label for="shipping_full_name" class="required">Họ và tên (*)</label>
                                <input type="text" class="input form-control" name="shipping_full_name" ng-model="checkoutItem.shipping_full_name" id="shipping_full_name" required="">
                                <label class="control-label" ng-show="submitted && formCheckout.shipping_full_name.$error.required">
                                    Bạn chưa nhập tên
                                </label>
                            </div>
                            <div class="col-md-6" ng-class="{true: 'has-error'}[submitted && formCheckout.shipping_phone.$invalid]">
                                <label for="shipping_phone" class="required">Số điện thoại (*)</label>
                                <input type="text" class="input form-control" name="shipping_phone" ng-model="checkoutItem.shipping_phone" id="shipping_phone" required="">
                                <label class="control-label" ng-show="submitted && formCheckout.shipping_phone.$error.required">
                                    Bạn chưa nhập số điện thoại
                                </label>
                            </div>
                        </li>
                        <li class="row">
                            <div class="col-md-6" ng-class="{true: 'has-error'}[submitted && formCheckout.shipping_address.$invalid]">
                                <label for="shipping_address" class="required">Địa chỉ (*)</label>
                                <input type="text" class="input form-control" name="shipping_address" ng-model="checkoutItem.shipping_address" id="shipping_address" required="">
                                <label class="control-label" ng-show="submitted && formCheckout.shipping_address.$error.required">
                                    Bạn chưa nhập địa chỉ
                                </label>
                            </div>
                            <div class="col-md-6">
                                <label for="note" class="required">Ghi chú</label>
                                <textarea type="text" class="input form-control" name="note" ng-model="checkoutItem.note" id="note"></textarea>
                            </div>
                        </li>
                    </ul>
                </div>
                <h3 class="checkout-sep">3. Phương thức thanh toán</h3>
                <div class="box-border">
                    <ul class="type_payment">
                        <style type="text/css">
                            .type_payment .cnt {
                                display: none;
                                margin-top: 5px;
                                float: left;
                                width: 100%;
                                background: #efefef;
                                padding: 15px;
                                box-sizing: border-box;
                                border-radius: 5px;
                            }
                            .checkout-page .box-border label {
                                margin-top: 10px;
                            }
                        </style>
                        <li>
                            <label for="radio_button_5">
                                <input type="radio" name="payment_method" id="radio_button_5" ng-model="checkoutItem.payment_method" ng-value="0"> 
                                Thanh toán trực tiếp - COD
                            </label>
                            <div class="cnt cnt_tab" id="show_tab3" style="display: block;">
                                <p>Quý khách có thể chuyển tiền sau khi nhận được sản phẩm</p>
                            </div>
                        </li>
                        <li>
                            <label for="radio_button_6">
                                <input type="radio" name="payment_method" id="radio_button_6" ng-model="checkoutItem.payment_method" ng-value="1">
                                Chuyển khoản qua tài khoản ngân hàng
                            </label>
                            <div class="cnt cnt_tab" id="show_tab4" style="display: block;">
                                <p>Quý khách có thể chuyển tiền thanh toán tới một trong các tài khoản sau:</p>
                                <p class="txt_ctk">
                                    Chủ tài khoản: <strong>CÔNG TY CỔ PHẦN DƯỢC PHẨM THANH HÀ</strong>
                                </p>
                                <p>
                                    - Ngân hàng <strong>Vietinbank</strong> 
                                    - Chi nhánh Thanh Xuân - Số tài khoản: <strong class="red">119000086840</strong>
                                </p>
                                <p>
                                    - Ngân hàng <strong>Vietcombank</strong> 
                                    - Sở giao dịch - Số tài khoản: <strong class="red">0011004049424</strong>
                                </p>
                            </div>
                        </li>
                    </ul>
                </div>
                <h3 class="checkout-sep">4. Thông tin giỏ hàng</h3>
                <small><strong>Chú ý:<i> Nhấn cập nhật sau khi thay đổi số lượng sản phẩm</i></strong></small>
                <div class="box-border">
                    <table class="table table-bordered table-responsive cart_summary">
                        <thead>
                            <tr>
                                <th class="cart_product">Ảnh</th>
                                <th class="cart_description">Tên</th>
                                <th>Giá</th>
                                <th>Số lượng</th>
                                <th class="text-center">Tổng</th>
                                <th class="action hidden-xs"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="(key, cart) in checkoutItem.carts">
                                <td class="cart_product">
                                    <a href="@{{cart.options.url}}">
                                        <img ng-src="@{{cart.options.image_feature}}" alt="Product">
                                    </a>
                                    <a class="cart-name" href="@{{cart.options.url}}">@{{cart.name}}</a>
                                </td>
                                <td class="cart_description">
                                    <p class="product-name"><a href="@{{cart.options.url}}">@{{cart.name}} </a></p>
                                </td>
                                <td class="price">
                                    <span>@{{cart.price|currency:"":0}} ₫</span>
                                </td>
                                <td class="qty">
                                    <input class="form-control input-sm" type="number" min="1" ng-model="cart.qty" ng-change="changeValueQty(cart.id, cart.qty)">
                                    <a class="btn btn-primary" ng-click="addToCart(cart.id, cart.qty, true)" style="padding: 5px 10px;">
                                        Cập nhật
                                    </a>
                                </td>
                                <td class="price">
                                    <span>@{{cart.price * cart.qty|currency:"":0}} ₫</span>
                                </td>
                                <td class="text-center hidden-xs">
                                    <a class="btn btn-danger" ng-click="removeCart(key)">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="@{{screenWidth < 480 ? 2 : 4}}"><strong>Tổng cộng</strong></td>
                                <td colspan="2"><strong>@{{cartItem.subtotal}} ₫</strong></td>
                            </tr>
                        </tfoot>    
                    </table>
                    <a href="javascript:void(0)" class="button pull-right" style="background: #f96d10;" ng-click="submit(formCheckout.$valid)">
                        Tiếp tục
                    </a>
                    <a href="/" class="button pull-right" style="background: #f96d10; margin-right: 30px;">Thêm sản phẩm</a>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- ./page wapper -->