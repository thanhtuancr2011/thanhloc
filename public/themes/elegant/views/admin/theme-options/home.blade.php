@extends('base::layouts.master')

@section('title')
    Hiển thị trang chủ
@endsection

@section('breadcrumb')
    <div class="row">
        <div class="container col-md-12 admin-breadcrumb">
            <div class="breadcrumb">
                <div class="col-md-6 no-padding">
                    <h3>Hiển thị trang chủ</h3>
                </div>
                <div class="col-md-6 no-padding content-end">
                    <span class="breadcrumb-item">Trang chủ</span>
                    <span class="breadcrumb-item active">Hiển thị trang chủ</span>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <style>
        .capitalize {
            text-transform: capitalize;
        }
    </style>
    <div class="card-header">
        <i class="fa fa-cog"></i> Cài đặt
    </div>
    <div class="card-body" ng-controller="SettingController"
         ng-init="fieldsHome = {{json_encode($fieldsHome)}}; mimeContentTypes={{ json_encode($mimeContentTypes) }}; maxSizeUpload={{$maxSizeUpload}};">
        <div class="col-md-12" style="padding: 0">

            <div class="col-md-12">
                <div class="tab-content border-none">
                    <form method="POST" accept-charset="UTF-8" name="formAddData">
                        <div ng-repeat="(key, field) in fieldsHome" class="form-group" ng-init="initSelect(key)">
                            <label for="">@{{ field.title }}</label>
                            <div class="">
                                <select class="form-control" multiple="" type="text" id="@{{key}}" name="@{{key}}">
                                    <option ng-repeat="(id, text) in field.options" value="@{{id}}">@{{text}}</option>
                                </select>

                                <div class="tag-show" ng-if="field.value.length > 0">
                                    <label for="name">@{{ field.caption }}</label>
                                    <div class="list-tags">
                                        <div class="tagchecklist">
                                            <span ng-repeat="categoryId in field.value track by $index">
                                                <button type="button" id="post_tag-check-num-0" class="ntdelbutton" ng-click="removeItem(categoryId, key)">
                                                    <span class="remove-tag-icon" aria-hidden="true"></span>
                                                </button>&nbsp;@{{ field.options[categoryId] }}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="col-md-12 form-group" style="padding: 0">
                        <button class="btn btn-primary pull-right" ng-click="submit(formAddData.$valid)">
                            <span><i class="fa fa-pencil-square-o"></i> Cập nhật</span>
                        </button>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>

        </div>
    </div>

@endsection 
@section('script')
    {!! Html::script('backend/app/components/library/LibraryService.js?v='.getVersionScript()) !!}
    {!! Html::script('backend/app/components/custom-setting/SettingService.js?v='.getVersionScript())!!}
    {!! Html::script('backend/app/components/custom-setting/SettingController.js?v='.getVersionScript()) !!}
    {!! Html::script('backend/app/shared/file-upload/FileService.js?v='.getVersionScript())!!}
    {!! Html::script('backend/app/shared/file-upload/FileUploadDirective.js?v='.getVersionScript()) !!}
@endsection